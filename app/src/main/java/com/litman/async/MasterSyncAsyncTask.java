package com.litman.async;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.litman.db.DbManager;
import com.litman.interfaces.CCMatters;
import com.litman.interfaces.CaseHistory;
import com.litman.interfaces.CaseType;
import com.litman.interfaces.Comments;
import com.litman.interfaces.CompanyContacts;
import com.litman.interfaces.Const;
import com.litman.interfaces.Courts;
import com.litman.interfaces.CustomHearing;
import com.litman.interfaces.Hearing;
import com.litman.interfaces.MPContacts;
import com.litman.interfaces.MatterUsers;
import com.litman.interfaces.Matters;
import com.litman.interfaces.Notes;
import com.litman.interfaces.Notifications;
import com.litman.interfaces.NotificationsStatus;
import com.litman.interfaces.Orders;
import com.litman.interfaces.PersonContacts;
import com.litman.interfaces.Post;
import com.litman.interfaces.Tasks;
import com.litman.interfaces.UploadAssets;
import com.litman.interfaces.Uploads;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;


public class MasterSyncAsyncTask extends AsyncTask<Void, Void, String> {

    private Context ctx;
    private String token = "";
    Handler mainHandler;

    private onAsyncTaskComplete mListener;

    public interface onAsyncTaskComplete {
        void OnResponse(String response, Boolean status);
    }

    public MasterSyncAsyncTask(Context _ctx) {
        token = AppPreferences.getToken(_ctx);
        ctx = _ctx;
        mainHandler = new Handler(ctx.getMainLooper());
    }

    public void setListener(onAsyncTaskComplete dataDownloadListner) {
        mListener = dataDownloadListner;
    }

    @Override
    protected String doInBackground(Void... params) {

        Boolean uploadResult = prepareData();

        if (!uploadResult) {
            Runnable myRunnable4 = new Runnable() {
                @Override
                public void run() {
                    if (mListener != null) {
                        mListener.OnResponse("Please try again.", false);
                    }
                }
            };
            mainHandler.post(myRunnable4);
            return "";
        }


        Runnable myRunnable3 = new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.OnResponse("Downloading data from server...", true);
                }
            }
        };
        mainHandler.post(myRunnable3);

        String result = excutePost(Const.BASE_URL + Const.SYNC +
                (AppPreferences.getDate(ctx).length() > 0 ? "?after_date=" + AppPreferences.getDate(ctx) : ""), "");

        DbManager.getInstance().openDatabase();

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.OnResponse("Saving data in database...", true);
                }
            }
        };

        mainHandler.post(myRunnable);

        if (result != null && !result.equals("")) {
            try {
                JSONObject main = new JSONObject(result);
                if (main.has("error")) {
                    final String err = main.getString("error");
                    Runnable myRunnable1 = new Runnable() {
                        @Override
                        public void run() {
                            if (mListener != null) {
                                mListener.OnResponse(err, false);
                            }
                        }
                    };
                    mainHandler.post(myRunnable1);
                } else {
                    //Delete all data with key 101
                    DbManager.getInstance().deleteList("101");

                    if (main.has("all_case_histories")) {
                        JSONArray arr = main.getJSONArray("all_case_histories");
                        ArrayList<ContentValues> ch = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(CaseHistory.R_ID, getInt(ob, CaseHistory.ID));
                            cv.put(CaseHistory.MATTER_ID, getInt(ob, CaseHistory.MATTER_ID));
                            cv.put(CaseHistory.SNO, getInt(ob, CaseHistory.SNO));
                            cv.put(CaseHistory.CASE_NO, getString(ob, CaseHistory.CASE_NO));
                            cv.put(CaseHistory.CASE_LINK, getString(ob, CaseHistory.CASE_LINK));
                            cv.put(CaseHistory.ORDER_DATE, getString(ob, CaseHistory.ORDER_DATE));
                            cv.put(CaseHistory.DETAILS, getString(ob, CaseHistory.DETAILS));

                            ch.add(cv);
                        }
                        DbManager.getInstance().replaceList(CaseHistory.T_NAME, ch);
                    }

                    if (main.has("all_uploads")) {
                        JSONArray arr = main.getJSONArray("all_uploads");
                        ArrayList<ContentValues> task = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Uploads.R_ID, getInt(ob, Uploads.ID));
                            cv.put(Uploads.U_ID, getInt(ob, Uploads.U_ID));
                            cv.put(Uploads.CREATOR_ID, getInt(ob, Uploads.CREATOR_ID));
                            cv.put(Uploads.ADMIN_ID, getInt(ob, Uploads.ADMIN_ID));
                            cv.put(Uploads.SUB, getString(ob, Uploads.SUB));
                            cv.put(Uploads.U_TYPE, getString(ob, Uploads.U_TYPE));
                            cv.put(Uploads.DESC, getString(ob, Uploads.DESC));
                            cv.put(Uploads.CATEGORY, getString(ob, Uploads.CATEGORY));
                            cv.put(Uploads.CREATED_AT, getString(ob, Uploads.CREATED_AT));
                            cv.put(Uploads.UPDATED_AT, getString(ob, Uploads.UPDATED_AT));
                            cv.put(Uploads.UPDATED, 0);
                            cv.put(Uploads.INSERTED, 0);
                            cv.put(Uploads.DELETED, 0);
                            task.add(cv);

                            ArrayList<ContentValues> uploadAsset = new ArrayList<>();
                            int id = getInt(ob, Uploads.ID);
                            JSONArray obj = ob.getJSONArray("assets");
                            for (int j = 0; j < obj.length(); j++) {
                                JSONObject url = obj.getJSONObject(j);
                                ContentValues cval = new ContentValues();
                                cval.put(UploadAssets.U_ID, id);
                                cval.put(UploadAssets.URL, getString(url, UploadAssets.URL));
                                cval.put(UploadAssets.UPDATED, 0);
                                cval.put(UploadAssets.INSERTED, 0);
                                cval.put(UploadAssets.DELETED, 0);
                                uploadAsset.add(cval);
                            }
                            DbManager.getInstance().replaceList(UploadAssets.T_NAME, uploadAsset);
                        }
                        DbManager.getInstance().replaceList(Uploads.T_NAME, task);
                    }


                    if (main.has("all_tasks")) {
                        JSONArray arr = main.getJSONArray("all_tasks");
                        ArrayList<ContentValues> task = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Tasks.R_ID, getInt(ob, Tasks.ID));
                            cv.put(Tasks.MATTER_ID, getInt(ob, Tasks.MATTER_ID));
                            cv.put(Tasks.USER_ID, getInt(ob, Tasks.USER_ID));
                            cv.put(Tasks.ASSIGNED_TO, getInt(ob, Tasks.ASSIGNED_TO));
                            cv.put(Tasks.ADMIN_ID, getInt(ob, Tasks.ADMIN_ID));
                            cv.put(Tasks.COMPLETED, getBoolean(ob,Tasks.COMPLETED));
                            cv.put(Tasks.TITLE, getString(ob, Tasks.TITLE));
                            cv.put(Tasks.DUE_AT, getString(ob, Tasks.DUE_AT));
                            cv.put(Tasks.CATEGORY, getString(ob, Tasks.CATEGORY));
                            cv.put(Tasks.CREATED_AT, getString(ob, Tasks.CREATED_AT));
                            cv.put(Tasks.UPDATED_AT, getString(ob, Tasks.UPDATED_AT));
                            cv.put(Tasks.UPDATED, 0);
                            cv.put(Tasks.INSERTED, 0);
                            cv.put(Tasks.DELETED, 0);
                            task.add(cv);
                        }
                        DbManager.getInstance().replaceList(Tasks.T_NAME, task);
                    }

                    if (main.has("all_comments")) {
                        JSONArray arr = main.getJSONArray("all_comments");
                        ArrayList<ContentValues> comments = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Comments.R_ID, getInt(ob, Comments.ID));
                            cv.put(Comments.TEXT, getString(ob, Comments.TEXT));
                            cv.put(Comments.CREATOR_ID, getInt(ob, Comments.CREATOR_ID));
                            cv.put(Comments.COMMENTABLE_ID, getInt(ob, Comments.COMMENTABLE_ID));
                            cv.put(Comments.COMMENTABLE_TYPE, getString(ob, Comments.COMMENTABLE_TYPE));
                            cv.put(Comments.CREATED_AT, getString(ob, Comments.CREATED_AT));
                            cv.put(Comments.UPDATED_AT, getString(ob, Comments.UPDATED_AT));
                            cv.put(Comments.ADMIN_ID, getInt(ob, Comments.ADMIN_ID));
                            cv.put(Comments.UPDATED, 0);
                            cv.put(Comments.INSERTED, 0);
                            cv.put(Comments.DELETED, 0);
                            comments.add(cv);
                        }
                        DbManager.getInstance().replaceList(Comments.T_NAME, comments);
                    }

                    if (main.has("all_notes")) {
                        JSONArray arr = main.getJSONArray("all_notes");
                        ArrayList<ContentValues> notes = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Notes.R_ID, getInt(ob, Notes.ID));
                            cv.put(Notes.USER_ID, getInt(ob, Notes.USER_ID));
                            cv.put(Notes.MATTER_ID, getInt(ob, Notes.MATTER_ID));
                            cv.put(Notes.DESCRIPTION, getString(ob, Notes.DESCRIPTION));
                            cv.put(Notes.CREATED_AT, getString(ob, Notes.CREATED_AT));
                            cv.put(Notes.UPDATED_AT, getString(ob, Notes.UPDATED_AT));
                            cv.put(Notes.ADMIN_ID, getInt(ob, Notes.ADMIN_ID));
                            cv.put(Notes.UPDATED, 0);
                            cv.put(Notes.INSERTED, 0);
                            cv.put(Notes.DELETED, 0);
                            notes.add(cv);
                        }
                        DbManager.getInstance().replaceList(Notes.T_NAME, notes);
                    }

                    if (main.has("all_case_types")) {
                        JSONArray arr = main.getJSONArray("all_case_types");
                        ArrayList<ContentValues> caseType = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(CaseType.R_ID, getInt(ob, CaseType.ID));
                            cv.put(CaseType.COURT_ID, getInt(ob, CaseType.COURT_ID));
                            cv.put(CaseType.TYPE, getString(ob, CaseType.TYPE));
                            cv.put(CaseType.CREATED_AT, getString(ob, CaseType.CREATED_AT));
                            cv.put(CaseType.UPDATED_AT, getString(ob, CaseType.UPDATED_AT));
                            cv.put(CaseType.UPDATED, 0);
                            cv.put(CaseType.INSERTED, 0);
                            cv.put(CaseType.DELETED, 0);
                            caseType.add(cv);
                        }
                        DbManager.getInstance().replaceList(CaseType.T_NAME, caseType);
                    }

                    if (main.has("all_company_matters_contacts")) {
                        JSONArray arr = main.getJSONArray("all_company_matters_contacts");
                        ArrayList<ContentValues> caseType = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(CCMatters.R_ID, getInt(ob, CCMatters.ID));
                            cv.put(CCMatters.MATTER_ID, getInt(ob, CCMatters.MATTER_ID));
                            cv.put(CCMatters.CC_ID, getInt(ob, CCMatters.CC_ID));
                            cv.put(CCMatters.CREATED_AT, getString(ob, CCMatters.CREATED_AT));
                            cv.put(CCMatters.UPDATED_AT, getString(ob, CCMatters.UPDATED_AT));
                            cv.put(CCMatters.UPDATED, 0);
                            cv.put(CCMatters.INSERTED, 0);
                            cv.put(CCMatters.DELETED, 0);
                            caseType.add(cv);
                        }
                        DbManager.getInstance().replaceList(CCMatters.T_NAME, caseType);
                    }

                    if (main.has("all_matters_person_contacts")) {
                        JSONArray arr = main.getJSONArray("all_matters_person_contacts");
                        ArrayList<ContentValues> caseType = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(MPContacts.R_ID, getInt(ob, MPContacts.ID));
                            cv.put(MPContacts.MATTER_ID, getInt(ob, MPContacts.MATTER_ID));
                            cv.put(MPContacts.PC_ID, getInt(ob, MPContacts.PC_ID));
                            cv.put(MPContacts.CREATED_AT, getString(ob, MPContacts.CREATED_AT));
                            cv.put(MPContacts.UPDATED_AT, getString(ob, MPContacts.UPDATED_AT));
                            cv.put(MPContacts.UPDATED, 0);
                            cv.put(MPContacts.INSERTED, 0);
                            cv.put(MPContacts.DELETED, 0);
                            caseType.add(cv);
                        }
                        DbManager.getInstance().replaceList(MPContacts.T_NAME, caseType);
                    }

                    if (main.has("all_company_contacts")) {
                        JSONArray arr = main.getJSONArray("all_company_contacts");
                        ArrayList<ContentValues> companyContact = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(CompanyContacts.R_ID, getInt(ob, CompanyContacts.ID));
                            cv.put(CompanyContacts.NAME, getString(ob, CompanyContacts.NAME));
                            cv.put(CompanyContacts.EMAIL, getString(ob, CompanyContacts.EMAIL));
                            cv.put(CompanyContacts.TELEPHONE, getString(ob, CompanyContacts.TELEPHONE));
                            cv.put(CompanyContacts.WEBSITE, getString(ob, CompanyContacts.WEBSITE));
                            cv.put(CompanyContacts.ADDRESS1, getString(ob, CompanyContacts.ADDRESS1));
                            cv.put(CompanyContacts.ADDRESS2, getString(ob, CompanyContacts.ADDRESS2));
                            cv.put(CompanyContacts.CITY, getString(ob, CompanyContacts.CITY));
                            cv.put(CompanyContacts.ZIP, getString(ob, CompanyContacts.ZIP));
                            cv.put(CompanyContacts.STATE, getString(ob, CompanyContacts.STATE));
                            cv.put(CompanyContacts.COUNTRY, getString(ob, CompanyContacts.COUNTRY));
                            cv.put(CompanyContacts.CREATOR_ID, getInt(ob, CompanyContacts.CREATOR_ID));
                            cv.put(CompanyContacts.ADMIN_ID, getInt(ob, CompanyContacts.ADMIN_ID));
                            cv.put(CompanyContacts.IS_CLIENT, getBoolean(ob,CompanyContacts.IS_CLIENT));
                            JSONObject obj = ob.getJSONObject("logo");
                            if (!obj.isNull("url"))
                                cv.put(CompanyContacts.LOGO, obj.getString("url"));
                            if (!obj.getJSONObject("small").isNull("url"))
                                cv.put(CompanyContacts.THUMB, obj.getJSONObject("small").getString("url"));
                            cv.put(CompanyContacts.CREATED_AT, getString(ob, CompanyContacts.CREATED_AT));
                            cv.put(CompanyContacts.UPDATED_AT, getString(ob, CompanyContacts.UPDATED_AT));
                            cv.put(CompanyContacts.UPDATED, 0);
                            cv.put(CompanyContacts.INSERTED, 0);
                            cv.put(CompanyContacts.DELETED, 0);
                            companyContact.add(cv);
                        }
                        DbManager.getInstance().replaceList(CompanyContacts.T_NAME, companyContact);
                    }

                    if (main.has("all_person_contacts")) {
                        JSONArray arr = main.getJSONArray("all_person_contacts");
                        ArrayList<ContentValues> personContact = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(PersonContacts.R_ID, getInt(ob, PersonContacts.ID));
                            cv.put(PersonContacts.TITLE, getString(ob, PersonContacts.TITLE));
                            cv.put(PersonContacts.F_NAME, getString(ob, PersonContacts.F_NAME));
                            cv.put(PersonContacts.L_NAME, getString(ob, PersonContacts.L_NAME));
                            cv.put(PersonContacts.C_TYPE, getString(ob, PersonContacts.C_TYPE));
                            cv.put(PersonContacts.C_CONTACT_ID, getInt(ob, PersonContacts.C_CONTACT_ID));
                            cv.put(PersonContacts.ADDRESS1, getString(ob, PersonContacts.ADDRESS1));
                            cv.put(PersonContacts.ADDRESS2, getString(ob, PersonContacts.ADDRESS2));
                            cv.put(PersonContacts.CITY, getString(ob, PersonContacts.CITY));
                            cv.put(PersonContacts.ZIP, getString(ob, PersonContacts.ZIP));
                            cv.put(PersonContacts.STATE, getString(ob, PersonContacts.STATE));
                            cv.put(PersonContacts.COUNTRY, getString(ob, PersonContacts.COUNTRY));
                            cv.put(PersonContacts.CREATOR_ID, getInt(ob, PersonContacts.CREATOR_ID));
                            cv.put(PersonContacts.ADMIN_ID, getInt(ob, PersonContacts.ADMIN_ID));
                            cv.put(PersonContacts.IS_CLIENT,getBoolean(ob,PersonContacts.IS_CLIENT));
                            cv.put(PersonContacts.EMAIL, getString(ob, PersonContacts.EMAIL));
                            cv.put(PersonContacts.TELEPHONE, getString(ob, PersonContacts.TELEPHONE));
                            JSONObject obj = ob.getJSONObject("avatar");
                            if (!obj.isNull("url"))
                                cv.put(PersonContacts.LOGO, obj.getString("url"));
                            if (!obj.getJSONObject("thumb").isNull("url"))
                                cv.put(PersonContacts.THUMB, obj.getJSONObject("thumb").getString("url"));
                            cv.put(PersonContacts.CREATED_AT, getString(ob, PersonContacts.CREATED_AT));
                            cv.put(PersonContacts.UPDATED_AT, getString(ob, PersonContacts.UPDATED_AT));
                            cv.put(PersonContacts.UPDATED, 0);
                            cv.put(PersonContacts.INSERTED, 0);
                            cv.put(PersonContacts.DELETED, 0);
                            personContact.add(cv);
                        }
                        DbManager.getInstance().replaceList(PersonContacts.T_NAME, personContact);
                    }

                    if (main.has("all_courts")) {
                        JSONArray arr = main.getJSONArray("all_courts");
                        ArrayList<ContentValues> court = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Courts.R_ID, getInt(ob, Courts.ID));
                            cv.put(Courts.NAME, getString(ob, Courts.NAME));
                            cv.put(Courts.CREATED_AT, getString(ob, Courts.CREATED_AT));
                            cv.put(Courts.UPDATED_AT, getString(ob, Courts.UPDATED_AT));
                            cv.put(Courts.UPDATED, 0);
                            cv.put(Courts.INSERTED, 0);
                            cv.put(Courts.DELETED, 0);
                            court.add(cv);
                        }
                        DbManager.getInstance().replaceList(Courts.T_NAME, court);
                    }

                    if (main.has("all_custom_hearings")) {
                        JSONArray arr = main.getJSONArray("all_custom_hearings");
                        ArrayList<ContentValues> custom = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(CustomHearing.R_ID, getInt(ob, CustomHearing.ID));
                            cv.put(CustomHearing.MATTER_ID, getInt(ob, CustomHearing.MATTER_ID));
                            cv.put(CustomHearing.CREATOR_ID, getInt(ob, CustomHearing.CREATOR_ID));
                            cv.put(CustomHearing.ADMIN_ID, getInt(ob, CustomHearing.ADMIN_ID));
                            cv.put(CustomHearing.ATTENDEE_ID, getInt(ob, CustomHearing.ATTENDEE_ID));
                            cv.put(CustomHearing.HEARING_ID, getInt(ob, CustomHearing.HEARING_ID));
                            cv.put(CustomHearing.ON_DATE, getString(ob, CustomHearing.ON_DATE));
                            cv.put(CustomHearing.JUDGE_NAME, getString(ob, CustomHearing.JUDGE_NAME));
                            cv.put(CustomHearing.ADDED_ON, getString(ob, CustomHearing.ADDED_ON));
                            cv.put(CustomHearing.COURT_ROOM_NO, getString(ob, CustomHearing.COURT_ROOM_NO));
                            cv.put(CustomHearing.ITEM_NO, getString(ob, CustomHearing.ITEM_NO));
                            cv.put(CustomHearing.CREATED_AT, getString(ob, CustomHearing.CREATED_AT));
                            cv.put(CustomHearing.UPDATED_AT, getString(ob, CustomHearing.UPDATED_AT));
                            cv.put(CustomHearing.UPDATED, 0);
                            cv.put(CustomHearing.INSERTED, 0);
                            cv.put(CustomHearing.DELETED, 0);
                            custom.add(cv);
                        }
                        DbManager.getInstance().replaceList(CustomHearing.T_NAME, custom);
                    }

                    if (main.has("all_hearings")) {
                        JSONArray arr = main.getJSONArray("all_hearings");
                        ArrayList<ContentValues> hearing = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Hearing.R_ID, getInt(ob, Hearing.ID));
                            cv.put(Hearing.MATTER_ID, getInt(ob, Hearing.MATTER_ID));
                            cv.put(Hearing.HEARING_ID, getInt(ob, Hearing.HEARING_ID));
                            cv.put(Hearing.ON_DATE, getString(ob, Hearing.ON_DATE));
                            cv.put(Hearing.JUDGE_NAME, getString(ob, Hearing.JUDGE_NAME));
                            cv.put(Hearing.ADDED_ON, getString(ob, Hearing.ADDED_ON));
                            cv.put(Hearing.COURT_ROOM_NO, getString(ob, Hearing.COURT_ROOM_NO));
                            cv.put(Hearing.ITEM_NO, getString(ob, Hearing.ITEM_NO));
                            cv.put(Hearing.CREATED_AT, getString(ob, Hearing.CREATED_AT));
                            cv.put(Hearing.UPDATED_AT, getString(ob, Hearing.UPDATED_AT));
                            cv.put(Hearing.UPDATED, 0);
                            cv.put(Hearing.INSERTED, 0);
                            cv.put(Hearing.DELETED, 0);
                            hearing.add(cv);
                        }
                        DbManager.getInstance().replaceList(Hearing.T_NAME, hearing);
                    }

                    if (main.has("all_orders")) {
                        JSONArray arr = main.getJSONArray("all_orders");
                        ArrayList<ContentValues> order = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Orders.R_ID, getInt(ob, Orders.ID));
                            cv.put(Orders.MATTER_ID, getInt(ob, Orders.MATTER_ID));
                            cv.put(Orders.API_ORDER_ID, getInt(ob, Orders.API_ORDER_ID));
                            cv.put(Orders.DESC, getString(ob, Orders.DESC));
                            cv.put(Orders.LINK, getString(ob, Orders.LINK));
                            cv.put(Orders.DATE, getString(ob, Orders.DATE));
                            cv.put(Orders.CREATED_AT, getString(ob, Orders.CREATED_AT));
                            cv.put(Orders.UPDATED_AT, getString(ob, Orders.UPDATED_AT));
                            cv.put(Orders.UPDATED, 0);
                            cv.put(Orders.INSERTED, 0);
                            cv.put(Orders.DELETED, 0);
                            order.add(cv);
                        }
                        DbManager.getInstance().replaceList(Orders.T_NAME, order);
                    }

                    if (main.has("all_matter_users")) {
                        JSONArray arr = main.getJSONArray("all_matter_users");
                        ArrayList<ContentValues> matterUser = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(MatterUsers.R_ID, getInt(ob, MatterUsers.ID));
                            cv.put(MatterUsers.USER_ID, getInt(ob, MatterUsers.USER_ID));
                            cv.put(MatterUsers.MATTER_ID, getInt(ob, MatterUsers.MATTER_ID));
                            cv.put(MatterUsers.MY_CLIENT, getString(ob, MatterUsers.MY_CLIENT));
                            cv.put(MatterUsers.CREATED_AT, getString(ob, MatterUsers.CREATED_AT));
                            cv.put(MatterUsers.UPDATED_AT, getString(ob, MatterUsers.UPDATED_AT));
                            cv.put(MatterUsers.UPDATED, 0);
                            cv.put(MatterUsers.INSERTED, 0);
                            cv.put(MatterUsers.DELETED, 0);
                            matterUser.add(cv);
                        }
                        DbManager.getInstance().replaceList(MatterUsers.T_NAME, matterUser);
                    }

                    if (main.has("all_posts")) {
                        JSONArray arr = main.getJSONArray("all_posts");
                        ArrayList<ContentValues> post = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Post.R_ID, getInt(ob, Post.ID));
                            cv.put(Post.POSTABLE_ID, getInt(ob, Post.POSTABLE_ID));
                            cv.put(Post.CREATOR_ID, getInt(ob, Post.CREATOR_ID));
                            cv.put(Post.ADMIN_ID, getInt(ob, Post.ADMIN_ID));
                            cv.put(Post.TEXT, getString(ob, Post.TEXT));
                            cv.put(Post.POSTABLE_TYPE, getString(ob, Post.POSTABLE_TYPE));
                            cv.put(Post.CREATED_AT, getString(ob, Post.CREATED_AT));
                            cv.put(Post.UPDATED_AT, getString(ob, Post.UPDATED_AT));
                            cv.put(Post.UPDATED, 0);
                            cv.put(Post.INSERTED, 0);
                            cv.put(Post.DELETED, 0);
                            post.add(cv);
                        }
                        DbManager.getInstance().replaceList(Post.T_NAME, post);
                    }

                    if (main.has("all_notifications")) {
                        JSONArray arr = main.getJSONArray("all_notifications");
                        ArrayList<ContentValues> noti = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Notifications.R_ID, getInt(ob, Notifications.ID));
                            cv.put(Notifications.NOTICE_TYPE, getString(ob, Notifications.NOTICE_TYPE));
                            cv.put(Notifications.CREATOR_ID, getInt(ob, Notifications.CREATOR_ID));
                            cv.put(Notifications.NOTI_ID, getInt(ob, Notifications.NOTI_ID));
                            cv.put(Notifications.TEXT, getString(ob, Notifications.TEXT));
                            cv.put(Notifications.LINK, getString(ob, Notifications.LINK));
                            cv.put(Notifications.NOTI_TYPE, getString(ob, Notifications.NOTI_TYPE));
                            cv.put(Notifications.CREATED_AT, getString(ob, Notifications.CREATED_AT));
                            cv.put(Notifications.UPDATED_AT, getString(ob, Notifications.UPDATED_AT));
                            cv.put(Notifications.UPDATED, 0);
                            cv.put(Notifications.INSERTED, 0);
                            cv.put(Notifications.DELETED, 0);
                            noti.add(cv);


                            ArrayList<ContentValues> notiStatus = new ArrayList<>();
                            int id = getInt(ob, Notifications.ID);
                            JSONArray obj = ob.getJSONArray("members");
                            for (int j = 0; j < obj.length(); j++) {
                                ContentValues cval = new ContentValues();
                                cval.put(NotificationsStatus.NOTI_ID, id);
                                cval.put(NotificationsStatus.MEMBER_ID, obj.getInt(j));
                                cval.put(NotificationsStatus.READ, 0);
                                cval.put(NotificationsStatus.UPDATED, 0);
                                cval.put(NotificationsStatus.INSERTED, 0);
                                cval.put(NotificationsStatus.DELETED, 0);
                                notiStatus.add(cval);
                            }
                            DbManager.getInstance().replaceList(NotificationsStatus.T_NAME, notiStatus);

                            JSONArray read = ob.getJSONArray("read_by");
                            for (int k = 0; k < read.length(); k++) {
                                ContentValues cval = new ContentValues();
                                cval.put(NotificationsStatus.READ, 1);
                                cval.put(NotificationsStatus.UPDATED, 0);
                                cval.put(NotificationsStatus.INSERTED, 0);
                                cval.put(NotificationsStatus.DELETED, 0);
                                DbManager.getInstance().update(NotificationsStatus.T_NAME, cval, NotificationsStatus.NOTI_ID,
                                        NotificationsStatus.MEMBER_ID, id + "", read.getInt(k) + "");
                            }
                        }
                        DbManager.getInstance().replaceList(Notifications.T_NAME, noti);
                    }

                    if (main.has("all_matters")) {
                        JSONArray arr = main.getJSONArray("all_matters");
                        ArrayList<ContentValues> matter = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Matters.R_ID, getInt(ob, Matters.ID));
                            cv.put(Matters.MATTER_NO, getString(ob, Matters.MATTER_NO));
                            cv.put(Matters.P_ATT, getString(ob, Matters.P_ATT));
                            cv.put(Matters.D_ATT, getString(ob, Matters.D_ATT));
                            cv.put(Matters.I_STATUS, getString(ob, Matters.I_STATUS));
                            cv.put(Matters.C_STATUS, getString(ob, Matters.C_STATUS));
                            cv.put(Matters.PLAINTIFF, getString(ob, Matters.PLAINTIFF));
                            cv.put(Matters.DEFFENDANT, getString(ob, Matters.DEFFENDANT));
                            cv.put(Matters.CLIENT_TYPE, getString(ob, Matters.CLIENT_TYPE));
                            cv.put(Matters.CASE_YEAR, getString(ob, Matters.CASE_YEAR));
                            cv.put(Matters.COURT_ID, getInt(ob, Matters.COURT_ID));
                            cv.put(Matters.CREATOR_ID, getInt(ob, Matters.CREATOR_ID));
                            cv.put(Matters.ADMIN_ID, getInt(ob, Matters.ADMIN_ID));
                            cv.put(Matters.CASE_TYPE_ID, getInt(ob, Matters.CASE_TYPE_ID));
                            cv.put(Matters.API_MATTER_ID, getInt(ob, Matters.API_MATTER_ID));
                            cv.put(Matters.CREATED_AT, getString(ob, Matters.CREATED_AT));
                            cv.put(Matters.UPDATED_AT, getString(ob, Matters.UPDATED_AT));
                            cv.put(Matters.UPDATED, 0);
                            cv.put(Matters.INSERTED, 0);
                            cv.put(Matters.DELETED, 0);
                            matter.add(cv);
                        }
                        DbManager.getInstance().replaceList(Matters.T_NAME, matter);
                    }

                    if (main.has("all_team_members")) {
                        JSONArray arr = main.getJSONArray("all_team_members");
                        ArrayList<ContentValues> users = new ArrayList<>();
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Users.R_ID, getInt(ob, Users.ID));
                            cv.put(Users.FIRST_NAME, getString(ob, Users.FIRST_NAME));
                            cv.put(Users.M_NAME, getString(ob, Users.M_NAME));
                            cv.put(Users.L_NAME, getString(ob, Users.L_NAME));
                            cv.put(Users.IS_COMPANY,getBoolean(ob,Users.IS_COMPANY));
                            cv.put(Users.COMPANY_NAME, getString(ob, Users.COMPANY_NAME));
                            cv.put(Users.EXPERTISE, getString(ob, Users.EXPERTISE));
                            cv.put(Users.CONTACT, getString(ob, Users.CONTACT));
                            cv.put(Users.COUNCIL_NO, getString(ob, Users.COUNCIL_NO));
                            cv.put(Users.C_TYPE, getString(ob, Users.C_TYPE));
                            cv.put(Users.ROLE, getString(ob, Users.ROLE));
                            cv.put(Users.C_ID, getInt(ob, Users.C_ID));
                            cv.put(Users.LOCATION_ID, getInt(ob, Users.LOCATION_ID));

                            JSONObject obj = ob.getJSONObject("user_setting");
                            JSONObject avatar = obj.getJSONObject("avatar");
                            JSONObject thumb = avatar.getJSONObject("thumb");

                            if (!avatar.isNull("url")) {
                                cv.put(Users.URL, avatar.getString("url"));
                            }

                            if (!thumb.isNull("url")) {
                                cv.put(Users.THUMB, thumb.getString("url"));
                            }

                            cv.put(Users.CREATED_AT, getString(ob, Users.CREATED_AT));
                            cv.put(Users.UPDATED_AT, getString(ob, Users.UPDATED_AT));
                            cv.put(Users.UPDATED, 0);
                            cv.put(Users.INSERTED, 0);
                            cv.put(Users.DELETED, 0);
                            users.add(cv);
                        }
                        DbManager.getInstance().replaceList(Users.T_NAME, users);
                    }

                    if (main.has("current_server_time")) {
                        String date = getString(main, "current_server_time");
                        String fdate = "", sdate = "";
                        Calendar c = Calendar.getInstance();
                        if (date.equals("")) {
                            sdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(c.getTime());
                        } else {
                            sdate = getFormattedDate(date);
                        }
                        fdate = new SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.getDefault()).format(c.getTime());
                        AppPreferences.setDate(ctx, sdate);
                        AppPreferences.setFDate(ctx, fdate);
                        AppLogger.logE("SYNC", "SUCEESS" + date);
                    }

                    AppPreferences.setLogin(ctx, true);
                    AppPreferences.setIncremental(ctx, true);

                    Runnable myRunnable2 = new Runnable() {
                        @Override
                        public void run() {
                            if (mListener != null) {
                                mListener.OnResponse("", true);
                            }
                        }
                    };
                    mainHandler.post(myRunnable2);
                }

            } catch (final JSONException e) {
                e.printStackTrace();
                Runnable myRunnable2 = new Runnable() {
                    @Override
                    public void run() {
                        if (mListener != null) {
                            mListener.OnResponse("Error saving data ("+e.getMessage()+")", false);
                        }
                    }
                };
                mainHandler.post(myRunnable2);
            }
        } else {
            Runnable myRunnable2 = new Runnable() {
                @Override
                public void run() {
                    if (mListener != null) {
                        mListener.OnResponse("Sync Failed, Please try again later..", false);
                    }
                }
            };
            mainHandler.post(myRunnable2);
        }

        return "";
    }

    private Boolean prepareData() {
        AppPreferences.setKey(ctx, UUID.randomUUID().toString());
        Cursor curs = CacheManager.INSTANCE.getAllKey();
        String msg = "";
        if (curs != null && curs.getCount() > 0) {
            while (curs != null && curs.moveToNext()) {
                String key = curs.getString(curs.getColumnIndex("key_name"));
                JSONObject obj = uploadData(key);
                String urlParameters = obj.toString();

                do {
                    String result = sendPost(Const.BASE_URL + Const.UPDATE, urlParameters);
                    msg = parseResult(result);
                    if (msg.equalsIgnoreCase("failed") || msg.equalsIgnoreCase("") || msg.equalsIgnoreCase("successful")) {
                        break;
                    }
                } while (true);

                if (!msg.equalsIgnoreCase("successful")) {
                    break;
                } else {
                    /// set 101 as key
                    DbManager.getInstance().openDatabase();
                    DbManager.getInstance().updateList(key);
                    DbManager.getInstance().closeDatabase();
                }
            }
            if (curs != null) {
                curs.close();
            }

            if (msg.equalsIgnoreCase("") || msg.equalsIgnoreCase("failed")) {
                return false;
            } else if (msg.equalsIgnoreCase("successful")) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    private String parseResult(String result) {
        String msg = "";
        if (result != null && !result.equals("")) {
            try {
                msg = "";
                JSONObject main = new JSONObject(result);
                if (main.has("sync_status")) {
                    msg = main.getString("sync_status");
                }
            } catch (Exception e) {
                e.printStackTrace();
                msg = "";
            }
        }
        return msg;
    }

    private JSONObject uploadData(String key) {
        JSONObject obj = new JSONObject();
        try {

            obj.put("keyname", key);

            JSONObject ob = new JSONObject();
            ob.put("authentication_token", AppPreferences.getToken(ctx));
            ob.put("id", AppPreferences.getUserId(ctx));
            ob.put("insertStatement", getInsertStatement(key));
            ob.put("deleteStatement", getDeleteStatement(key));
            ob.put("updateStatement", getUpdateStatement(key));

            obj.put("payload", ob);

            Log.e("Final objet", obj + "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    private JSONObject getInsertStatement(String key) throws Exception {
        JSONObject obj = new JSONObject();
        obj.put("all_hearings", insertHearing(key));
        obj.put("all_tasks", insertTask(key));
        obj.put("all_company_contacts", insertCompanyContact(key));
        obj.put("all_person_contacts", insertPersonContact(key));
        obj.put("all_custom_hearings", insertCustomHearing(key));
        obj.put("all_orders", insertOrder(key));
        obj.put("all_matter_users", insertMatterUsers(key));
        obj.put("all_posts", insertPost(key));
        obj.put("all_notes", insertNote(key));
        obj.put("all_notifications", insertNotification(key));

        return obj;
    }

    private JSONArray insertNotification(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertNotification(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));

                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getMemberForNotification(key, id);
                while (c != null && c.moveToNext()) {
                    ar.put(c.getString(c.getColumnIndex(c.getColumnName(0))));
                }
                if (c != null) {
                    c.close();
                }
                ob.put("members", ar);

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertNote(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertNote(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));

                if (curs.getInt(curs.getColumnIndex(curs.getColumnName(7))) == 1) {
                    ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                    ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                    ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                    ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                    ob.put(curs.getColumnName(10), curs.getString(curs.getColumnIndex(curs.getColumnName(10))));
                } else {
                    ob.put("id", id);
                }

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getNewComments(key, id, "Note");
                while (c != null && c.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put(c.getColumnName(0), c.getString(c.getColumnIndex(c.getColumnName(0))));
                    obj.put(c.getColumnName(1), c.getString(c.getColumnIndex(c.getColumnName(1))));
                    obj.put(c.getColumnName(2), c.getString(c.getColumnIndex(c.getColumnName(2))));
                    obj.put(c.getColumnName(3), c.getString(c.getColumnIndex(c.getColumnName(3))));
                    obj.put(c.getColumnName(4), c.getString(c.getColumnIndex(c.getColumnName(4))));
                    ar.put(obj);
                }
                if (c != null) {
                    c.close();
                }
                ob.put("comments", ar);
                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertPost(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertPost(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));

                if (curs.getInt(curs.getColumnIndex(curs.getColumnName(6))) == 1) {
                    ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                    ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                    ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                } else {
                    ob.put("id", id);
                }

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getNewComments(key, id, "Post");
                while (c != null && c.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put(c.getColumnName(0), c.getString(c.getColumnIndex(c.getColumnName(0))));
                    obj.put(c.getColumnName(1), c.getString(c.getColumnIndex(c.getColumnName(1))));
                    obj.put(c.getColumnName(2), c.getString(c.getColumnIndex(c.getColumnName(2))));
                    obj.put(c.getColumnName(3), c.getString(c.getColumnIndex(c.getColumnName(3))));
                    obj.put(c.getColumnName(4), c.getString(c.getColumnIndex(curs.getColumnName(4))));
                    ar.put(obj);
                }
                if (c != null) {
                    c.close();
                }
                ob.put("comments", ar);
                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;

    }

    private JSONArray insertMatterUsers(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertMatterUsers(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                ob.put(curs.getColumnName(0), curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertOrder(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertOrder(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));
                ob.put("id", id);

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getNewComments(key, id, "Order");
                while (c != null && c.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put(curs.getColumnName(0), curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                    obj.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    obj.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    obj.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    obj.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ar.put(obj);
                }
                if (c != null) {
                    c.close();
                }
                ob.put("comments", ar);
                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }
        return arr;
    }

    private JSONArray insertCustomHearing(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertCustomHearing(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));

                if (curs.getInt(curs.getColumnIndex(curs.getColumnName(10))) == 1) {
                    ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                    ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                    ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                    ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                    ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                } else {
                    ob.put("id", id);
                }

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getNewComments(key, id, "CustomHearing");
                while (c != null && c.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put(c.getColumnName(0), c.getString(c.getColumnIndex(c.getColumnName(0))));
                    obj.put(c.getColumnName(1), c.getString(c.getColumnIndex(c.getColumnName(1))));
                    obj.put(c.getColumnName(2), c.getString(c.getColumnIndex(c.getColumnName(2))));
                    obj.put(c.getColumnName(3), c.getString(c.getColumnIndex(c.getColumnName(3))));
                    obj.put(c.getColumnName(4), c.getString(c.getColumnIndex(c.getColumnName(4))));
                    ar.put(obj);
                }
                if (c != null) {
                    c.close();
                }
                ob.put("comments", ar);
                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }
        return arr;
    }

    private JSONArray insertPersonContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertPersonContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));
                if (curs.getInt(curs.getColumnIndex(curs.getColumnName(21))) == 1) {
                    ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                    ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                    ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                    ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                    ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                    ob.put(curs.getColumnName(10), curs.getString(curs.getColumnIndex(curs.getColumnName(10))));
                    ob.put(curs.getColumnName(11), curs.getString(curs.getColumnIndex(curs.getColumnName(11))));
                    ob.put(curs.getColumnName(12), curs.getString(curs.getColumnIndex(curs.getColumnName(12))));
                    ob.put(curs.getColumnName(13), curs.getString(curs.getColumnIndex(curs.getColumnName(13))));
                    ob.put(curs.getColumnName(14), curs.getString(curs.getColumnIndex(curs.getColumnName(14))));
                    ob.put(curs.getColumnName(15), curs.getString(curs.getColumnIndex(curs.getColumnName(15))));
                    ob.put(curs.getColumnName(16), curs.getString(curs.getColumnIndex(curs.getColumnName(16))));
                    ob.put(curs.getColumnName(17), curs.getString(curs.getColumnIndex(curs.getColumnName(17))));
                    ob.put(curs.getColumnName(18), curs.getString(curs.getColumnIndex(curs.getColumnName(18))));
                    ob.put(curs.getColumnName(19), curs.getString(curs.getColumnIndex(curs.getColumnName(19))));
                    ob.put(curs.getColumnName(20), curs.getString(curs.getColumnIndex(curs.getColumnName(20))));
                } else {
                    ob.put("id", id);
                }

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getPersonContactsMatterForKey(key, id);
                while (c != null && c.moveToNext()) {
                    ar.put(c.getString(c.getColumnIndex(c.getColumnName(0))));
                }
                if (c != null) {
                    c.close();
                }
                ob.put("matters", ar);

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertCompanyContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertCompanyContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));
                if (curs.getInt(curs.getColumnIndex(curs.getColumnName(16))) == 1) {
                    ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                    ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                    ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                    ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                    ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                    ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                    ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                    ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                    ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                    ob.put(curs.getColumnName(10), curs.getString(curs.getColumnIndex(curs.getColumnName(10))));
                    ob.put(curs.getColumnName(11), curs.getString(curs.getColumnIndex(curs.getColumnName(11))));
                    ob.put(curs.getColumnName(12), curs.getString(curs.getColumnIndex(curs.getColumnName(12))));
                    ob.put(curs.getColumnName(13), curs.getString(curs.getColumnIndex(curs.getColumnName(13))));
                    ob.put(curs.getColumnName(14), curs.getString(curs.getColumnIndex(curs.getColumnName(14))));
                    ob.put(curs.getColumnName(15), curs.getString(curs.getColumnIndex(curs.getColumnName(15))));
                } else {
                    ob.put("id", id);
                }

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getCompanyContactsMatterForKey(key, id);
                while (c != null && c.moveToNext()) {
                    ar.put(c.getString(c.getColumnIndex(c.getColumnName(0))));
                }
                if (c != null) {
                    c.close();
                }
                ob.put("matters", ar);

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertHearing(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertHearing(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));
                ob.put("id", id);

                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getNewComments(key, id, "Hearing");
                while (c != null && c.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put(c.getColumnName(0), c.getString(c.getColumnIndex(c.getColumnName(0))));
                    obj.put(c.getColumnName(1), c.getString(c.getColumnIndex(c.getColumnName(1))));
                    obj.put(c.getColumnName(2), c.getString(c.getColumnIndex(c.getColumnName(2))));
                    obj.put(c.getColumnName(3), c.getString(c.getColumnIndex(c.getColumnName(3))));
                    obj.put(c.getColumnName(4), c.getString(c.getColumnIndex(c.getColumnName(4))));
                    ar.put(obj);
                }
                if (c != null) {
                    c.close();
                }
                ob.put("comments", ar);
                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray insertTask(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getInsertTask(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                ob.put(curs.getColumnName(0), curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONObject getDeleteStatement(String key) throws Exception {
        JSONObject obj = new JSONObject();
        obj.put("all_person_contacts", deletePersonContact(key));
        obj.put("all_company_matters_contacts", deleteCompanyMatterContact(key));
        obj.put("all_matter_users", deleteMatterUsers(key));
        obj.put("all_matters_person_contacts", deleteMatterPersonContact(key));
        obj.put("all_company_contacts", deleteCompanyContact(key));

        return obj;
    }

    private JSONArray deleteCompanyContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getDeleteCompanyContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                arr.put(curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray deleteMatterPersonContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getDeleteMatterPersonContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                arr.put(curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray deleteMatterUsers(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getDeleteMatterUser(key);
        while (curs != null && curs.moveToNext()) {
            try {
                arr.put(curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray deleteCompanyMatterContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getDeleteCompanyMatterContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                arr.put(curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray deletePersonContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getDeletedPersonContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                arr.put(curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONObject getUpdateStatement(String key) throws Exception {
        JSONObject obj = new JSONObject();
        obj.put("all_person_contacts", updatePersonContact(key));
        obj.put("all_notifications", updateNotification(key));
        obj.put("all_tasks", updateTask(key));
        obj.put("all_company_contacts", updateCompanyContact(key));

        return obj;
    }

    private JSONArray updateCompanyContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getUpdateCompanyContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                ob.put("id", curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                ob.put(curs.getColumnName(10), curs.getString(curs.getColumnIndex(curs.getColumnName(10))));
                ob.put(curs.getColumnName(11), curs.getString(curs.getColumnIndex(curs.getColumnName(11))));
                ob.put(curs.getColumnName(12), curs.getString(curs.getColumnIndex(curs.getColumnName(12))));
                ob.put(curs.getColumnName(13), curs.getString(curs.getColumnIndex(curs.getColumnName(13))));
                ob.put(curs.getColumnName(14), curs.getString(curs.getColumnIndex(curs.getColumnName(14))));
                ob.put(curs.getColumnName(15), curs.getString(curs.getColumnIndex(curs.getColumnName(15))));

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray updateTask(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getUpdateTask(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                ob.put("id", curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray updateNotification(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getUpdateNotification(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                String id = curs.getString(curs.getColumnIndex(curs.getColumnName(0)));
                ob.put("id", id);
                JSONArray ar = new JSONArray();
                Cursor c = CacheManager.INSTANCE.getUpdateReadNotification(key, id);
                while (c != null && c.moveToNext()) {
                    ar.put(c.getString(c.getColumnIndex(c.getColumnName(0))));
                }
                if (c != null) {
                    c.close();
                }
                ob.put("read_by", ar);

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private JSONArray updatePersonContact(String key) {
        JSONArray arr = new JSONArray();
        Cursor curs = CacheManager.INSTANCE.getUpdatePersonContact(key);
        while (curs != null && curs.moveToNext()) {
            try {
                JSONObject ob = new JSONObject();
                ob.put("id", curs.getString(curs.getColumnIndex(curs.getColumnName(0))));
                ob.put(curs.getColumnName(1), curs.getString(curs.getColumnIndex(curs.getColumnName(1))));
                ob.put(curs.getColumnName(2), curs.getString(curs.getColumnIndex(curs.getColumnName(2))));
                ob.put(curs.getColumnName(3), curs.getString(curs.getColumnIndex(curs.getColumnName(3))));
                ob.put(curs.getColumnName(4), curs.getString(curs.getColumnIndex(curs.getColumnName(4))));
                ob.put(curs.getColumnName(5), curs.getString(curs.getColumnIndex(curs.getColumnName(5))));
                ob.put(curs.getColumnName(6), curs.getString(curs.getColumnIndex(curs.getColumnName(6))));
                ob.put(curs.getColumnName(7), curs.getString(curs.getColumnIndex(curs.getColumnName(7))));
                ob.put(curs.getColumnName(8), curs.getString(curs.getColumnIndex(curs.getColumnName(8))));
                ob.put(curs.getColumnName(9), curs.getString(curs.getColumnIndex(curs.getColumnName(9))));
                ob.put(curs.getColumnName(10), curs.getString(curs.getColumnIndex(curs.getColumnName(10))));
                ob.put(curs.getColumnName(11), curs.getString(curs.getColumnIndex(curs.getColumnName(11))));
                ob.put(curs.getColumnName(12), curs.getString(curs.getColumnIndex(curs.getColumnName(12))));
                ob.put(curs.getColumnName(13), curs.getString(curs.getColumnIndex(curs.getColumnName(13))));
                ob.put(curs.getColumnName(14), curs.getString(curs.getColumnIndex(curs.getColumnName(14))));
                ob.put(curs.getColumnName(15), curs.getString(curs.getColumnIndex(curs.getColumnName(15))));
                ob.put(curs.getColumnName(16), curs.getString(curs.getColumnIndex(curs.getColumnName(16))));
                ob.put(curs.getColumnName(17), curs.getString(curs.getColumnIndex(curs.getColumnName(17))));
                ob.put(curs.getColumnName(18), curs.getString(curs.getColumnIndex(curs.getColumnName(18))));
                ob.put(curs.getColumnName(19), curs.getString(curs.getColumnIndex(curs.getColumnName(19))));
                ob.put(curs.getColumnName(20), curs.getString(curs.getColumnIndex(curs.getColumnName(20))));

                arr.put(ob);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (curs != null) {
            curs.close();
        }

        return arr;
    }

    private String getFormattedDate(String date) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat appFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String f_date = "";
        try {
            f_date = appFormat.format(serverFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f_date;
    }

    private String getString(JSONObject ob, String createdAt) {
        if (!ob.isNull(createdAt)) {
            try {
                return ob.getString(createdAt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private int getInt(JSONObject ob, String createdAt) {
        if (!ob.isNull(createdAt)) {
            try {
                return ob.getInt(createdAt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private int getBoolean(JSONObject ob, String createdAt) {
        int val = 0;
        if (!ob.isNull(createdAt)) {
            try {
                boolean value = ob.getBoolean(createdAt);
                val = value ? 1 : 0;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (!result.equals("")) {
            AppUtils.openUtilityDialog(ctx, result);
        }
        DbManager.getInstance().closeDatabase();
    }

    public String sendPost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.addRequestProperty("authentication_token", token);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int status = connection.getResponseCode();

            InputStream is;
            if (status == HttpsURLConnection.HTTP_OK) {
                is = connection.getInputStream();
            } else {
                is = connection.getErrorStream();
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }


    public String excutePost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.addRequestProperty("authentication_token", token);

            int status = connection.getResponseCode();

            InputStream is;
            if (status == HttpsURLConnection.HTTP_OK) {
                is = connection.getInputStream();
            } else {
                is = connection.getErrorStream();
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }


}