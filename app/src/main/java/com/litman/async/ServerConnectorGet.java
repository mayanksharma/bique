package com.litman.async;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class ServerConnectorGet extends AsyncTask<String, String, String> {

    private onAsyncTaskComplete mListener;

    String urlParameters;
    String token = "";

    public ServerConnectorGet(String urlParameters) {
        this.urlParameters = urlParameters;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        String response = "";
        try {
            token = urls[1];
            response = excutePost(urls[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (mListener != null)
            mListener.OnResponse(s);
    }

    public void setDataDownloadListener(onAsyncTaskComplete dataDownloadListner) {
        mListener = dataDownloadListner;
    }

    public interface onAsyncTaskComplete {
        void OnResponse(String response);
    }

    public String excutePost(String targetURL) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.addRequestProperty("authentication_token", token);

            int status = connection.getResponseCode();

            InputStream is;
            if (status == HttpsURLConnection.HTTP_OK) {
                is = connection.getInputStream();
            } else {
                is = connection.getErrorStream();
            }

            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
