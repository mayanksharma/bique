package com.litman.interfaces;


public interface Users {

    String T_NAME="users";

    String ID = "id";
    String R_ID = "remoteid";
    String FIRST_NAME="first_name";
    String M_NAME="middle_name";
    String L_NAME="last_name";
    String C_ID="company_admin_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String IS_COMPANY="is_company";
    String COMPANY_NAME="company_name";
    String LOCATION_ID="location_id";
    String EXPERTISE="area_of_expertise";
    String CONTACT="contact_number";
    String COUNCIL_NO="bar_council_no";
    String USER_TYPE="user_type";
    String C_TYPE="company_type";
    String ROLE="role";
    String URL="avatarurl";
    String THUMB="avatarthumburl";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
