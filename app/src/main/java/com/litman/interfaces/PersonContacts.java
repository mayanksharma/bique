package com.litman.interfaces;


public interface PersonContacts {

    String T_NAME="person_contacts";

    String ID = "id";
    String R_ID = "remoteid";
    String CREATOR_ID="creator_id";
    String ADMIN_ID="admin_id";
    String C_TYPE="contact_type";
    String TITLE="title";
    String F_NAME="first_name";
    String L_NAME="last_name";
    String C_CONTACT_ID="company_contact_id";
    String ADDRESS1="address1";
    String ADDRESS2="address2";
    String CITY="city";
    String ZIP="zip";
    String STATE="state";
    String COUNTRY="country";
    String IS_CLIENT="is_client";
    String EMAIL="email";
    String TELEPHONE="telephone";
    String LOGO="avatarurl";
    String THUMB="avatarthumburl";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
