package com.litman.interfaces;


public interface UploadAssets {

    String T_NAME="uploads_assets";

    String ID = "id";
    String U_ID="upload_id";
    String URL="url";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
