package com.litman.interfaces;

/**
 * Created by mayank on 05/10/16.
 */
public interface Const {

    int TYPE_CUSTOM_HEARING = 4;
    int TYPE_HEARING = 3;
    int TYPE_ORDER = 1;
    int TYPE_POST = 2;
    int TYPE_API = 5;

    String BASE_URL = "http://webapp.biqe.tech/api/v1/";
    String PAGE_BASE_URL = "http://webapp.biqe.tech/";
    String LOGIN = "login";
    String LOGOUT = "logout";
    String CURRENT_USER = "currentuser";
    String UPDATE_SETTINGS = "update_settings";
    String SYNC = "syncs";
    String UPDATE = "receive";
    String STATUS = "status?keyname=";
    String TOKEN = "device_token";
    String SIGN_UP = PAGE_BASE_URL + "users/sign_up?deviceType=mobile";
    String PASSWORD = PAGE_BASE_URL + "users/password/new?deviceType=mobile";
    String SUGGEST_ME = PAGE_BASE_URL + "matters/list_matters?deviceType=mobile&authentication_token=";
    String DISPLAY_BOARD = PAGE_BASE_URL + "hearings/calendar?deviceType=mobile&authentication_token=";
    String ADD_NEW_MATTER = PAGE_BASE_URL + "matters/new?deviceType=mobile&authentication_token=";
    String PROFILE_OTHER_1 = PAGE_BASE_URL + "user/details/";
    String PROFILE_OTHER_2 = "?deviceType=mobile&authentication_token=";
    String CONTACT_US = PAGE_BASE_URL + "contacts/new?deviceType=mobile&authentication_token=";
    String FILE_UPLOAD = PAGE_BASE_URL + "uploads/new?deviceType=mobile&authentication_token=";
    String USER_EDIT = PAGE_BASE_URL + "users/edit?deviceType=mobile&authentication_token=";
    String SUCCESS = PAGE_BASE_URL + "hybrid/success?deviceType=mobile";
    String FAIL = PAGE_BASE_URL + "hybrid/failure?deviceType=mobile";
    String DISPLAY_BOARD_API = "http://api.biqe.tech/biqeapi/api/displayboard";

    String KEY_STAGGING = "322131";
    String KEY_DEVELOPMENT = "8000eb28e81575993b44";
    String KEY_PRODUCTION = "63cc217f3cdf53a3e617";

}
