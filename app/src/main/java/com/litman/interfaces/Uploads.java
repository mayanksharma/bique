package com.litman.interfaces;


public interface Uploads {

    String T_NAME="uploads";

    String ID = "id";
    String R_ID = "remoteid";
    String U_TYPE="uploadable_type";
    String U_ID="uploadable_id";
    String CATEGORY="category";
    String DESC="description";
    String CREATOR_ID="creator_id";
    String SUB="subcategory";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
