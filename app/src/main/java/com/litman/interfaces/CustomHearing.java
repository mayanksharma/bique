package com.litman.interfaces;


public interface CustomHearing {

    String T_NAME="custom_hearings";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String ON_DATE="on_date";
    String JUDGE_NAME="judge_name";
    String ADDED_ON="added_on";
    String COURT_ROOM_NO="court_room_no";
    String ITEM_NO="item_no";
    String HEARING_ID="hearing_id";
    String CREATOR_ID = "creator_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID = "admin_id";
    String ATTENDEE_ID = "attendee_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
