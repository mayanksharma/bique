package com.litman.interfaces;


public interface MatterUsers {

    String T_NAME="matter_users";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String USER_ID="user_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String MY_CLIENT="my_client";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
