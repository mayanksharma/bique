package com.litman.interfaces;



public interface NotificationsStatus {

    String T_NAME="notification_status";

    String ID = "id";
    String READ = "read";
    String MEMBER_ID="member_id";
    String NOTI_ID="notification_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
