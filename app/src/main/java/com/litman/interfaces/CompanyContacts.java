package com.litman.interfaces;


public interface CompanyContacts {

    String T_NAME="company_contacts";

    String ID = "id";
    String R_ID = "remoteid";
    String NAME="name";
    String EMAIL="email";
    String TELEPHONE="telephone";
    String WEBSITE="website";
    String ADDRESS1="address1";
    String ADDRESS2="address2";
    String CITY="city";
    String ZIP="zip";
    String STATE="state";
    String COUNTRY="country";
    String CREATOR_ID="creator_id";
    String LOGO="logourl";
    String THUMB="smallurl";
    String IS_CLIENT="is_client";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
