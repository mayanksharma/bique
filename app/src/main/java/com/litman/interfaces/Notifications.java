package com.litman.interfaces;



public interface Notifications {

    String T_NAME="notifications";

    String ID = "id";
    String R_ID = "remoteid";
    String CREATOR_ID = "creator_id";
    String NOTICE_TYPE = "notice_type";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String LINK = "link";
    String TEXT = "notification_text";
    String NOTI_TYPE="notificationable_type";
    String NOTI_ID="notificationable_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
}
