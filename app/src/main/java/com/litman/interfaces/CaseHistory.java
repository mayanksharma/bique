package com.litman.interfaces;



public interface CaseHistory {

    String T_NAME="case_history";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matterId";
    String SNO="serialNumber";
    String CASE_NO = "caseNumber";
    String CASE_LINK = "caseLink";
    String DETAILS="details";
    String STATUS="status";
    String ORDER_DATE="orderdt";

}
