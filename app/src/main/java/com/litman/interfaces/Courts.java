package com.litman.interfaces;



public interface Courts {

    String T_NAME="courts";

    String ID = "id";
    String R_ID = "remoteid";
    String NAME = "name";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
