package com.litman.interfaces;


public interface Tasks {

    String T_NAME="tasks";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String USER_ID="user_id";
    String COMPLETED="completed";
    String TITLE="title";
    String DUE_AT="due_at";
    String ASSIGNED_TO="assigned_to";
    String CATEGORY="category";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
