package com.litman.interfaces;



public interface CaseType {

    String T_NAME="case_types";

    String ID = "id";
    String R_ID = "remoteid";
    String TYPE="case_type";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String COURT_ID="court_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
