package com.litman.interfaces;


public interface Comments {

    String T_NAME="comments";

    String ID = "id";
    String R_ID = "remoteid";
    String TEXT="text";
    String CREATOR_ID = "creator_id";
    String COMMENTABLE_ID="commentable_id";
    String COMMENTABLE_TYPE="commentable_type";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
