package com.litman.interfaces;


public interface Matters {

    String T_NAME="matters";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_NO="matter_number";
    String P_ATT="plaintiff_attorney";
    String D_ATT="diffendent_attorney";
    String I_STATUS="internal_status";
    String C_STATUS="court_status";
    String PLAINTIFF="plaintiff";
    String DEFFENDANT="defendant";
    String CLIENT_TYPE="client_type";
    String CASE_TYPE="case_type";
    String CASE_YEAR="case_year";
    String COURT_ID="court_id";
    String CREATOR_ID="creator_id";
    String CASE_TYPE_ID="case_type_id";
    String API_MATTER_ID="api_matter_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
