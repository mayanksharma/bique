package com.litman.interfaces;



public interface Post {

    String T_NAME="posts";

    String ID = "id";
    String R_ID = "remoteid";
    String TEXT = "text";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String POSTABLE_ID = "postable_id";
    String POSTABLE_TYPE = "postable_type";
    String CREATOR_ID = "creator_id";
    String ADMIN_ID="admin_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
}
