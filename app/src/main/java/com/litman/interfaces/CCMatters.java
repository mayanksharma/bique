package com.litman.interfaces;


public interface CCMatters {

    String T_NAME="company_contacts_matters";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String CC_ID="company_contact_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
