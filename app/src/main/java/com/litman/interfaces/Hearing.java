package com.litman.interfaces;


public interface Hearing {

    String T_NAME="hearings";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String ON_DATE="on_date";
    String JUDGE_NAME="judge_name";
    String ADDED_ON="added_on";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String COURT_ROOM_NO="court_room_no";
    String ITEM_NO="item_no";
    String HEARING_ID="api_hearing_id";
    String CREATED_ID = "created_by";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";

}
