package com.litman.interfaces;



public interface Orders {

    String T_NAME="orders";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String DESC = "description";
    String DATE = "date";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String LINK = "link";
    String API_ORDER_ID = "api_order_id";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
}
