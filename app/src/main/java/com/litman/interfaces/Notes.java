package com.litman.interfaces;


public interface Notes {

    String T_NAME = "notes";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID = "matter_id";
    String USER_ID = "user_id";
    String DESCRIPTION = "description";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String ADMIN_ID = "admin_id";
    String UPDATED = "updated";
    String INSERTED = "inserted";
    String DELETED = "deleted";
    String KEY = "key_name";
    String TEAM = "teams";
    String CONTACTS = "contacts";
    String EMAILS = "emails";

}
