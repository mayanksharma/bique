package com.litman.interfaces;


public interface MPContacts {

    String T_NAME="matters_person_contacts";

    String ID = "id";
    String R_ID = "remoteid";
    String MATTER_ID="matter_id";
    String PC_ID="person_contact_id";
    String CREATED_AT = "created_at";
    String UPDATED_AT = "updated_at";
    String UPDATED="updated";
    String INSERTED="inserted";
    String DELETED="deleted";
    String KEY="key_name";
    

}
