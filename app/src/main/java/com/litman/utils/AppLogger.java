package com.litman.utils;

import android.util.Log;

/**
 * Created by Tushar Agarwal on 16-Mar-17.
 */

public class AppLogger {

    public static void logD(String TAG, String message){
        if(AppUtils.IS_DEBUG){
            Log.d(TAG, message);
        }
    }

    public static void logE(String TAG, String message){
        if(AppUtils.IS_DEBUG){
            Log.e(TAG, message);
        }
    }

    public static void logE(String TAG, String message, Exception e){
        if(AppUtils.IS_DEBUG && e != null){
            Log.e(TAG, message + e.getMessage());
            e.printStackTrace();
        }
    }
}
