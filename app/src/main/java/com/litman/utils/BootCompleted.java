package com.litman.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mayank on 04/04/17.
 */

public class BootCompleted extends BroadcastReceiver {

    private Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            new Alarm().setAlarm(context);
        }
    }

}
