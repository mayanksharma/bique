package com.litman.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;


public class AppUtils {

    public static AlertDialog d;
    public static boolean IS_DEBUG = false;
    private static ProgressDialog progressDialog;

    private final static String PATH = File.pathSeparator;

    public static final String BIQE_DIR = "data" + PATH + "data" + PATH + "com.biqe" + PATH + "databases" + PATH;

    public static boolean isNotEmpty(EditText etText) {
        return !(etText.getText().toString() == null
                || etText.getText().toString().equals("null") ||
                (etText.getText().toString().trim().length() == 0));
    }

    public static boolean isNotEmpty(String str) {
        return !((str == null || str.equalsIgnoreCase("null")
                || str.trim().length() == 0));
    }

    public static boolean checkInternetConnection(Context ctx) {
        ConnectivityManager mManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mManager.getActiveNetworkInfo();
        if ((mNetworkInfo != null) && (mNetworkInfo.isConnected())) {
            return true;
        }
        return false;
    }

    // display a no Internet toast if no connectivity is available
    public static void noInternetDialog(Context ctx) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setTitle("Please");
        dialog.setMessage("Check you're online or try again later.");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // dialog.show();
        d = dialog.create();
        d.show();
    }// noInternetDialog


    // display a message dialog with custom message
    public static void openUtilityDialog(final Context ctx,
                                         final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // dialog.show();
        d = dialog.create();
        d.show();
    }

    public static void sendEmail(Context ctx, String email) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lit Man");
        i.putExtra(android.content.Intent.EXTRA_TEXT, "");
        ctx.startActivity(Intent.createChooser(i, "Send email"));
    }

    public static void dialNo(Context ctx, String num) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(num.trim())));
            ctx.startActivity(callIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showMessage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static void showError(Context ctx, String msg) {
        Toast.makeText(ctx, "Parsing error:" + msg, Toast.LENGTH_LONG).show();
    }


    public static boolean isValidCursor(Cursor cursor) {
        if (cursor != null && cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static void showProgressBar(Context context) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } else {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }

        }

    }

    public static void hideProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
