package com.litman.utils;

import android.util.Patterns;

import java.util.regex.Pattern;


/**
 * Created by Tushar Agarwal on 08-Apr-17.
 */

public class StringUtils {
    public static boolean isValidString(String val){
        if(val!=null && !val.isEmpty()){
            return true;
        }
        return false;
    }

    public static boolean isValidEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if(pattern.matcher(email).matches()){
            return true;
        }
        return false;
    }

    public static boolean isValidWebAddress(String webAddress){
        Pattern pattern = Patterns.WEB_URL;
        if(pattern.matcher(webAddress).matches()){
            return true;
        }
        return false;
    }

    public static boolean isValidPhoneNumber(String phone_no){
        Pattern pattern = Patterns.PHONE;
        if(pattern.matcher(phone_no).matches()){
            return true;
        }
        return false;
    }
}
