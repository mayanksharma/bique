package com.litman;

import android.app.Application;
import android.content.Context;

import com.litman.db.DBHelper;
import com.litman.db.DbManager;


public class MyApplication extends Application {


    private static MyApplication mInstance;
    private static Context mCtx;

    public MyApplication(){
        super();
    }

    private MyApplication(Context ctx) {
        mCtx=ctx;
    }

    public static synchronized MyApplication getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyApplication(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DbManager.initializeInstance(new DBHelper(getApplicationContext()),
                getApplicationContext());
    }
}
