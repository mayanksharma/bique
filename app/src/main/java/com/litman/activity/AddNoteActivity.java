package com.litman.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.litman.R;
import com.litman.dao.MatterInfo;
import com.litman.db.DbManager;
import com.litman.interfaces.Notes;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;

public class AddNoteActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtTitle, edtEmail;
    private TextView txtTeam, txtContact;
    private MatterInfo info;
    private String teams = "", contacts = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        info = (MatterInfo) getIntent().getSerializableExtra("INFO");
        setUpView();
    }

    private void setUpView() {
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        edtEmail = (EditText) findViewById(R.id.edtEmails);

        txtTeam = (TextView) findViewById(R.id.txtTeam);
        txtContact = (TextView) findViewById(R.id.txtContacts);

        (findViewById(R.id.btnAdd)).setOnClickListener(this);

        txtTeam.setOnClickListener(this);
        txtContact.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                saveData();
                break;
            case R.id.txtTeam:
                startActivityForResult(new Intent(this, SelectUserActivity.class).putExtra("M_ID", info.ID)
                        .putExtra("ID", teams).putExtra("TYPE", 1), 1);
                break;
            case R.id.txtContacts:
                startActivityForResult(new Intent(this, SelectUserActivity.class).putExtra("M_ID", info.ID)
                        .putExtra("ID", contacts).putExtra("TYPE", 2), 2);
                break;
        }
    }

    private void saveData() {
        if (valid()) {
            ContentValues cv = new ContentValues();
            cv.put(Notes.R_ID, CacheManager.INSTANCE.getMaxId(Notes.T_NAME));
            cv.put(Notes.MATTER_ID, info.ID);
            cv.put(Notes.USER_ID, AppPreferences.getUserId(this));
            cv.put(Notes.DESCRIPTION, edtTitle.getText().toString());
            cv.put(Notes.INSERTED, 1);
            cv.put(Notes.ADMIN_ID, AppPreferences.getadminId(this));
            cv.put(Notes.KEY, AppPreferences.getKey(this));
            cv.put(Notes.CREATED_AT, CacheManager.INSTANCE.getDate());
            cv.put(Notes.UPDATED_AT, CacheManager.INSTANCE.getDate());
            cv.put(Notes.EMAILS, edtEmail.getText().toString().trim());
            cv.put(Notes.TEAM, teams);
            cv.put(Notes.CONTACTS, contacts);
            DbManager.getInstance().openDatabase();
            long id = DbManager.getInstance().insertAndGet(Notes.T_NAME, cv);
            DbManager.getInstance().closeDatabase();
            if (id == -1) {
                Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
            } else {
                openUtilityDialog(this, "Note added successfully");
            }
        }
    }

    private boolean valid() {
        if (edtTitle.getText().toString().trim().length() <= 0) {
            Toast.makeText(this, "Please enter Note text", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data.getExtras() != null) {
                teams = data.getStringExtra("ID");
                getTeamName();
            }
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data.getExtras() != null) {
                contacts = data.getStringExtra("ID");
                getContactName();
            }
        }
    }

    private void getTeamName() {
        if (teams.length() > 0) {
            String name = "";
            Cursor curs = CacheManager.INSTANCE.getNonMatterUserName(teams);
            while (curs != null && curs.moveToNext()) {
                name += curs.getString(curs.getColumnIndex("name"));
                name += ", ";
            }
            if (curs != null) {
                curs.close();
            }
            name=name.trim();
            if (name.length() > 1)
                name = name.substring(0, name.length() - 1);
            txtTeam.setText(name);
        } else {
            txtTeam.setText("");
        }
    }

    private void getContactName() {
        if (contacts.length() > 0) {
            String name = "";
            Cursor curs = CacheManager.INSTANCE.getNonMatterContactsName(contacts);
            while (curs != null && curs.moveToNext()) {
                name += curs.getString(curs.getColumnIndex("name"));
                name += ", ";
            }
            if (curs != null) {
                curs.close();
            }
            name=name.trim();
            if (name.length() > 1)
                name = name.substring(0, name.length() - 1);
            txtContact.setText(name);
        } else {
            txtContact.setText("");
        }
    }
}
