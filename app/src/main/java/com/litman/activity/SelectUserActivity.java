package com.litman.activity;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.MultiUserListAdapter;
import com.litman.dao.CourtInfo;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

public class SelectUserActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private ArrayList<CourtInfo> info = new ArrayList<>();
    private HashSet<Integer> selectedId;

    private Cursor curs;

    private String userId;
    private int type, m_id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_user_layout);
        userId = getIntent().getStringExtra("ID");
        type = getIntent().getIntExtra("TYPE", 0);
        m_id = getIntent().getIntExtra("M_ID", 0);
        setupView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.setGroupVisible(R.id.grp_search, false);
        menu.setGroupVisible(R.id.grp_save, true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                Intent in = new Intent();
                in.putExtra("ID", getSelectedIds());
                setResult(Activity.RESULT_OK, in);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupView() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        if (type == 1) {
            txtTitle.setText("Select Team Members");
        } else {
            txtTitle.setText("Select Contacts");
        }
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getIds(userId);

        getData();
    }

    private void getIds(String courtId) {
        StringTokenizer token = new StringTokenizer(courtId, ",");
        selectedId = new HashSet<>();
        while (token.hasMoreElements()) {
            selectedId.add(Integer.valueOf(token.nextToken()));
        }
    }

    private void getData() {
        Cursor curs = null;
        if (type == 1) {
            curs = CacheManager.INSTANCE.getNonMatterUser(m_id);
        } else {
            curs = CacheManager.INSTANCE.getNonMatterContacts(m_id);
        }
        info.clear();

        while (curs != null && curs.moveToNext()) {
            CourtInfo cInfo = new CourtInfo();
            cInfo.ID = curs.getInt(curs.getColumnIndex(Courts.R_ID));
            cInfo.NAME = curs.getString(curs.getColumnIndex(Courts.NAME));
            cInfo.URL = curs.getString(curs.getColumnIndex("avatarthumburl"));
            cInfo.SELECTED = selectedId.contains(cInfo.ID);
            info.add(cInfo);
        }
        if (curs != null) {
            curs.close();
        }

        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new MultiUserListAdapter(this, info, selectedId);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);
    }

    public String getSelectedIds() {
        String str = "";
        if (mAdapter != null) {
            str = ((MultiUserListAdapter) mAdapter).getAllSelected();
        }
        return str;
    }

}
