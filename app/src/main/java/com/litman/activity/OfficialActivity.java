package com.litman.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.litman.R;
import com.litman.dao.MatterInfo;
import com.litman.fragments.MatterOfficialAboutFragment;
import com.litman.fragments.MattersTimeLineFragment;
import com.litman.interfaces.Matters;


public class OfficialActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager pager;
    private PagerAdapter adapter;
    private Spinner spnSelect;
    private MatterInfo info;

    private static final String[] tabs = {"About", "Time Line"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_official);
        info = (MatterInfo) getIntent().getSerializableExtra(Matters.T_NAME);

        setUpView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.setGroupVisible(R.id.grp_main, false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showAlert("Add in Matter");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpView() {

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        pager = (ViewPager) findViewById(R.id.pager);

        spnSelect = (Spinner) findViewById(R.id.spnSelect);

        adapter = new PagerAdapter(getSupportFragmentManager(), info);
        pager.setAdapter(adapter);

        pager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    spnSelect.setVisibility(View.GONE);
                } else {
                    spnSelect.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        spnSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((MattersTimeLineFragment) adapter.getRegisteredFragment(1)).setData(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
        FragmentManager mFragmentManager;
        private MatterInfo info;

        public PagerAdapter(FragmentManager fm, MatterInfo info) {
            super(fm);
            mFragmentManager = fm;
            this.info = info;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return MatterOfficialAboutFragment.newInstance(info);
            } else if (position == 1) {
                return MattersTimeLineFragment.newInstance(info);
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }


    public void showAlert(String msg) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.add_official_dialog, null);
        final AlertDialog d = new AlertDialog.Builder(this)
                .setMessage(msg)
                .setView(view)
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        (view.findViewById(R.id.txtNote)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(OfficialActivity.this, AddNoteActivity.class).putExtra("INFO", info), 1);
                d.dismiss();
            }
        });
        (view.findViewById(R.id.txtTask)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OfficialActivity.this, AddTaskActivity.class).putExtra("M_ID",info.ID));
                d.dismiss();
            }
        });

    }

}
