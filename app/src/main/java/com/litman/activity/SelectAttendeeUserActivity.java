package com.litman.activity;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.SingleUserListAdapter;
import com.litman.dao.AttendeeModel;
import com.litman.dao.CourtInfo;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;

import java.util.ArrayList;

public class SelectAttendeeUserActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private ArrayList<CourtInfo> info = new ArrayList<>();
    private ArrayList<AttendeeModel> dataList = new ArrayList<>();

    private int m_id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_user_layout);
        m_id = getIntent().getIntExtra("M_ID", 0);
        setupView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.setGroupVisible(R.id.grp_search, false);
        menu.setGroupVisible(R.id.grp_save, true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                String selectedId=getSelectedIds();
                AttendeeModel mod=new AttendeeModel();
                for(AttendeeModel model:dataList){
                    if(model.getId()==Integer.parseInt(selectedId)){
                        mod.setId(model.getId());
                        mod.setName(model.getName());
                        break;
                    }
                }

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", mod);
                intent.putExtras(bundle);
                setResult(Activity.RESULT_OK, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupView() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);

        txtTitle.setText("Select Attendee");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getData();
    }

    private void getData() {
        Cursor curs = null;
        curs = CacheManager.INSTANCE.getMatterUsers(m_id + "");
        info.clear();
        dataList.clear();
        while (curs != null && curs.moveToNext()) {
            CourtInfo cInfo = new CourtInfo();
            cInfo.ID = curs.getInt(curs.getColumnIndex(Courts.R_ID));
            cInfo.NAME = curs.getString(curs.getColumnIndex(Courts.NAME));
            cInfo.URL = curs.getString(curs.getColumnIndex("avatarthumburl"));
            cInfo.SELECTED = false;

            AttendeeModel attendeeModel = new AttendeeModel();
            attendeeModel.setName(curs.getString(curs.getColumnIndex("name")));
            attendeeModel.setId(curs.getInt(curs.getColumnIndex("remoteid")));
            dataList.add(attendeeModel);

            info.add(cInfo);
        }
        if (curs != null) {
            curs.close();
        }

        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new SingleUserListAdapter(this, info);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);
    }

    public String getSelectedIds() {
        String str = "";
        if (mAdapter != null) {
            str = ((SingleUserListAdapter) mAdapter).getAllSelected();
        }
        return str;
    }

}
