package com.litman.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.MatterActivityCursorAdapter;
import com.litman.dao.MatterInfo;
import com.litman.interfaces.Matters;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;


public class MatterActivityActivity extends BaseActivity {


    private Spinner spnSelect, spnSubSelect;
    private MatterInfo info;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Cursor curs;

    String sub1[] = {"All", "Orders", "Listing"};
    String sub2[] = {"All", "My Post"};
    String sub3[] = {"All", "My Notes"};
    String sub4[] = {"All", "Court File", "Evidence", "NOA", "Miscellaneous", "New Category II"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        info = (MatterInfo) getIntent().getSerializableExtra(Matters.T_NAME);

        setUpView();
    }


    private void setSubSpinner(int i) {
        spnSubSelect.setVisibility(View.VISIBLE);

        ArrayAdapter<String> spinnerArrayAdapter = null;
        if (i == 1) {
            spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sub1);
        } else if (i == 2) {
            spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sub2);
        } else if (i == 3) {
            spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sub3);
        } else if (i == 4) {
            spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sub4);
        }
        if (spinnerArrayAdapter != null) {
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spnSubSelect.setAdapter(spinnerArrayAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (CacheManager.INSTANCE.isAddAllowed(this)) {
            menu.setGroupVisible(R.id.grp_main, true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showAlert("Add in Matter");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpView() {
        ((TextView) findViewById(R.id.txtName)).setText(info.PLAINTIFF + " Vs " + info.DEFENDANT);
        ((TextView) findViewById(R.id.txtCourt)).setText(info.COURT_NAME + ", Matter No: " + info.MATTER_NUMBER);

        spnSelect = (Spinner) findViewById(R.id.spnSelect);
        spnSubSelect = (Spinner) findViewById(R.id.spnSubSelect);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            mAdapter = new MatterActivityCursorAdapter(this, curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        spnSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    setData();
                    spnSubSelect.setVisibility(View.GONE);
                } else {
                    setSubSpinner(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spnSubSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setData() {
        if (spnSelect.getSelectedItemPosition() == 0) {
            curs = CacheManager.INSTANCE.getAllMatterActivity(info.ID);
        } else {
            curs = CacheManager.INSTANCE.getAllMatterActivity(info.ID, spnSelect.getSelectedItemPosition(),
                    spnSubSelect.getSelectedItemPosition(), AppPreferences.getUserId(MatterActivityActivity.this));
        }
        if (mAdapter != null)
            ((MatterActivityCursorAdapter) mAdapter).changeCursor(curs);
    }

    public void showAlert(String msg) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.add_dialog, null);
        final AlertDialog d = new AlertDialog.Builder(this)
                .setMessage(msg)
                .setView(view)
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        (view.findViewById(R.id.txtPost)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MatterActivityActivity.this, AddPostActivity.class)
                        .putExtra("ID", info.ID).putExtra("TYPE", "Matter"), 1);
                d.dismiss();
            }
        });
        (view.findViewById(R.id.txtNote)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MatterActivityActivity.this, AddNoteActivity.class).putExtra("INFO", info), 1);
                d.dismiss();
            }
        });
        (view.findViewById(R.id.txtFile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MatterActivityActivity.this, AddFileActivity.class).putExtra("ID",info.ID));
                d.dismiss();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setData();
    }
}
