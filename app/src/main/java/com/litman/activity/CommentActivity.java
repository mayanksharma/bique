package com.litman.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.litman.R;
import com.litman.adapters.CommentCursorAdapter;
import com.litman.dao.HomeFeedInfo;
import com.litman.db.DbManager;
import com.litman.interfaces.Comments;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Locale;
import java.util.StringTokenizer;

import static com.litman.interfaces.Const.TYPE_CUSTOM_HEARING;
import static com.litman.interfaces.Const.TYPE_HEARING;
import static com.litman.interfaces.Const.TYPE_ORDER;
import static com.litman.interfaces.Const.TYPE_POST;

public class CommentActivity extends BaseActivity {

    private TextView txtDesc, txtImg, txtPost;
    private ImageView imgUser;
    private EditText edtComment;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private Cursor curs;


    private HomeFeedInfo info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_comment);
        info = (HomeFeedInfo) getIntent().getSerializableExtra("DAO");
        setupView();
    }


    private void setupView() {
        txtDesc = (TextView) findViewById(R.id.txtDesc);
        txtImg = (TextView) findViewById(R.id.txtImg);
        imgUser = (ImageView) findViewById(R.id.imgUser);

        txtPost = (TextView) findViewById(R.id.txtPost);
        edtComment = (EditText) findViewById(R.id.edtComment);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        //mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            mAdapter = new CommentCursorAdapter(this, curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        txtPost.setEnabled(false);

        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    txtPost.setEnabled(true);
                } else {
                    txtPost.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        txtPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtComment.getText().toString().trim().length() > 0) {
                    DbManager.getInstance().openDatabase();
                    ContentValues cv = new ContentValues();
                    cv.put(Comments.R_ID, CacheManager.INSTANCE.getMaxId(Comments.T_NAME));
                    cv.put(Comments.TEXT, edtComment.getText().toString().trim());
                    cv.put(Comments.CREATOR_ID, AppPreferences.getUserId(CommentActivity.this));
                    cv.put(Comments.COMMENTABLE_ID, info.ID);
                    cv.put(Comments.COMMENTABLE_TYPE, getCommentType(info.TYPE));
                    cv.put(Comments.CREATED_AT, CacheManager.INSTANCE.getDate());
                    cv.put(Comments.UPDATED_AT, CacheManager.INSTANCE.getDate());
                    cv.put(Comments.INSERTED, 1);
                    cv.put(Comments.ADMIN_ID, AppPreferences.getadminId(CommentActivity.this));
                    cv.put(Comments.KEY, AppPreferences.getKey(CommentActivity.this));

                    DbManager.getInstance().insert(Comments.T_NAME, cv);
                    DbManager.getInstance().closeDatabase();
                    edtComment.setText("");
                    CacheManager.INSTANCE.hideKeyboard(edtComment, CommentActivity.this);
                    setData();
                } else {
                    Toast.makeText(CommentActivity.this, "Please write your comment", Toast.LENGTH_SHORT).show();
                }
            }
        });

        int type = info.TYPE;

        switch (type) {
            case TYPE_CUSTOM_HEARING:
                configureViewHolder4(info);
                break;
            case TYPE_HEARING:
                configureViewHolder3(info);
                break;
            case TYPE_ORDER:
                configureViewHolder1(info);
                break;
            case TYPE_POST:
                configureViewHolder2(info);
                break;
            case 6:
                configureViewHolder6(info);
                break;
        }

        setData();
    }

    private String getCommentType(int typeId) {
        String type = "";
        switch (typeId) {
            case 1:
                type = "Order";
                break;
            case 2:
                type = "Post";
                break;
            case 3:
                type = "Hearing";
                break;
            case 4:
                type = "CustomHearing";
                break;
            case 6:
                type = "Note";
                break;
        }
        return type;
    }

    private void setData() {
        curs = CacheManager.INSTANCE.getComment(info.ID, info.TYPE);
        if (mAdapter != null)
            ((CommentCursorAdapter) mAdapter).changeCursor(curs);
    }

    private void configureViewHolder6(HomeFeedInfo info) {
        txtImg.setBackground(ContextCompat.getDrawable(this, R.drawable.grey_round));
        imgUser.setVisibility(View.GONE);
        txtImg.setVisibility(View.VISIBLE);
        txtImg.setText(getImageName(info));

        Picasso.with(this).load(info.THUMB).transform(new CircleTransform()).into(imgUser, new Callback() {
            @Override
            public void onSuccess() {
                txtImg.setVisibility(View.GONE);
                imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        txtDesc.setText(getNotes(info));
    }

    private Spannable getNotes(final HomeFeedInfo info) {
        String name = getCapitalizeFirstLetterFullName(info);
        String text = info.TEXT;

        String description = name + "\n" + text;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)),
                    description.indexOf(text), description.indexOf(text) + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CommentActivity.this, ProfileOtherActivity.class).putExtra("ID", info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return descriptionSpanned;
    }


    private void configureViewHolder4(HomeFeedInfo info) {
        txtImg.setBackground(ContextCompat.getDrawable(this, R.drawable.grey_round));
        imgUser.setVisibility(View.GONE);
        txtImg.setVisibility(View.VISIBLE);
        txtImg.setText(getImageName(info));

        Picasso.with(this).load(info.THUMB).transform(new CircleTransform()).into(imgUser, new Callback() {
            @Override
            public void onSuccess() {
                txtImg.setVisibility(View.GONE);
                imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        txtDesc.setText(getDescriptionCustomHearing(info));
    }

    private void configureViewHolder2(HomeFeedInfo info) {
        txtImg.setBackground(ContextCompat.getDrawable(this, R.drawable.grey_round));
        imgUser.setVisibility(View.GONE);
        txtImg.setVisibility(View.VISIBLE);
        txtImg.setText(getImageName(info));

        Picasso.with(this).load(info.THUMB).transform(new CircleTransform()).into(imgUser, new Callback() {
            @Override
            public void onSuccess() {
                txtImg.setVisibility(View.GONE);
                imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        txtDesc.setText(getDescriptionPost(info));
    }

    private void configureViewHolder3(HomeFeedInfo info) {
        imgUser.setVisibility(View.GONE);
        txtImg.setVisibility(View.VISIBLE);
        txtImg.setBackground(ContextCompat.getDrawable(this, R.drawable.green_round));
        txtImg.setText(getCapitalizeName(info.COURT_NAME));
        //txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        txtDesc.setText(getDescriptionHearing(info));
    }

    private void configureViewHolder1(HomeFeedInfo info) {
        imgUser.setVisibility(View.GONE);
        txtImg.setVisibility(View.VISIBLE);
        txtImg.setBackground(ContextCompat.getDrawable(this, R.drawable.green_round));
        txtImg.setText(getCapitalizeName(info.COURT_NAME));
        //txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        txtDesc.setText(getDescriptionOrder(info));
    }


    private String getCapitalizeName(String name) {
        StringBuilder builder = new StringBuilder();
        try {
            StringTokenizer token = new StringTokenizer(name, " ");
            while (token.hasMoreElements()) {
                builder.append(token.nextElement().toString().substring(0, 1).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private String getImageName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();
        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private Spannable getDescriptionOrder(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String newOrder = "New Order";
        String releasedOn = " released on ";
        String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);

        String description = courtName + "\n" + vsString + "\n" + newOrder + releasedOn + on + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            if (!AppUtils.isNotEmpty(description)) {
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.grey)), description.indexOf(newOrder),
                    description.indexOf(newOrder) + newOrder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    openLinksToView(info.LINK);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.grey));
                }
            }, description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(releasedOn),
                    description.indexOf(releasedOn) + releasedOn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
//                    FragmentManager manager = this.getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.MATTER_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.black));
                }
            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.grey)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(on),
                    description.indexOf(on) + on.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionHearing(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String listedIn = "is listed in court ";
        String court = "" + info.COURT_ROOM_NO;
        String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String judgeName = " (" + info.JUDGE_NAME + ")";
        String item = " As Item No " + info.ITEM_NO;
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);
        String sDesc = listedIn + court + judgeName + item + on;
        String description = courtName + "\n" + vsString + "\n" + listedIn + court + judgeName + item + on + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {

            if (!AppUtils.isNotEmpty(description)) {
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan bss = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
//                    FragmentManager manager = this.getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.MATTER_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.black));
                }
            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(sDesc), description.indexOf(sDesc) + sDesc.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan iss = new StyleSpan(Typeface.ITALIC);
            descriptionSpanned.setSpan(iss, description.indexOf(judgeName), description.indexOf(judgeName) + judgeName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.grey)), description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionPost(final HomeFeedInfo info) {
        String userName = getCapitalizeFirstLetterFullName(info);
        String posted = "Posted - " + info.TEXT + " on ";
        String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);

        String description = userName + "\n" + vsString + "\n" + posted + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {

            if (!AppUtils.isNotEmpty(description)) {
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), description.indexOf(userName),
                    description.indexOf(userName) + userName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CommentActivity.this, ProfileOtherActivity.class).putExtra("ID", info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.colorAccent));
                }
            }, description.indexOf(userName), description.indexOf(userName) + userName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(posted),
                    description.indexOf(posted) + posted.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
//                    FragmentManager manager = this.getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.MATTER_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.black));
                }
            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionCustomHearing(final HomeFeedInfo info) {

        String userName = getCapitalizeFirstLetterFullName(info);
        String addedText = "Added a new manual hearing ";
        String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String t_for = " for " + CacheManager.INSTANCE.getFormattedDate(info.D_DATE) + " (" + info.JUDGE_NAME + ")" + " to be attended by";
        String name = " " + userName;

        String description = userName + "\n" + vsString + "\n" + addedText + t_for + name;

        Spannable descriptionSpanned = new SpannableString(description);
        try {

            if (!AppUtils.isNotEmpty(description)) {
                return new SpannableString("");
            }

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CommentActivity.this, ProfileOtherActivity.class).putExtra("ID", info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.colorAccent));
                }
            }, description.indexOf(userName), description.indexOf(userName) + userName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(CommentActivity.this, ProfileOtherActivity.class).putExtra("ID", info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.colorAccent));
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(addedText),
                    description.indexOf(addedText) + addedText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
//                    FragmentManager manager = this.getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.MATTER_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(CommentActivity.this, R.color.black));
                }
            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.black)), description.indexOf(t_for),
                    description.indexOf(t_for) + t_for.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private String getCapitalizeFirstLetterFullName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();

        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()) + data[0].substring(1).toLowerCase());
            }
            if (data.length > 1) {
                builder.append(" ");
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()) + data[data.length - 1].substring(1).toLowerCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private void openLinksToView(String link) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            if (browserIntent.resolveActivity(this.getPackageManager()) != null) {
                this.startActivity(browserIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        finish();
    }
}
