package com.litman.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.litman.R;
import com.litman.interfaces.Const;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class AddMatterActivity extends BaseActivity {

    private WebView web;
    private int id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_profile);
        id = getIntent().getIntExtra("ID", 0);
        setUpView();
    }

    private void setUpView() {
        web = (WebView) findViewById(R.id.web);
        (findViewById(R.id.txtTitle)).setVisibility(View.GONE);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);

        if (AppUtils.checkInternetConnection(this)) {
            initializeWebView();
            web.loadUrl(Const.ADD_NEW_MATTER + AppPreferences.getToken(this));
        } else {
            AppUtils.noInternetDialog(this);
        }

        hideKeyboard(web);
    }

    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        web.clearCache(true);

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);

                if (url.startsWith(Const.SUCCESS)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    openUtilityDialog(AddMatterActivity.this, value);
                    return true;
                } else if (url.startsWith(Const.FAIL)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    AppUtils.openUtilityDialog(AddMatterActivity.this, value);
                    view.loadUrl(Const.ADD_NEW_MATTER + AppPreferences.getToken(AddMatterActivity.this));
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });

    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((AddMatterActivity) ctx).finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}
