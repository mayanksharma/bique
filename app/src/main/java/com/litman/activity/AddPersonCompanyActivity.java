package com.litman.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.litman.R;
import com.litman.adapters.SpinnerNormalAdapter;
import com.litman.db.DbManager;
import com.litman.interfaces.CompanyContacts;
import com.litman.interfaces.PersonContacts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;
import com.litman.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

public class AddPersonCompanyActivity extends BaseActivity {

    private final String TAG = AddPersonCompanyActivity.class.getSimpleName();

    private Context context;
    private RadioGroup grp_person_comp;
    private RadioButton person, company;
    private EditText f_name, l_name, email_id, phone_no, address_1, address_2, city, zip_code, state, company_name, company_website, p_company_name;
    private Spinner title_person, country, type;
    private LinearLayout name_lay, name_lay_txt, _person_title, _company_title, title_lay, website_lay, p_company_lay;
    private CheckBox person_is_client, company_is_client;
    private Button add_btn;
    private boolean isCompany = false;
    private HashMap<String, String> map = new HashMap<>();
    private String fname = "", companyName = "", lname = "", email = "", number = "", citytxt = "", countrytxt = "", street = "";
    private int fragmentType = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView(R.layout.add_person_company);


        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getString("fname") != null) {
                fname = getIntent().getExtras().getString("fname");
            }

            if (getIntent().getExtras().getString("lname") != null) {
                lname = getIntent().getExtras().getString("lname");
            }

            if (getIntent().getExtras().getString("email") != null) {
                email = getIntent().getExtras().getString("email");
            }


            if (getIntent().getExtras().getString("phoneNumber") != null) {
                number = getIntent().getExtras().getString("phoneNumber");
            }

            if (getIntent().getExtras().getString("city") != null) {
                citytxt = getIntent().getExtras().getString("city");
            }

            if (getIntent().getExtras().getString("country") != null) {
                countrytxt = getIntent().getExtras().getString("country");
            }

            if (getIntent().getExtras().getString("street") != null) {
                street = getIntent().getExtras().getString("street");
            }


            if (getIntent().getExtras().getString("companyName") != null) {
                companyName = getIntent().getExtras().getString("companyName");
            }


            fragmentType = getIntent().getExtras().getInt("fragmentType");


        }


        setUpView();
    }

    private void setUpView() {
        grp_person_comp = (RadioGroup) findViewById(R.id.grp_person_comp);
        f_name = (EditText) findViewById(R.id.f_name);
        l_name = (EditText) findViewById(R.id.l_name);
        email_id = (EditText) findViewById(R.id.email_id);
        phone_no = (EditText) findViewById(R.id.phone_no);
        address_1 = (EditText) findViewById(R.id.address_1);
        address_2 = (EditText) findViewById(R.id.address_2);
        p_company_name = (EditText) findViewById(R.id.p_company_name);
        city = (EditText) findViewById(R.id.city);
        zip_code = (EditText) findViewById(R.id.zip_code);
        state = (EditText) findViewById(R.id.state);
        company_name = (EditText) findViewById(R.id.company_name);
        company_website = (EditText) findViewById(R.id.company_website);

        title_person = (Spinner) findViewById(R.id.title_person);
        country = (Spinner) findViewById(R.id.country);
        type = (Spinner) findViewById(R.id.type);

        loadAllCountries(country);
        loadValuesFromStringFile(title_person, R.array.title_person);
        loadValuesFromStringFile(type, R.array.type_person);

        person_is_client = (CheckBox) findViewById(R.id.person_is_client);
        company_is_client = (CheckBox) findViewById(R.id.company_is_client);

        name_lay = (LinearLayout) findViewById(R.id.name_lay);
        name_lay_txt = (LinearLayout) findViewById(R.id.name_lay_txt);
        _person_title = (LinearLayout) findViewById(R.id._person_title);
        _company_title = (LinearLayout) findViewById(R.id._company_title);
        title_lay = (LinearLayout) findViewById(R.id.title_lay);
        website_lay = (LinearLayout) findViewById(R.id.website_lay);
        p_company_lay = (LinearLayout) findViewById(R.id.p_company_lay);

        add_btn = (Button) findViewById(R.id.add_btn);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCompany) {
                    addCompany();
                } else {
                    addPerson();
                }

            }
        });


        person = (RadioButton) findViewById(R.id.person);
        company = (RadioButton) findViewById(R.id.company);


        grp_person_comp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.person) {
                    setUpPersonView();
                } else if (checkedId == R.id.company) {
                    setUpCompanyView();
                }
            }
        });
        setUpPersonView();

        if (fragmentType == 0) {
            person.setChecked(true);
            setUpPersonView();
        } else {

            company.setChecked(true);
            setUpCompanyView();
        }

        f_name.setText(fname);
        l_name.setText(lname);
        email_id.setText(email);
        phone_no.setText(number);
        city.setText(citytxt);
        address_1.setText(street);
        company_name.setText(companyName);



    }


    private void loadValuesFromStringFile(Spinner spinner, int resourceId) {
        String[] dataPoints = context.getResources().getStringArray(resourceId);
        SpinnerNormalAdapter adapter = new SpinnerNormalAdapter(context, android.R.layout.simple_spinner_item, dataPoints);
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.spinner_item, new ArrayList<>(Arrays.asList(dataPoints)));
        spinner.setAdapter(adapter);
    }

    private void loadAllCountries(final Spinner citizenship) {
        String[] isoCountryCodes = Locale.getISOCountries();
        map.clear();
        ArrayList<String> countries = new ArrayList<String>();
        for (String code : isoCountryCodes) {
            Locale locale = new Locale("", code);
            String name = locale.getDisplayCountry();
            map.put(name, code);
            countries.add(name);
        }

        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        countries.add(0, "Select");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, countries);

        citizenship.setAdapter(adapter);


/*
        final int sel = index;

        citizenship.post(new Runnable() {
            @Override
            public void run() {
                if(sel!=-1)
                    citizenship.setSelection(sel);
            }
        });
*/
    }

    private void setUpCompanyView() {
        isCompany = true;

        name_lay.setVisibility(View.GONE);
        name_lay_txt.setVisibility(View.GONE);

        _company_title.setVisibility(View.VISIBLE);
        _person_title.setVisibility(View.GONE);

        title_lay.setVisibility(View.GONE);
        website_lay.setVisibility(View.VISIBLE);

        person_is_client.setVisibility(View.GONE);
        company_is_client.setVisibility(View.VISIBLE);
        p_company_lay.setVisibility(View.GONE);

        add_btn.setText("Add Company");
    }

    private void setUpPersonView() {
        isCompany = false;

        p_company_lay.setVisibility(View.GONE);
        name_lay.setVisibility(View.VISIBLE);
        name_lay_txt.setVisibility(View.VISIBLE);

        _company_title.setVisibility(View.GONE);
        _person_title.setVisibility(View.VISIBLE);

        title_lay.setVisibility(View.VISIBLE);
        website_lay.setVisibility(View.GONE);

        person_is_client.setVisibility(View.VISIBLE);
        company_is_client.setVisibility(View.GONE);

        add_btn.setText("Add Person");


    }


    private void addCompany() {
        String companyName = "" + company_name.getText().toString();
        String email = "" + email_id.getText().toString();
        String phone = "" + phone_no.getText().toString();
        String website = "" + company_website.getText().toString();
        String address1 = "" + address_1.getText().toString();
        String address2 = "" + address_2.getText().toString();
        String city_txt = "" + city.getText().toString();
        String zip = "" + zip_code.getText().toString();
        String state_txt = "" + state.getText().toString();
        String country_txt = "" + country.getSelectedItem().toString();
        boolean companyIsClient = company_is_client.isChecked();

        if (!StringUtils.isValidString(companyName)) {
            Toast.makeText(context, "In-valid name", Toast.LENGTH_SHORT).show();
            return;
        }

        saveCompanyToDatabase(companyName, email, phone, website, address1, address2, city_txt, state_txt, country_txt, companyIsClient);
    }


    private void addPerson() {
        String title = "" + title_person.getSelectedItem().toString();
        String fname = "" + f_name.getText().toString();
        String lname = "" + l_name.getText().toString();
        String email = "" + email_id.getText().toString();
        String company = "" + p_company_name.getText().toString();
        String phone = "" + phone_no.getText().toString();
        String address1 = "" + address_1.getText().toString();
        String address2 = "" + address_2.getText().toString();
        String city_txt = "" + city.getText().toString();
        String zip = "" + zip_code.getText().toString();
        String state_txt = "" + state.getText().toString();
        String country_txt = "" + country.getSelectedItem().toString();
        boolean personIsClient = person_is_client.isChecked();

        if (title_person.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Please select title", Toast.LENGTH_SHORT).show();
            return;
        } else if (!StringUtils.isValidString(fname)) {
            Toast.makeText(context, "Please enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }

        savePersonToDatabase(title, fname, lname, email, company, phone, address1, address2, zip, state_txt, country_txt, personIsClient);
    }

    private void saveCompanyToDatabase(String companyName, String email, String phone, String website, String address1,
                                       String address2, String city_txt, String state_txt, String country_txt, boolean companyIsClient) {
        ContentValues cv = new ContentValues();
        cv.put(CompanyContacts.R_ID, CacheManager.INSTANCE.getMaxId(CompanyContacts.T_NAME));
        cv.put(CompanyContacts.CREATOR_ID, AppPreferences.getUserId(this));
        cv.put(CompanyContacts.ADMIN_ID, AppPreferences.getadminId(this));
        cv.put(CompanyContacts.NAME, companyName);
        cv.put(CompanyContacts.EMAIL, email);
        cv.put(CompanyContacts.TELEPHONE, phone);
        cv.put(CompanyContacts.WEBSITE, website);
        cv.put(CompanyContacts.ADDRESS1, address1);
        cv.put(CompanyContacts.ADDRESS2, address2);
        cv.put(CompanyContacts.CITY, city_txt);
        cv.put(CompanyContacts.ZIP, zip_code.getText().toString());
        cv.put(CompanyContacts.STATE, state_txt);
        cv.put(CompanyContacts.COUNTRY, country.getSelectedItemPosition() == 0 ? "" : country.getSelectedItem().toString());
        cv.put(CompanyContacts.CREATED_AT, CacheManager.INSTANCE.getDate());
        cv.put(CompanyContacts.UPDATED_AT, CacheManager.INSTANCE.getDate());
        cv.put(CompanyContacts.INSERTED, 1);
        cv.put(CompanyContacts.IS_CLIENT, companyIsClient ? 1 : 0);
        cv.put(CompanyContacts.KEY, AppPreferences.getKey(this));
        DbManager.getInstance().openDatabase();
        long id = DbManager.getInstance().insertAndGet(CompanyContacts.T_NAME, cv);
        DbManager.getInstance().closeDatabase();
        if (id == -1) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
        } else {
            openUtilityDialog(this, "Contact added successfully");
        }

    }

    private void savePersonToDatabase(String title, String fname, String lname, String email, String company, String phone, String address1,
                                      String address2, String zip, String state_txt, String country_txt, boolean personIsClient) {

        ContentValues cv = new ContentValues();
        cv.put(PersonContacts.R_ID, CacheManager.INSTANCE.getMaxId(PersonContacts.T_NAME));
        cv.put(PersonContacts.CREATOR_ID, AppPreferences.getUserId(this));
        cv.put(PersonContacts.ADMIN_ID, AppPreferences.getadminId(this));
        cv.put(PersonContacts.C_TYPE, type.getSelectedItemPosition() == 0 ? "" : type.getSelectedItem().toString());
        cv.put(PersonContacts.TITLE, title_person.getSelectedItemPosition() == 0 ? "" : title_person.getSelectedItem().toString());
        cv.put(PersonContacts.F_NAME, fname);
        cv.put(PersonContacts.L_NAME, lname);
        cv.put(PersonContacts.ADDRESS1, address1);
        cv.put(PersonContacts.ADDRESS2, address2);
        cv.put(PersonContacts.CITY, city.getText().toString());
        cv.put(PersonContacts.ZIP, zip);
        cv.put(PersonContacts.STATE, state_txt);
        cv.put(PersonContacts.COUNTRY, country.getSelectedItemPosition() == 0 ? "" : (map.get(country.getSelectedItem().toString())));
        cv.put(PersonContacts.IS_CLIENT, personIsClient ? 1 : 0);
        cv.put(PersonContacts.EMAIL, email);
        cv.put(PersonContacts.TELEPHONE, phone);
        cv.put(PersonContacts.INSERTED, 1);
        cv.put(PersonContacts.KEY, AppPreferences.getKey(this));
        cv.put(PersonContacts.CREATED_AT, CacheManager.INSTANCE.getDate());
        cv.put(PersonContacts.UPDATED_AT, CacheManager.INSTANCE.getDate());
        DbManager.getInstance().openDatabase();
        long id = DbManager.getInstance().insertAndGet(PersonContacts.T_NAME, cv);
        DbManager.getInstance().closeDatabase();
        if (id == -1) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
        } else {
            openUtilityDialog(this, "Contact added successfully");
        }
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
