package com.litman.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import com.litman.R;
import com.litman.fragments.FilterCourtFragment;
import com.litman.fragments.FilterUserFragment;
import com.litman.utils.AppUtils;

public class FilterActivity extends AppCompatActivity {

    private static final String[] tabs = {"User", "Court"};

    private TabLayout tabLayout;
    private ViewPager search_pager;
    private SearchPagerAdapter adapter;
    private String userId, courtId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userId = getIntent().getStringExtra("USER");
        courtId = getIntent().getStringExtra("COURT");

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        search_pager = (ViewPager) findViewById(R.id.pager);

        adapter = new SearchPagerAdapter(getSupportFragmentManager(), userId, courtId);

        search_pager.setAdapter(adapter);

        search_pager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(search_pager);

        (findViewById(R.id.btnApply)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFilterValues();
            }
        });

    }

    private void getFilterValues() {
        String courtId = ((FilterCourtFragment) adapter.getRegisteredFragment(1)).getSelectedIds();
        if (courtId.length() <= 0) {
            AppUtils.openUtilityDialog(this, "Please select a court");
            return;
        }

        String userId = ((FilterUserFragment) adapter.getRegisteredFragment(0)).getSelectedIds();
        if (userId.length() <= 0) {
            AppUtils.openUtilityDialog(this, "Please select a user");
            return;
        }

        Intent intent = new Intent();
        intent.putExtra("COURT", courtId);
        intent.putExtra("USER", userId);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    static class SearchPagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        FragmentManager mFragmentManager;
        private String userId, courtId;

        public SearchPagerAdapter(FragmentManager fm, String user, String court) {
            super(fm);
            mFragmentManager = fm;
            this.userId = user;
            this.courtId = court;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return FilterUserFragment.newInstance(userId);
            } else if (position == 1) {
                return FilterCourtFragment.newInstance(courtId);
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }

}
