package com.litman.activity;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.SpinnerNormalAdapter;
import com.litman.interfaces.CompanyContacts;
import com.litman.interfaces.PersonContacts;
import com.litman.manager.CacheManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.litman.R.id.company;

public class ViewPersonCompanyActivity extends BaseActivity {

    private final String TAG = ViewPersonCompanyActivity.class.getSimpleName();

    private Context context;
    private RadioGroup grp_person_comp;
    private EditText f_name, l_name, email_id, phone_no, address_1, address_2, city, zip_code, state, company_name, company_website, p_company_name;
    private Spinner title_person, country, type;
    private LinearLayout name_lay, name_lay_txt, _person_title, _company_title, title_lay, website_lay, p_company_lay;
    private CheckBox person_is_client, company_is_client;
    private Button add_btn;
    private boolean isCompany = false;
    private int ID, isUser;
    private HashMap<String, String> map = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        ID = getIntent().getIntExtra("ID", 0);
        isUser = getIntent().getIntExtra("ISUSER", 0);
        setContentView(R.layout.add_person_company);
        setUpView();
    }

    private void setUpView() {
        grp_person_comp = (RadioGroup) findViewById(R.id.grp_person_comp);
        findViewById(R.id.person).setEnabled(false);
        findViewById(R.id.company).setEnabled(false);
        grp_person_comp.setEnabled(false);
        f_name = (EditText) findViewById(R.id.f_name);
        f_name.setEnabled(false);
        l_name = (EditText) findViewById(R.id.l_name);
        l_name.setEnabled(false);
        email_id = (EditText) findViewById(R.id.email_id);
        email_id.setEnabled(false);
        phone_no = (EditText) findViewById(R.id.phone_no);
        phone_no.setEnabled(false);
        address_1 = (EditText) findViewById(R.id.address_1);
        address_1.setEnabled(false);
        address_2 = (EditText) findViewById(R.id.address_2);
        address_2.setEnabled(false);
        p_company_name = (EditText) findViewById(R.id.p_company_name);
        p_company_name.setEnabled(false);
        city = (EditText) findViewById(R.id.city);
        city.setEnabled(false);
        zip_code = (EditText) findViewById(R.id.zip_code);
        zip_code.setEnabled(false);
        state = (EditText) findViewById(R.id.state);
        state.setEnabled(false);
        company_name = (EditText) findViewById(R.id.company_name);
        company_name.setEnabled(false);
        company_website = (EditText) findViewById(R.id.company_website);
        company_website.setEnabled(false);

        title_person = (Spinner) findViewById(R.id.title_person);
        country = (Spinner) findViewById(R.id.country);
        type = (Spinner) findViewById(R.id.type);

        title_person.setEnabled(false);
        country.setEnabled(false);
        type.setEnabled(false);

        person_is_client = (CheckBox) findViewById(R.id.person_is_client);
        company_is_client = (CheckBox) findViewById(R.id.company_is_client);

        person_is_client.setEnabled(false);
        company_is_client.setEnabled(false);

        name_lay = (LinearLayout) findViewById(R.id.name_lay);
        name_lay_txt = (LinearLayout) findViewById(R.id.name_lay_txt);
        _person_title = (LinearLayout) findViewById(R.id._person_title);
        _company_title = (LinearLayout) findViewById(R.id._company_title);
        title_lay = (LinearLayout) findViewById(R.id.title_lay);
        website_lay = (LinearLayout) findViewById(R.id.website_lay);
        p_company_lay = (LinearLayout) findViewById(R.id.p_company_lay);

        add_btn = (Button) findViewById(R.id.add_btn);

        add_btn.setVisibility(View.GONE);

        if (isUser == 1) {
            setUpPersonView();
            grp_person_comp.check(R.id.person);
        } else {
            setUpCompanyView();
            grp_person_comp.check(R.id.company);
        }

        grp_person_comp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.person) {

                } else if (checkedId == company) {

                }
            }
        });

        if (isUser == 1) {
            setUserData();
        } else {
            setCompanyData();
        }

    }

    private void setCompanyData() {
        Cursor curs = CacheManager.INSTANCE.getCompany(ID);
        if (curs != null && curs.moveToFirst()) {
            company_name.setText(curs.getString(curs.getColumnIndex(CompanyContacts.NAME)));
            email_id.setText(curs.getString(curs.getColumnIndex(CompanyContacts.EMAIL)));
            phone_no.setText(curs.getString(curs.getColumnIndex(CompanyContacts.TELEPHONE)));
            company_name.setText(curs.getString(curs.getColumnIndex(CompanyContacts.NAME)));
            company_website.setText(curs.getString(curs.getColumnIndex(CompanyContacts.WEBSITE)));
            address_1.setText(curs.getString(curs.getColumnIndex(CompanyContacts.ADDRESS1)));
            address_2.setText(curs.getString(curs.getColumnIndex(CompanyContacts.ADDRESS2)));
            city.setText(curs.getString(curs.getColumnIndex(CompanyContacts.CITY)));
            zip_code.setText(curs.getString(curs.getColumnIndex(CompanyContacts.ZIP)));
            state.setText(curs.getString(curs.getColumnIndex(CompanyContacts.STATE)));
            company_is_client.setChecked(curs.getInt(curs.getColumnIndex(CompanyContacts.IS_CLIENT)) == 0 ? false : true);
            loadAllCountries(country,curs.getString(curs.getColumnIndex(CompanyContacts.COUNTRY)));
            curs.close();
        }
    }

    private void setUserData() {
        Cursor curs = CacheManager.INSTANCE.getPerson(ID);
        if (curs != null && curs.moveToFirst()) {
            f_name.setText(curs.getString(curs.getColumnIndex(PersonContacts.F_NAME)));
            l_name.setText(curs.getString(curs.getColumnIndex(PersonContacts.L_NAME)));
            email_id.setText(curs.getString(curs.getColumnIndex(PersonContacts.EMAIL)));
            phone_no.setText(curs.getString(curs.getColumnIndex(PersonContacts.TELEPHONE)));
            address_1.setText(curs.getString(curs.getColumnIndex(PersonContacts.ADDRESS1)));
            address_2.setText(curs.getString(curs.getColumnIndex(PersonContacts.ADDRESS2)));
            city.setText(curs.getString(curs.getColumnIndex(PersonContacts.CITY)));
            zip_code.setText(curs.getString(curs.getColumnIndex(PersonContacts.ZIP)));
            state.setText(curs.getString(curs.getColumnIndex(PersonContacts.STATE)));
            person_is_client.setChecked(curs.getInt(curs.getColumnIndex(PersonContacts.IS_CLIENT)) == 0 ? false : true);
            loadValuesFromStringFile(title_person, R.array.title_person, curs.getString(curs.getColumnIndex(PersonContacts.TITLE)));
            loadValuesFromStringFile(type, R.array.type_person, curs.getString(curs.getColumnIndex(PersonContacts.TITLE)));
            loadAllCountries(country,curs.getString(curs.getColumnIndex(CompanyContacts.COUNTRY)));
            curs.close();
        }
    }


    private void loadValuesFromStringFile(Spinner spinner, int resourceId, String value) {
        String[] dataPoints = context.getResources().getStringArray(resourceId);
        SpinnerNormalAdapter adapter = new SpinnerNormalAdapter(context, android.R.layout.simple_spinner_item, dataPoints);
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.spinner_item, new ArrayList<>(Arrays.asList(dataPoints)));
        spinner.setAdapter(adapter);
        for (int i = 0; i < dataPoints.length; i++) {
            if (dataPoints[i].equalsIgnoreCase(value)) {
                spinner.setSelection(i);
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (view != null) {
                    ((TextView) view).setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadAllCountries(final Spinner citizenship, String value) {
        String[] isoCountryCodes = Locale.getISOCountries();
        map.clear();
        ArrayList<String> countries = new ArrayList<String>();
        for (String code : isoCountryCodes) {
            Locale locale = new Locale("", code);
            String name = locale.getDisplayCountry();
            map.put(name, code);
            countries.add(name);
        }

        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        countries.add(0, "Select");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, countries);

        citizenship.setAdapter(adapter);

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                citizenship.setSelection(countries.indexOf(entry.getKey()));
                break;
            }
        }

        citizenship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (view != null) {
                    ((TextView) view).setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setUpCompanyView() {
        isCompany = true;

        name_lay.setVisibility(View.GONE);
        name_lay_txt.setVisibility(View.GONE);

        _company_title.setVisibility(View.VISIBLE);
        _person_title.setVisibility(View.GONE);

        title_lay.setVisibility(View.GONE);
        website_lay.setVisibility(View.VISIBLE);

        person_is_client.setVisibility(View.GONE);
        company_is_client.setVisibility(View.VISIBLE);
        p_company_lay.setVisibility(View.GONE);

        add_btn.setText("Add Company");
    }

    private void setUpPersonView() {
        isCompany = false;

        p_company_lay.setVisibility(View.GONE);
        name_lay.setVisibility(View.VISIBLE);
        name_lay_txt.setVisibility(View.VISIBLE);

        _company_title.setVisibility(View.GONE);
        _person_title.setVisibility(View.VISIBLE);

        title_lay.setVisibility(View.VISIBLE);
        website_lay.setVisibility(View.GONE);

        person_is_client.setVisibility(View.VISIBLE);
        company_is_client.setVisibility(View.GONE);

        add_btn.setText("Add Person");
    }


}
