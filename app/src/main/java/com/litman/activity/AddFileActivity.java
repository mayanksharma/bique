package com.litman.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.litman.R;
import com.litman.interfaces.Const;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class AddFileActivity extends BaseActivity {

    private WebView web;
    private int id;
    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_profile);
        id = getIntent().getIntExtra("ID", 0);
        setUpView();
    }

    private void setUpView() {
        web = (WebView) findViewById(R.id.web);
        (findViewById(R.id.txtTitle)).setVisibility(View.GONE);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);
        web.getSettings().setAllowFileAccess(true);

        if (AppUtils.checkInternetConnection(this)) {
            initializeWebView();
            if (id == 0) {
                web.loadUrl(Const.FILE_UPLOAD + AppPreferences.getToken(this));
            } else {
                web.loadUrl(Const.FILE_UPLOAD + AppPreferences.getToken(this) + "&uploadtype=" + id);
            }
        } else {
            AppUtils.noInternetDialog(this);
        }

        hideKeyboard(web);
    }

    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        web.clearCache(true);

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);

                if (url.startsWith(Const.SUCCESS)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    openUtilityDialog(AddFileActivity.this,value);
                    return true;
                } else if (url.startsWith(Const.FAIL)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    AppUtils.openUtilityDialog(AddFileActivity.this, value);
                    if (id == 0) {
                        view.loadUrl(Const.FILE_UPLOAD + AppPreferences.getToken(AddFileActivity.this));
                    } else {
                        view.loadUrl(Const.FILE_UPLOAD + AppPreferences.getToken(AddFileActivity.this) + "&uploadtype=" + id);
                    }
                    return true;
                }
                view.loadUrl(url);

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });

        web.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                String acceptTypes[] = new String[0];
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    acceptTypes = fileChooserParams.getAcceptTypes();
                }

                String acceptType = "";
                for (int i = 0; i < acceptTypes.length; ++ i) {
                    if (acceptTypes[i] != null && acceptTypes[i].length() != 0)
                        acceptType += acceptTypes[i] + ";";
                }
                if (acceptType.length() == 0)
                    acceptType = "*/*";

                final ValueCallback<Uri[]> finalFilePathCallback = filePathCallback;

                ValueCallback<Uri> vc = new ValueCallback<Uri>() {

                    @Override
                    public void onReceiveValue(Uri value) {

                        Uri[] result;
                        if (value != null)
                            result = new Uri[]{value};
                        else
                            result = null;

                        finalFilePathCallback.onReceiveValue(result);

                    }
                };

                openFileChooser(vc, acceptType, "filesystem");

                return true;
            }

            //For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }

        });


    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((AddFileActivity) ctx).finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) return;
            Uri result = intent == null || resultCode != RESULT_OK ? null
                    : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        }
    }

}
