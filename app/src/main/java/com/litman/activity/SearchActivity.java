package com.litman.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.litman.R;
import com.litman.fragments.SearchContactsFragment;
import com.litman.fragments.SearchFileFragment;
import com.litman.fragments.SearchMattersFragment;
import com.litman.fragments.SearchPersonFragment;
import com.litman.fragments.SearchTaskFragment;


public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private TabLayout tabLayout;
    private ViewPager search_pager;
    private SearchPagerAdapter adapter;
    private SearchView searchView;

    public static String keyword = "";

    private static final String[] tabs = {"Person", "Matters", "Task", "Files", "Contacts"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_search);
        setUpView();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        setSearchText(newText);
        return false;
    }

    private void setUpView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        search_pager = (ViewPager) findViewById(R.id.search_pager);

        adapter = new SearchPagerAdapter(getSupportFragmentManager());
        search_pager.setAdapter(adapter);

        search_pager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(search_pager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(this);

        searchView.setOnCloseListener(this);

        MenuItemCompat.expandActionView(searchMenuItem);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onBackPressed();
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onClose() {
        onBackPressed();
        return false;
    }

    static class SearchPagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        FragmentManager mFragmentManager;

        public SearchPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return SearchPersonFragment.newInstance();
            } else if (position == 1) {
                return SearchMattersFragment.newInstance();
            } else if (position == 2) {
                return SearchTaskFragment.newInstance();
            } else if (position == 3) {
                return SearchFileFragment.newInstance();
            } else if (position == 4) {
                return SearchContactsFragment.newInstance();
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }

    public void setSearchText(String queryString) {
        keyword = queryString;
        int c = search_pager.getCurrentItem();
        if (c == 0) {
            notifyChange(c, queryString);
            notifyChange(c + 1, queryString);
        } else if (c == 5) {
            notifyChange(c, queryString);
            notifyChange(c - 1, queryString);
        } else {
            notifyChange(c, queryString);
            notifyChange(c + 1, queryString);
            notifyChange(c - 1, queryString);
        }
    }

    private void notifyChange(int pos, String queryString) {
        switch (pos) {
            case 0:
                ((SearchPersonFragment) adapter.getRegisteredFragment(pos)).setData();
                break;
            case 1:
                ((SearchMattersFragment) adapter.getRegisteredFragment(pos)).setData();
                break;
            case 2:
                ((SearchTaskFragment) adapter.getRegisteredFragment(pos)).setData();
                break;
            case 3:
                ((SearchFileFragment) adapter.getRegisteredFragment(pos)).setData();
                break;
            case 4:
                ((SearchContactsFragment) adapter.getRegisteredFragment(pos)).setData();
                break;
        }
    }


}
