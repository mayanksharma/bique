package com.litman.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.HearingListAdapter;
import com.litman.adapters.TaskListAdapter;
import com.litman.dao.HomeFeedInfo;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;

import java.util.ArrayList;

public class MonthCalenderDetailActivity extends BaseActivity {
    private RecyclerView hearingList, taskList;
    private String startdate = "", courtId, endDate = "";
    private TextView hearingCountTv, taskCountTv;
    private NestedScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_calender_detail);
        hearingList = (RecyclerView) findViewById(R.id.hearingList);
        taskList = (RecyclerView) findViewById(R.id.taskList);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        taskCountTv = (TextView) findViewById(R.id.taskCountTv);
        hearingCountTv = (TextView) findViewById(R.id.hearingCountTv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        hearingList.setLayoutManager(linearLayoutManager);
        taskList.setLayoutManager(linearLayoutManager2);


        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("START_DATE") != null) {
                startdate = getIntent().getExtras().getString("START_DATE");
            }
            if (getIntent().getExtras().getString("END_DATE") != null) {
                endDate = getIntent().getExtras().getString("END_DATE");
            }

            if (getIntent().getExtras().getString("COURT") != null) {
                courtId = getIntent().getExtras().getString("COURT");
            }
        }


        Cursor curs = CacheManager.INSTANCE.getHearingList(AppPreferences.getUserId(this), courtId, startdate, endDate);

        ArrayList<HomeFeedInfo> list = new ArrayList<>();

        if (curs != null && curs.moveToFirst()) {
            curs.moveToFirst();
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            while (!curs.isAfterLast()) {
                HomeFeedInfo info = new HomeFeedInfo();
                info.MATTER_ID = curs.getInt(curs.getColumnIndex("matter_id"));
                info.COURT_NAME = curs.getString(curs.getColumnIndex("court_name"));
                info.PLAINTIFF = curs.getString(curs.getColumnIndex("plaintiff"));
                info.DEFENDANT = curs.getString(curs.getColumnIndex("defendant"));
                info.COURT_ROOM_NO = curs.getInt(curs.getColumnIndex("court_room_no"));
                info.JUDGE_NAME = curs.getString(curs.getColumnIndex("judge_name"));
                info.ITEM_NO = curs.getString(curs.getColumnIndex("item_no"));
                info.MATTER_NO = curs.getString(curs.getColumnIndex("matter_number"));
                info.CASE_YEAR = curs.getString(curs.getColumnIndex("case_year"));
                info.CASE_TYPE = curs.getString(curs.getColumnIndex("case_type"));
                info.TYPE = 2;
                list.add(info);
                curs.moveToNext();
            }
            if (curs != null)
                curs.close();
        }
        curs = CacheManager.INSTANCE.getCustomHearingList(AppPreferences.getUserId(this), courtId, startdate, endDate);
        if (curs != null && curs.moveToFirst()) {
            curs.moveToFirst();
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            while (!curs.isAfterLast()) {
                HomeFeedInfo info = new HomeFeedInfo();
                info.MATTER_ID = curs.getInt(curs.getColumnIndex("matter_id"));
                info.COURT_NAME = curs.getString(curs.getColumnIndex("court_name"));
                info.PLAINTIFF = curs.getString(curs.getColumnIndex("plaintiff"));
                info.DEFENDANT = curs.getString(curs.getColumnIndex("defendant"));
                info.COURT_ROOM_NO = curs.getInt(curs.getColumnIndex("court_room_no"));
                info.JUDGE_NAME = curs.getString(curs.getColumnIndex("judge_name"));
                info.ITEM_NO = curs.getString(curs.getColumnIndex("item_no"));
                info.MATTER_NO = curs.getString(curs.getColumnIndex("matter_number"));
                info.CASE_YEAR = curs.getString(curs.getColumnIndex("case_year"));
                info.CASE_TYPE = curs.getString(curs.getColumnIndex("case_type"));
                info.F_NAME = curs.getString(curs.getColumnIndex("first_name"));
                info.L_NAME = curs.getString(curs.getColumnIndex("last_name"));
                info.USER_ID = curs.getInt(curs.getColumnIndex("attendee_id"));
                info.TYPE = 1;
                list.add(info);
                curs.moveToNext();
            }
            if (curs != null)
                curs.close();
        }

        hearingCountTv.setText("Hearing (" + list.size() + ")");

        curs = CacheManager.INSTANCE.getTaskList(AppPreferences.getUserId(this), courtId, startdate, endDate);

        if (curs != null && curs.moveToFirst()) {
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            taskList.setAdapter(new TaskListAdapter(this, curs));
            taskCountTv.setText("Task (" + curs.getCount() + ")");
            if (curs.getCount() == 0) {
                taskList.setVisibility(View.GONE);
                taskCountTv.setVisibility(View.GONE);
            } else {
                taskList.setVisibility(View.VISIBLE);
                taskCountTv.setVisibility(View.VISIBLE);
            }

        } else {
            taskList.setVisibility(View.GONE);
            taskCountTv.setVisibility(View.GONE);
        }


        hearingList.setAdapter(new HearingListAdapter(this, list));
        if (list.size() < 1) {
            hearingCountTv.setVisibility(View.GONE);
            hearingList.setVisibility(View.GONE);
        } else {
            hearingCountTv.setVisibility(View.VISIBLE);
            hearingList.setVisibility(View.VISIBLE);
        }

        if (taskList.getVisibility() == View.GONE && hearingList.getVisibility() == View.GONE) {
            scrollView.setVisibility(View.GONE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
        }


    }


}
