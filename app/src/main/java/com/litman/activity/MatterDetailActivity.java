package com.litman.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.MatterInfo;
import com.litman.db.DbManager;
import com.litman.interfaces.Courts;
import com.litman.interfaces.MatterUsers;
import com.litman.interfaces.Matters;
import com.litman.interfaces.Notifications;
import com.litman.interfaces.NotificationsStatus;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;


public class MatterDetailActivity extends BaseActivity implements View.OnClickListener {

    private Integer matterId;
    private TextView txtName, txtCase, txtCourt, txtDate, txtCHDate, txtAddHearing, txtAddMe, txtAdded, txtRequest;
    private LinearLayout linMatterMy;
    private MatterInfo info;
    private Boolean isMyMatter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_matter_detail);
        matterId = getIntent().getIntExtra("ID", -1);
        setupView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    private void setupView() {

        txtName = (TextView) findViewById(R.id.txtName);
        txtCourt = (TextView) findViewById(R.id.txtCourt);
        txtCase = (TextView) findViewById(R.id.txtCase);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtCHDate = (TextView) findViewById(R.id.txtCHDate);
        txtAddHearing = (TextView) findViewById(R.id.txtAddHearing);
        txtAdded = (TextView) findViewById(R.id.txtAddedBy);
        txtAddMe = (TextView) findViewById(R.id.txtAddMe);
        txtRequest = (TextView) findViewById(R.id.txtRequest);

        linMatterMy = (LinearLayout) findViewById(R.id.linMatterView);

        txtAddHearing.setOnClickListener(this);
        txtAddMe.setOnClickListener(this);
        (findViewById(R.id.relOfficial)).setOnClickListener(this);
        (findViewById(R.id.relMatterActivity)).setOnClickListener(this);
        (findViewById(R.id.relTask)).setOnClickListener(this);
        (findViewById(R.id.relContact)).setOnClickListener(this);

        try {
            getData();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getData() throws Exception {
        Cursor curs = CacheManager.INSTANCE.getMatterDetail(matterId);
        if (curs != null && curs.moveToFirst()) {
            info = new MatterInfo();
            info.ID = curs.getInt(curs.getColumnIndex(Matters.R_ID));
            info.COURT_NAME = curs.getString(curs.getColumnIndex(Courts.NAME));
            info.PLAINTIFF = curs.getString(curs.getColumnIndex(Matters.PLAINTIFF));
            info.DEFENDANT = curs.getString(curs.getColumnIndex(Matters.DEFFENDANT));
            info.A_PLAINTIFF = curs.getString(curs.getColumnIndex(Matters.P_ATT));
            info.A_DEFENDANT = curs.getString(curs.getColumnIndex(Matters.D_ATT));
            info.MATTER_NUMBER = curs.getString(curs.getColumnIndex(Matters.MATTER_NO));
            info.CASE_YEAR = curs.getString(curs.getColumnIndex(Matters.CASE_YEAR));
            info.CASE_TYPE = curs.getString(curs.getColumnIndex(Matters.CASE_TYPE));
            info.U_ID = curs.getInt(curs.getColumnIndex(MatterUsers.USER_ID));
            info.H_DATE = curs.getString(curs.getColumnIndex("hearing_date"));
            info.CH_DATE = curs.getString(curs.getColumnIndex("custom_date"));
            info.CLIENT = curs.getString(curs.getColumnIndex(MatterUsers.MY_CLIENT));
        }
        if (curs != null) {
            curs.close();
        }
        setData(info);
    }

    private void setData(MatterInfo info) {
        txtName.setText(info.PLAINTIFF + " Vs " + info.DEFENDANT);
        txtCourt.setText(info.COURT_NAME);
        txtCase.setText(info.CASE_TYPE + " " + info.MATTER_NUMBER + "/" + info.CASE_YEAR);
        txtDate.setText(CacheManager.INSTANCE.getFormattedDate(info.H_DATE));
        txtCHDate.setText(CacheManager.INSTANCE.getFormattedDate(info.CH_DATE));

        txtAdded.setText("Added By: " + CacheManager.INSTANCE.getCapitalizeFullName(getUserName(info.U_ID + "")));

        isMyMatter = CacheManager.INSTANCE.isMyMatter(info.ID + "", AppPreferences.getUserId(this));

        if (isMyMatter) {
            linMatterMy.setVisibility(View.VISIBLE);
            if (CacheManager.INSTANCE.compareDate(info.H_DATE)) {
                txtAddHearing.setVisibility(View.GONE);
            }
            if (CacheManager.INSTANCE.compareDate(info.CH_DATE)) {
                txtAddHearing.setVisibility(View.GONE);
            }
            txtAddMe.setVisibility(View.GONE);
        } else {
            linMatterMy.setVisibility(View.GONE);
            txtAddHearing.setVisibility(View.GONE);

            if (CacheManager.INSTANCE.isRequestSent(info.ID + "", AppPreferences.getUserId(this))) {
                txtAddMe.setVisibility(View.GONE);
                txtRequest.setVisibility(View.VISIBLE);
            } else {
                txtAddMe.setVisibility(View.VISIBLE);
                txtRequest.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtAddHearing:
                Intent intent = new Intent(MatterDetailActivity.this, AddHearingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("info", info);
                intent.putExtras(bundle);
                startActivityForResult(intent,202);
                break;
            case R.id.txtAddMe:
                addMeMatter();
                break;
            case R.id.relOfficial:
                startActivity(new Intent(this, OfficialActivity.class).putExtra(Matters.T_NAME, info));
                break;
            case R.id.relMatterActivity:
                startActivity(new Intent(this, MatterActivityActivity.class).putExtra(Matters.T_NAME, info));
                break;
            case R.id.relTask:
                startActivity(new Intent(this, MatterTaskActivity.class).putExtra(Matters.T_NAME, info));
                break;
            case R.id.relContact:
                startActivity(new Intent(this, ContactAndTeamActivity.class).putExtra(Matters.T_NAME, info));
                break;
        }
    }

    private void addMeMatter() {
        ContentValues cv = new ContentValues();
        int maxId = CacheManager.INSTANCE.getMaxId(Notifications.T_NAME);
        cv.put(Notifications.R_ID, maxId);
        cv.put(Notifications.KEY, AppPreferences.getKey(this));
        cv.put(Notifications.INSERTED, 1);
        cv.put(Notifications.NOTICE_TYPE, "Request");
        cv.put(Notifications.NOTI_TYPE, "Matter");
        cv.put(Notifications.LINK, "http://biqe.herokuapp.com/matters/" + info.ID);
        String text = CacheManager.INSTANCE.getCapitalizeFullName(getUserName(AppPreferences.getUserId(this))) +
                " has requested you to add them on " + info.PLAINTIFF + " v/s " + info.DEFENDANT + ".";
        cv.put(Notifications.TEXT, text);
        cv.put(Notifications.NOTI_ID, info.ID);
        cv.put(Notifications.CREATOR_ID, AppPreferences.getUserId(this));
        cv.put(Notifications.CREATED_AT, CacheManager.INSTANCE.getDate());
        cv.put(Notifications.UPDATED_AT, CacheManager.INSTANCE.getDate());

        ContentValues cVal = new ContentValues();
        cVal.put(NotificationsStatus.NOTI_ID, maxId);
        cVal.put(NotificationsStatus.MEMBER_ID, AppPreferences.getadminId(this));
        cVal.put(NotificationsStatus.READ, 0);
        cVal.put(NotificationsStatus.INSERTED, 1);
        cVal.put(NotificationsStatus.KEY, AppPreferences.getKey(this));

        DbManager.getInstance().openDatabase();
        DbManager.getInstance().insert(Notifications.T_NAME, cv);
        DbManager.getInstance().insert(NotificationsStatus.T_NAME, cVal);
        DbManager.getInstance().closeDatabase();

        if (CacheManager.INSTANCE.isRequestSent(info.ID + "", AppPreferences.getUserId(this))) {
            txtAddMe.setVisibility(View.GONE);
            txtRequest.setVisibility(View.VISIBLE);
        } else {
            txtAddMe.setVisibility(View.VISIBLE);
            txtRequest.setVisibility(View.GONE);
        }
    }

    private String getUserName(String userId) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getUserName(userId);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex("name")));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
