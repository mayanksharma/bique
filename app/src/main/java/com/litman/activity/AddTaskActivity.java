package com.litman.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.litman.R;
import com.litman.adapters.SpinnerAdapter;
import com.litman.adapters.SpinnerNormalAdapter;
import com.litman.dao.CourtInfo;
import com.litman.db.DbManager;
import com.litman.interfaces.Courts;
import com.litman.interfaces.Tasks;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AddTaskActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtTitle;
    private static TextView txtDate;
    private Spinner spnAssign, spnCat;
    private ArrayList<CourtInfo> info = new ArrayList<>();
    private static Calendar c;
    private int id;

    String cat[] = {"Please Select", "Briefing", "Conference", "Meeting"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        id = getIntent().getIntExtra("M_ID", 0);
        setUpView();
    }

    private void setUpView() {
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        txtDate = (TextView) findViewById(R.id.txtDate);

        spnAssign = (Spinner) findViewById(R.id.spnAssign);
        spnCat = (Spinner) findViewById(R.id.spnCategory);

        (findViewById(R.id.btnAdd)).setOnClickListener(this);
        txtDate.setOnClickListener(this);

        SpinnerNormalAdapter spinnerArrayAdapter = new SpinnerNormalAdapter(this, android.R.layout.simple_spinner_item, cat);
        if (spinnerArrayAdapter != null) {
            spnCat.setAdapter(spinnerArrayAdapter);
        }

        getData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                saveData();
                break;
            case R.id.txtDate:
                showDatePickerDialog(view);
                CacheManager.INSTANCE.hideKeyboard(view, this);
                break;
        }
    }

    private void saveData() {
        if (valid()) {
            ContentValues cv = new ContentValues();
            cv.put(Tasks.R_ID, CacheManager.INSTANCE.getMaxId(Tasks.T_NAME));
            cv.put(Tasks.MATTER_ID, id);
            cv.put(Tasks.USER_ID, AppPreferences.getUserId(this));
            cv.put(Tasks.COMPLETED, 0);
            cv.put(Tasks.TITLE, edtTitle.getText().toString());
            cv.put(Tasks.DUE_AT, getDate(c));
            cv.put(Tasks.ASSIGNED_TO, ((CourtInfo) spnAssign.getSelectedItem()).ID);
            cv.put(Tasks.CATEGORY, spnCat.getSelectedItemPosition() == 0 ? "" : spnCat.getSelectedItem().toString());
            cv.put(Tasks.INSERTED, 1);
            cv.put(Tasks.ADMIN_ID, AppPreferences.getadminId(this));
            cv.put(Tasks.KEY, AppPreferences.getKey(this));
            cv.put(Tasks.CREATED_AT, CacheManager.INSTANCE.getDate());
            cv.put(Tasks.UPDATED_AT, CacheManager.INSTANCE.getDate());
            DbManager.getInstance().openDatabase();
            long id = DbManager.getInstance().insertAndGet(Tasks.T_NAME, cv);
            DbManager.getInstance().closeDatabase();
            if (id == -1) {
                Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
            } else {
                openUtilityDialog(this, "Task added successfully");
            }
        }
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String getDate(Calendar c) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            return serverFormat.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private boolean valid() {
        if (edtTitle.getText().toString().trim().length() <= 0) {
            Toast.makeText(this, "Enter title", Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtDate.getText().toString().length() <= 0) {
            Toast.makeText(this, "Enter date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (spnAssign.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "select assignee", Toast.LENGTH_SHORT).show();
            return false;
        }
//        else if (spnCat.getSelectedItemPosition() == 0) {
//            Toast.makeText(this, "select category", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return true;
    }

    private void getData() {
        info.clear();
        CourtInfo in = new CourtInfo();
        in.NAME = "Who is Responsible?";
        in.ID = -1;
        info.add(in);
        if (id == 0) {
            Cursor curs = CacheManager.INSTANCE.getUser();
            while (curs != null && curs.moveToNext()) {
                CourtInfo cInfo = new CourtInfo();
                cInfo.ID = curs.getInt(curs.getColumnIndex(Courts.R_ID));
                cInfo.NAME = curs.getString(curs.getColumnIndex(Courts.NAME));
                info.add(cInfo);
            }
            if (curs != null) {
                curs.close();
            }
        } else {
            CourtInfo in1 = new CourtInfo();
            in1.NAME = ((AppPreferences.getFname(this) + " " + AppPreferences.getMname(this)).trim() + " " + AppPreferences.getLname(this)).trim();
            in1.ID = Integer.parseInt(AppPreferences.getUserId(this));
            info.add(in1);
        }

        SpinnerAdapter adapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item, info);
        spnAssign.setAdapter(adapter);

        for (int i = 0; i < info.size(); i++) {
            if (info.get(i).ID == Integer.valueOf(AppPreferences.getUserId(this))) {
                spnAssign.setSelection(i);
                break;
            }
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis());

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, day);
            c.set(Calendar.HOUR, c.getActualMinimum(Calendar.HOUR));
            c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
            c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
            c.set(Calendar.MILLISECOND, c.getActualMinimum(Calendar.MILLISECOND));
            SimpleDateFormat serverFormat = new SimpleDateFormat("dd-MM-yyyy");
            try {
                txtDate.setText(serverFormat.format(c.getTime()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
