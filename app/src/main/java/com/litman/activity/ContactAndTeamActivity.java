package com.litman.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import com.litman.R;
import com.litman.dao.MatterInfo;
import com.litman.db.DbManager;
import com.litman.fragments.MattersContactFragment;
import com.litman.fragments.MattersTeamFragment;
import com.litman.interfaces.MPContacts;
import com.litman.interfaces.MatterUsers;
import com.litman.interfaces.Matters;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class ContactAndTeamActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager pager;
    private PagerAdapter adapter;
    private MatterInfo info;

    private static final String[] tabs = {"Contacts", "Team"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_and_team);
        info = (MatterInfo) getIntent().getSerializableExtra(Matters.T_NAME);

        setUpView();
    }

    private void setUpView() {

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        pager = (ViewPager) findViewById(R.id.pager);

        adapter = new PagerAdapter(getSupportFragmentManager(), info);
        pager.setAdapter(adapter);

        pager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(pager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (CacheManager.INSTANCE.isAddAllowed(this)) {
            menu.setGroupVisible(R.id.grp_main, true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                if (pager.getAdapter() != null) {
                    int sel = pager.getCurrentItem();
                    if (sel == 1) {
                        startActivityForResult(new Intent(this, SelectSingleUserActivity.class).putExtra("M_ID", info.ID)
                                .putExtra("TYPE", 1), 1);
                    } else {
                        startActivityForResult(new Intent(this, SelectSingleUserActivity.class).putExtra("M_ID", info.ID)
                                .putExtra("TYPE", 2), 2);
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data.getExtras() != null) {
                String id = data.getStringExtra("ID");
                ContentValues cv = new ContentValues();
                cv.put(MatterUsers.R_ID, CacheManager.INSTANCE.getMaxId(MatterUsers.T_NAME));
                cv.put(MatterUsers.MATTER_ID, info.ID);
                cv.put(MatterUsers.USER_ID, id);
                cv.put(MatterUsers.INSERTED, 1);
                cv.put(MatterUsers.KEY, AppPreferences.getKey(this));
                cv.put(MatterUsers.CREATED_AT, CacheManager.INSTANCE.getDate());
                cv.put(MatterUsers.UPDATED_AT, CacheManager.INSTANCE.getDate());
                DbManager.getInstance().openDatabase();
                long ids = DbManager.getInstance().insertAndGet(MatterUsers.T_NAME, cv);
                DbManager.getInstance().closeDatabase();
                if (ids == -1) {
                    Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    AppUtils.openUtilityDialog(this, "Added successfully");
                }
                ((MattersTeamFragment)adapter.getRegisteredFragment(1)).setData();
            }
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data.getExtras() != null) {
                String id = data.getStringExtra("ID");
                ContentValues cv = new ContentValues();
                cv.put(MPContacts.R_ID, CacheManager.INSTANCE.getMaxId(MPContacts.T_NAME));
                cv.put(MPContacts.MATTER_ID, info.ID);
                cv.put(MPContacts.PC_ID, id);
                cv.put(MPContacts.INSERTED, 1);
                cv.put(MPContacts.KEY, AppPreferences.getKey(this));
                cv.put(MPContacts.CREATED_AT, CacheManager.INSTANCE.getDate());
                cv.put(MPContacts.UPDATED_AT, CacheManager.INSTANCE.getDate());
                DbManager.getInstance().openDatabase();
                long ids = DbManager.getInstance().insertAndGet(MPContacts.T_NAME, cv);
                DbManager.getInstance().closeDatabase();
                if (ids == -1) {
                    Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
                } else {
                    AppUtils.openUtilityDialog(this, "Added successfully");
                }
                ((MattersContactFragment)adapter.getRegisteredFragment(0)).setData();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
        FragmentManager mFragmentManager;
        private MatterInfo info;

        public PagerAdapter(FragmentManager fm, MatterInfo info) {
            super(fm);
            mFragmentManager = fm;
            this.info = info;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return MattersContactFragment.newInstance(info);
            } else if (position == 1) {
                return MattersTeamFragment.newInstance(info);
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }


}
