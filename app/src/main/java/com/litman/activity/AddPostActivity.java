package com.litman.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.litman.R;
import com.litman.db.DbManager;
import com.litman.interfaces.Post;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;

public class AddPostActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtTitle;
    private Integer id;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        id =  getIntent().getIntExtra("ID",0);
        type =  getIntent().getStringExtra("TYPE");
        setUpView();
    }

    private void setUpView() {
        edtTitle = (EditText) findViewById(R.id.edtTitle);

        (findViewById(R.id.btnAdd)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                saveData();
                break;
        }
    }

    private void saveData() {
        if (valid()) {
            ContentValues cv = new ContentValues();
            cv.put(Post.R_ID, CacheManager.INSTANCE.getMaxId(Post.T_NAME));
            cv.put(Post.POSTABLE_ID, id);
            cv.put(Post.POSTABLE_TYPE, type);
            cv.put(Post.CREATOR_ID, AppPreferences.getUserId(this));
            cv.put(Post.TEXT, edtTitle.getText().toString());
            cv.put(Post.INSERTED, 1);
            cv.put(Post.ADMIN_ID, AppPreferences.getadminId(this));
            cv.put(Post.KEY, AppPreferences.getKey(this));
            cv.put(Post.CREATED_AT, CacheManager.INSTANCE.getDate());
            cv.put(Post.UPDATED_AT, CacheManager.INSTANCE.getDate());
            DbManager.getInstance().openDatabase();
            long id = DbManager.getInstance().insertAndGet(Post.T_NAME, cv);
            DbManager.getInstance().closeDatabase();
            if (id == -1) {
                Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
            } else {
                openUtilityDialog(this, "Post added successfully");
            }
        }

//        ContentValues cv = new ContentValues();
//        cv.put(CustomHearing.R_ID, CacheManager.INSTANCE.getMaxId(CustomHearing.T_NAME));
//        cv.put(CustomHearing.MATTER_ID, "");
//        cv.put(CustomHearing.ON_DATE, "");
//        cv.put(CustomHearing.JUDGE_NAME, "");
//        cv.put(CustomHearing.COURT_ROOM_NO, "");
//        cv.put(CustomHearing.CREATOR_ID, AppPreferences.getUserId(this));
//        cv.put(CustomHearing.ATTENDEE_ID, "");
//        cv.put(CustomHearing.INSERTED, 1);
//        cv.put(CustomHearing.ADMIN_ID, AppPreferences.getadminId(this));
//        cv.put(CustomHearing.KEY, AppPreferences.getKey(this));
//        cv.put(CustomHearing.CREATED_AT, CacheManager.INSTANCE.getDate());
//        cv.put(CustomHearing.UPDATED_AT, CacheManager.INSTANCE.getDate());
//        DbManager.getInstance().openDatabase();
//        long id = DbManager.getInstance().insertAndGet(CustomHearing.T_NAME, cv);
//        DbManager.getInstance().closeDatabase();
//        if (id == -1) {
//            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
//        } else {
//            openUtilityDialog(this, "Hearing added successfully");
//        }
    }




    private boolean valid() {
        if (edtTitle.getText().toString().trim().length() <= 0) {
            Toast.makeText(this, "Please enter Post text", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
