package com.litman.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.litman.R;
import com.litman.async.ServerConnector;
import com.litman.async.ServerConnectorGet;
import com.litman.interfaces.Const;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class ManageNotiActivity extends BaseActivity implements ServerConnectorGet.onAsyncTaskComplete, CompoundButton.OnCheckedChangeListener {

    private Switch swAll, swWeekly, swHearing, swOrder, swMatter, swTask, swN1, swN2, swN3, swN4, swN5, swN6, swN7, swN8;
    private ProgressDialog dialog;
    private Boolean isClicked = true;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_manage_noti);
        setupView();
    }

    private void setupView() {
        swAll = (Switch) findViewById(R.id.swAll);
        swWeekly = (Switch) findViewById(R.id.swWeekly);
        swHearing = (Switch) findViewById(R.id.swHDate);
        swOrder = (Switch) findViewById(R.id.swOrderRelease);
        swMatter = (Switch) findViewById(R.id.swMatterAct);
        swTask = (Switch) findViewById(R.id.swTaskUp);
        swN1 = (Switch) findViewById(R.id.swN1);
        swN2 = (Switch) findViewById(R.id.swN2);
        swN3 = (Switch) findViewById(R.id.swN3);
        swN4 = (Switch) findViewById(R.id.swN4);
        swN5 = (Switch) findViewById(R.id.swN5);
        swN6 = (Switch) findViewById(R.id.swN6);
        swN7 = (Switch) findViewById(R.id.swN7);
        swN8 = (Switch) findViewById(R.id.swN8);

        swAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isClicked) {
                    setAll(b);
                }
                isClicked = true;
            }
        });

        swWeekly.setOnCheckedChangeListener(this);
        swHearing.setOnCheckedChangeListener(this);
        swOrder.setOnCheckedChangeListener(this);
        swMatter.setOnCheckedChangeListener(this);
        swTask.setOnCheckedChangeListener(this);
        swN1.setOnCheckedChangeListener(this);
        swN2.setOnCheckedChangeListener(this);
        swN3.setOnCheckedChangeListener(this);
        swN4.setOnCheckedChangeListener(this);
        swN5.setOnCheckedChangeListener(this);
        swN6.setOnCheckedChangeListener(this);
        swN7.setOnCheckedChangeListener(this);
        swN8.setOnCheckedChangeListener(this);

        getData();
    }

    private void setAll(boolean b) {
        swWeekly.setChecked(b);
        swHearing.setChecked(b);
        swOrder.setChecked(b);
        swMatter.setChecked(b);
        swTask.setChecked(b);
        swN1.setChecked(b);
        swN2.setChecked(b);
        swN3.setChecked(b);
        swN4.setChecked(b);
        swN5.setChecked(b);
        swN6.setChecked(b);
        swN7.setChecked(b);
        swN8.setChecked(b);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.setGroupVisible(R.id.grp_search, false);
        menu.setGroupVisible(R.id.grp_save, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getData() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        String urlParameters = "";
        ServerConnectorGet noti = new ServerConnectorGet(urlParameters);
        noti.setDataDownloadListener(this);
        noti.execute(Const.BASE_URL + Const.CURRENT_USER, AppPreferences.getToken(this));
    }

    private void saveData() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        String urlParameters = "";
        try {
            urlParameters =
                    "weekly_digest=" + swWeekly.isChecked() +
                            "&remind_scheduled_hearing=" + swHearing.isChecked() +
                            "&notifiy_new_order=" + swOrder.isChecked() +
                            "&notifiy_matter_activity=" + swMatter.isChecked() +
                            "&notifiy_task_activity=" + swTask.isChecked() +
                            "&notifiy_someone_follow=" + swN1.isChecked() +
                            "&notifiy_people_following_post_added=" + swN2.isChecked() +
                            "&notifiy_someone_comment_on_post=" + swN3.isChecked() +
                            "&notifiy_someone_like_post=" + swN4.isChecked() +
                            "&notifiy_new_matter_added=" + swN5.isChecked() +
                            "&notifiy_new_contact_added=" + swN6.isChecked() +
                            "&notifiy_user_added=" + swN7.isChecked() +
                            "&notifiy_file_deleted=" + swN8.isChecked();
        } catch (Exception e) {
            e.printStackTrace();
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        ServerConnector noti = new ServerConnector(urlParameters);
        noti.setDataDownloadListener(listener);
        noti.execute(Const.BASE_URL + Const.UPDATE_SETTINGS, AppPreferences.getToken(this));
    }

    @Override
    public void OnResponse(String response) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        try {
            JSONObject jsonObject = new JSONObject(response);
            AppLogger.logD("noti response", response);
            if (jsonObject.has("error")) {
                String err = jsonObject.getString("error");
                AppUtils.openUtilityDialog(this, err);
            } else {
                JSONObject user = jsonObject.getJSONObject("user_setting");

                swWeekly.setChecked(getBoolean(user, "weekly_digest"));
                swHearing.setChecked(getBoolean(user, "remind_scheduled_hearing"));
                swOrder.setChecked(getBoolean(user, "notifiy_new_order"));
                swMatter.setChecked(getBoolean(user, "notifiy_matter_activity"));
                swTask.setChecked(getBoolean(user, "notifiy_task_activity"));
                swN1.setChecked(getBoolean(user, "notifiy_someone_follow"));
                swN2.setChecked(getBoolean(user, "notifiy_people_following_post_added"));
                swN3.setChecked(getBoolean(user, "notifiy_someone_comment_on_post"));
                swN4.setChecked(getBoolean(user, "notifiy_someone_like_post"));
                swN5.setChecked(getBoolean(user, "notifiy_new_matter_added"));
                swN6.setChecked(getBoolean(user, "notifiy_new_contact_added"));
                swN7.setChecked(getBoolean(user, "notifiy_user_added"));
                swN8.setChecked(getBoolean(user, "notifiy_file_deleted"));

                setAllChecked();
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppUtils.openUtilityDialog(this, e.getLocalizedMessage());
            AppLogger.logE("Login", e.getLocalizedMessage());
        }
    }

    private void setAllChecked() {
        if (swWeekly.isChecked() && swHearing.isChecked() && swOrder.isChecked() && swMatter.isChecked() &&
                swTask.isChecked() && swN1.isChecked() && swN2.isChecked() && swN3.isChecked() && swN4.isChecked()
                && swN5.isChecked() && swN6.isChecked() && swN7.isChecked() && swN8.isChecked()) {
            isClicked = false;
            swAll.setChecked(true);
        } else {
            isClicked = false;
            swAll.setChecked(false);
        }
    }

    private Boolean getBoolean(JSONObject ob, String createdAt) {
        if (!ob.isNull(createdAt)) {
            try {
                return ob.getBoolean(createdAt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    ServerConnector.onAsyncTaskComplete listener = new ServerConnector.onAsyncTaskComplete() {
        @Override
        public void OnResponse(String response) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("error")) {
                    String err = jsonObject.getString("error");
                    AppUtils.openUtilityDialog(ManageNotiActivity.this, err);
                } else {
                    AppUtils.openUtilityDialog(ManageNotiActivity.this, "Settings updated successfully");
                }
            } catch (Exception e) {
                e.printStackTrace();
                AppUtils.openUtilityDialog(ManageNotiActivity.this, e.getLocalizedMessage());
                AppLogger.logE("Login", e.getLocalizedMessage());
            }

        }
    };

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        setAllChecked();
    }
}
