package com.litman.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.litman.R;
import com.litman.calui.CalendarMonthFragmentForAddHearing;
import com.litman.calui.CalendarPage;
import com.litman.dao.AttendeeModel;
import com.litman.dao.CalenderHearingTaskModel;
import com.litman.dao.MatterInfo;
import com.litman.db.DbManager;
import com.litman.interfaces.Courts;
import com.litman.interfaces.CustomHearing;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class AddHearingActivity extends BaseActivity implements View.OnClickListener {

    private FrameLayout frameLayout;
    private FragmentManager fragmentManager;
    private TextView monthYearSpan;
    public int currentMonth, currentYear;
    String[] months = new DateFormatSymbols().getMonths();
    //   private MyPager myPager;
    private Cursor curs;
    private Context context;
    private Calendar calendar;
    private Button nextBtn, previousBtn;
    public TextView dateBelowTv, attendeeNameTv;
    private MatterInfo info;
    public String selectedDate = "";
    private AttendeeModel attendeeModel = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_add_hearing);
        frameLayout = (FrameLayout) findViewById(R.id.pager_data);
        fragmentManager = getSupportFragmentManager();
        previousBtn = (Button) findViewById(R.id.previousBtn);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        attendeeNameTv = (TextView) findViewById(R.id.attendeeNameTv);
        dateBelowTv = (TextView) findViewById(R.id.dateBelowTv);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getSerializable("info") != null) {
                info = (MatterInfo) getIntent().getExtras().getSerializable("info");
            }
        }

        setUpView();
        previousBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        attendeeNameTv.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.attendee, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_attendee) {
            addHearing();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addHearing() {
        if (attendeeModel == null) {
            AppUtils.openUtilityDialog(this, "Please select attendee");
            return;
        }

        if (dateBelowTv.getText().toString().equals("")) {
            AppUtils.openUtilityDialog(this, "Please select date");
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(CustomHearing.R_ID, CacheManager.INSTANCE.getMaxId(CustomHearing.T_NAME));
        cv.put(CustomHearing.MATTER_ID, info.ID);
        cv.put(CustomHearing.ON_DATE, selectedDate);
        cv.put(CustomHearing.JUDGE_NAME, info.JUDGE);
        cv.put(CustomHearing.COURT_ROOM_NO, info.COURT_ROOM);
        cv.put(CustomHearing.CREATOR_ID, AppPreferences.getUserId(this));
        cv.put(CustomHearing.ATTENDEE_ID, attendeeModel.getId());
        cv.put(CustomHearing.INSERTED, 1);
        cv.put(CustomHearing.ADMIN_ID, AppPreferences.getadminId(this));
        cv.put(CustomHearing.KEY, AppPreferences.getKey(this));
        cv.put(CustomHearing.CREATED_AT, CacheManager.INSTANCE.getDate());
        cv.put(CustomHearing.UPDATED_AT, CacheManager.INSTANCE.getDate());
        DbManager.getInstance().openDatabase();
        long id = DbManager.getInstance().insertAndGet(CustomHearing.T_NAME, cv);
        DbManager.getInstance().closeDatabase();
        if (id == -1) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
        } else {
            openUtilityDialog(this, "Hearing added successfully");
        }

    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setResult(Activity.RESULT_OK);
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void setUpView() {
        // myPager = new MyPager(getChildFragmentManager());
        CalendarPage.calendar = Calendar.getInstance(Locale.getDefault());
        CalendarPage.calendar.setFirstDayOfWeek(Calendar.SUNDAY);

        monthYearSpan = (TextView) findViewById(R.id.monthYearSpan);

        setUpCalendarView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previousBtn:
                previousYear();
                break;
            case R.id.nextBtn:
                nextYear();
                break;
            case R.id.attendeeNameTv:
                startActivityForResult(new Intent(AddHearingActivity.this, SelectAttendeeUserActivity.class).putExtra("M_ID", info.ID), 200);
                break;
        }
    }


    private int getYear() {
        return CalendarPage.calendar.get(Calendar.YEAR);
    }

    private int getMonth() {

        return CalendarPage.calendar.get(Calendar.MONTH);
    }

    private void nextYear() {

        CalendarPage.calendar.add(Calendar.MONTH, 1);
        // setUpCalendarView();
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        setUpCalendarView();

    }

    private void previousYear() {
        CalendarPage.calendar.add(Calendar.MONTH, -1);
        // setUpCalendarView();
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        setUpCalendarView();
    }

    private void setUpCalendarView() {
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        // pager_data.setAdapter(myPager);
        monthYearSpan.setText(months[getMonth()] + " " + getYear());
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

        SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


        String startDate = startDateFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());

        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                R.anim.reverse_enter).replace(frameLayout.getId(),
                CalendarMonthFragmentForAddHearing.newInstance(getMonth(), getYear(), getCourts(), startDate, endDate, setData("-1", "-1"))).commitAllowingStateLoss();
    }


    private ArrayList<CalenderHearingTaskModel> setData(String type, String courtId) {
        try {
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
            start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
            start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
            start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
            start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
            start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

            end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
            end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
            end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
            end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
            end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
            end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

            SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");


            String startDate = startDateFormat.format(start.getTimeInMillis());
            String endDate = simpleDateFormat.format(end.getTimeInMillis());

            AppLogger.logE(TAG, "StartDate: " + startDate);
            AppLogger.logE(TAG, "endDate: " + endDate);

            AppLogger.logE(TAG, "courseID: " + courtId);
            curs = CacheManager.INSTANCE.getCalenderHearing(getCourts(), AppPreferences.getUserId(context), startDate, endDate);

            ArrayList<CalenderHearingTaskModel> list = new ArrayList<>();

            if (curs != null && curs.moveToFirst()) {

                AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
                while (!curs.isAfterLast()) {
                    CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                    AppLogger.logE(TAG, "display_date: " + curs.getString(curs.getColumnIndex("display_date")));

                    AppLogger.logE(TAG, "total: " + curs.getInt(curs.getColumnIndex("total")));

                    calenderHearingTaskModel.setDateString(curs.getString(curs.getColumnIndex("display_date")));
                    calenderHearingTaskModel.setHearing(curs.getInt(curs.getColumnIndex("total")));
                    list.add(calenderHearingTaskModel);
                    curs.moveToNext();
                }
                curs.close();
            }
            Cursor taskcurs = CacheManager.INSTANCE.getTaskList(AppPreferences.getUserId(context), courtId.equals("-1") ? getCourts() : courtId, startDate, endDate);


            if (taskcurs != null && taskcurs.moveToFirst()) {

                AppLogger.logE("count", "" + taskcurs.getCount() + "=" + taskcurs.getColumnCount());
                while (!taskcurs.isAfterLast()) {
                    CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                    //    AppLogger.logE(TAG, "display_date: " + taskcurs.getString(taskcurs.getColumnIndex("display_date")));

//                    AppLogger.logE(TAG, "total: " + taskcurs.getInt(taskcurs.getColumnIndex("total")));
                    if (list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            CalenderHearingTaskModel calenderHearingTaskModel1 = list.get(i);

                            if (calenderHearingTaskModel1.getDateString().equals(taskcurs.getString(taskcurs.getColumnIndex("due_at")))) {
                                calenderHearingTaskModel1.setTask(true);
                                list.set(i, calenderHearingTaskModel1);
                            }
                        }

                    } else {
                        calenderHearingTaskModel.setDateString(taskcurs.getString(taskcurs.getColumnIndex("due_at")));
                        calenderHearingTaskModel.setTask(true);
                        list.add(calenderHearingTaskModel);

                    }

                    taskcurs.moveToNext();
                }
                taskcurs.close();
            }

            return list;

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            AppLogger.logE(TAG, "date: " + data.getExtras().toString());
            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {

                monthYearSpan.setText(months[getMonth()] + " " + getYear());

                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();

                start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
                start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
                start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
                start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
                start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
                start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

                end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
                end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
                end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
                end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
                end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
                end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

                SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");


                String startDate = startDateFormat.format(start.getTimeInMillis());
                String endDate = simpleDateFormat.format(end.getTimeInMillis());

                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                        R.anim.reverse_enter).replace(frameLayout.getId(),
                        CalendarMonthFragmentForAddHearing.newInstance(getMonth(), getYear(), getCourts(), startDate, endDate, setData("", getCourts()))).commitAllowingStateLoss();
                //  setData(type, court);
            } else {
                if (data.getExtras() != null) {
                    if (data.getExtras().getSerializable("data") != null) {
                        attendeeModel = (AttendeeModel) data.getExtras().getSerializable("data");
                        attendeeNameTv.setText(attendeeModel.getName());
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }


}