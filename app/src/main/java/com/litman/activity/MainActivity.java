package com.litman.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.async.ServerConnector;
import com.litman.calui.CalendarPage;
import com.litman.fragments.ContactUsFragment;
import com.litman.fragments.ContactsFragment;
import com.litman.fragments.DisplayBoardFragment;
import com.litman.fragments.FileFragment;
import com.litman.fragments.HomeFragment;
import com.litman.fragments.MattersFragment;
import com.litman.fragments.NotificationFragment;
import com.litman.fragments.SettingFragment;
import com.litman.fragments.SuggestMeFragment;
import com.litman.fragments.TaskFragment;
import com.litman.interfaces.Const;
import com.litman.manager.CacheManager;
import com.litman.service.DisplayBoardService;
import com.litman.utils.Alarm;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;
import com.litman.utils.CircleTransform;
import com.pusher.android.PusherAndroid;
import com.pusher.android.notifications.ManifestValidator;
import com.pusher.android.notifications.PushNotificationRegistration;
import com.pusher.android.notifications.tokens.PushNotificationRegistrationListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Locale;
import java.util.StringTokenizer;

import static com.litman.R.id.txtImg;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        HomeFragment.OnFragmentInteractionListener, ServerConnector.onAsyncTaskComplete {

    public SearchView searchView;
    private ProgressDialog dialog;
    private ImageView imgUser;
    private TextView txtName, txtSync, txtImage;
    private RelativeLayout relRefresh;
    private TextView txtCount, txtDrawerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        subScribePusher();

        if (!isMyServiceRunning(DisplayBoardService.class))
            startService(new Intent(this, DisplayBoardService.class));

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                updateCount();
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        imgUser = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageUser);
        txtImage = (TextView) navigationView.getHeaderView(0).findViewById(txtImg);
        txtName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtName);
        txtSync = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtSync);
        relRefresh = (RelativeLayout) navigationView.getHeaderView(0).findViewById(R.id.relRefresh);

        MenuItem item = navigationView.getMenu().findItem(R.id.nav_noti);
        FrameLayout rootView = (FrameLayout) item.getActionView();

        txtDrawerCount = (TextView) rootView.findViewById(R.id.view_alert_count_textview);

        txtSync.setText("Last Sync: " + AppPreferences.getFDate(this));

        (navigationView.getHeaderView(0).findViewById(R.id.relImg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(MainActivity.this, ProfileOtherActivity.class).putExtra("ID", AppPreferences.getUserId(MainActivity.this) + ""));
            }
        });

        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(MainActivity.this, ProfileOtherActivity.class).putExtra("ID", AppPreferences.getUserId(MainActivity.this) + ""));
            }
        });

        txtSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(MainActivity.this, ProfileOtherActivity.class).putExtra("ID", AppPreferences.getUserId(MainActivity.this) + ""));
            }
        });

        relRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                startActivityForResult(new Intent(MainActivity.this, SyncActivity.class).putExtra("ISNEW", false), 100);
            }
        });

        imgUser.setVisibility(View.GONE);
        txtImage.setVisibility(View.VISIBLE);
        txtImage.setText(getImageName());

        if (!AppPreferences.getThumb(this).equalsIgnoreCase("")) {
            Picasso.with(this).load(AppPreferences.getThumb(this)).transform(new CircleTransform()).into(imgUser, new Callback() {
                @Override
                public void onSuccess() {
                    txtImage.setVisibility(View.GONE);
                    imgUser.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {

                }
            });
        }

        txtName.setText(getCapitalizeFirstLetterFullName());

//        onNavigationItemSelected(navigationView.getMenu().getItem(0));
//        navigationView.setCheckedItem(R.id.nav_home);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content_main, HomeFragment.newInstance());
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().hasExtra("TYPE")) {
            getIntent().removeExtra("TYPE");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivityForResult(new Intent(MainActivity.this, SyncActivity.class), 300);
                }
            }, 300);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("TYPE")) {
            getIntent().removeExtra("TYPE");
            startActivityForResult(new Intent(this, SyncActivity.class), 300);
        }
    }

    private void subScribePusher() {
        final PusherAndroid pusher = new PusherAndroid(Const.KEY_PRODUCTION);
        String id = AppPreferences.getUserId(MainActivity.this);
        PushNotificationRegistration nativePusher = pusher.nativePusher();
        try {
            nativePusher.registerFCM(this, new PushNotificationRegistrationListener() {
                @Override
                public void onSuccessfulRegistration() {
                    PushNotificationRegistration nativePusher = pusher.nativePusher();
                    String id = AppPreferences.getUserId(MainActivity.this);
                    nativePusher.subscribe("private_push_" + id);
                }

                @Override
                public void onFailedRegistration(int statusCode, String response) {
                    System.out.println("A real sad day. Registration failed with code " + statusCode +
                            " " + response);
                }
            });

//            nativePusher.setFCMListener(new FCMPushNotificationReceivedListener() {
//                @Override
//                public void onMessageReceived(RemoteMessage remoteMessage) {
//                    Log.e("on","msg rec");
//                    if (remoteMessage.getData().size() > 0) {
//                        Log.e("FCM", "Message data payload: " + remoteMessage.getData());
//
//                        String dataa = remoteMessage.getData().toString();
//                        Map<String, String> data = remoteMessage.getData();
//
//                        String title = data.get("title");
//                        String body = data.get("message");
//                        //String time = data.get("sentTime");
//                    }
//                }
//            });
        } catch (ManifestValidator.InvalidManifestException e) {
            e.printStackTrace();
        }
    }

    private String getCapitalizeFirstLetterFullName() {
        String name = ((AppPreferences.getFname(this) + " " + AppPreferences.getMname(this)).trim() + " " + AppPreferences.getLname(this)).trim();

        StringBuilder builder = new StringBuilder();
        try {
            StringTokenizer token = new StringTokenizer(name, " ");
            while (token.hasMoreElements()) {
                String sub = token.nextToken();
                builder.append(sub.substring(0, 1).toUpperCase(Locale.getDefault()) + sub.substring(1).toLowerCase());
                builder.append(" ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString().trim();
    }

    private String getImageName() {
        String name = ((AppPreferences.getFname(this) + " " + AppPreferences.getMname(this)).trim() + " " + AppPreferences.getLname(this)).trim();
        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (searchView != null && !searchView.isIconified()) {
            searchView.setIconified(true);
            getFragmentManager().popBackStack();
        } else if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem alertMenuItem = menu.findItem(R.id.action_noti);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();

        txtCount = (TextView) rootView.findViewById(R.id.view_alert_count_textview);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });

        updateCount();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_search) {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
            return true;
        } else if (id == R.id.action_noti) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_main, NotificationFragment.newInstance());
            transaction.addToBackStack("notificationManager");
            transaction.commit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (id == R.id.nav_home) {
            if (manager.getBackStackEntryCount() > 0) {
                manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else if (id == R.id.nav_calendar) {
            transaction.replace(R.id.content_main, CalendarPage.newInstance());
            transaction.addToBackStack("calendar");
            transaction.commit();
        } else if (id == R.id.nav_display_board) {
            transaction.replace(R.id.content_main, DisplayBoardFragment.newInstance());
            transaction.addToBackStack("displayboard");
            transaction.commit();
        } else if (id == R.id.nav_matters) {
            transaction.replace(R.id.content_main, MattersFragment.newInstance());
            transaction.addToBackStack("matters");
            transaction.commit();
        } else if (id == R.id.nav_sug_matters) {
            transaction.replace(R.id.content_main, SuggestMeFragment.newInstance());
            transaction.addToBackStack("suggest_me");
            transaction.commit();
        } else if (id == R.id.nav_contacts) {
            transaction.replace(R.id.content_main, ContactsFragment.newInstance());
            transaction.addToBackStack("contactsMain");
            transaction.commit();
        } else if (id == R.id.nav_tasks) {
            transaction.replace(R.id.content_main, TaskFragment.newInstance());
            transaction.addToBackStack("tasks");
            transaction.commit();
        } else if (id == R.id.nav_files) {
            transaction.replace(R.id.content_main, FileFragment.newInstance());
            transaction.addToBackStack("files");
            transaction.commit();
        } else if (id == R.id.nav_settings) {
            transaction.replace(R.id.content_main, SettingFragment.newInstance());
            transaction.addToBackStack("Settings");
            transaction.commit();
        } else if (id == R.id.nav_noti) {
            transaction.replace(R.id.content_main, NotificationFragment.newInstance());
            transaction.addToBackStack("Notification");
            transaction.commit();
        } else if (id == R.id.nav_support) {
            transaction.replace(R.id.content_main, ContactUsFragment.newInstance());
            transaction.addToBackStack("ContactUs");
            transaction.commit();
        } else if (id == R.id.nav_sign_out) {
            if (AppUtils.checkInternetConnection(this)) {
                doLogout();
            } else {
                AppUtils.noInternetDialog(this);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void doLogout() {

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        String urlParameters = "";
        ServerConnector login = new ServerConnector(urlParameters);
        login.setDataDownloadListener(this);
        login.execute(Const.BASE_URL + Const.LOGOUT, AppPreferences.getToken(this));
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void OnResponse(String response) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        try {
            JSONObject jsonObject = new JSONObject(response);
            AppLogger.logD("logout response", response);
            if (jsonObject.has("error")) {
                new Alarm().cancelAlarm(this);
                String err = jsonObject.getString("error");
                AppPreferences.setLogin(this, false);
                AppPreferences.setPass(this, "");
                AppPreferences.setToken(this, "");
                stopService(new Intent(this, DisplayBoardService.class));
                openUtilityDialog(this, "Logged Out Successfully");
            } else {
                Boolean val = jsonObject.getBoolean("success");
                String msg = jsonObject.getString("info");
                if (val) {
                    new Alarm().cancelAlarm(this);
                    AppPreferences.setLogin(this, false);
                    AppPreferences.setPass(this, "");
                    AppPreferences.setToken(this, "");
                    openUtilityDialog(this, msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppUtils.openUtilityDialog(this, "" + e.getLocalizedMessage());
        }
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("request", "3001");
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            txtSync.setText("Last Sync: " + AppPreferences.getFDate(this));
        } else if (requestCode == 200) {
            txtSync.setText("Last Sync: " + AppPreferences.getFDate(this));
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_main, MattersFragment.newInstance());
            transaction.addToBackStack("matters");
            transaction.commit();
        } else if (requestCode == 300) {
            Log.e("request", "300");
            txtSync.setText("Last Sync: " + AppPreferences.getFDate(this));
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_main, NotificationFragment.newInstance());
            transaction.addToBackStack("notifications");
            transaction.commit();
        }
    }

    public void updateCount() {
        if (txtCount != null) {
            int count = CacheManager.INSTANCE.getNotiCount(AppPreferences.getUserId(this));
            if (count == 0) {
                txtCount.setVisibility(View.GONE);
                txtDrawerCount.setVisibility(View.GONE);
            } else {
                txtCount.setText(count + "");
                txtCount.setVisibility(View.VISIBLE);
                txtDrawerCount.setVisibility(View.VISIBLE);
                txtDrawerCount.setText(count + "");
            }
        }
    }

}
