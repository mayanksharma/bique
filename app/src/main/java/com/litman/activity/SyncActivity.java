package com.litman.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.litman.R;
import com.litman.async.MasterSyncAsyncTask;
import com.litman.async.ServerConnector;
import com.litman.db.DbManager;
import com.litman.interfaces.Const;
import com.litman.utils.Alarm;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;

import org.json.JSONObject;

public class SyncActivity extends AppCompatActivity implements View.OnClickListener, ServerConnector.onAsyncTaskComplete,
        MasterSyncAsyncTask.onAsyncTaskComplete {

    private Button btnRetry, btnSkip, btnLogout;
    private TextView txtTitle, txtMsg;
    private ProgressBar pgBar;

    private ProgressDialog dialog;

    private Boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sync);

        isNew = getIntent().getBooleanExtra("ISNEW", true);

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtMsg = (TextView) findViewById(R.id.txtMsg);

        btnRetry = (Button) findViewById(R.id.btnRetry);
        btnSkip = (Button) findViewById(R.id.btnSkip);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        pgBar = (ProgressBar) findViewById(R.id.pgBar);

        btnRetry.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

        setData();
    }

    private void setData() {
        txtTitle.setText("Uploading data to server....");
        txtMsg.setText("");
        if (AppUtils.checkInternetConnection(this)) {
            txtMsg.setVisibility(View.GONE);
            txtTitle.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.GONE);
            btnRetry.setVisibility(View.GONE);
            btnSkip.setVisibility(View.GONE);
            pgBar.setVisibility(View.VISIBLE);
            if (AppPreferences.isIncremental(this)) {
                MasterSyncAsyncTask task = new MasterSyncAsyncTask(this);
                task.setListener(this);
                task.execute();
            } else {
                DbManager.getInstance().openDatabase();
                DbManager.getInstance().deleteAll();
                DbManager.getInstance().closeDatabase();
                MasterSyncAsyncTask task = new MasterSyncAsyncTask(this);
                task.setListener(this);
                task.execute();
            }
        } else {
            txtMsg.setVisibility(View.VISIBLE);
            txtTitle.setVisibility(View.GONE);
            txtMsg.setText("Check you're online or try again later.");
            pgBar.setVisibility(View.INVISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.equals(btnRetry)) {
            setData();
        } else if (view.equals(btnSkip)) {
            startMain();
        } else if (view.equals(btnLogout)) {
            if (AppUtils.checkInternetConnection(this))
                doLogout();
            else
                AppUtils.noInternetDialog(this);
        }

    }

    private void startMain() {
        Alarm alarm = new Alarm();
        alarm.cancelAlarm(this);
        alarm.setAlarm(this);
        if (isNew) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            setResult(Activity.RESULT_OK);
        }
        finish();
    }

    private void doLogout() {
        if (dialog != null) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Please Wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        String urlParameters = "";
        ServerConnector login = new ServerConnector(urlParameters);
        login.setDataDownloadListener(this);
        login.execute(Const.BASE_URL + Const.LOGOUT, AppPreferences.getToken(this));
    }

    @Override
    public void OnResponse(String response) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        try {
            JSONObject jsonObject = new JSONObject(response);
            AppLogger.logD("logout response", response);
            if (jsonObject.has("error")) {
                String err = jsonObject.getString("error");
                new Alarm().cancelAlarm(this);
                AppPreferences.setLogin(this, false);
                AppPreferences.setPass(this, "");
                AppPreferences.setToken(this, "");
                openUtilityDialog(this, "Logged Out Successfully");
            } else {
                Boolean val = jsonObject.getBoolean("success");
                String msg = jsonObject.getString("info");
                if (val) {
                    new Alarm().cancelAlarm(this);
                    AppPreferences.setLogin(this, false);
                    AppPreferences.setPass(this, "");
                    AppPreferences.setToken(this, "");
                    openUtilityDialog(this, msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            openUtilityDialog(this, "" + e.getLocalizedMessage());
        }
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(SyncActivity.this, LoginActivity.class));
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void OnResponse(String msg, Boolean result) {
        if (!result) {
            txtMsg.setVisibility(View.VISIBLE);
            txtTitle.setVisibility(View.GONE);
            txtMsg.setText("" + msg);
            if (AppPreferences.isIncremental(this)) {
                btnSkip.setVisibility(View.VISIBLE);
            } else {
                btnSkip.setVisibility(View.GONE);
            }
            btnRetry.setVisibility(View.VISIBLE);
            pgBar.setVisibility(View.INVISIBLE);
            if (isNew)
                btnLogout.setVisibility(View.VISIBLE);
        } else if (result && msg.equals("")) {
            startMain();
        } else {
            txtMsg.setVisibility(View.GONE);
            txtTitle.setVisibility(View.VISIBLE);
            txtTitle.setText(msg + "");
        }
    }

}
