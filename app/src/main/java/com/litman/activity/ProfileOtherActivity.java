package com.litman.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.litman.R;
import com.litman.interfaces.Const;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class ProfileOtherActivity extends BaseActivity {

    private WebView web;
    private String userId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_profile);
        userId = getIntent().getStringExtra("ID");
        setUpView();
    }

    private void setUpView() {
        web = (WebView) findViewById(R.id.web);
        (findViewById(R.id.txtTitle)).setVisibility(View.GONE);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);

        if (AppUtils.checkInternetConnection(this)) {
            initializeWebView();
            web.loadUrl(Const.PROFILE_OTHER_1 + userId + Const.PROFILE_OTHER_2 + AppPreferences.getToken(this));
        } else {
            AppUtils.noInternetDialog(this);
        }

        hideKeyboard(web);
    }


    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);
//                if (url.endsWith("users/sign_in")) {
//                    finish();
//                    return true;
//                } else if (url.endsWith("users/sign_up")) {
//                    getSupportActionBar().setTitle("SignUp");
//                } else if(url.endsWith("/users/confirmation/new")){
//                    getSupportActionBar().setTitle("Resend");
//                }else {
//                    getSupportActionBar().setTitle("Forgot Password");
//                }
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });


    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}
