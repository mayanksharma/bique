package com.litman.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.litman.R;
import com.litman.utils.AppPreferences;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {

    private Runnable runnable;
    private Handler handler;

    Boolean isExtra=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);
        handler = new Handler();
        if (getIntent().getExtras()!=null) {
           isExtra=true;
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (runnable != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showSplashScreen();
        //  showAlert("Add In Matter");
    }

    private void showSplashScreen() {
        runnable = new Runnable() {
            public void run() {
                Intent intent;
                if (AppPreferences.isLogin(SplashActivity.this)) {
                    if(isExtra) {
                        intent = new Intent(getApplicationContext(), MainActivity.class).putExtra("TYPE",0);
                    }else{
                        intent = new Intent(getApplicationContext(), MainActivity.class);
                    }
                    startActivity(intent);
                } else {
                    intent = new Intent(getApplicationContext(),
                            LoginActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        };
        handler.postDelayed(runnable, 2500);
    }

    public void showAlert(String msg) {
        LayoutInflater inflater = LayoutInflater.from(SplashActivity.this);
        View view = inflater.inflate(R.layout.add_in_matter, null);
        new AlertDialog.Builder(SplashActivity.this)
                .setMessage(msg)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


}
