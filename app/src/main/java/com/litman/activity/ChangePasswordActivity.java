package com.litman.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.litman.R;
import com.litman.async.ServerConnector;
import com.litman.interfaces.Const;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;

import org.json.JSONObject;

import java.net.URLEncoder;


public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtOld, edtNew, edtConfirm;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_change_password);
        setupView();
    }

    private void setupView() {
        edtOld = (EditText) findViewById(R.id.edtOld);
        edtNew = (EditText) findViewById(R.id.edtNew);
        edtConfirm = (EditText) findViewById(R.id.edtConfirm);

        (findViewById(R.id.btnUpdate)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                validate();
                break;
        }
    }



    private void validate() {
        if (edtOld.getText().toString().trim().length() == 0) {
            AppUtils.openUtilityDialog(this, "Current password cannot be blank");
            return;
        }
        if (edtNew.getText().toString().trim().length() == 0) {
            AppUtils.openUtilityDialog(this, "New password cannot be blank");
            return;
        }
        if (edtConfirm.getText().toString().trim().length() == 0) {
            AppUtils.openUtilityDialog(this, "Confirm password cannot be blank");
            return;
        }
        if (!edtNew.getText().toString().equals(edtConfirm.getText().toString())) {
            AppUtils.openUtilityDialog(this, "Your New password and Confirm password do not match");
            return;
        }

        if (AppUtils.checkInternetConnection(this)) {
            changePassword();
        } else {
            AppUtils.noInternetDialog(this);
        }
    }

    private void changePassword() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        String urlParameters = "";
        try {
            urlParameters =
                    "old_password=" + URLEncoder.encode(edtOld.getText().toString().trim(), "UTF-8") +
                            "&password=" + URLEncoder.encode(edtNew.getText().toString().trim(), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        ServerConnector noti = new ServerConnector(urlParameters);
        noti.setDataDownloadListener(listener);
        noti.execute(Const.BASE_URL + Const.CURRENT_USER, AppPreferences.getToken(this));
    }

    ServerConnector.onAsyncTaskComplete listener = new ServerConnector.onAsyncTaskComplete() {
        @Override
        public void OnResponse(String response) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("error")) {
                    String err = jsonObject.getString("error");
                    AppUtils.openUtilityDialog(ChangePasswordActivity.this, err);
                } else {
                    openUtilityDialog(ChangePasswordActivity.this, "Password changed successfully");
                }
            } catch (Exception e) {
                e.printStackTrace();
                AppUtils.openUtilityDialog(ChangePasswordActivity.this, e.getLocalizedMessage());
                AppLogger.logE("Login", e.getLocalizedMessage());
            }

        }
    };

    public  void openUtilityDialog(final Context ctx,
                                         final String messageID) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((ChangePasswordActivity) ctx).finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
