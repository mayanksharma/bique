package com.litman.activity;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.MatterTaskCursorAdapter;
import com.litman.dao.MatterInfo;
import com.litman.interfaces.Matters;
import com.litman.manager.CacheManager;

public class MatterTaskActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private MatterInfo info;

    private Cursor curs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_task);
        info = (MatterInfo) getIntent().getSerializableExtra(Matters.T_NAME);
        setupView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (CacheManager.INSTANCE.isAddAllowed(this)) {
            menu.setGroupVisible(R.id.grp_main, true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivityForResult(new Intent(this, AddTaskActivity.class).putExtra("M_ID", info.ID), 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void setupView() {
        ((TextView) findViewById(R.id.txtName)).setText(info.PLAINTIFF + " Vs " + info.DEFENDANT);
        ((TextView) findViewById(R.id.txtCourt)).setText(info.COURT_NAME + ", Matter No: " + info.MATTER_NUMBER);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            mAdapter = new MatterTaskCursorAdapter(this, curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        setData();
    }


    public void setData() {
        curs = CacheManager.INSTANCE.getMatterTask(info.ID);
        if (mAdapter != null)
            ((MatterTaskCursorAdapter) mAdapter).changeCursor(curs);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setData();
    }
}
