package com.litman.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.litman.R;
import com.litman.async.ServerConnector;
import com.litman.dao.login.Login;
import com.litman.dao.login.UserSetting;
import com.litman.interfaces.Const;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URLEncoder;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ServerConnector.onAsyncTaskComplete {

    private EditText edtEmail, edtPass;
    private TextView txtForgot;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtPass);

        txtForgot = (TextView) findViewById(R.id.txtForgot);
        txtForgot.setPaintFlags(txtForgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        findViewById(R.id.btnSignin).setOnClickListener(this);
        findViewById(R.id.txtSignup).setOnClickListener(this);
        txtForgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignin:
                validate();
                break;
            case R.id.txtForgot:
                if (AppUtils.checkInternetConnection(this)) {
                    startActivity(new Intent(this, ForgotPasswordActivity.class));
                } else {
                    AppUtils.noInternetDialog(this);
                }
                break;
            case R.id.txtSignup:
                if (AppUtils.checkInternetConnection(this)) {
                    startActivity(new Intent(this, SignUpActivity.class));
                } else {
                    AppUtils.noInternetDialog(this);
                }
                break;
        }
    }

    private void validate() {
        if (AppUtils.checkInternetConnection(this)) {
            if (!AppUtils.isNotEmpty(edtEmail)) {
                AppUtils.openUtilityDialog(this, "Please enter email Id");
                return;
            }
            if (!AppUtils.isNotEmpty(edtPass)) {
                AppUtils.openUtilityDialog(this, "Password cannot be blan");
                return;
            }
            doLogin();
        } else {
            AppUtils.noInternetDialog(this);
        }
    }

    private void doLogin() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        String urlParameters = "";
        try {
            urlParameters =
                    "email=" + URLEncoder.encode(edtEmail.getText().toString().trim(), "UTF-8") +
                            "&password=" + URLEncoder.encode(edtPass.getText().toString().trim(), "UTF-8");// +
//                            "&deviceType=" + URLEncoder.encode("A", "UTF-8") +
//                            "&deviceToken=" + URLEncoder.encode("", "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        ServerConnector login = new ServerConnector(urlParameters);
        login.setDataDownloadListener(this);
        login.execute(Const.BASE_URL + Const.LOGIN, AppPreferences.getToken(this));
    }

    @Override
    public void OnResponse(String response) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        try {
            JSONObject jsonObject = new JSONObject(response);
            AppLogger.logD("login response", response);
            if (jsonObject.has("error")) {
                String err = jsonObject.getString("error");
                int code = jsonObject.getInt("status");
                AppUtils.openUtilityDialog(this, err);
            } else {
                Login login = new Gson().fromJson(response, Login.class);
                AppPreferences.setLogin(this, true);
                AppPreferences.setPass(this, edtPass.getText().toString().trim());
                if (login.getId().toString().equals(AppPreferences.getUserId(LoginActivity.this))) {
                    AppPreferences.setIncremental(this, true);
                } else {
                    AppPreferences.setDate(this,"");
                    AppPreferences.setFDate(this,"");
                    AppPreferences.setIncremental(this, false);
                }
                AppPreferences.setUserId(this, login.getId() + "");
                AppPreferences.setToken(this, login.getAuthenticationToken());
                AppPreferences.setFname(this, login.getFirstName());
                AppPreferences.setLname(this, login.getLastName());
                AppPreferences.setMname(this, login.getMiddleName());
                AppPreferences.setEmail(this, login.getEmail());
                AppPreferences.setCompany(this, login.getIsCompany());
                AppPreferences.setCname(this, login.getCompanyName());
                AppPreferences.setadminid(this, login.getCompanyAdminId());
                AppPreferences.setlocationid(this, login.getLocationId() != null ? login.getLocationId() : 0);
                AppPreferences.setArea(this, login.getAreaOfExpertise());
                AppPreferences.setContact(this, login.getContactNumber());
                AppPreferences.setCouncil(this, login.getBarCouncilNo());
                AppPreferences.setCtype(this, login.getCompanyType());
                AppPreferences.setRole(this, login.getRole());
                AppPreferences.setUrl(this, login.getUserSetting().getAvatar().getUrl());
                AppPreferences.setThumb(this, login.getUserSetting().getAvatar().getThumb().getUrl());
                UserSetting user = login.getUserSetting();
                AppPreferences.setwebnoti(this, user.getWebNotification());
                AppPreferences.setweekly(this, user.getWeeklyDigest());
                AppPreferences.setEmailaction(this, user.getEmailOnAction());
                AppPreferences.setDaybefore(this, user.getEmailADayBefore());
                AppPreferences.setEmailassigntask(this, user.getEmailOnAssignTask());
                AppPreferences.setEmailCommentntes(this, user.getEmailOnCommentsNotes());
                AppPreferences.setProfilepublic(this, user.getMakeProfilePublic());
                AppPreferences.setMatterPublic(this, user.getMakeMattersPublic());
                AppPreferences.setInfoPublic(this, user.getMakePersonalInfoPublic());
                AppPreferences.setNewHearing(this, user.getNotifiyNewHearing());
                AppPreferences.setNewOrder(this, user.getNotifiyNewOrder());
                AppPreferences.setNewSchedule(this, user.getRemindScheduledHearing());
                AppPreferences.setMatterActivity(this, user.getNotifiyMatterActivity());
                AppPreferences.setPostadded(this, user.getNotifiyPostAddedMatter());
                AppPreferences.setTaskActivity(this, user.getNotifiyTaskActivity());
                AppPreferences.setNoteadded(this, user.getNotifiyNoteAddedMatter());
                AppPreferences.setFileadded(this, user.getNotifiyFileAddedMatter());
                AppPreferences.setTimelineadded(this, user.getNotifiyCommentOnNotesPostTimelineAdded() != null ? user.getNotifiyCommentOnNotesPostTimelineAdded() : false);
                AppPreferences.setUseradded(this, user.getNotifiyUserAdded());
                AppPreferences.setCommentadded(this, user.getNotifiyCommentOnMatterAdded());
                AppPreferences.setTaskCompleted(this, user.getNotifiyTaskCompleted());
                AppPreferences.setSfollow(this, user.getNotifiySomeoneFollow());
                AppPreferences.setPeopleFadded(this, user.getNotifiyPeopleFollowingPostAdded());
                AppPreferences.setSComment(this, user.getNotifiySomeoneCommentOnPost());
                AppPreferences.setSLike(this, user.getNotifiySomeoneLikePost());
                AppPreferences.setNewMatter(this, user.getNotifiyNewMatterAdded());
                AppPreferences.setNewContact(this, user.getNotifiyNewContactAdded());

                startActivity(new Intent(this, SyncActivity.class));
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppUtils.openUtilityDialog(this, e.getLocalizedMessage());
            AppLogger.logE("Login", e.getLocalizedMessage());
        }
    }

}
