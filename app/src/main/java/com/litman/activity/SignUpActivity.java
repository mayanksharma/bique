package com.litman.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.litman.R;
import com.litman.interfaces.Const;
import com.litman.utils.AppUtils;

public class SignUpActivity extends AppCompatActivity {

    private WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        web = (WebView) findViewById(R.id.content_sign_up);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);

        if (AppUtils.checkInternetConnection(this)) {
            initializeWebView();
            web.loadUrl(Const.SIGN_UP);
        } else {
            AppUtils.noInternetDialog(this);
        }

        hideKeyboard(web);
    }

    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);
                if (url.endsWith("users/sign_in")) {
                    finish();
                    return true;
                } else if (url.endsWith("users/password/new")) {
                    getSupportActionBar().setTitle("Forgot Password");
                } else if (url.endsWith("/users/confirmation/new")) {
                    getSupportActionBar().setTitle("Resend");
                } else if (url.startsWith(Const.SUCCESS)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    openUtilityDialog(SignUpActivity.this,value);
                    return true;
                } else if (url.startsWith(Const.FAIL)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    AppUtils.openUtilityDialog(SignUpActivity.this,value);
                    view.loadUrl(Const.SIGN_UP);
                    return true;
                } else {
                    getSupportActionBar().setTitle("SignUp");
                }

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });


//        web.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                    WebView webView = (WebView) v;
//
//                    switch (keyCode) {
//                        case KeyEvent.KEYCODE_BACK:
//                            if (webView.canGoBack()) {
//                                webView.goBack();
//                                return true;
//                            }
//                            break;
//                    }
//                }
//
//                return false;
//            }
//        });

    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        final android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(ctx);
        dialog.setMessage(messageID);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((SignUpActivity) ctx).finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
