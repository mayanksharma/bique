package com.litman;

import android.content.Context;

import com.litman.async.ServerConnector;
import com.litman.interfaces.Const;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.net.URLEncoder;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements ServerConnector.onAsyncTaskComplete {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AppPreferences.setDeviceToken(getApplicationContext(), refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        UpdateToken(token, this);
    }

    public static void UpdateToken(String token, Context ctx) {


        String urlParameters = "";
        try {
            urlParameters ="device_type=" + URLEncoder.encode("android", "UTF-8")+
                    "&device_token=" + URLEncoder.encode(token, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ServerConnector tokenU = new ServerConnector(urlParameters);
        tokenU.execute(Const.BASE_URL + Const.TOKEN, AppPreferences.getToken(ctx));

    }

    @Override
    public void OnResponse(String response) {
        AppLogger.logE("TOKEN-", response);
    }

}

