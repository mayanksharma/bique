package com.litman.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.SearchUserInfo;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class SearchContactCursorAdapter extends CursorRecyclerAdapter<SearchContactCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = SearchContactCursorAdapter.class.getSimpleName();

    public SearchContactCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName, txtImg;
        private ImageView imgUser;
        private CardView cardView;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtImg = (TextView) v.findViewById(R.id.txtImg);
            imgUser = (ImageView) v.findViewById(R.id.imgUser);
            cardView = (CardView) v.findViewById(R.id.cardView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.search_user_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final SearchUserInfo rowItem = getData(cursor);

        holder.txtName.setText(CacheManager.INSTANCE.getCapitalizeFullName(rowItem.NAME));
        holder.txtImg.setText(CacheManager.INSTANCE.getCapitalizeName(rowItem.NAME));
        holder.txtImg.setVisibility(View.VISIBLE);
        holder.imgUser.setVisibility(View.GONE);
        Picasso.with(ctx).load(rowItem.THUMB).transform(new CircleTransform()).into(holder.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                holder.txtImg.setVisibility(View.GONE);
                holder.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FragmentManager manager = ((MainActivity) ctx).getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.replace(R.id.content_main, ProfileOtherFragment.newInstance(rowItem.ID+""));
//                transaction.addToBackStack("profileOther");
//                transaction.commit();
            }
        });
    }

    private SearchUserInfo getData(Cursor cursor) {
        SearchUserInfo info = new SearchUserInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Users.R_ID));
        info.NAME = cursor.getString(cursor.getColumnIndex("name"));
        info.URL = cursor.getString(cursor.getColumnIndex("avatarurl"));
        info.THUMB = cursor.getString(cursor.getColumnIndex("avatarthumburl"));
        return info;
    }


}