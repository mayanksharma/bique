package com.litman.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.HomeFeedInfo;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Locale;


public class CommentCursorAdapter extends CursorRecyclerAdapter<CommentCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = CommentCursorAdapter.class.getSimpleName();

    public CommentCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDesc, txtName, txtDate, txtImg;
        private ImageView imgUser;

        public ViewHolder(View v) {
            super(v);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtImg = (TextView) v.findViewById(R.id.txtImg);
            txtDate = (TextView) v.findViewById(R.id.txtDate);

            imgUser = (ImageView) v.findViewById(R.id.imgUser);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.comment_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final HomeFeedInfo rowItem = getData(cursor);

        ViewHolder vh = holder;
        configureViewHolder(vh, position, rowItem);
    }

    private HomeFeedInfo getData(Cursor cursor) {
        HomeFeedInfo info = new HomeFeedInfo();
        info.USER_ID = cursor.getInt(cursor.getColumnIndex("user_id"));
        info.F_NAME = cursor.getString(cursor.getColumnIndex("first_name"));
        info.M_NAME = cursor.getString(cursor.getColumnIndex("middle_name"));
        info.L_NAME = cursor.getString(cursor.getColumnIndex("last_name"));
        info.TEXT = cursor.getString(cursor.getColumnIndex("text"));
        info.URL = cursor.getString(cursor.getColumnIndex(Users.URL));
        info.THUMB = cursor.getString(cursor.getColumnIndex(Users.THUMB));
        info.D_DATE = cursor.getString(cursor.getColumnIndex("display_date"));
        return info;
    }

    private void configureViewHolder(final ViewHolder vh4, int position, HomeFeedInfo info) {
        vh4.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        vh4.imgUser.setVisibility(View.GONE);
        vh4.txtImg.setVisibility(View.VISIBLE);
        vh4.txtImg.setText(getImageName(info));

        Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh4.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                vh4.txtImg.setVisibility(View.GONE);
                vh4.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        vh4.txtName.setText(getCapitalizeFirstLetterFullName(info));
        vh4.txtDesc.setText(info.TEXT);
        vh4.txtDate.setText(CacheManager.INSTANCE.getFormattedDate(info.D_DATE));
    }

    private String getImageName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();
        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }


    private String getCapitalizeFirstLetterFullName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();

        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()) + data[0].substring(1).toLowerCase());
            }
            if (data.length > 1) {
                builder.append(" ");
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()) + data[data.length - 1].substring(1).toLowerCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }


}