package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.MatterDetailActivity;
import com.litman.activity.ProfileOtherActivity;
import com.litman.dao.HomeFeedInfo;

import java.util.ArrayList;


public class HearingListAdapter extends RecyclerView.Adapter<HearingListAdapter.ViewHolder> {

    private static final String TAG = "VerticalHomeAdapter";

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<HomeFeedInfo> data;


    public HearingListAdapter(Context context, ArrayList<HomeFeedInfo> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        View view;
        view = inflater.inflate(R.layout.hearing_list, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final HomeFeedInfo rowItem = data.get(position);

        holder.txtName.setText(rowItem.PLAINTIFF + " VS " + rowItem.DEFENDANT);
        holder.txtCourt.setText(rowItem.COURT_NAME + "\n" + rowItem.CASE_TYPE + " " + rowItem.MATTER_NO + "/" + rowItem.CASE_YEAR);
        holder.txtJudge.setText(rowItem.JUDGE_NAME);

        holder.txtItem.setText("Court Room: " + rowItem.COURT_ROOM_NO + "; Item: " + rowItem.ITEM_NO);

        if (rowItem.TYPE == 1) {
            holder.linAtt.setVisibility(View.VISIBLE);
            holder.txtAtende.setText(rowItem.getF_NAME() + " " + rowItem.getL_NAME());
            holder.txtAtende.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ProfileOtherActivity.class).putExtra("ID", rowItem.USER_ID + ""));
                }
            });
        } else {
            holder.linAtt.setVisibility(View.GONE);
        }

        holder.linMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MatterDetailActivity.class).putExtra("ID", rowItem.MATTER_ID));
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;


    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtCourt, txtJudge, txtItem, txtAtende;
        private LinearLayout linAtt, linMain;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtCourt = (TextView) v.findViewById(R.id.txtCourt);
            txtJudge = (TextView) v.findViewById(R.id.txtJudgeName);
            txtItem = (TextView) v.findViewById(R.id.txtItem);
            txtAtende = (TextView) v.findViewById(R.id.txtAttende);

            linAtt = (LinearLayout) v.findViewById(R.id.linhearing);
            linMain = (LinearLayout) v.findViewById(R.id.linMain);
        }
    }

}




