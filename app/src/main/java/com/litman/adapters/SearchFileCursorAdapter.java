package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.MatterDetailActivity;
import com.litman.activity.ProfileOtherActivity;
import com.litman.dao.FileInfo;
import com.litman.interfaces.Matters;
import com.litman.interfaces.UploadAssets;
import com.litman.interfaces.Uploads;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppUtils;


public class SearchFileCursorAdapter extends CursorRecyclerAdapter<SearchFileCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = SearchFileCursorAdapter.class.getSimpleName();

    public SearchFileCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtFile;
        private CardView cardView;

        public ViewHolder(View v) {
            super(v);
            txtFile = (TextView) v.findViewById(R.id.txtDesc);
            cardView = (CardView) v.findViewById(R.id.cardView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.search_file_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final FileInfo rowItem = getData(cursor);
        String name = CacheManager.INSTANCE.getCapitalizeFullName(rowItem.NAME);
        String due = " Added " + (!rowItem.URL.equals("") ? URLUtil.guessFileName(rowItem.URL, null, null) : "") + " in "
                + rowItem.CATEGORY + (rowItem.SUBCATEGORY.equals("") ? "" : " >> " + rowItem.SUBCATEGORY) + "\n"
                + CacheManager.INSTANCE.getFormattedDate(rowItem.DATE) + "\n" + rowItem.DESC;
        String forMatter = "For Matter";
        String vs =" "+ rowItem.PLAIN + " v/s " + rowItem.DIFFE;
        holder.txtFile.setMovementMethod(LinkMovementMethod.getInstance());

        String description = "";
        if (rowItem.TYPE.equalsIgnoreCase("Matter")) {
            description = name + due + "\n" + forMatter + vs;
        } else {
            description = name + due;
        }


        Spannable descriptionSpanned = new SpannableString(description);
        try {
            if (!AppUtils.isNotEmpty(description)) {
                holder.txtFile.setText("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(due), description.indexOf(due) + due.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx, ProfileOtherActivity.class).putExtra("ID", rowItem.U_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    if (AppUtils.checkInternetConnection(ctx)) {
                        openLinksToView(rowItem.URL);
                    } else {
                        AppUtils.noInternetDialog(ctx);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(due), description.indexOf(due) + due.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (rowItem.TYPE.equalsIgnoreCase("Matter")) {
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                        description.indexOf(forMatter), description.indexOf(forMatter) + forMatter.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)),
                        description.indexOf(vs), description.indexOf(vs) + vs.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        ctx.startActivity(new Intent(ctx,MatterDetailActivity.class).putExtra("ID",rowItem.UPLOADABLE_ID));
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setUnderlineText(false);
                    }
                }, description.indexOf(vs), description.indexOf(vs) + vs.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            holder.txtFile.setText(descriptionSpanned);
        } catch (Exception e) {
            AppLogger.logE(TAG, e.getLocalizedMessage());
            holder.txtFile.setText("");
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.checkInternetConnection(ctx)) {
                    openLinksToView(rowItem.URL);
                } else {
                    AppUtils.noInternetDialog(ctx);
                }
            }
        });
    }

    private FileInfo getData(Cursor cursor) {
        FileInfo info = new FileInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Uploads.R_ID));
        info.A_ID = cursor.getInt(cursor.getColumnIndex("asset_id"));
        info.U_ID = cursor.getInt(cursor.getColumnIndex(Uploads.CREATOR_ID));
        info.TYPE = cursor.getString(cursor.getColumnIndex(Uploads.U_TYPE));
        info.DESC = cursor.getString(cursor.getColumnIndex(Uploads.DESC));
        info.CATEGORY = cursor.getString(cursor.getColumnIndex(Uploads.CATEGORY));
        info.SUBCATEGORY = cursor.getString(cursor.getColumnIndex(Uploads.SUB));
        info.URL = cursor.getString(cursor.getColumnIndex(UploadAssets.URL));
        info.DATE = cursor.getString(cursor.getColumnIndex(Uploads.CREATED_AT));
        info.NAME = cursor.getString(cursor.getColumnIndex("name"));
        info.PLAIN = cursor.getString(cursor.getColumnIndex(Matters.PLAINTIFF));
        info.DIFFE = cursor.getString(cursor.getColumnIndex(Matters.DEFFENDANT));
        info.UPLOADABLE_ID = cursor.getInt(cursor.getColumnIndex(Uploads.U_ID));
        return info;
    }

    private void openLinksToView(String link) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            if (browserIntent.resolveActivity(ctx.getPackageManager()) != null) {
                ctx.startActivity(browserIntent);
            }
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while opening link", e);
        }
    }

}