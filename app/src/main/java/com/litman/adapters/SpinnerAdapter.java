package com.litman.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.litman.dao.CourtInfo;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<CourtInfo> {

    private Context context;
    private ArrayList<CourtInfo> info;

    public SpinnerAdapter(Context context, int textViewResourceId,
                          ArrayList<CourtInfo> info) {
        super(context, textViewResourceId, info);
        this.context = context;
        this.info = info;
    }

    public int getCount() {
        return info.size();
    }

    public CourtInfo getItem(int position) {
        return info.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(info.get(position).NAME);

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setPadding(5,10,5,10);
        label.setText(info.get(position).NAME);

        return label;
    }
}