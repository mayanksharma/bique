package com.litman.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.CourtInfo;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

public class MultiUserListAdapter extends RecyclerView.Adapter<MultiUserListAdapter.ViewHolder> {

    private Context ctx;
    private ArrayList<CourtInfo> origData;
    private HashSet<Integer> selected;

    public MultiUserListAdapter(Context ctx, ArrayList<CourtInfo> d, HashSet<Integer> selectedId) {
        this.ctx = ctx;
        this.origData = d;
        selected = selectedId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.user_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CourtInfo info = origData.get(position);
        holder.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        holder.imgUser.setVisibility(View.GONE);
        holder.txtImg.setVisibility(View.VISIBLE);
        holder.txtImg.setText(getImageName(info.NAME));

        if (info.URL != null && !info.URL.equals(""))
            Picasso.with(ctx).load(info.URL).transform(new CircleTransform()).into(holder.imgUser, new Callback() {
                @Override
                public void onSuccess() {
                    holder.txtImg.setVisibility(View.GONE);
                    holder.imgUser.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {

                }
            });
        holder.txtName.setText(info.NAME);
        holder.txtName.setChecked(info.SELECTED);
        holder.txtName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                addData(info.SELECTED, position);
            }
        });
    }

    private void addData(boolean b, int pos) {
        if (b) {
            origData.get(pos).SELECTED = false;
            selected.remove(origData.get(pos).ID);
        } else {
            origData.get(pos).SELECTED = true;
            selected.add(origData.get(pos).ID);
        }
        notifyDataSetChanged();
    }

    private String getImageName(String name) {
        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    @Override
    public int getItemCount() {
        return origData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckedTextView txtName;
        private TextView txtImg;
        private ImageView imgUser;

        public ViewHolder(View v) {
            super(v);
            txtName = (CheckedTextView) v.findViewById(R.id.txtName);
            txtImg = (TextView) v.findViewById(R.id.txtImg);

            imgUser = (ImageView) v.findViewById(R.id.imgUser);
        }
    }

    public String getAllSelected() {
        StringBuilder str = new StringBuilder();
        for (Integer val : selected) {
            str.append(val + ",");
        }
        String value = "";
        if (str.toString().length() > 1)
            value = str.substring(0, str.length() - 1);
        return value;
    }

}
