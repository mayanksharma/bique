package com.litman.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.litman.R;
import com.litman.activity.MainActivity;
import com.litman.activity.MatterDetailActivity;
import com.litman.activity.ProfileOtherActivity;
import com.litman.dao.HomeFeedInfo;
import com.litman.db.DbManager;
import com.litman.fragments.NotificationFragment;
import com.litman.fragments.TaskFragment;
import com.litman.interfaces.Notifications;
import com.litman.interfaces.NotificationsStatus;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Locale;
import java.util.StringTokenizer;


public class NotificationCursorAdapter extends CursorRecyclerAdapter<NotificationCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = NotificationCursorAdapter.class.getSimpleName();
    private NotificationFragment fragment;

    public NotificationCursorAdapter(Context context, Cursor cursor, NotificationFragment notificationFragment) {
        super(cursor);
        ctx = context;
        fragment = notificationFragment;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDesc, txtDate, txtImg;
        private ImageView imgUser;
        private LinearLayout linMain;
        private RelativeLayout relMain;

        public ViewHolder(View v) {
            super(v);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtImg = (TextView) v.findViewById(R.id.txtImg);
            txtDate = (TextView) v.findViewById(R.id.txtDate);

            imgUser = (ImageView) v.findViewById(R.id.imgUser);
            linMain = (LinearLayout) v.findViewById(R.id.linMain);
            relMain = (RelativeLayout) v.findViewById(R.id.relMain);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.notification_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final HomeFeedInfo rowItem = getData(cursor);

        ViewHolder vh = holder;
        configureViewHolder(vh, position, rowItem);

        vh.linMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues cv = new ContentValues();
                cv.put(NotificationsStatus.READ, 1);
                cv.put(NotificationsStatus.UPDATED, 1);
                cv.put(NotificationsStatus.KEY, AppPreferences.getKey(ctx));
                DbManager.getInstance().openDatabase();
                DbManager.getInstance().updateQuery(NotificationsStatus.T_NAME, cv,
                        "notification_id = ? and member_id = ? and read =?", new String[]{rowItem.ID + "", AppPreferences.getUserId(ctx), 0 + ""});
                DbManager.getInstance().closeDatabase();
                try {
                    ((MainActivity) ctx).updateCount();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (rowItem.URL.contains("/matters/")) {
                    String url = rowItem.URL;
                    url = url.substring(url.lastIndexOf("/") + 1, url.length());
                    int id = Integer.valueOf(url);
                    if (CacheManager.INSTANCE.isMatterExist(id)) {
                        ctx.startActivity(new Intent(ctx, MatterDetailActivity.class).putExtra("ID", id));
                    } else {
                        Toast.makeText(ctx, "Matter not found kindly sync and Try again!", Toast.LENGTH_SHORT).show();
                    }
                } else if (rowItem.URL.contains("/tasks/")) {
                    FragmentManager manager = ((MainActivity) ctx).getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.content_main, TaskFragment.newInstance());
                    transaction.addToBackStack("taskFragment");
                    transaction.commit();
                } else if (rowItem.URL.contains("/user/details/")) {
                    String url = rowItem.URL;
                    url = url.substring(url.lastIndexOf("/") + 1, url.length());
                    ctx.startActivity(new Intent(ctx, ProfileOtherActivity.class).putExtra("ID", url));
                } else if (rowItem.URL.endsWith("/user/details")) {
                    ctx.startActivity(new Intent(ctx, ProfileOtherActivity.class).putExtra("ID", AppPreferences.getUserId(ctx)));
                } else {
                    Toast.makeText(ctx, "No action found", Toast.LENGTH_SHORT).show();
                }
                fragment.setData();
                notifyDataSetChanged();
            }
        });
    }

    private HomeFeedInfo getData(Cursor cursor) {
        HomeFeedInfo info = new HomeFeedInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Notifications.R_ID));
        info.USER_ID = cursor.getInt(cursor.getColumnIndex("creator_id"));
        info.TEXT = cursor.getString(cursor.getColumnIndex(Notifications.TEXT));
        info.URL = cursor.getString(cursor.getColumnIndex(Notifications.LINK));
        info.D_DATE = cursor.getString(cursor.getColumnIndex(Notifications.UPDATED_AT));
        info.N_TYPE = cursor.getString(cursor.getColumnIndex(Notifications.NOTI_TYPE));
        info.N_ID = cursor.getInt(cursor.getColumnIndex(Notifications.NOTI_ID));
        info.READ = cursor.getInt(cursor.getColumnIndex(NotificationsStatus.READ));
        if (info.N_TYPE.equalsIgnoreCase("Post") || info.N_TYPE.equalsIgnoreCase("Task")
                || info.N_TYPE.equalsIgnoreCase("Upload") || info.N_TYPE.equalsIgnoreCase("Comment")
                || info.N_TYPE.equalsIgnoreCase("Note") || info.N_TYPE.equalsIgnoreCase("Matter")) {
            info.F_NAME = getUserName(info.USER_ID + "");
            info.THUMB = getThumb(info.USER_ID);
        } else if (info.N_TYPE.equalsIgnoreCase("Hearing") || info.N_TYPE.equalsIgnoreCase("Order")) {
            info.F_NAME = getCourtName(info.USER_ID + "");
        }

        return info;
    }

    private String getThumb(int user_id) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getImageThumb(user_id);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex(Users.THUMB)));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }

    private String getCourtName(String courtId) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getCourtName(courtId);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex("name")));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }

    private String getUserName(String userId) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getUserName(userId);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex("name")));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }

    private void configureViewHolder(final ViewHolder vh4, int position, HomeFeedInfo info) {
        if (info.N_TYPE.equalsIgnoreCase("Hearing") || info.N_TYPE.equalsIgnoreCase("Order")) {
            vh4.imgUser.setVisibility(View.GONE);
            vh4.txtImg.setVisibility(View.VISIBLE);
            vh4.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
            vh4.txtImg.setText(getCapitalizeName(info.F_NAME));
        } else {
            vh4.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
            vh4.imgUser.setVisibility(View.GONE);
            vh4.txtImg.setVisibility(View.VISIBLE);
            vh4.txtImg.setText(getImageName(info));

            if (info.THUMB != null && !info.THUMB.equals(""))
                Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh4.imgUser, new Callback() {
                    @Override
                    public void onSuccess() {
                        vh4.txtImg.setVisibility(View.GONE);
                        vh4.imgUser.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        }

        if (info.READ == 0) {
            vh4.relMain.setBackgroundColor(ContextCompat.getColor(ctx, R.color.grey_40));
        } else {
            vh4.relMain.setBackgroundColor(ContextCompat.getColor(ctx, android.R.color.transparent));
        }

        vh4.txtDesc.setText(info.TEXT);
        vh4.txtDate.setText(CacheManager.INSTANCE.getFormattedDate(info.D_DATE));
    }

    private String getImageName(HomeFeedInfo info) {
        String name = info.F_NAME;
        StringBuilder builder = new StringBuilder();
        if (info.F_NAME!=null && !info.F_NAME.equalsIgnoreCase("null") && name.length() > 0) {
            try {
                String[] data = name.split(" ");
                if (data.length > 0) {
                    builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
                }
                if (data.length > 1) {
                    builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
                }
                if (builder.toString().length() < 2) {
                    builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return builder.toString();
    }


    private String getCapitalizeFirstLetterFullName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();

        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()) + data[0].substring(1).toLowerCase());
            }
            if (data.length > 1) {
                builder.append(" ");
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()) + data[data.length - 1].substring(1).toLowerCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private String getCapitalizeName(String name) {
        StringBuilder builder = new StringBuilder();
        try {
            StringTokenizer token = new StringTokenizer(name, " ");
            while (token.hasMoreElements()) {
                builder.append(token.nextElement().toString().substring(0, 1).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }


}