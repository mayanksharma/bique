package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.CommentActivity;
import com.litman.activity.ProfileOtherActivity;
import com.litman.dao.HomeFeedInfo;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppUtils;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Locale;
import java.util.StringTokenizer;


public class MatterActivityCursorAdapter extends CursorRecyclerAdapter<MatterActivityCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = MatterActivityCursorAdapter.class.getSimpleName();

    public MatterActivityCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDesc, txtComments, txtImg;
        private RelativeLayout relAddComment, relComment;
        private ImageView imgUser;
        private View vLine;

        public ViewHolder(View v) {
            super(v);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtComments = (TextView) v.findViewById(R.id.txtComment);
            txtImg = (TextView) v.findViewById(R.id.txtImg);

            imgUser = (ImageView) v.findViewById(R.id.imgUser);
            vLine = (View) v.findViewById(R.id.vLine);

            relAddComment = (RelativeLayout) v.findViewById(R.id.relAddComment);
            relComment = (RelativeLayout) v.findViewById(R.id.relComment);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.home_feed_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final HomeFeedInfo rowItem = getData(cursor);

        ViewHolder vh = holder;

        vh.vLine.setVisibility(View.VISIBLE);
        vh.relComment.setVisibility(View.VISIBLE);

        vh.txtComments.setText(rowItem.COUNT + " Comments");

        int type = rowItem.TYPE;

        switch (type) {
            case 1:
                configureViewHolder1(vh, position, rowItem);
                break;
            case 2:
                configureViewHolder2(vh, position, rowItem);
                break;
            case 3:
                configureViewHolder3(vh, position, rowItem);
                break;
            case 4:
                configureViewHolder4(vh, position, rowItem);
                break;
            case 5:
                configureViewHolder5(vh, position, rowItem);
                break;
            case 6:
                configureViewHolder6(vh, position, rowItem);
                break;
            case 7:
                configureViewHolder7(vh, position, rowItem);
                break;
        }

        vh.relComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx,CommentActivity.class).putExtra("DAO",rowItem));
            }
        });
    }

    private void configureViewHolder7(final ViewHolder vh, int position, HomeFeedInfo info) {
        vh.vLine.setVisibility(View.GONE);
        vh.relComment.setVisibility(View.GONE);
        vh.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        vh.imgUser.setVisibility(View.GONE);
        vh.txtImg.setVisibility(View.VISIBLE);
        vh.txtImg.setText(getImageName(info));

        Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                vh.txtImg.setVisibility(View.GONE);
                vh.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        vh.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh.txtDesc.setText(getFiles(info));

    }

    private Spannable getFiles(final HomeFeedInfo rowItem) {
        String name = getCapitalizeFirstLetterFullName(rowItem);
        String due = " Uploaded ";
        String text = rowItem.TEXT;
        String t1 = " (";
        String file = (!rowItem.URL.equals("") ? URLUtil.guessFileName(rowItem.LINK, null, null) : "");
        String in = ") in ";
        String cat = rowItem.CAT + (rowItem.SUB.equals("") ? "" : " >> " + rowItem.SUB);
        String date = "\n" + CacheManager.INSTANCE.getFormattedDate(rowItem.D_DATE);


        String description = name + "\n" + due + text + t1 + file + in + cat + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(due), description.indexOf(due) + due.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(t1), description.indexOf(t1) + t1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(in), description.indexOf(in) + in.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(text), description.indexOf(text) + text.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(text), description.indexOf(text) + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss2 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss2, description.indexOf(cat), description.indexOf(cat) + cat.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(cat), description.indexOf(cat) + cat.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ProfileOtherActivity.class).putExtra("ID",rowItem.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    if (AppUtils.checkInternetConnection(ctx)) {
                        openLinksToView(rowItem.URL);
                    } else {
                        AppUtils.noInternetDialog(ctx);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(file), description.indexOf(file) + file.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(file), description.indexOf(file) + file.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss3 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss3, description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return descriptionSpanned;
    }

    private void configureViewHolder6(final ViewHolder vh, int position, HomeFeedInfo info) {
        vh.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        vh.imgUser.setVisibility(View.GONE);
        vh.txtImg.setVisibility(View.VISIBLE);
        vh.txtImg.setText(getImageName(info));

        Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                vh.txtImg.setVisibility(View.GONE);
                vh.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        vh.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh.txtDesc.setText(getNotes(info));
    }

    private Spannable getNotes(final HomeFeedInfo info) {
        String name = getCapitalizeFirstLetterFullName(info);
        String text = info.TEXT;

        String description = name + "\n" + text;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(text), description.indexOf(text) + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ProfileOtherActivity.class).putExtra("ID",info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return descriptionSpanned;
    }

    private void configureViewHolder5(final ViewHolder vh, int position, HomeFeedInfo info) {
        vh.vLine.setVisibility(View.GONE);
        vh.relComment.setVisibility(View.GONE);

        vh.imgUser.setVisibility(View.GONE);
        vh.txtImg.setVisibility(View.VISIBLE);
        vh.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh.txtImg.setText(getCapitalizeName(info.COURT_NAME));

        vh.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh.txtDesc.setText(getCaseHistory(info));
    }

    private Spannable getCaseHistory(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String text = info.TEXT;
        String please = " . Please ";
        String click = "Click";
        String tosee = " to see more";
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormatDate(info.D_DATE);

        String description;
        if (info.LINK.equals("")) {
            description = courtName + "\n" + text + on + date;
        } else {
            description = courtName + "\n" + text + please + click + tosee + on + date;
        }
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (!info.LINK.equals("")) {
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(please),
                        description.indexOf(please) + please.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(tosee),
                        description.indexOf(tosee) + tosee.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(click),
                        description.indexOf(click) + click.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        openLinksToView(info.LINK);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setColor(ContextCompat.getColor(ctx, R.color.grey));
                    }
                }, description.indexOf(click), description.indexOf(click) + click.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(on),
                    description.indexOf(on) + on.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(text),
                    description.indexOf(text) + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;

    }

    private HomeFeedInfo getData(Cursor cursor) {
        HomeFeedInfo info=new HomeFeedInfo();

        info.ID = cursor.getInt(cursor.getColumnIndex("remoteid"));
        info.MATTER_ID = cursor.getInt(cursor.getColumnIndex("matter_id"));
        info.USER_ID = cursor.getInt(cursor.getColumnIndex("user_id"));
        info.COURT_NAME = cursor.getString(cursor.getColumnIndex("court_name"));
        info.LINK = cursor.getString(cursor.getColumnIndex("link"));
        info.F_NAME = cursor.getString(cursor.getColumnIndex("first_name"));
        info.M_NAME = cursor.getString(cursor.getColumnIndex("middle_name"));
        info.L_NAME = cursor.getString(cursor.getColumnIndex("last_name"));
        info.TEXT = cursor.getString(cursor.getColumnIndex("text"));
        info.PLAINTIFF = cursor.getString(cursor.getColumnIndex("plaintiff"));
        info.DEFENDANT = cursor.getString(cursor.getColumnIndex("defendant"));
        info.COURT_ROOM_NO = cursor.getInt(cursor.getColumnIndex("court_room_no"));
        info.JUDGE_NAME = cursor.getString(cursor.getColumnIndex("judge_name"));
        info.ITEM_NO = cursor.getString(cursor.getColumnIndex("item_no"));
        info.TYPE = cursor.getInt(cursor.getColumnIndex("type"));
        info.D_DATE = cursor.getString(cursor.getColumnIndex("display_date"));
        info.S_DATE = cursor.getString(cursor.getColumnIndex("sort_date"));
        info.URL = cursor.getString(cursor.getColumnIndex(Users.URL));
        info.THUMB = cursor.getString(cursor.getColumnIndex(Users.THUMB));
        info.TYPE_NAME = cursor.getString(cursor.getColumnIndex("type_name"));
        info.A_F_NAME = cursor.getString(cursor.getColumnIndex("attendee_first_name"));
        info.A_M_NAME = cursor.getString(cursor.getColumnIndex("attendee_middle_name"));
        info.A_L_NAME = cursor.getString(cursor.getColumnIndex("attendee_last_name"));
        info.CAT = cursor.getString(cursor.getColumnIndex("category"));
        info.SUB = cursor.getString(cursor.getColumnIndex("subcategory"));
        info.A_ID = cursor.getInt(cursor.getColumnIndex("attendee_id"));
        info.COUNT = cursor.getInt(cursor.getColumnIndex("count"));
        return info;
    }

    private void configureViewHolder4(final ViewHolder vh4, int position, HomeFeedInfo info) {
        vh4.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        vh4.imgUser.setVisibility(View.GONE);
        vh4.txtImg.setVisibility(View.VISIBLE);
        vh4.txtImg.setText(getImageName(info));

        Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh4.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                vh4.txtImg.setVisibility(View.GONE);
                vh4.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        vh4.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh4.txtDesc.setText(getDescriptionCustomHearing(info));
    }

    private void configureViewHolder2(final ViewHolder vh2, int position, HomeFeedInfo info) {
        vh2.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.grey_round));
        vh2.imgUser.setVisibility(View.GONE);
        vh2.txtImg.setVisibility(View.VISIBLE);
        vh2.txtImg.setText(getImageName(info));

        Picasso.with(ctx).load(info.THUMB).transform(new CircleTransform()).into(vh2.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                vh2.txtImg.setVisibility(View.GONE);
                vh2.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        vh2.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh2.txtDesc.setText(getDescriptionPost(info));
    }

    private void configureViewHolder3(ViewHolder vh3, int position, HomeFeedInfo info) {
        vh3.imgUser.setVisibility(View.GONE);
        vh3.txtImg.setVisibility(View.VISIBLE);
        vh3.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh3.txtImg.setText(getCapitalizeName(info.COURT_NAME));
        vh3.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh3.txtDesc.setText(getDescriptionHearing(info));
    }

    private void configureViewHolder1(ViewHolder vh1, int position, HomeFeedInfo info) {
        vh1.imgUser.setVisibility(View.GONE);
        vh1.txtImg.setVisibility(View.VISIBLE);
        vh1.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh1.txtImg.setText(getCapitalizeName(info.COURT_NAME));
        vh1.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh1.txtDesc.setText(getDescriptionOrder(info));
    }


    private String getCapitalizeName(String name) {
        StringBuilder builder = new StringBuilder();
        try {
            StringTokenizer token = new StringTokenizer(name, " ");
            while (token.hasMoreElements()) {
                builder.append(token.nextElement().toString().substring(0, 1).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private String getImageName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();
        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private Spannable getDescriptionOrder(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String newOrder = "New Order";
        String releasedOn = " released on ";
        //String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);

        String description = courtName + "\n" + newOrder + releasedOn + on + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(newOrder),
                    description.indexOf(newOrder) + newOrder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    openLinksToView(info.LINK);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(ctx, R.color.grey));
                }
            }, description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(releasedOn),
                    description.indexOf(releasedOn) + releasedOn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//            final StyleSpan bss = new StyleSpan(Typeface.BOLD);
//            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//            descriptionSpanned.setSpan(new ClickableSpan() {
//                @Override
//                public void onClick(View v) {
//                    FragmentManager manager = ((MainActivity) ctx).getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.M_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
//                }
//
//                @Override
//                public void updateDrawState(TextPaint ds) {
//                    ds.setColor(ContextCompat.getColor(ctx, R.color.black));
//                }
//            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(on),
                    description.indexOf(on) + on.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionHearing(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String listedIn = "listed in court ";
        String court = "" + info.COURT_ROOM_NO;
        //String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String judgeName = " (" + info.JUDGE_NAME + ")";
        String item = " As Item No " + info.ITEM_NO;
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);
        String sDesc = listedIn + court + judgeName + item + on;
        String description = courtName + "\n" + listedIn + court + judgeName + item + on + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//            final StyleSpan bss = new StyleSpan(Typeface.BOLD);
//            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            descriptionSpanned.setSpan(new ClickableSpan() {
//                @Override
//                public void onClick(View v) {
//                    FragmentManager manager = ((MainActivity) ctx).getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.M_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
//                }
//
//                @Override
//                public void updateDrawState(TextPaint ds) {
//                    ds.setColor(ContextCompat.getColor(ctx, R.color.black));
//                }
//            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(sDesc), description.indexOf(sDesc) + sDesc.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan iss = new StyleSpan(Typeface.ITALIC);
            descriptionSpanned.setSpan(iss, description.indexOf(judgeName), description.indexOf(judgeName) + judgeName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionPost(final HomeFeedInfo info) {
        String userName = getCapitalizeFirstLetterFullName(info);
        String posted = "Posted - " + info.TEXT + " on ";
        //String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);

        String description = userName + "\n" + posted + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.colorAccent)), description.indexOf(userName),
                    description.indexOf(userName) + userName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ProfileOtherActivity.class).putExtra("ID",info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(ctx, R.color.colorAccent));
                }
            }, description.indexOf(userName), description.indexOf(userName) + userName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(posted),
                    description.indexOf(posted) + posted.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionCustomHearing(final HomeFeedInfo info) {

        String userName = getCapitalizeFirstLetterFullName(info);
        String addedText = "Added a new manual hearing ";
        // String vsString = info.PLAINTIFF + " v/s " + info.DEFENDANT;
        String t_for = " for " + CacheManager.INSTANCE.getFormattedDate(info.D_DATE) + " (" + info.JUDGE_NAME + ")" + " to be attended by";
        String name = " " + userName;

        String description = userName + "\n" + addedText + t_for + name;

        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ProfileOtherActivity.class).putExtra("ID",info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(ctx, R.color.colorAccent));
                }
            }, description.indexOf(userName), description.indexOf(userName) + userName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ProfileOtherActivity.class).putExtra("ID",info.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(ctx, R.color.colorAccent));
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(addedText),
                    description.indexOf(addedText) + addedText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//            descriptionSpanned.setSpan(new ClickableSpan() {
//                @Override
//                public void onClick(View v) {
//                    FragmentManager manager = ((MainActivity) ctx).getSupportFragmentManager();
//                    FragmentTransaction transaction = manager.beginTransaction();
//                    transaction.replace(R.id.content_main, MatterDetailFragment.newInstance(info.M_ID));
//                    transaction.addToBackStack("matterDetail");
//                    transaction.commit();
//                }
//
//                @Override
//                public void updateDrawState(TextPaint ds) {
//                    ds.setColor(ContextCompat.getColor(ctx, R.color.black));
//                }
//            }, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//            final StyleSpan bss = new StyleSpan(Typeface.BOLD);
//            descriptionSpanned.setSpan(bss, description.indexOf(vsString), description.indexOf(vsString) + vsString.length(),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(t_for),
                    description.indexOf(t_for) + t_for.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private String getCapitalizeFirstLetterFullName(HomeFeedInfo info) {
        String name = (((info.F_NAME.length() > 0) ? info.F_NAME + " " : "") + ((info.M_NAME.length() > 0) ? info.M_NAME + " " : "")
                + ((info.L_NAME.length() > 0) ? info.L_NAME + " " : "")).trim();

        StringBuilder builder = new StringBuilder();
        try {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()) + data[0].substring(1).toLowerCase());
            }
            if (data.length > 1) {
                builder.append(" ");
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()) + data[data.length - 1].substring(1).toLowerCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private void openLinksToView(String link) {
        AppLogger.logD(TAG, "Called --> openLinksToView(String link)");
        AppLogger.logD(TAG, "Link clicked : " + link);
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            if (browserIntent.resolveActivity(ctx.getPackageManager()) != null) {
                ctx.startActivity(browserIntent);
            }
            AppLogger.logD(TAG, "Called --> openLinksToView(String link) completed");
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while opening link", e);
        }
    }


}