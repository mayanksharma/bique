package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.MatterDetailActivity;
import com.litman.activity.ProfileOtherActivity;
import com.litman.dao.SearchTaskInfo;
import com.litman.interfaces.Matters;
import com.litman.interfaces.Tasks;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppUtils;

/**
 * Created by Namit on 23-04-2017.
 */

public class TaskListAdapter extends CursorRecyclerAdapter<TaskListAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = SearchTaskCursorAdapter.class.getSimpleName();

    public TaskListAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName, txtType, txtMatter;
        private CheckBox chkBox;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtType = (TextView) v.findViewById(R.id.txtType);
            txtMatter = (TextView) v.findViewById(R.id.txtMatter);
            chkBox = (CheckBox) v.findViewById(R.id.chkBox);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.task_list_items, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final SearchTaskInfo rowItem = getData(cursor);

        String due = CacheManager.INSTANCE.getFormattedDate(rowItem.DUE_AT) + " - " + rowItem.TITLE + " (Res - ";
        String name = rowItem.NAME;
        String last = ")";

        holder.txtName.setMovementMethod(LinkMovementMethod.getInstance());

        String description = due + name + last;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            if (!AppUtils.isNotEmpty(description)) {
                holder.txtName.setText("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(due), description.indexOf(due) + due.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(last), description.indexOf(last) + last.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx, ProfileOtherActivity.class).putExtra("ID", rowItem.USER_ID + ""));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.txtName.setText(descriptionSpanned);
        } catch (Exception e) {
            AppLogger.logE(TAG, e.getLocalizedMessage());
            holder.txtName.setText("");
        }

        if (rowItem.MATTER_ID != 0) {
            holder.txtMatter.setVisibility(View.VISIBLE);
            holder.txtMatter.setMovementMethod(LinkMovementMethod.getInstance());
            holder.txtMatter.setText(getFormattedMatter(rowItem));
        } else {
            holder.txtMatter.setVisibility(View.GONE);
        }

        holder.txtType.setText(rowItem.CATEGORY);

        if (rowItem.COMPLETED == 1) {
//            holder.txtMatter.setPaintFlags(holder.txtMatter.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.txtName.setPaintFlags(holder.txtName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.txtType.setPaintFlags(holder.txtType.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.chkBox.setChecked(true);
            holder.chkBox.setClickable(false);
        } else {
//            holder.txtMatter.setPaintFlags(holder.txtMatter.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
//            holder.txtName.setPaintFlags(holder.txtName.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
//            holder.txtType.setPaintFlags(holder.txtType.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.chkBox.setChecked(false);
            holder.chkBox.setClickable(false);
        }

    }

    private Spannable getFormattedMatter(final SearchTaskInfo rowItem) {
        String due = "For Matter - ";
        String name = rowItem.PLAINTIFF + " v/s " + rowItem.DEFENDANT;


        String description = due + name;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            if (!AppUtils.isNotEmpty(description)) {
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)),
                    description.indexOf(due), description.indexOf(due) + due.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx, MatterDetailActivity.class).putExtra("ID", rowItem.MATTER_ID));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setUnderlineText(false);
                    ds.setColor(ContextCompat.getColor(ctx, R.color.grey));
                }
            }, description.indexOf(name), description.indexOf(name) + name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            return descriptionSpanned;
        } catch (Exception e) {
            AppLogger.logE(TAG, e.getLocalizedMessage());
            return new SpannableString("");
        }
    }

    private SearchTaskInfo getData(Cursor cursor) {
        SearchTaskInfo info = new SearchTaskInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Tasks.R_ID));
        info.MATTER_ID = cursor.getInt(cursor.getColumnIndex(Tasks.MATTER_ID));
        info.COMPLETED = cursor.getInt(cursor.getColumnIndex(Tasks.COMPLETED));
        info.TITLE = cursor.getString(cursor.getColumnIndex(Tasks.TITLE));
        info.DUE_AT = cursor.getString(cursor.getColumnIndex(Tasks.DUE_AT));
        info.CATEGORY = cursor.getString(cursor.getColumnIndex(Tasks.CATEGORY));
        info.PLAINTIFF = cursor.getString(cursor.getColumnIndex(Matters.PLAINTIFF));
        info.DEFENDANT = cursor.getString(cursor.getColumnIndex(Matters.DEFFENDANT));
        info.USER_ID = cursor.getInt(cursor.getColumnIndex(Tasks.ASSIGNED_TO));
        info.NAME = cursor.getString(cursor.getColumnIndex("name"));

        return info;
    }


}
