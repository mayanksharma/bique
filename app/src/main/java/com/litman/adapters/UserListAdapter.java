package com.litman.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.litman.R;
import com.litman.dao.CourtInfo;

import java.util.ArrayList;
import java.util.HashSet;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private Context ctx;
    private ArrayList<CourtInfo> origData;
    private HashSet<Integer> selected;


    public UserListAdapter(Context ctx, ArrayList<CourtInfo> d, HashSet<Integer> selectedId) {
        this.ctx = ctx;
        this.origData = d;
        selected = selectedId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.court_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CourtInfo info = origData.get(position);
        holder.txtName.setText(info.NAME);
        holder.txtName.setChecked(info.SELECTED);
        holder.txtName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!info.SELECTED) {
                    checkAll(position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    private void checkAll(int position) {
        setAllSelected(false);
        origData.get(position).SELECTED = true;
        selected.add(origData.get(position).ID);
//        if (origData.get(position).SELECTED) {
//            origData.get(position).SELECTED = false;
//            origData.get(0).SELECTED = false;
//            selected.remove(-1);
//            selected.remove(origData.get(position).ID);
//        } else {
//            origData.get(position).SELECTED = true;
//            selected.add(origData.get(position).ID);
//            if (selected.size() == origData.size() - 1) {
//                selected.add(-1);
//                origData.get(0).SELECTED = true;
//            }
//        }
    }

    @Override
    public int getItemCount() {
        return origData.size();
    }

    private void setAllSelected(Boolean SELECTED) {
        for (CourtInfo in : origData) {
            in.SELECTED = SELECTED;
            if (SELECTED) {
                selected.add(in.ID);
            }
        }
        if (!SELECTED) {
            selected.clear();
        }

        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckedTextView txtName;

        public ViewHolder(View v) {
            super(v);
            txtName = (CheckedTextView) v.findViewById(R.id.txtName);
        }
    }

    public String getAllSelected() {
        StringBuilder str = new StringBuilder();
        for (Integer val : selected) {
            str.append(val + ",");
        }
        String value = "";
        if (str.toString().length() > 1)
            value = str.substring(0, str.length() - 1);
        return value;
    }

}
