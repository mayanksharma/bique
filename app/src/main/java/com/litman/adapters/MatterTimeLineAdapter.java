package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.HomeFeedInfo;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppUtils;

import static com.litman.interfaces.Const.TYPE_API;
import static com.litman.interfaces.Const.TYPE_HEARING;
import static com.litman.interfaces.Const.TYPE_ORDER;


public class MatterTimeLineAdapter extends CursorRecyclerAdapter<MatterTimeLineAdapter.ViewHolder> {

    private Context ctx;

    private final String TAG = MatterTimeLineAdapter.class.getSimpleName();


    public MatterTimeLineAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDesc, txtImg;
        private ImageView imgUser;

        public ViewHolder(View v) {
            super(v);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtImg = (TextView) v.findViewById(R.id.txtImg);
            imgUser = (ImageView) v.findViewById(R.id.imgUser);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.matter_timeline_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final HomeFeedInfo rowItem = getData(cursor);

        ViewHolder vh = holder;

        int type = rowItem.TYPE;

        switch (type) {
            case TYPE_API:
                configureViewHolder5(vh, position, rowItem);
                break;
            case TYPE_HEARING:
                configureViewHolder3(vh, position, rowItem);
                break;
            case TYPE_ORDER:
                configureViewHolder1(vh, position, rowItem);
                break;
        }
    }

    private HomeFeedInfo getData(Cursor cursor) {
        HomeFeedInfo info = new HomeFeedInfo();
        info.MATTER_ID = cursor.getInt(cursor.getColumnIndex("matter_id"));
        info.USER_ID = cursor.getInt(cursor.getColumnIndex("user_id"));
        info.COURT_NAME = cursor.getString(cursor.getColumnIndex("court_name"));
        info.LINK = cursor.getString(cursor.getColumnIndex("link"));
        info.PLAINTIFF = cursor.getString(cursor.getColumnIndex("plaintiff"));
        info.DEFENDANT = cursor.getString(cursor.getColumnIndex("defendant"));
        info.COURT_ROOM_NO = cursor.getInt(cursor.getColumnIndex("court_room_no"));
        info.JUDGE_NAME = cursor.getString(cursor.getColumnIndex("judge_name"));
        info.ITEM_NO = cursor.getString(cursor.getColumnIndex("item_no"));
        info.TYPE = cursor.getInt(cursor.getColumnIndex("type"));
        info.TEXT=cursor.getString(cursor.getColumnIndex("text"));
        info.D_DATE = cursor.getString(cursor.getColumnIndex("display_date"));
        return info;
    }

    private void configureViewHolder5(final ViewHolder vh, int position, HomeFeedInfo info) {
        vh.imgUser.setVisibility(View.GONE);
        vh.txtImg.setVisibility(View.VISIBLE);
        vh.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh.txtImg.setText(CacheManager.INSTANCE.getCapitalizeName(info.COURT_NAME));

        vh.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh.txtDesc.setText(getCaseHistory(info));
    }


    private void configureViewHolder3(ViewHolder vh3, int position, HomeFeedInfo info) {
        vh3.imgUser.setVisibility(View.GONE);
        vh3.txtImg.setVisibility(View.VISIBLE);
        vh3.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh3.txtImg.setText(CacheManager.INSTANCE.getCapitalizeName(info.COURT_NAME));
        vh3.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh3.txtDesc.setText(getDescriptionHearing(info));
    }

    private void configureViewHolder1(ViewHolder vh1, int position, HomeFeedInfo info) {
        vh1.imgUser.setVisibility(View.GONE);
        vh1.txtImg.setVisibility(View.VISIBLE);
        vh1.txtImg.setBackground(ContextCompat.getDrawable(ctx, R.drawable.green_round));
        vh1.txtImg.setText(CacheManager.INSTANCE.getCapitalizeName(info.COURT_NAME));
        vh1.txtDesc.setMovementMethod(LinkMovementMethod.getInstance());
        vh1.txtDesc.setText(getDescriptionOrder(info));
    }


    private Spannable getDescriptionOrder(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String newOrder = "New Order";
        String releasedOn = " released on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);

        String description = courtName + "\n" + newOrder + releasedOn + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(newOrder),
                    description.indexOf(newOrder) + newOrder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View v) {
                    openLinksToView(info.LINK);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ContextCompat.getColor(ctx, R.color.grey));
                }
            }, description.indexOf(newOrder), description.indexOf(newOrder) + newOrder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(releasedOn),
                    description.indexOf(releasedOn) + releasedOn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getDescriptionHearing(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String listedIn = "is listed in court ";
        String court = "" + info.COURT_ROOM_NO;
        String judgeName = " (" + info.JUDGE_NAME + ")";
        String item = " As Item No " + info.ITEM_NO;
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormattedDate(info.D_DATE);
        String sDesc = listedIn + court + judgeName + item + on;
        String description = courtName + "\n" + listedIn + court + judgeName + item + on + date;
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(sDesc), description.indexOf(sDesc) + sDesc.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            final StyleSpan iss = new StyleSpan(Typeface.ITALIC);
            descriptionSpanned.setSpan(iss, description.indexOf(judgeName), description.indexOf(judgeName) + judgeName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;
    }

    private Spannable getCaseHistory(final HomeFeedInfo info) {
        String courtName = info.COURT_NAME + ": ";
        String text = info.TEXT;
        String please = " . Please ";
        String click = "Click";
        String tosee = " to see more";
        String on = " on ";
        String date = CacheManager.INSTANCE.getFormatDate(info.D_DATE);

        String description;
        if (info.LINK.equals("")) {
            description = courtName + "\n" + text + on + date;
        } else {
            description = courtName + "\n" + text + please + click + tosee + on + date;
        }
        Spannable descriptionSpanned = new SpannableString(description);
        try {
            AppLogger.logD(TAG, description);
            if (!AppUtils.isNotEmpty(description)) {
                AppLogger.logD(TAG, "Getting null description");
                return new SpannableString("");
            }
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.green)), description.indexOf(courtName),
                    description.indexOf(courtName) + courtName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (!info.LINK.equals("")) {
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(please),
                        description.indexOf(please) + please.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(tosee),
                        description.indexOf(tosee) + tosee.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(click),
                        description.indexOf(click) + click.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                descriptionSpanned.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        openLinksToView(info.LINK);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setColor(ContextCompat.getColor(ctx, R.color.grey));
                    }
                }, description.indexOf(click), description.indexOf(click) + click.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.grey)), description.indexOf(date),
                    description.indexOf(date) + date.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final StyleSpan bss1 = new StyleSpan(Typeface.BOLD);
            descriptionSpanned.setSpan(bss1, description.indexOf(date), description.indexOf(date) + date.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(on),
                    description.indexOf(on) + on.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ctx, R.color.black)), description.indexOf(text),
                    description.indexOf(text) + text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while getDescriptionOrder(HomeFeedInfo info) : " + info.toString(), e);
            return new SpannableString("");
        }
        return descriptionSpanned;

    }

    private void openLinksToView(String link) {
        AppLogger.logD(TAG, "Called --> openLinksToView(String link)");
        AppLogger.logD(TAG, "Link clicked : " + link);
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            ctx.startActivity(browserIntent);
            AppLogger.logD(TAG, "Called --> openLinksToView(String link) completed");
        } catch (Exception e) {
            AppLogger.logE(TAG, "Exception while opening link", e);
        }
    }


}