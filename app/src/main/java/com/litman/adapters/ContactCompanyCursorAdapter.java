package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.ViewPersonCompanyActivity;
import com.litman.dao.SearchUserInfo;
import com.litman.interfaces.CompanyContacts;
import com.litman.interfaces.Users;
import com.litman.manager.CacheManager;
import com.litman.utils.AppUtils;
import com.litman.utils.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class ContactCompanyCursorAdapter extends CursorRecyclerAdapter<ContactCompanyCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = ContactCompanyCursorAdapter.class.getSimpleName();

    public ContactCompanyCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName, txtImg, txtCName, txtEmail;
        private ImageView imgUser, imgCall;
        private CardView cardView;

        public ViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtImg = (TextView) v.findViewById(R.id.txtImg);
            txtCName = (TextView) v.findViewById(R.id.txtCName);
            txtEmail = (TextView) v.findViewById(R.id.txtEmail);
            imgUser = (ImageView) v.findViewById(R.id.imgUser);
            imgCall = (ImageView) v.findViewById(R.id.imgCall);
            cardView = (CardView) v.findViewById(R.id.cardView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.person_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final SearchUserInfo rowItem = getData(cursor);

        holder.txtName.setText(CacheManager.INSTANCE.getCapitalizeFullName(rowItem.NAME));

        holder.txtCName.setVisibility(View.GONE);

        if (!rowItem.TELEPHONE.equals("")) {
            holder.imgCall.setVisibility(View.VISIBLE);
        } else {
            holder.imgCall.setVisibility(View.GONE);
        }

        if (!rowItem.EMAIL.equals("")) {
            holder.txtEmail.setVisibility(View.VISIBLE);
            holder.txtEmail.setText(rowItem.EMAIL);
        } else {
            holder.txtEmail.setVisibility(View.GONE);
        }

        holder.txtImg.setText(CacheManager.INSTANCE.getCapitalizeName(rowItem.NAME));

        holder.txtImg.setVisibility(View.VISIBLE);
        holder.imgUser.setVisibility(View.GONE);
        Picasso.with(ctx).load(rowItem.THUMB).transform(new CircleTransform()).into(holder.imgUser, new Callback() {
            @Override
            public void onSuccess() {
                holder.txtImg.setVisibility(View.GONE);
                holder.imgUser.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {
            }
        });

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.dialNo(ctx, rowItem.TELEPHONE);
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx, ViewPersonCompanyActivity.class).putExtra("ID", rowItem.ID).putExtra("ISUSER", 0));
            }
        });
    }

    private SearchUserInfo getData(Cursor cursor) {
        SearchUserInfo info = new SearchUserInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Users.R_ID));
        info.NAME = cursor.getString(cursor.getColumnIndex("name"));
        info.URL = cursor.getString(cursor.getColumnIndex("logourl"));
        info.THUMB = cursor.getString(cursor.getColumnIndex("smallurl"));
        info.TELEPHONE = cursor.getString(cursor.getColumnIndex(CompanyContacts.TELEPHONE));
        info.EMAIL = cursor.getString(cursor.getColumnIndex(CompanyContacts.EMAIL));
        return info;
    }


}