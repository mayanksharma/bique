package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.MatterDetailActivity;
import com.litman.dao.SearchMatterInfo;
import com.litman.interfaces.Matters;


public class SearchMatterCursorAdapter extends CursorRecyclerAdapter<SearchMatterCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = SearchMatterCursorAdapter.class.getSimpleName();

    public SearchMatterCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtDesc, txtCourt;

        public ViewHolder(View v) {
            super(v);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtCourt = (TextView) v.findViewById(R.id.txtCourt);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.search_matter_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final SearchMatterInfo rowItem = getData(cursor);

        holder.txtDesc.setText(rowItem.PLAINTIFF + "\n VS \n" + rowItem.DEFENDANT);
        holder.txtCourt.setTextColor(ContextCompat.getColor(ctx, R.color.black));
        holder.txtCourt.setText(rowItem.COURT_NAME + "\n" + rowItem.CASE_TYPE + " " + rowItem.MATTER_NUMBER + "/" + rowItem.CASE_YEAR);

        holder.txtDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx,MatterDetailActivity.class).putExtra("ID",rowItem.ID));
            }
        });
    }

    private SearchMatterInfo getData(Cursor cursor) {
        SearchMatterInfo info = new SearchMatterInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Matters.R_ID));
        info.MATTER_NUMBER = cursor.getString(cursor.getColumnIndex(Matters.MATTER_NO));
        info.COURT_NAME = cursor.getString(cursor.getColumnIndex("court_name"));
        info.PLAINTIFF = cursor.getString(cursor.getColumnIndex(Matters.PLAINTIFF));
        info.DEFENDANT = cursor.getString(cursor.getColumnIndex(Matters.DEFFENDANT));
        info.CASE_YEAR = cursor.getString(cursor.getColumnIndex(Matters.CASE_YEAR));
        info.CASE_TYPE = cursor.getString(cursor.getColumnIndex(Matters.CASE_TYPE));
        return info;
    }


}