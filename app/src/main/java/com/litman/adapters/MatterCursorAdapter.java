package com.litman.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.MatterDetailActivity;
import com.litman.dao.MatterInfo;
import com.litman.interfaces.Hearing;
import com.litman.interfaces.Matters;
import com.litman.manager.CacheManager;


public class MatterCursorAdapter extends CursorRecyclerAdapter<MatterCursorAdapter.ViewHolder> {

    private Context ctx;
    private final String TAG = MatterCursorAdapter.class.getSimpleName();

    public MatterCursorAdapter(Context context, Cursor cursor) {
        super(cursor);
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName, txtCourt, txtJudge, txtItem, txtDate;
        private CardView crdMain;

        public ViewHolder(View v) {
            super(v);
            crdMain = (CardView) v.findViewById(R.id.cardMain);
            txtName = (TextView) v.findViewById(R.id.txtName);
            txtCourt = (TextView) v.findViewById(R.id.txtCourt);
            txtJudge = (TextView) v.findViewById(R.id.txtJudgeName);
            txtItem = (TextView) v.findViewById(R.id.txtItem);
            txtDate = (TextView) v.findViewById(R.id.txtDate);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vh = inflater.inflate(R.layout.matter_layout, parent, false);
        viewHolder = new ViewHolder(vh);
        return viewHolder;
    }

    @Override
    public void onBindViewHolderCursor(final ViewHolder holder, int position, Cursor cursor) {
        final MatterInfo rowItem = getData(cursor);

        holder.txtName.setText(rowItem.PLAINTIFF + " VS " + rowItem.DEFENDANT);
        holder.txtCourt.setText(rowItem.COURT_NAME + "\n" + rowItem.CASE_TYPE + " " + rowItem.MATTER_NUMBER + "/" + rowItem.CASE_YEAR);
        holder.txtJudge.setText(rowItem.JUDGE);
        holder.txtItem.setText("Court Room: " + rowItem.COURT_ROOM + "; Item: " + rowItem.ITEM_NO);
        holder.txtDate.setText(CacheManager.INSTANCE.getFormattedDate(rowItem.H_DATE));

        holder.crdMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx,MatterDetailActivity.class).putExtra("ID",rowItem.ID));
            }
        });
    }

    private MatterInfo getData(Cursor cursor) {
        MatterInfo info = new MatterInfo();
        info.ID = cursor.getInt(cursor.getColumnIndex(Matters.R_ID));
        info.MATTER_NUMBER = cursor.getString(cursor.getColumnIndex(Matters.MATTER_NO));
        info.COURT_NAME = cursor.getString(cursor.getColumnIndex("court_name"));
        info.PLAINTIFF = cursor.getString(cursor.getColumnIndex(Matters.PLAINTIFF));
        info.DEFENDANT = cursor.getString(cursor.getColumnIndex(Matters.DEFFENDANT));
        info.CASE_YEAR = cursor.getString(cursor.getColumnIndex(Matters.CASE_YEAR));
        info.CASE_TYPE = cursor.getString(cursor.getColumnIndex(Matters.CASE_TYPE));
        info.JUDGE = cursor.getString(cursor.getColumnIndex(Hearing.JUDGE_NAME));
        info.COURT_ROOM = cursor.getString(cursor.getColumnIndex(Hearing.COURT_ROOM_NO));
        info.ITEM_NO = cursor.getString(cursor.getColumnIndex(Hearing.ITEM_NO));
        info.H_DATE = cursor.getString(cursor.getColumnIndex(Hearing.ON_DATE));

//        Cursor c = CacheManager.INSTANCE.getMatterNextHearing(info.ID);
//        if (c != null && c.moveToFirst()) {
//            info.JUDGE = c.getString(c.getColumnIndex(Hearing.JUDGE_NAME));
//            info.COURT_ROOM = c.getString(c.getColumnIndex(Hearing.COURT_ROOM_NO));
//            info.ITEM_NO = c.getString(c.getColumnIndex(Hearing.ITEM_NO));
//            info.H_DATE = c.getString(c.getColumnIndex(Hearing.ON_DATE));
//            c.close();
//        }

        return info;
    }


}