package com.litman.fragments;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.litman.R;
import com.litman.activity.AddPersonCompanyActivity;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;

import java.util.ArrayList;


public class ContactsFragment extends Fragment {

    private static final String DEBUG_TAG = "ContactsFragment";
    private TabLayout tabLayout;
    private ViewPager search_pager;
    private SearchPagerAdapter adapter;
    private Button importContactBtn;
    private int fragmentType = 0;

    private static final String[] tabs = {"Person", "Company"};

    public ContactsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        setUpView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (CacheManager.INSTANCE.isAddAllowed(getActivity())) {
            menu.setGroupVisible(R.id.grp_main, true);
        } else {
            menu.setGroupVisible(R.id.grp_main, false);
        }
        menu.setGroupVisible(R.id.grp_search, true);
        menu.setGroupVisible(R.id.grp_Noti, false);
        menu.setGroupVisible(R.id.grp_save, false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivityForResult(new Intent(getActivity(), AddPersonCompanyActivity.class), 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpView(View view) {

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        search_pager = (ViewPager) view.findViewById(R.id.search_pager);

        adapter = new SearchPagerAdapter(getChildFragmentManager());
        search_pager.setAdapter(adapter);

        search_pager.setOffscreenPageLimit(1);

        tabLayout.setupWithViewPager(search_pager);

        importContactBtn = (Button) view.findViewById(R.id.importContactBtn);

        importContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContactList();
            }
        });


        search_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragmentType = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void openContactList() {
        if (ContextCompat.checkSelfPermission(getContext(), "android.permission.READ_CONTACTS")
                != PackageManager.PERMISSION_GRANTED) {
            ArrayList<String> permissions = new ArrayList<>();
            permissions.add("android.permission.READ_CONTACTS");
            String[] stringArray = permissions.toArray(new String[0]);
            requestPermissions(stringArray, 123);
        } else {
            Intent it = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(it, 100);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           final String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openContactList();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_LONG).show();


                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    static class SearchPagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        FragmentManager mFragmentManager;

        public SearchPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return ContactsPersonFragment.newInstance();
            } else if (position == 1) {
                return ContactsCompanyFragment.newInstance();
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == 100) {

                String name = "";
                String email = "";
                String number = "";
                String city = "";
                String country = "";
                String street = "";
                String fName = "";
                String lName = "";
                String companyName = "";

                Uri contactData = data.getData();
                Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    //String contactId = c.getString(c.getColumnIndex(ContactsContract.Data.CONTACT_ID));


                    ContentResolver cr = getActivity().getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                            "DISPLAY_NAME = '" + name + "'", null, null);
                    if (cursor.moveToFirst()) {
                        String contactId =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        //
                        //  Get all phone numbers.
                        //
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            AppLogger.logE(DEBUG_TAG, "number: " + number);
                        }
                        if (phones != null) {
                            phones.close();
                        }


                        Cursor cEmail = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS}, ContactsContract.CommonDataKinds.Email.CONTACT_ID
                                        + "=?",
                                new String[]{String.valueOf(contactId)}, null);
                        while (cEmail != null && cEmail.moveToNext()) {
                            email = cEmail.getString(cEmail
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                        }
                        if (cEmail != null)
                            cEmail.close();

                        Uri postal_uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI;
                        Cursor postal_cursor = cr.query(postal_uri, null, ContactsContract.Data.CONTACT_ID + "=" + contactId, null, null);
                        while (postal_cursor.moveToNext()) {
                            street = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                            country = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                            city = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));

                        }
                        if (postal_cursor != null)
                            postal_cursor.close();


                        Uri org_uri = ContactsContract.Data.CONTENT_URI;
                        Cursor org_cursor = cr.query(org_uri, null, ContactsContract.Data.CONTACT_ID + "=" + contactId, null, null);
                        while (org_cursor.moveToNext()) {
                            companyName = org_cursor.getString(org_cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                            AppLogger.logE(DEBUG_TAG, "companyName: " + companyName);
                        }
                        if (org_cursor != null)
                            org_cursor.close();


                        Cursor cUname = cr
                                .query(ContactsContract.Data.CONTENT_URI,
                                        new String[]{ContactsContract.CommonDataKinds.StructuredName.PREFIX,
                                                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                                                ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
                                                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                                                ContactsContract.CommonDataKinds.StructuredName.SUFFIX},
                                        ContactsContract.Data.CONTACT_ID
                                                + " = ? AND "
                                                + ContactsContract.Data.MIMETYPE
                                                + " = ?",
                                        new String[]{
                                                String.valueOf(contactId),
                                                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE},
                                        null);

                        if (cUname != null && cUname.moveToFirst()) {
                            fName = cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PREFIX)) == null ? ""
                                    : cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PREFIX))
                                    + " ";
                            fName += cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)) == null ? ""
                                    : cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME))
                                    + " ";
                            fName += cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME)) == null ? ""
                                    : cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME))
                                    + " ";

                            lName = cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)) == null ? ""
                                    : cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME))
                                    + " ";
                            lName += cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.SUFFIX)) == null ? ""
                                    : cUname.getString(cUname
                                    .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.SUFFIX));
                        }
                        if (cUname != null)
                            cUname.close();
                    }

                    if (cursor != null)
                        cursor.close();

                    Intent addPerson = new Intent(getContext(), AddPersonCompanyActivity.class);
                    Bundle bundle = new Bundle();


                    bundle.putString("fname", fName);
                    bundle.putString("lname", lName);
                    bundle.putString("email", email);
                    bundle.putString("phoneNumber", number);
                    bundle.putString("city", city);
                    bundle.putString("country", country);
                    bundle.putString("street", street);
                    bundle.putInt("fragmentType", fragmentType);
                    bundle.putString("companyName", companyName);


                    AppLogger.logE(DEBUG_TAG, bundle.toString());
                    addPerson.putExtras(bundle);

                    startActivityForResult(addPerson, 202);

                }
                if (c != null)
                    c.close();
            } else if (requestCode == 202) {
                if (adapter != null) {
                    ((ContactsPersonFragment) adapter.getRegisteredFragment(0)).setData();
                    ((ContactsCompanyFragment) adapter.getRegisteredFragment(1)).setData();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
