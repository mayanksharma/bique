package com.litman.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.UserListAdapter;
import com.litman.dao.CourtInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

public class FilterTypeFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private ArrayList<CourtInfo> info = new ArrayList<>();
    private HashSet<Integer> selectedId;

    private Cursor curs;

    private String type;

    public static FilterTypeFragment newInstance(String type) {
        FilterTypeFragment fragment = new FilterTypeFragment();
        Bundle args = new Bundle();
        args.putString("TYPE", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString("TYPE");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtTitle.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getIds(type);
    }

    private void getIds(String courtId) {
        StringTokenizer token = new StringTokenizer(courtId, ",");
        selectedId = new HashSet<>();
        while (token.hasMoreElements()) {
            selectedId.add(Integer.valueOf(token.nextToken()));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getData();
    }

    private void getData() {

        info.clear();
        CourtInfo in = new CourtInfo();
        in.NAME = "Both";
        in.ID = -1;
        if (selectedId.contains(-1)) {
            in.SELECTED = true;
        } else {
            in.SELECTED = false;
        }
        info.add(in);
        CourtInfo in1 = new CourtInfo();
        in1.NAME = "Hearings";
        in1.ID = 1;
        if (selectedId.contains(1)) {
            in1.SELECTED = true;
        } else {
            in1.SELECTED = false;
        }
        info.add(in1);

        CourtInfo in2 = new CourtInfo();
        in2.NAME = "Tasks";
        in2.ID = 2;
        if (selectedId.contains(2)) {
            in2.SELECTED = true;
        } else {
            in2.SELECTED = false;
        }
        info.add(in2);



        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new UserListAdapter(getActivity(), info, selectedId);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);
    }

    public String getSelectedIds() {
        String str = "";
        if (mAdapter != null) {
            str = ((UserListAdapter) mAdapter).getAllSelected();
        }
        return str;
    }

}
