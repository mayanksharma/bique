package com.litman.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.litman.R;
import com.litman.interfaces.Const;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class PublicProfileFragment extends Fragment {

    private WebView web;

    public PublicProfileFragment() {
        // Required empty public constructor
    }

    public static PublicProfileFragment newInstance() {
        PublicProfileFragment fragment = new PublicProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggest_me, container, false);
        setUpView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);

        if (AppUtils.checkInternetConnection(getActivity())) {
            initializeWebView();
            web.loadUrl(Const.PROFILE_OTHER_1 + AppPreferences.getUserId(getActivity()) + Const.PROFILE_OTHER_2 + AppPreferences.getToken(getActivity()));
        } else {
            AppUtils.noInternetDialog(getActivity());
        }

        hideKeyboard(web);
    }

    private void setUpView(View view) {
        ((TextView) view.findViewById(R.id.txtTitle)).setText("Public Profile");
        ((TextView) view.findViewById(R.id.txtTitle)).setVisibility(View.GONE);
        web = (WebView) view.findViewById(R.id.web);
    }

    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);
//                if (url.endsWith("users/sign_in")) {
//                    finish();
//                    return true;
//                } else if (url.endsWith("users/sign_up")) {
//                    getSupportActionBar().setTitle("SignUp");
//                } else if(url.endsWith("/users/confirmation/new")){
//                    getSupportActionBar().setTitle("Resend");
//                }else {
//                    getSupportActionBar().setTitle("Forgot Password");
//                }
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });


    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}
