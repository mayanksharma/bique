package com.litman.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.SearchActivity;
import com.litman.adapters.SearchMatterCursorAdapter;
import com.litman.manager.CacheManager;

public class SearchMattersFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private Cursor curs;

    public static SearchMattersFragment newInstance() {
        SearchMattersFragment fragment = new SearchMattersFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        if (mAdapter == null) {
            mAdapter = new SearchMatterCursorAdapter(getActivity(), curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    public void setData() {
        txtTitle.setText("Showing Result for " + SearchActivity.keyword);
        curs = CacheManager.INSTANCE.searchMatter(SearchActivity.keyword);
        if (mAdapter != null)
            ((SearchMatterCursorAdapter) mAdapter).changeCursor(curs);
    }

}
