package com.litman.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.AddMatterActivity;
import com.litman.activity.FilterActivity;
import com.litman.adapters.MatterCursorAdapter;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;


public class MattersFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;
    private ImageView imgFilter;
    private Cursor curs;
    private String userId = "", courtId = "", userName = "", courtName = "";

    public MattersFragment() {
        // Required empty public constructor
    }

    public static MattersFragment newInstance() {
        MattersFragment fragment = new MattersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_matters, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);

        imgFilter = (ImageView) view.findViewById(R.id.imgFilter);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        if (mAdapter == null) {
            mAdapter = new MatterCursorAdapter(getActivity(), curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        imgFilter.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (curs == null) {
            userId = AppPreferences.getUserId(getActivity());
            courtId = "-1";
            getNames(userId, courtId);
            setData(userId, getCourts());
        } else {
            txtTitle.setText("Showing " + curs.getCount() + " matters for " + userName + " in " + courtName);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (CacheManager.INSTANCE.isAddAllowed(getActivity())) {
            menu.setGroupVisible(R.id.grp_main, true);
        } else {
            menu.setGroupVisible(R.id.grp_main, false);
        }
        menu.setGroupVisible(R.id.grp_search, true);
        menu.setGroupVisible(R.id.grp_Noti, false);
        menu.setGroupVisible(R.id.grp_save, false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivity(new Intent(getActivity(), AddMatterActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getNames(String userId, String courtId) {
        if (userId.equals("-1")) {
            userName = "All Users";
        } else {
            userName = getUserName(userId);
        }

        if (courtId.equals("-1")) {
            courtName = "All Court";
        } else {
            courtName = getCourtName(courtId);
        }
    }

    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }

    private String getUsers() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getUser();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }

    private void setData(final String userId, final String courts) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new getDataAsync(userId, courts).execute();
            }
        },200);
    }


    @Override
    public void onClick(View view) {
        startActivityForResult(new Intent(getActivity(), FilterActivity.class).putExtra("USER", userId).putExtra("COURT", courtId), 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            userId = data.getStringExtra("USER");
            courtId = data.getStringExtra("COURT");
            getNames(userId, courtId);
            String court = "";
            if (courtId.equals("-1")) {
                court = getCourts();
            } else {
                court = courtId;
            }

            String user = "";
            if (userId.equals("-1")) {
                user = getUsers();
            } else {
                user = userId;
            }
            setData(user, court);
        }
    }

    public class getDataAsync extends AsyncTask<Void, Void, Cursor> {

        private ProgressDialog dialog;

        String userId, courts;

        public getDataAsync(String userId, String courts) {
            this.userId = userId;
            this.courts = courts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setCancelable(false);
            dialog.setMessage("Please wait...");
            dialog.show();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            return CacheManager.INSTANCE.getMatter(userId, courts);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            curs = cursor;

            if (curs != null) {
                txtTitle.setText("Showing " + curs.getCount() + " Matters for " + userName + " for " + courtName);
            }
            if (mAdapter != null)
                ((MatterCursorAdapter) mAdapter).changeCursor(curs);

            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    private String getCourtName(String courtId) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getCourtName(courtId);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex("name")));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }

    private String getUserName(String userId) {
        String name = "";
        Cursor curs = CacheManager.INSTANCE.getUserName(userId);
        if (curs != null && curs.moveToNext()) {
            name = (curs.getString(curs.getColumnIndex("name")));
        }
        if (curs != null) {
            curs.close();
        }
        return name;
    }


}
