package com.litman.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.MatterTeamCursorAdapter;
import com.litman.dao.MatterInfo;
import com.litman.interfaces.Matters;
import com.litman.manager.CacheManager;

public class MattersTeamFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private MatterInfo info;

    private Cursor curs;

    public static MattersTeamFragment newInstance(MatterInfo info) {
        MattersTeamFragment fragment = new MattersTeamFragment();
        Bundle args = new Bundle();
        args.putSerializable(Matters.T_NAME, info);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            info = (MatterInfo) getArguments().getSerializable(Matters.T_NAME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        ((TextView) view.findViewById(R.id.txtName)).setText(info.PLAINTIFF + " Vs " + info.DEFENDANT);
        ((TextView) view.findViewById(R.id.txtCourt)).setText(info.COURT_NAME + ", Matter No: " + info.MATTER_NUMBER);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            mAdapter = new MatterTeamCursorAdapter(getActivity(), curs,info.ID);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    public void setData() {
        curs = CacheManager.INSTANCE.getMatterTeam(info.ID);
        if (mAdapter != null)
            ((MatterTeamCursorAdapter) mAdapter).changeCursor(curs);
    }

}
