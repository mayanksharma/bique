package com.litman.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.SearchActivity;
import com.litman.adapters.SearchTaskCursorAdapter;
import com.litman.manager.CacheManager;

public class SearchTaskFragment extends Fragment implements SearchTaskCursorAdapter.onComplete {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private Cursor curs;

    public static SearchTaskFragment newInstance() {
        SearchTaskFragment fragment = new SearchTaskFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        if (mAdapter == null) {
            mAdapter = new SearchTaskCursorAdapter(getActivity(), curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
            ((SearchTaskCursorAdapter) mAdapter).setListener(this);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    public void setData() {
        txtTitle.setText("Showing Result for " + SearchActivity.keyword);
        curs = CacheManager.INSTANCE.searchTask(SearchActivity.keyword);
        if (mAdapter != null)
            ((SearchTaskCursorAdapter) mAdapter).changeCursor(curs);
    }

    @Override
    public void OnClick(String response) {
        setData();
    }
}
