package com.litman.fragments;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.AddFileActivity;
import com.litman.adapters.SearchFileCursorAdapter;
import com.litman.manager.CacheManager;

public class FileFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;

    private Cursor curs;

    public static FileFragment newInstance() {
        FileFragment fragment = new FileFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setupView(view);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (CacheManager.INSTANCE.isAddAllowed(getActivity())) {
            menu.setGroupVisible(R.id.grp_main, true);
        } else {
            menu.setGroupVisible(R.id.grp_main, false);
        }
        menu.setGroupVisible(R.id.grp_search, true);
        menu.setGroupVisible(R.id.grp_Noti, false);
        menu.setGroupVisible(R.id.grp_save, false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivity(new Intent(getActivity(), AddFileActivity.class).putExtra("ID", 0));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        if (mAdapter == null) {
            mAdapter = new SearchFileCursorAdapter(getActivity(), curs);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    public void setData() {
        txtTitle.setText("Files");
        curs = CacheManager.INSTANCE.searchFiles("");
        if (mAdapter != null) {
            ((SearchFileCursorAdapter) mAdapter).changeCursor(curs);
            txtTitle.setText("Showing "+curs.getCount()+" Files");
        }
    }

}
