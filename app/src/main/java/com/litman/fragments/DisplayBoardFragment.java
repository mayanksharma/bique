package com.litman.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.DisplayBoardAdapter;
import com.litman.dao.HomeFeedInfo;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class DisplayBoardFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle, txtNoData;
    ArrayList<HomeFeedInfo> list;
    private HashMap<Integer, HashMap<String, String>> map;

    public DisplayBoardFragment() {
        // Required empty public constructor
    }

    public static DisplayBoardFragment newInstance() {
        DisplayBoardFragment fragment = new DisplayBoardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_display_matter, container, false);
        setUpView(view);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter("DISPLAY_SYNC"));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getData();
    }

    private void getData() {
        map = new HashMap<>();
        txtTitle.setText("Last Sync: " + AppPreferences.getDisplayDate(getActivity()));
        if (!AppPreferences.getDisplayData(getActivity()).equals("")) {
            parseResult();
        }
        Calendar calendar = Calendar.getInstance();
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, calendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        end.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        end.set(Calendar.HOUR, calendar.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

        SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");

        String startDate = startDateFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());

        Cursor curs = CacheManager.INSTANCE.getHearingList(AppPreferences.getUserId(getActivity()), getCourts(), startDate, endDate);
        list.clear();
        ArrayList<HomeFeedInfo> infoList = new ArrayList<>();
        while (curs != null && curs.moveToNext()) {
            HomeFeedInfo info = new HomeFeedInfo();
            info.MATTER_ID = curs.getInt(curs.getColumnIndex("matter_id"));
            info.COURT_NAME = curs.getString(curs.getColumnIndex("court_name"));
            info.PLAINTIFF = curs.getString(curs.getColumnIndex("plaintiff"));
            info.DEFENDANT = curs.getString(curs.getColumnIndex("defendant"));
            info.COURT_ROOM_NO = curs.getInt(curs.getColumnIndex("court_room_no"));
            info.COURT_ID = curs.getInt(curs.getColumnIndex("court_id"));
            info.JUDGE_NAME = curs.getString(curs.getColumnIndex("judge_name"));
            info.ITEM_NO = curs.getString(curs.getColumnIndex("item_no"));
            info.MATTER_NO = curs.getString(curs.getColumnIndex("matter_number"));
            info.CASE_YEAR = curs.getString(curs.getColumnIndex("case_year"));
            info.CASE_TYPE = curs.getString(curs.getColumnIndex("case_type"));
            String currentItem = "";
            try {
                HashMap<String, String> getMap = map.get(info.COURT_ID);
                if (getMap != null) {
                    currentItem = getMap.get(info.COURT_ROOM_NO + "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            info.CURRENT_ITEM = currentItem != null ? currentItem : "";
            info.TYPE = 2;
            infoList.add(info);
        }
        if (curs != null)
            curs.close();

        if (infoList.size() > 0) {
            getSectionedData(infoList);
            txtNoData.setVisibility(View.GONE);
        } else {
            txtNoData.setVisibility(View.VISIBLE);
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getSectionedData(ArrayList<HomeFeedInfo> infoList) {
        int lastSelectedId = 0;
        for (int in = 0; in < infoList.size(); in++) {
            HomeFeedInfo objItem = infoList.get(in);

            int id = objItem.COURT_ID;

            if (lastSelectedId != 0) {
                if (lastSelectedId != id) {
                    HomeFeedInfo objSectionItem = new HomeFeedInfo();
                    objSectionItem.COURT_NAME = objItem.COURT_NAME;
                    objSectionItem.IS_SECTION = true;
                    objSectionItem.COURT_ID = objItem.COURT_ID;
                    list.add(objSectionItem);
                }
            } else {
                HomeFeedInfo objSectionItem = new HomeFeedInfo();
                objSectionItem.COURT_NAME = objItem.COURT_NAME;
                objSectionItem.IS_SECTION = true;
                objSectionItem.COURT_ID = objItem.COURT_ID;
                list.add(objSectionItem);
            }

            lastSelectedId = id;
            list.add(objItem);
        }
    }

    private void parseResult() {
        String result = AppPreferences.getDisplayData(getActivity());
        try {
            if (result != null && result.length() > 0) {
                JSONArray arr = new JSONArray(result);
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    int id = obj.getInt("court_id");
                    HashMap<String, String> localMap = new HashMap<>();
                    if (!obj.isNull("displayboard")) {
                        JSONArray ar = obj.getJSONArray("displayboard");
                        for (int j = 0; j < ar.length(); j++) {
                            JSONObject ob = ar.getJSONObject(j);
                            localMap.put(getString(ob, "courtRoomNo"), getString(ob, "status"));
                        }
                    }
                    map.put(id, localMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getString(JSONObject ob, String createdAt) {
        if (!ob.isNull(createdAt)) {
            try {
                return ob.getString(createdAt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private void setUpView(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtNoData = (TextView) view.findViewById(R.id.txtNoData);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            list = new ArrayList<>();
            mAdapter = new DisplayBoardAdapter(getActivity(), list);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getData();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (receiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        }
    }

    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }
}
