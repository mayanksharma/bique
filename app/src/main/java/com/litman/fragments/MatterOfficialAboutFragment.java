package com.litman.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.MatterInfo;
import com.litman.interfaces.Matters;


public class MatterOfficialAboutFragment extends Fragment {

    private MatterInfo info;

    public MatterOfficialAboutFragment() {
        // Required empty public constructor
    }

    public static MatterOfficialAboutFragment newInstance(MatterInfo param1) {
        MatterOfficialAboutFragment fragment = new MatterOfficialAboutFragment();
        Bundle args = new Bundle();
        args.putSerializable(Matters.T_NAME, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
        if (getArguments() != null) {
            info = (MatterInfo) getArguments().getSerializable(Matters.T_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_official_about, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        ((TextView) view.findViewById(R.id.txtName)).setText(info.PLAINTIFF + " Vs " + info.DEFENDANT);
        ((TextView) view.findViewById(R.id.txtCourt)).setText(info.COURT_NAME + ", Matter No: " + info.MATTER_NUMBER);
        ((TextView) view.findViewById(R.id.txtPlaintiff)).setText(info.PLAINTIFF);
        ((TextView) view.findViewById(R.id.txtDeffendant)).setText(info.DEFENDANT);
        ((TextView) view.findViewById(R.id.txtPPlaintiff)).setText(info.A_PLAINTIFF);
        ((TextView) view.findViewById(R.id.txtPDeffendant)).setText(info.A_DEFENDANT);
        ((TextView) view.findViewById(R.id.txtMatterNo)).setText(info.MATTER_NUMBER);
        ((TextView) view.findViewById(R.id.txtType)).setText(info.CASE_TYPE);
        ((TextView) view.findViewById(R.id.txtYear)).setText(info.CASE_YEAR);
        ((TextView) view.findViewById(R.id.txtCourtName)).setText(info.COURT_NAME);
    }

}
