package com.litman.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.litman.R;
import com.litman.activity.SyncActivity;
import com.litman.interfaces.Const;
import com.litman.utils.AppPreferences;
import com.litman.utils.AppUtils;


public class SuggestMeFragment extends Fragment {

    private WebView web;

    public SuggestMeFragment() {
        // Required empty public constructor
    }

    public static SuggestMeFragment newInstance() {
        SuggestMeFragment fragment = new SuggestMeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggest_me, container, false);
        setUpView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);

        if (AppUtils.checkInternetConnection(getActivity())) {
            initializeWebView();
            web.loadUrl(Const.SUGGEST_ME + AppPreferences.getToken(getActivity()));
        } else {
            AppUtils.noInternetDialog(getActivity());
        }

        hideKeyboard(web);
    }

    private void setUpView(View view) {
        web = (WebView) view.findViewById(R.id.web);
    }

    private void initializeWebView() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e("UURL--", url);

                if (url.startsWith(Const.SUCCESS)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    openUtilityDialog(getActivity(), value);
                    return true;
                } else if (url.startsWith(Const.FAIL)) {
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("message");
                    AppUtils.openUtilityDialog(getActivity(), value);
                    web.loadUrl(Const.SUGGEST_ME + AppPreferences.getToken(getActivity()));
                    return true;
                }

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });


    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void openUtilityDialog(final Context ctx,
                                  final String messageID) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.searc_matter_dialog, null);
        final AlertDialog d = new AlertDialog.Builder(getActivity())
                .setMessage("Matters(s) has been successfully added.")
                .setTitle("Litman")
                .setView(view)
                .setCancelable(false)
                .setNegativeButton("Go back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getFragmentManager().popBackStack();
                        dialog.dismiss();
                    }
                })
                .show();
        (view.findViewById(R.id.txtSync)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivityForResult(new Intent(getActivity(), SyncActivity.class).putExtra("ISNEW", false), 200);
                d.dismiss();
            }
        });
        (view.findViewById(R.id.txtMList)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_main, MattersFragment.newInstance());
                transaction.addToBackStack("matters");
                transaction.commit();
                d.dismiss();
            }
        });
        (view.findViewById(R.id.txtSearchMatter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                web.loadUrl(Const.SUGGEST_ME + AppPreferences.getToken(getActivity()));
                d.dismiss();
            }
        });

    }

}
