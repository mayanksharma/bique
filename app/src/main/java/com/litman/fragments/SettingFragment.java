package com.litman.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.litman.R;
import com.litman.activity.ChangePasswordActivity;
import com.litman.activity.EditProfileActivity;
import com.litman.activity.ManageNotiActivity;
import com.litman.utils.AppUtils;


public class SettingFragment extends Fragment implements View.OnClickListener {


    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        (view.findViewById(R.id.linChange)).setOnClickListener(this);
        (view.findViewById(R.id.linPrivacy)).setOnClickListener(this);
        (view.findViewById(R.id.linNoti)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linChange:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                break;
            case R.id.linNoti:
                if (AppUtils.checkInternetConnection(getActivity())) {
                    startActivity(new Intent(getActivity(), ManageNotiActivity.class));
                } else {
                    AppUtils.noInternetDialog(getActivity());
                }
                break;
            case R.id.linPrivacy:
               startActivity(new Intent(getActivity(), EditProfileActivity.class));
                break;
        }
    }
}
