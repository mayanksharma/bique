package com.litman.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.litman.R;
import com.litman.activity.AddMatterActivity;
import com.litman.adapters.HomeCursorAdapter;
import com.litman.manager.CacheManager;
import com.litman.utils.AppPreferences;


public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView txtTitle;
    private Cursor curs;


    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setupView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    private void setWelcome(int count) {
        if (count > 0) {
            String first = "Welcome ";
            String name = CacheManager.INSTANCE.getCapitalizeFullName(AppPreferences.getFname(getActivity()));
            String last = ". Here's what happened while you were away...";
            String description = first + name + last;
            Spannable descriptionSpanned = new SpannableString(description);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(first), description.indexOf(first) + first.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(last), description.indexOf(last) + last.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtTitle.setTextSize(18);
            txtTitle.setText(descriptionSpanned);
        } else {
            String first = "Welcome ";
            String name = CacheManager.INSTANCE.getCapitalizeFullName(AppPreferences.getFname(getActivity()));
            String last = ".\n\nWe understand you are new to the system, here's what you can do to begin with:\n\n1. ";
            String search = "Search Matter:";
            String searchDesc = " We can recommend you some matters based on your search criteria.\n\n2. ";
            String add = "Add a Matter:";
            String addDesc = " If you know the matter deatils, you can add it by entering the relevant information.";
            String description = first + name + last + search + searchDesc + add + addDesc;
            Spannable descriptionSpanned = new SpannableString(description);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(first), description.indexOf(first) + first.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new RelativeSizeSpan(1.5f),
                    description.indexOf(first), description.indexOf(first) + first.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new RelativeSizeSpan(1.5f),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)),
                    description.indexOf(name), description.indexOf(name) + name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(last), description.indexOf(last) + last.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(searchDesc), description.indexOf(searchDesc) + searchDesc.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.black)),
                    description.indexOf(addDesc), description.indexOf(addDesc) + addDesc.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(search), description.indexOf(search) + search.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            descriptionSpanned.setSpan(new UnderlineSpan(), description.indexOf(add), description.indexOf(add) + add.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                                           @Override
                                           public void onClick(View view) {
                                               startActivity(new Intent(getActivity(), AddMatterActivity.class));
                                           }

                                           @Override
                                           public void updateDrawState(TextPaint ds) {
                                               ds.setColor(ContextCompat.getColor(getActivity(), R.color.grey));
                                           }
                                       },
                    description.indexOf(add), description.indexOf(add) + add.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            descriptionSpanned.setSpan(new ClickableSpan() {
                                           @Override
                                           public void onClick(View view) {
                                               FragmentManager manager = getActivity().getSupportFragmentManager();
                                               FragmentTransaction transaction = manager.beginTransaction();
                                               transaction.replace(R.id.content_main, SuggestMeFragment.newInstance());
                                               transaction.addToBackStack("suggest_me");
                                               transaction.commit();
                                           }

                                           @Override
                                           public void updateDrawState(TextPaint ds) {
                                               ds.setColor(ContextCompat.getColor(getActivity(), R.color.grey));
                                           }
                                       },
                    description.indexOf(search), description.indexOf(search) + search.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            txtTitle.setTextSize(14);
            txtTitle.setMovementMethod(LinkMovementMethod.getInstance());
            txtTitle.setText(descriptionSpanned);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(R.id.grp_main, false);
        menu.setGroupVisible(R.id.grp_search, true);
        menu.setGroupVisible(R.id.grp_Noti, true);
        menu.setGroupVisible(R.id.grp_save, false);
        super.onPrepareOptionsMenu(menu);
    }


    private void setData() {
        curs = CacheManager.INSTANCE.getHomeData(AppPreferences.getUserId(getActivity()));
        if (mAdapter != null)
            ((HomeCursorAdapter) mAdapter).changeCursor(curs);
        if (curs != null) {
            setWelcome(curs.getCount());
        } else {
            setWelcome(0);
        }
    }

    private void setupView(View view) {
        // ((AppCompatActivity)getActivity()).getSupportActionBar().set
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (mAdapter == null) {
            mAdapter = new HomeCursorAdapter(getActivity(), curs, this);
            if (recyclerView != null)
                recyclerView.setAdapter(mAdapter);
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(mAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setData();
    }
}
