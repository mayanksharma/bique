package com.litman.manager;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.litman.db.DbManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;


public enum CacheManager {

    INSTANCE;

    private final String TAG = CacheManager.class.getSimpleName();

    public Cursor getHomeData(String userId) {

        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select * from ( SELECT orders.remoteid,matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,'' as " +
                    "first_name,'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,'' as court_room_no,'' as judge_name,'' " +
                    "as item_no,orders.date as display_date,orders.created_at as sort_date,1 as type,'Order' as type_name,'' as avatarurl,'' as avatarthumburl," +
                    "(Select count(remoteid) from comments where commentable_id=orders.remoteid and commentable_type='Order') as count FROM orders left join matters on " +
                    "orders.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid where orders.matter_id " +
                    "in (SELECT matter_id FROM matter_users where user_id = " + userId + " ) union all SELECT posts.remoteid,matters.remoteid as matterid,posts.creator_id " +
                    "as user_id,'' as courtname,'' as link,first_name,middle_name,last_name,posts.text,plaintiff,defendant,'' as court_room_no,''" +
                    " as judge_name,'' as item_no,posts.created_at as displaydate,posts.created_at as sortdate,2 as type,'Post' as type_name,avatarurl,avatarthumburl," +
                    "(Select count(remoteid) from comments where commentable_id=posts.remoteid and commentable_type='Post') as count FROM posts left join users" +
                    " on posts.creator_id = users.remoteid left join matters on posts.postable_id = matters.remoteid and postable_type = 'Matter' " +
                    "WHERE postable_type = 'Matter' and posts.postable_id in (SELECT matter_id FROM matter_users where user_id = " + userId + " ) union all " +
                    "SELECT hearings.remoteid,matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,'' as first_name,'' as middle_name,'' " +
                    "as last_name,'' as text,plaintiff,defendant,court_room_no,judge_name,item_no,on_date as display_date,hearings.created_at as " +
                    "sort_date,3 as type,'Hearing' as type_name,'' as avatarurl,'' as avatarthumburl,(Select count(remoteid) from comments where " +
                    "commentable_id=hearings.remoteid and commentable_type='Hearing') as count FROM hearings left join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id " +
                    "= courts.remoteid where hearings.matter_id in (SELECT matter_id FROM matter_users where user_id = " + userId + " ) union all " +
                    "SELECT custom_hearings.remoteid,matters.remoteid as matter_id, custom_hearings.creator_id as user_id,courts.name as court_name,'' as link,first_name,middle_name," +
                    "last_name,'' as text,plaintiff,defendant,court_room_no,judge_name,item_no,on_date as display_date,custom_hearings.created_at as " +
                    "sort_date,4 as type,'CustomHearing' as type_name,avatarurl,avatarthumburl,(Select count(remoteid) from comments where commentable_id=custom_hearings.remoteid " +
                    "and commentable_type='CustomHearing') as count FROM custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid left join courts " +
                    "on matters.court_id = courts.remoteid left join users  on users.remoteid=custom_hearings.creator_id  where custom_hearings.matter_id " +
                    "in (SELECT matter_id FROM matter_users where user_id = " + userId + " )) as feed order by sort_date desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor searchMatter(String keyword) {
        Cursor cursor = null;
        if (keyword.length() >= 1 && keyword.length() < 3)
            return cursor;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT matters.remoteid,plaintiff,defendant,matter_number,case_types.case_type,case_year,courts.name as court_name from " +
                    "matters inner join courts on courts.remoteid=matters.court_id  inner join case_types on case_types.remoteid=matters.case_type_id" +
                    " where plaintiff like  '%" + keyword + "%' or defendant like '%" + keyword + "%' or plaintiff_attorney like '%" + keyword + "%'" +
                    " or diffendent_attorney like '%" + keyword + "%'";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor searchUser(String keyword) {
        Cursor cursor = null;
        if (keyword.length() >= 1 && keyword.length() < 3)
            return cursor;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid,trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name,avatarurl,avatarthumburl from users " +
                    "where name like '%" + keyword + "%'";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor searchTask(String keyword) {
        Cursor cursor = null;
        if (keyword.length() >= 1 && keyword.length() < 3)
            return cursor;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select tasks.remoteid,matter_id,completed,title,due_at,category,plaintiff,defendant,tasks.assigned_to," +
                    "trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name from tasks left join matters" +
                    " m on m.remoteid=matter_id  inner join users u on u.remoteid=tasks.assigned_to where title like '%" + keyword + "%' or category " +
                    "like '%" + keyword + "%' order by completed asc, due_at desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor searchFiles(String keyword) {
        Cursor cursor = null;
        if (keyword.length() >= 1 && keyword.length() < 3)
            return cursor;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select up.remoteid,ua.id as asset_id,uploadable_type,category,description,subcategory,up.creator_id ,url," +
                    "trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name ,up.created_at,plaintiff,defendant,uploadable_id from" +
                    " uploads up left join users us on up.creator_id=us.remoteid inner join uploads_assets ua on ua.upload_id=up.remoteid " +
                    "left join matters on up.uploadable_id=matters.remoteid and up.uploadable_type='Matter'" +
                    " where ua.deleted = 0 and description like '%" + keyword + "%' order by up.created_at desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor searchContacts(String keyword) {
        Cursor cursor = null;
        if (keyword.length() >= 1 && keyword.length() < 3)
            return cursor;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid,avatarurl,avatarthumburl,trim(trim(first_name) ||' '|| trim(last_name)) as name from person_contacts " +
                    "where name like '%" + keyword + "%' ";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterDetail(int matterid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT matters.remoteid,courts.name,plaintiff,defendant,matter_number,case_year," +
                    "case_types.case_type,my_client,plaintiff_attorney,diffendent_attorney,matter_users.user_id,max(ch.on_date) as custom_date," +
                    " max(h.on_date) as hearing_date FROM matters left join courts on matters.court_id = courts.remoteid left join case_types on" +
                    " matters.case_type_id = case_types.remoteid left join matter_users on matters.remoteid = matter_users.matter_id left join" +
                    " custom_hearings ch on ch.matter_id= matters.remoteid left join hearings h on h.matter_id= matters.remoteid" +
                    " where matters.remoteid = '" + matterid + "'";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterTimelineHearing(int matterid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,plaintiff," +
                    "defendant,court_room_no,judge_name,item_no,on_date as display_date,hearings.created_at as sort_date,3 as type, '' as text FROM hearings left " +
                    "join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid where" +
                    " matters.remoteid = '" + matterid + "' order by display_date desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterTimelineOrders(int matterid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,plaintiff,defendant,'' as court_room_no," +
                    "'' as judge_name,'' as item_no,orders.date as display_date,orders.created_at as sort_date,1 as type,'' as text FROM orders left join " +
                    "matters on orders.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid where matters.remoteid = '" +
                    matterid + "' order by display_date desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterTimeline(int matterid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select * from (SELECT matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,plaintiff,defendant,'' as court_room_no," +
                    "'' as judge_name,'' as item_no,orders.date as display_date,orders.created_at as sort_date,1 as type,'' as text FROM orders left join " +
                    "matters on orders.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid where matters.remoteid = '" +
                    +matterid + "' union all " + "SELECT matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,plaintiff," +
                    "defendant,court_room_no,judge_name,item_no,on_date as display_date,hearings.created_at as sort_date,3 as type, '' as text FROM hearings left " +
                    "join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid where" +
                    " matters.remoteid = '" + matterid + "' union all SELECT matterid as matter_id, 0 as user_id,courts.name as court_name," +
                    "caseLink as link,plaintiff,defendant,'' as court_room_no,'' as judge_name,'' as item_no,orderdt as display_date,orderdt as" +
                    " sort_date,5 as type,details as text FROM case_history left join matters on case_history.matterid = matters.api_matter_id" +
                    " left join courts on matters.court_id = courts.remoteid where matters.remoteid = '" + matterid + "') order by display_date desc";

            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterTask(int matterid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select tasks.remoteid,matter_id,completed,title,due_at,category,plaintiff,defendant,tasks.user_id," +
                    "trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name from tasks " +
                    "left join matters m on m.remoteid=matter_id  inner join users u on u.remoteid=tasks.user_id where m.remoteid='" +
                    matterid + "' order by due_at desc";

            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterTeam(int matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid,avatarurl,avatarthumburl,trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name" +
                    " FROM users where remoteid in (select user_id from matter_users where matter_id = '" + matterId + "') ";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterContact(int matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select cc.remoteid,logourl,smallurl,name,email,telephone,website,  1 as type " +
                    "FROM company_contacts cc  left join  company_contacts_matters ccm on ccm.company_contact_id=cc.remoteid where matter_id='" +
                    matterId + "' union all " +
                    "Select pc.remoteid,avatarurl as logourl,avatarthumburl as smallurl,trim(trim(first_name) ||' '|| trim(last_name)) as name," +
                    "email,telephone,'' as website,  2 as type FROM person_contacts pc  left join  matters_person_contacts mpc on " +
                    "mpc.person_contact_id=pc.remoteid where matter_id='" + matterId + "'";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatter(String usersIds, String courtIds) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT matters.remoteid , courts.name as court_name, plaintiff, defendant, matter_number, case_year," +
                    " case_types.case_type, judge_name, court_room_no, item_no,on_date FROM matters " +
                    "left join courts on matters.court_id = courts.remoteid " +
                    "left join case_types on matters.case_type_id = case_types.remoteid " +
                    "left join hearings h on h.matter_id= matters.remoteid " +
                    "where matters.remoteid in (select matter_id from matter_users where user_id in (" + usersIds + ")) and" +
                    " matters.court_id in (" + courtIds + ") group by matters.remoteid order by on_date desc";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getCourt() {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid, name from courts order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUser() {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid, trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name" +
                    " from users order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getCourtName(String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select name from courts where remoteid =" + id;
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUserName(String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name" +
                    " from users where remoteid =" + id;
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getImageThumb(int id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select avatarthumburl from users where remoteid = '" + id + "'";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }


    public Cursor getPersonContacts() {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select pc.remoteid,avatarurl,avatarthumburl,trim(trim(first_name) ||' '|| trim(last_name)) as name,pc.email,pc.telephone, " +
                    "cc.name as company_name from person_contacts pc left join company_contacts cc on pc.company_contact_id=cc.remoteid ";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getCompanyContacts() {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid,logourl,smallurl, name,email,telephone from company_contacts";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getAllMatterActivity(int matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select * from (SELECT matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,'' as first_name," +
                    "'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,'' as court_room_no,'' as judge_name,'' as item_no,orders.date" +
                    " as display_date,orders.created_at as sort_date,1 as type,'' as attendee_first_name, '' as attendee_middle_name, " +
                    "'' as attendee_last_name,'' as attendee_id,'' as avatarurl, '' as avatarthumburl,'Order' as type_name,orders.remoteid " +
                    "as remoteid,(Select count(remoteid) from comments where commentable_id=orders.remoteid and commentable_type='Order') as count,'' as category,'' as subcategory" +
                    " FROM orders left join matters on orders.matter_id = matters.remoteid left join courts on matters.court_id =" +
                    " courts.remoteid where orders.matter_id = '" + matterId + "' union all SELECT matters.remoteid as matter_id,posts.creator_id as user_id,'' " +
                    "as court_name,'' as link,first_name,middle_name,last_name,posts.text,plaintiff,defendant,'' as court_room_no,'' as judge_name," +
                    "'' as item_no,posts.created_at as display_date,posts.created_at as sort_date,2 as type,'' as attendee_first_name, " +
                    "'' as attendee_middle_name, '' as attendee_last_name,'' as attendee_id, avatarurl, avatarthumburl,'Post'" +
                    " as type_name, posts.remoteid as remoteid, (Select count(remoteid) from comments where commentable_id=posts.remoteid and commentable_type='Post') as count,'' as category,'' as subcategory " +
                    " FROM posts left join users on posts.creator_id = users.remoteid " +
                    "left join matters on posts.postable_id = matters.remoteid and postable_type = 'Matter' WHERE postable_type = 'Matter' " +
                    "and posts.postable_id = '" + matterId + "' union all SELECT matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,''" +
                    " as first_name,'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,court_room_no,judge_name,item_no,on_date as " +
                    "display_date,hearings.created_at as sort_date,3 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as " +
                    "attendee_last_name,'' as attendee_id, '' as avatarurl, '' as avatarthumburl,'Hearing' as type_name, hearings.remoteid as " +
                    "remoteid,(Select count(remoteid) from comments where commentable_id=hearings.remoteid and commentable_type='Hearing') as count,'' as category,'' as subcategory " +
                    "FROM hearings left join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id = " +
                    "courts.remoteid where hearings.matter_id = '" + matterId + "' union all SELECT matters.remoteid as matter_id, custom_hearings.creator_id as " +
                    "user_id,courts.name as court_name,'' as link, users.first_name, users.middle_name, users.last_name,'' as text,plaintiff," +
                    "defendant,court_room_no,judge_name,item_no,on_date as display_date,custom_hearings.created_at as sort_date,4 as type, " +
                    "attendee.first_name as attendee_first_name, attendee.middle_name as attendee_middle_name, attendee.last_name as " +
                    "attendee_last_name, attendee_id, users.avatarurl, users.avatarthumburl,'CustomHearing' as type_name, custom_hearings.remoteid " +
                    "as remoteid,(Select count(remoteid) from comments where commentable_id=custom_hearings.remoteid and commentable_type='CustomHearing')" +
                    " as count,'' as category,'' as subcategory FROM custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid left join courts on " +
                    "matters.court_id = courts.remoteid left join users on custom_hearings.creator_id = users.remoteid left join users as " +
                    "attendee on custom_hearings.attendee_id = attendee.remoteid where custom_hearings.matter_id = '" + matterId + "' union all select '' " +
                    "as matter_id, '' as user_id, courts.name as court_name, caseLink as link,'' as first_name, '' as middle_name, '' " +
                    "as last_name, details as text, '' as plaintiff, '' as defendant, '' as court_room_no, '' as judge_name, '' " +
                    "as item_no, orderdt as display_date, orderdt as sort_date, 5 as type,'' as attendee_first_name, '' as attendee_middle_name, " +
                    "'' as attendee_last_name, '' as attendee_id, '' as avatarurl,  '' as avatarthumburl,'CaseHistory' as type_name, 0 as remoteid, 0 as count,'' as category,'' as subcategory " +
                    "from case_history left join matters on case_history.matterid = matters.api_matter_id left join courts on matters.court_id = " +
                    "courts.remoteid where case_history.matterid in (select api_matter_id from matters where remoteid = '" + matterId + "' ) union all " +
                    "select matters.remoteid as matter_id, users.remoteid as user_id, courts.name as court_name, '' as link,users.first_name, users.middle_name, " +
                    "users.last_name, description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, notes.created_at " +
                    "as display_date, notes.created_at as sort_date, 6 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as attendee_last_name," +
                    " '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'Note' as type_name, notes.remoteid as remoteid, " +
                    "(Select count(remoteid) from comments where commentable_id=notes.remoteid and commentable_type='Note') as count,'' as category,'' as subcategory from notes left join " +
                    "matters on notes.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid left join users on " +
                    "notes.user_id = users.remoteid where notes.matter_id = '" + matterId + "' union all select matters.remoteid as matter_id, users.remoteid " +
                    "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                    " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                    " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                    " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                    " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                    "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                    "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "') as feed order by sort_date desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }


    public String getFormattedDate(String date) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat appFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        String f_date = "";
        try {
            f_date = appFormat.format(serverFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f_date;
    }

    public String getFormatDate(String date) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat appFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        String f_date = "";
        try {
            f_date = appFormat.format(serverFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f_date;
    }

    public boolean compareDate(String date) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat appFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        try {
            String serverDate = appFormat.format(serverFormat.parse(date));
            Date sDate = appFormat.parse(serverDate);
            Date todayDate = appFormat.parse(appFormat.format(new Date()));
            if (sDate.after(todayDate)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getDate() {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Calendar c = Calendar.getInstance();
            return serverFormat.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getCapitalizeName(String name) {
        StringBuilder builder = new StringBuilder();
        if (name.length() > 0) {
            String[] data = name.split(" ");
            if (data.length > 0) {
                builder.append(data[0].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (data.length > 1) {
                builder.append(data[data.length - 1].substring(0, 1).toUpperCase(Locale.getDefault()));
            }
            if (builder.toString().length() < 2) {
                builder.append(data[0].substring(1, 2).toUpperCase(Locale.getDefault()));
            }
        }
        return builder.toString();
    }

    public String getCapitalizeFullName(String name) {
        StringBuilder builder = new StringBuilder();
        if (name.length() > 0) {
            StringTokenizer token = new StringTokenizer(name, " ");
            while (token.hasMoreElements()) {
                String sub = token.nextToken();
                builder.append(sub.substring(0, 1).toUpperCase(Locale.getDefault()) + sub.substring(1).toLowerCase());
                builder.append(" ");
            }
        }
        return builder.toString().trim();
    }


    public Cursor getAllMatterActivity(int matterId, int cat, int sub, String userId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "";
            if (cat == 1 && sub == 1) {
                query = "SELECT matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,'' as first_name," +
                        "'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,'' as court_room_no,'' as judge_name,'' as item_no,orders.date" +
                        " as display_date,orders.created_at as sort_date,1 as type,'' as attendee_first_name, '' as attendee_middle_name, " +
                        "'' as attendee_last_name,'' as attendee_id,'' as avatarurl, '' as avatarthumburl,'Order' as type_name,orders.remoteid " +
                        "as remoteid,(Select count(remoteid) from comments where commentable_id=orders.remoteid and commentable_type='Order') as count,'' as category,'' as subcategory" +
                        " FROM orders left join matters on orders.matter_id = matters.remoteid left join courts on matters.court_id =" +
                        " courts.remoteid where orders.matter_id = '" + matterId + "' order by sort_date desc";
            } else if (cat == 1 && sub == 2) {
                query = "SELECT matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,''" +
                        " as first_name,'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,court_room_no,judge_name,item_no,on_date as " +
                        "display_date,hearings.created_at as sort_date,3 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as " +
                        "attendee_last_name,'' as attendee_id, '' as avatarurl, '' as avatarthumburl,'Hearing' as type_name, hearings.remoteid as " +
                        "remoteid,(Select count(remoteid) from comments where commentable_id=hearings.remoteid and commentable_type='Hearing') as count,'' as category,'' as subcategory " +
                        "FROM hearings left join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id = " +
                        "courts.remoteid where hearings.matter_id = '" + matterId + "' order by sort_date desc";
            } else if (cat == 1 && sub == 0) {
                query = "select * from (SELECT matters.remoteid as matter_id,0 as user_id,courts.name as court_name,link,'' as first_name," +
                        "'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,'' as court_room_no,'' as judge_name,'' as item_no,orders.date" +
                        " as display_date,orders.created_at as sort_date,1 as type,'' as attendee_first_name, '' as attendee_middle_name, " +
                        "'' as attendee_last_name,'' as attendee_id,'' as avatarurl, '' as avatarthumburl,'Order' as type_name,orders.remoteid " +
                        "as remoteid,(Select count(remoteid) from comments where commentable_id=orders.remoteid and commentable_type='Order') as count,'' as category,'' as subcategory" +
                        " FROM orders left join matters on orders.matter_id = matters.remoteid left join courts on matters.court_id =" +
                        " courts.remoteid where orders.matter_id = '" + matterId + "' union all SELECT matters.remoteid as matter_id, 0 as user_id,courts.name as court_name,'' as link,''" +
                        " as first_name,'' as middle_name,'' as last_name,'' as text,plaintiff,defendant,court_room_no,judge_name,item_no,on_date as " +
                        "display_date,hearings.created_at as sort_date,3 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as " +
                        "attendee_last_name,'' as attendee_id, '' as avatarurl, '' as avatarthumburl,'Hearing' as type_name, hearings.remoteid as " +
                        "remoteid,(Select count(remoteid) from comments where commentable_id=hearings.remoteid and commentable_type='Hearing') as count,'' as category,'' as subcategory " +
                        "FROM hearings left join matters on hearings.matter_id = matters.remoteid left join courts on matters.court_id = " +
                        "courts.remoteid where hearings.matter_id = '" + matterId + "' union all select '' " +
                        "as matter_id, '' as user_id, courts.name as court_name, caseLink as link,'' as first_name, '' as middle_name, '' " +
                        "as last_name, details as text, '' as plaintiff, '' as defendant, '' as court_room_no, '' as judge_name, '' " +
                        "as item_no, orderdt as display_date, orderdt as sort_date, 5 as type,'' as attendee_first_name, '' as attendee_middle_name, " +
                        "'' as attendee_last_name, '' as attendee_id, '' as avatarurl,  '' as avatarthumburl,'CaseHistory' as type_name, 0 as remoteid, 0 as count,'' as category,'' as subcategory " +
                        "from case_history left join matters on case_history.matterid = matters.api_matter_id left join courts on matters.court_id = " +
                        "courts.remoteid where case_history.matterid in (select api_matter_id from matters where remoteid = '" + matterId + "') ) as feed order by sort_date desc";
            } else if (cat == 2 && sub == 0) {
                query = "select * from (SELECT matters.remoteid as matter_id,posts.creator_id as user_id,'' " +
                        "as court_name,'' as link,first_name,middle_name,last_name,posts.text,plaintiff,defendant,'' as court_room_no,'' as judge_name," +
                        "'' as item_no,posts.created_at as display_date,posts.created_at as sort_date,2 as type,'' as attendee_first_name, " +
                        "'' as attendee_middle_name, '' as attendee_last_name,'' as attendee_id, avatarurl, avatarthumburl,'Post'" +
                        " as type_name, posts.remoteid as remoteid, (Select count(remoteid) from comments where commentable_id=posts.remoteid and commentable_type='Post') as count,'' as category,'' as subcategory " +
                        " FROM posts left join users on posts.creator_id = users.remoteid " +
                        "left join matters on posts.postable_id = matters.remoteid and postable_type = 'Matter' WHERE postable_type = 'Matter' " +
                        "and posts.postable_id = '" + matterId + "' union all SELECT matters.remoteid as matter_id, custom_hearings.creator_id as " +
                        "user_id,courts.name as court_name,'' as link, users.first_name, users.middle_name, users.last_name,'' as text,plaintiff," +
                        "defendant,court_room_no,judge_name,item_no,on_date as display_date,custom_hearings.created_at as sort_date,4 as type, " +
                        "attendee.first_name as attendee_first_name, attendee.middle_name as attendee_middle_name, attendee.last_name as " +
                        "attendee_last_name, attendee_id, users.avatarurl, users.avatarthumburl,'CustomHearing' as type_name, custom_hearings.remoteid " +
                        "as remoteid,(Select count(remoteid) from comments where commentable_id=custom_hearings.remoteid and commentable_type='CustomHearing')" +
                        " as count,'' as category,'' as subcategory FROM custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid left join courts on " +
                        "matters.court_id = courts.remoteid left join users on custom_hearings.creator_id = users.remoteid left join users as " +
                        "attendee on custom_hearings.attendee_id = attendee.remoteid where custom_hearings.matter_id = '" + matterId + "') as feed order by sort_date desc";
            } else if (cat == 2 && sub == 1) {
                query = "select * from (SELECT matters.remoteid as matter_id,posts.creator_id as user_id,'' " +
                        "as court_name,'' as link,first_name,middle_name,last_name,posts.text,plaintiff,defendant,'' as court_room_no,'' as judge_name," +
                        "'' as item_no,posts.created_at as display_date,posts.created_at as sort_date,2 as type,'' as attendee_first_name, " +
                        "'' as attendee_middle_name, '' as attendee_last_name,'' as attendee_id, avatarurl, avatarthumburl,'Post'" +
                        " as type_name, posts.remoteid as remoteid, (Select count(remoteid) from comments where commentable_id=posts.remoteid and commentable_type='Post') as count,'' as category,'' as subcategory " +
                        " FROM posts left join users on posts.creator_id = users.remoteid " +
                        "left join matters on posts.postable_id = matters.remoteid and postable_type = 'Matter' WHERE postable_type = 'Matter' " +
                        "and posts.postable_id = '" + matterId + "' and posts.creator_id = " + userId + " union all SELECT matters.remoteid as matter_id, custom_hearings.creator_id as " +
                        "user_id,courts.name as court_name,'' as link, users.first_name, users.middle_name, users.last_name,'' as text,plaintiff," +
                        "defendant,court_room_no,judge_name,item_no,on_date as display_date,custom_hearings.created_at as sort_date,4 as type, " +
                        "attendee.first_name as attendee_first_name, attendee.middle_name as attendee_middle_name, attendee.last_name as " +
                        "attendee_last_name, attendee_id, users.avatarurl, users.avatarthumburl,'CustomHearing' as type_name, custom_hearings.remoteid " +
                        "as remoteid,(Select count(remoteid) from comments where commentable_id=custom_hearings.remoteid and commentable_type='CustomHearing')" +
                        " as count,'' as category,'' as subcategory FROM custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid left join courts on " +
                        "matters.court_id = courts.remoteid left join users on custom_hearings.creator_id = users.remoteid left join users as " +
                        "attendee on custom_hearings.attendee_id = attendee.remoteid where custom_hearings.matter_id = '" + matterId + "' and custom_hearings.creator_id = " + userId + ") as feed order by sort_date desc";
            } else if (cat == 3 && sub == 0) {
                query = "select matters.remoteid as matter_id, users.remoteid as user_id, courts.name as court_name, '' as link,users.first_name, users.middle_name, " +
                        "users.last_name, description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, notes.created_at " +
                        "as display_date, notes.created_at as sort_date, 6 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as attendee_last_name," +
                        " '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'Note' as type_name, notes.remoteid as remoteid, " +
                        "(Select count(remoteid) from comments where commentable_id=notes.remoteid and commentable_type='Note') as count,'' as category,'' as subcategory from notes left join " +
                        "matters on notes.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid left join users on " +
                        "notes.user_id = users.remoteid where notes.matter_id = '" + matterId + "' order by sort_date desc";
            } else if (cat == 3 && sub == 1) {
                query = "select matters.remoteid as matter_id, users.remoteid as user_id, courts.name as court_name, '' as link,users.first_name, users.middle_name, " +
                        "users.last_name, description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, notes.created_at " +
                        "as display_date, notes.created_at as sort_date, 6 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as attendee_last_name," +
                        " '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'Note' as type_name, notes.remoteid as remoteid, " +
                        "(Select count(remoteid) from comments where commentable_id=notes.remoteid and commentable_type='Note') as count,'' as category,'' as subcategory from notes left join " +
                        "matters on notes.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid left join users on " +
                        "notes.user_id = users.remoteid where notes.matter_id = '" + matterId + "' and users.remoteid =" + userId + " order by sort_date desc";
            } else if (cat == 4 && sub == 0) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' order by sort_date desc";
            } else if (cat == 4 && sub == 1) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' and category ='Court File' order by sort_date desc";
            } else if (cat == 4 && sub == 2) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' and category ='Evidence' order by sort_date desc";
            } else if (cat == 4 && sub == 3) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' and category ='NOA (Note On Arguments)' order by sort_date desc";
            } else if (cat == 4 && sub == 4) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' and category ='Miscellaneous' order by sort_date desc";
            } else if (cat == 4 && sub == 5) {
                query = "select matters.remoteid as matter_id, users.remoteid " +
                        "as user_id, courts.name as court_name, uploads_assets.url as link, users.first_name, users.middle_name, users.last_name," +
                        " description as text, plaintiff, defendant, '' as court_room_no, '' as judge_name, '' as item_no, uploads.updated_at as" +
                        " display_date, uploads.updated_at as sort_date, 7 as type,'' as attendee_first_name, '' as attendee_middle_name, '' as" +
                        " attendee_last_name, '' as attendee_id, users.avatarurl,  users.avatarthumburl, 'File' as type_name, uploads.remoteid as" +
                        " remoteid, 0 as count,category,subcategory from uploads left join matters on uploads.uploadable_id = matters.remoteid and uploads.uploadable_type = 'Matter' " +
                        "left join courts on matters.court_id = courts.remoteid left join users on uploads.creator_id = users.remoteid left join " +
                        "uploads_assets on uploads.remoteid = uploads_assets.upload_id where uploads.uploadable_id = '" + matterId + "' and category ='New CategoryII' order by sort_date desc";
            }

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getComment(int id, int typeId) {
        Cursor cursor = null;
        String type = "";
        switch (typeId) {
            case 1:
                type = "Order";
                break;
            case 2:
                type = "Post";
                break;
            case 3:
                type = "Hearing";
                break;
            case 4:
                type = "CustomHearing";
                break;
            case 6:
                type = "Note";
                break;
        }
        try {
            DbManager.getInstance().openDatabase();
            String query = "select creator_id as user_id, first_name, middle_name,last_name " +
                    ",avatarurl,avatarthumburl,text, comments.created_at " +
                    "as display_date from comments left join users on comments.creator_id = users.remoteid where commentable_id = '" + id + "' " +
                    "and commentable_type = '" + type + "' order by display_date desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getAllNotifications(String userid) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select notifications.remoteid, creator_id,link,updated_at,notification_text,notificationable_type,notificationable_id," +
                    "read from notifications left join notification_status on notifications.remoteid = notification_status.notification_id and " +
                    "notification_status.member_id = '" + userid + "' where notifications.inserted = 0 order by updated_at desc";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getAllKey() {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select * from (select key_name from company_contacts group by key_name union all select key_name from " +
                    "company_contacts_matters group by key_name union all select key_name from custom_hearings group by key_name union all " +
                    "select key_name from matter_users group by key_name union all select key_name from comments group by key_name union all " +
                    "select key_name from matters_person_contacts group by key_name union all select key_name from notes group by key_name union all" +
                    " select key_name from notification_status group by key_name union all select key_name from notifications group by key_name " +
                    "union all select key_name from person_contacts group by key_name union all select key_name from posts group by key_name " +
                    "union all select key_name from tasks group by key_name) as keys where key_name is not null and key_name != '101' group by key_name";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public int getMaxId(String tableName) {
        Cursor cursor = null;
        int id = 1;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select max(remoteid) from " + tableName;
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            if (cursor != null && cursor.moveToFirst()) {
                id = cursor.getInt(cursor.getColumnIndex(cursor.getColumnName(0)));
                id++;
            }
            if (cursor != null) {
                cursor.close();
            }
            DbManager.getInstance().closeDatabase();
            return id;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return id;
    }

    public Boolean isMyMatter(String id, String userId) {
        Cursor cursor = null;
        Boolean val = false;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select id from matter_users where matter_id = '" + id + "' and user_id = '" + userId + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            if (cursor != null && cursor.moveToFirst()) {
                val = true;
            }
            if (cursor != null) {
                cursor.close();
            }
            DbManager.getInstance().closeDatabase();
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return val;
    }

    public Boolean isRequestSent(String id, String userId) {
        Cursor cursor = null;
        Boolean val = false;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select * from notifications where creator_id = '" + userId + "' and notice_type = 'Request' and " +
                    "notificationable_type = 'Matter' and notificationable_id = '" + id + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            if (cursor != null && cursor.moveToFirst()) {
                val = true;
            }
            if (cursor != null) {
                cursor.close();
            }
            DbManager.getInstance().closeDatabase();
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return val;
    }


    /**
     * ======= Updated Records =========
     **/

    public Cursor getUpdatePersonContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select person_contacts.remoteid,creator_id,admin_id,contact_type,title,first_name,last_name,company_contact_id," +
                    "address1,address2,city,zip,state,country,is_client,person_contacts.created_at,person_contacts.updated_at,avatarurl," +
                    "avatarthumburl,email,telephone from person_contacts left join matters_person_contacts on person_contacts.remoteid = " +
                    "matters_person_contacts.person_contact_id where person_contacts.updated = 1 and person_contacts.deleted = 0 and " +
                    "person_contacts.inserted = 0 and person_contacts.key_name = '" + key + "'";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUpdateNotification(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query="SELECT remoteid from notifications n left join notification_status ns on n.remoteid=ns.notification_id" +
                    " where (n.updated = 1 and n.key_name = '" + key + "' ) or ( ns.updated = 1 and ns.key_name = '" + key + "' )";

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUpdateReadNotification(String key, String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select member_id from notification_status where notification_id = '" + id + "' and read = 1 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUpdateTask(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid, completed, updated_at from tasks where updated = 1 and inserted = 0 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getUpdateCompanyContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select company_contacts.remoteid, name, email, telephone, website, address1, address2, city, zip, state, country," +
                    " creator_id, admin_id, is_client, company_contacts.created_at, company_contacts.updated_at from company_contacts where" +
                    " company_contacts.updated = 1 and company_contacts.deleted = 0 and company_contacts.inserted = 0 and " +
                    "company_contacts.key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    /**
     * ======= Deleted Records =========
     **/

    public Cursor getDeletedPersonContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid from person_contacts where deleted = 1 and inserted = 0 and key_name =  '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getDeleteCompanyMatterContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid from company_contacts_matters where deleted = 1 and inserted = 0 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getDeleteMatterUser(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid from matter_users where deleted = 1 and inserted = 0 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getDeleteMatterPersonContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid from matters_person_contacts where deleted = 1 and inserted = 0 and key_name =  '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getDeleteCompanyContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select remoteid from company_contacts where deleted = 1 and inserted = 0 and key_name =  '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    /**
     * ======= Inserted Records =========
     **/


    public Cursor getInsertTask(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select matter_id,user_id,completed,title,due_at,assigned_to,category,admin_id,created_at,updated_at " +
                    "from tasks where deleted = 0 and inserted = 1 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertHearing(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select hearings.remoteid from hearings left join comments on hearings.remoteid = comments.commentable_id and " +
                    "comments.commentable_type = 'Hearing' where comments.inserted = 1 and comments.key_name = '" + key + "' group by hearings.remoteid ";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getNewComments(String key, String id, String type) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select text,creator_id,admin_id,created_at,updated_at from comments where commentable_id = '" + id + "'" +
                    " and commentable_type='" + type + "' and inserted = 1 and key_name ='" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertCompanyContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select company_contacts.remoteid, name, email, telephone, website, address1, address2, city, zip, state, country," +
                    " creator_id, admin_id, is_client, company_contacts.created_at, company_contacts.updated_at,  company_contacts.inserted" +
                    " from company_contacts left join company_contacts_matters on company_contacts.remoteid = company_contacts_matters." +
                    "company_contact_id where (company_contacts.inserted = 1 and company_contacts.key_name = '" + key + "' ) " +
                    "or (company_contacts_matters.inserted = 1 and company_contacts_matters.deleted = 0 and" +
                    " company_contacts_matters.key_name = '" + key + "' ) group by company_contacts.remoteid ";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getCompanyContactsMatterForKey(String key, String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select matter_id from company_contacts_matters where company_contact_id = '" + id + "' and inserted = 1 and deleted = 0 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertPersonContact(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select person_contacts.remoteid,creator_id,admin_id,contact_type,title,first_name,last_name,company_contact_id," +
                    "address1,address2,city,zip,state,country,is_client,person_contacts.created_at,person_contacts.updated_at,avatarurl," +
                    "avatarthumburl,email,telephone, person_contacts.inserted from person_contacts left join matters_person_contacts on" +
                    " person_contacts.remoteid = matters_person_contacts.person_contact_id where (person_contacts.inserted = 1 and" +
                    " person_contacts.key_name = '" + key + "') or (matters_person_contacts.inserted = 1 and matters_person_contacts.deleted = " +
                    "0 and matters_person_contacts.key_name = '" + key + "') group by person_contacts.remoteid ";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getPersonContactsMatterForKey(String key, String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select matter_id from matters_person_contacts where person_contact_id = '" + id + "' and inserted = 1 and deleted = 0 " +
                    "and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertCustomHearing(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select custom_hearings.remoteid,custom_hearings.matter_id,on_date,judge_name,court_room_no,custom_hearings.creator_id," +
                    "custom_hearings.admin_id,attendee_id,custom_hearings.created_at,custom_hearings.updated_at,custom_hearings.inserted from " +
                    "custom_hearings left join comments on custom_hearings.remoteid = comments.commentable_id and comments.commentable_type " +
                    "= 'CustomHearing' where (custom_hearings.inserted = 1 and custom_hearings.key_name = '" + key + "') or (comments.inserted = 1 and " +
                    "comments.key_name = '" + key + "') group by custom_hearings.remoteid";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertOrder(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select orders.remoteid from orders left join comments on orders.remoteid = comments.commentable_id and " +
                    "comments.commentable_type = 'Order' where comments.inserted = 1 and comments.key_name = '" + key + "' group by orders.remoteid";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertMatterUsers(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select matter_id,user_id,created_at,updated_at from matter_users where deleted = 0 and inserted = 1 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertPost(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select posts.remoteid,posts.text,postable_id,postable_type,posts.creator_id,posts.admin_id,posts.inserted," +
                    "posts.created_at,posts.updated_at from posts left join comments on posts.remoteid = comments.commentable_id " +
                    "and comments.commentable_type = 'Post' where (posts.inserted = 1 and posts.key_name = '" + key + "') or " +
                    "(comments.inserted = 1 and comments.key_name = '" + key + "') group by posts.remoteid";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertNote(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select notes.remoteid, user_id ,matter_id, description, notes.admin_id, notes.created_at, notes.updated_at, " +
                    "notes.inserted,notes.teams,notes.contacts,notes.emails from notes left join comments on notes.remoteid = comments.commentable_id and comments.commentable_type = " +
                    "'Note' where (notes.inserted = 1 and notes.key_name = '" + key + "') or (comments.inserted = 1 and comments.key_name = '" + key + "')" +
                    " group by notes.remoteid";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getInsertNotification(String key) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select notifications.remoteid, creator_id,notice_type,notificationable_type,notificationable_id,created_at,updated_at" +
                    " from notifications where inserted = 1 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMemberForNotification(String key, String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "select member_id from notification_status where notification_id = '" + id + "' and inserted = 1 and key_name = '" + key + "'";
            cursor = DbManager.getInstance().getDetails(query);
            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getCalenderHearing(String courtId, String userId, String startDate, String endDate) {
        Cursor cursor = null;


        String court = "matters.court_id in (" + courtId + ")";

        String query = "SELECT custom_hearings .on_date as display_date, count(custom_hearings .on_date)" +
                " as total FROM custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid" +
                " left join courts on matters.court_id = courts.remoteid where custom_hearings.matter_id in " +
                "(SELECT matter_id FROM matter_users where user_id = '" + userId + "') and " + court + " and custom_hearings.on_date >= '" + startDate +
                "' and custom_hearings.on_date <= '" + endDate + "' group By custom_hearings.on_date union all" +
                " SELECT hearings .on_date as display_date, count(hearings.on_date) as total FROM hearings" +
                " left join matters on hearings.matter_id = matters.remoteid left join courts on" +
                " matters.court_id = courts.remoteid where hearings.matter_id in (SELECT matter_id FROM matter_users" +
                " where user_id = '" + userId + "') and " + court + " and hearings.on_date >= '" + startDate + "' and hearings.on_date <= '" + endDate + "' group By hearings.on_date ORDER BY display_date ASC";


        try {
            DbManager.getInstance().openDatabase();
            AppLogger.logD(TAG, "query : " + query);
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();

            return cursor;

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;

    }

    public Cursor getCalenderTask(String userId, String startDate, String endDate) {
        Cursor cursor = null;


        String query = "select due_at as display_date, count(due_at) " +
                "as total  from tasks where user_id = '" + userId + "' and due_at >= '" + startDate +
                "' and due_at <= '" + endDate + "' group By due_at";


        try {
            DbManager.getInstance().openDatabase();
            AppLogger.logD(TAG, "query : " + query);
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();

            return cursor;

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;

    }

    public Cursor getHearingList(String userId, String courseId, String startDate, String endDate) {
        Cursor cursor = null;

        String query = "SELECT matters.remoteid as matter_id,courts.name as court_name,matters.court_id,plaintiff,defendant,court_room_no,judge_name," +
                "item_no,on_date,matter_number,case_year,case_types.case_type FROM hearings left join matters on " +
                "hearings.matter_id = matters.remoteid left join courts on matters.court_id = courts.remoteid left " +
                "join case_types on matters.case_type_id = case_types.remoteid where hearings.matter_id in " +
                "(SELECT matter_id FROM matter_users where user_id = '" + userId + "') and matters.court_id in (" + courseId + ") and hearings.on_date" +
                " >= '" + startDate + "' and hearings.on_date <= '" + endDate + "' order by matters.court_id";

        try {
            DbManager.getInstance().openDatabase();
            AppLogger.logD(TAG, "query : " + query);
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());


            return cursor;

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return cursor;

    }


    public Cursor getCustomHearingList(String userId, String courseId, String startDate, String endDate) {
        Cursor cursor = null;

        String query = "SELECT matters.remoteid as matter_id,courts.name as court_name,plaintiff,defendant,court_room_no,judge_name,item_no," +
                "on_date,matter_number,case_year,case_types.case_type,first_name,last_name,users.remoteid as attendee_id FROM " +
                "custom_hearings left join matters on custom_hearings.matter_id = matters.remoteid left join courts on" +
                " matters.court_id = courts.remoteid left join case_types on matters.case_type_id = case_types.remoteid " +
                "left join users on custom_hearings.attendee_id = users.remoteid where custom_hearings.matter_id in " +
                "(SELECT matter_id FROM matter_users where user_id = '" + userId + "') and matters.court_id in (" + courseId + ") and " +
                "custom_hearings.on_date >= '" + startDate + "' and custom_hearings.on_date <= '" + endDate + "' ";

        try {
            DbManager.getInstance().openDatabase();
            AppLogger.logD(TAG, "query : " + query);
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());


            return cursor;

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return cursor;

    }

    public Cursor getTaskList(String userId, String courseId, String startDate, String endDate) {
        Cursor cursor = null;

        String query = "select tasks.remoteid,tasks.matter_id,title,category,plaintiff,defendant,completed,due_at," +
                "trim (trim(trim(first_name) ||' '|| trim(middle_name)) ||' '|| trim(last_name)) as name,tasks.assigned_to" +
                " from tasks left join matters on tasks.matter_id = matters.remoteid inner join users u on u.remoteid=tasks.assigned_to where" +
                " user_id = " + userId + " and due_at >= '" + startDate + "' and" +
                " due_at <= '" + endDate + "'";
        try {
            DbManager.getInstance().openDatabase();
            AppLogger.logD(TAG, "query : " + query);
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());


            return cursor;

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return cursor;

    }

    public int getNotiCount(String userId) {
        Cursor cursor = null;
        int count = 0;
        String query = "select count(notification_id) from notification_status where member_id = '" + userId + "' and read = 0";
        try {
            DbManager.getInstance().openDatabase();
            cursor = DbManager.getInstance().getDetails(query);
            if (cursor != null && cursor.moveToFirst()) {
                count = cursor.getInt(cursor.getColumnIndex(cursor.getColumnName(0)));
            }
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return count;
    }

    public Cursor getPerson(int userId) {
        Cursor cursor = null;
        String query = "select * from person_contacts where remoteid = '" + userId + "'";
        try {
            DbManager.getInstance().openDatabase();
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return cursor;
    }

    public Cursor getCompany(int userId) {
        Cursor cursor = null;
        String query = "select * from company_contacts where remoteid = '" + userId + "'";
        try {
            DbManager.getInstance().openDatabase();
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
        return cursor;
    }

    public void hideKeyboard(View v, Context ctx) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public boolean isAddAllowed(Context ctx) {
        if (AppPreferences.getRole(ctx).equalsIgnoreCase("admin") || AppPreferences.getRole(ctx).equalsIgnoreCase("superuser")) {
            return true;
        }
        return false;
    }

    public boolean isMatterExist(int id) {
        Boolean val = false;
        Cursor cursor = null;
        String query = "select * from matters where remoteid = '" + id + "'";
        try {
            DbManager.getInstance().openDatabase();
            cursor = DbManager.getInstance().getDetails(query);
            if (cursor != null && cursor.getCount() > 0) {
                val = true;
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        } finally {
            DbManager.getInstance().closeDatabase();
        }

        return val;
    }

    public Cursor getNonMatterUser(int matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT users.remoteid, trim(trim(first_name) ||' '|| trim(last_name)) as name ,avatarthumburl" +
                    " FROM users where remoteid not in (select user_id from matter_users where matter_id = '" + matterId + "' and deleted = 0 ) order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getNonMatterContacts(int matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select remoteid,trim(trim(first_name) ||' '|| trim(last_name)) as name,avatarthumburl " +
                    " from person_contacts where remoteid not in (select person_contact_id from matters_person_contacts " +
                    "where matter_id = '" + matterId + "' and deleted = 0 ) order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getNonMatterUserName(String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT trim(trim(first_name) ||' '|| trim(last_name)) as name " +
                    " FROM users where remoteid  in (" + id + ") order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }


    public Cursor getNonMatterContactsName(String id) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "Select trim(trim(first_name) ||' '|| trim(last_name)) as name" +
                    " from person_contacts where remoteid  in (" + id + ") order by name";
            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    public Cursor getMatterUsers(String matterId) {
        Cursor cursor = null;
        try {
            DbManager.getInstance().openDatabase();
            String query = "SELECT remoteid,trim(trim(first_name) ||' '|| trim(last_name)) as name,avatarthumburl FROM users " +
                    "where remoteid in (select user_id from matter_users where matter_id = '" + matterId + "' and deleted = 0)";
            AppLogger.logE(TAG, "getMatterUser query: " + query);

            cursor = DbManager.getInstance().getDetails(query);

            AppLogger.logD(TAG, "Results counts : " + cursor.getCount());
            DbManager.getInstance().closeDatabase();
            return cursor;
        } catch (Exception e) {
            AppLogger.logE(TAG, "Error in fetching data from database", e);
        }
        return cursor;
    }

    // insert into custom_hearings(remoteid,matter_id ,on_date , judge_name,court_room_no,creator_id,admin_id,created_at,updated_at,attendee_id,inserted,key_name) values(?,?,?,?,?,?,?,?,?,?,?,?)
}
