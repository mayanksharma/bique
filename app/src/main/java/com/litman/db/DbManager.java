package com.litman.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.litman.interfaces.CCMatters;
import com.litman.interfaces.CaseType;
import com.litman.interfaces.Comments;
import com.litman.interfaces.CompanyContacts;
import com.litman.interfaces.Courts;
import com.litman.interfaces.CustomHearing;
import com.litman.interfaces.Hearing;
import com.litman.interfaces.MPContacts;
import com.litman.interfaces.MatterUsers;
import com.litman.interfaces.Matters;
import com.litman.interfaces.Notes;
import com.litman.interfaces.Notifications;
import com.litman.interfaces.NotificationsStatus;
import com.litman.interfaces.Orders;
import com.litman.interfaces.PersonContacts;
import com.litman.interfaces.Post;
import com.litman.interfaces.Tasks;
import com.litman.interfaces.UploadAssets;
import com.litman.interfaces.Uploads;
import com.litman.interfaces.Users;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


public class DbManager {

    public AtomicInteger mOpenCounter = new AtomicInteger();

    private static DbManager instance;
    private static SQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDb;
    private final String TAG = DbManager.class.getSimpleName();

    public static synchronized void initializeInstance(SQLiteOpenHelper helper, Context context) {
        if (instance == null) {
            instance = new DbManager();
            mDatabaseHelper = helper;
        }
    }

//    public synchronized void resetDatabase() {
//        ((DBHelper) mDatabaseHelper).dropDb(mDb);
//    }

    public static synchronized DbManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(
                    DbManager.class.getSimpleName()
                            + " is not initialized, call initializeInstance(..) method first.");
        }
        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        if (mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDb = mDatabaseHelper.getWritableDatabase();
        }

        return mDb;
    }

    public synchronized void closeDatabase() {
        if (mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDb.close();
        }
    }


    public boolean replace(String tableName, ContentValues values) {
        try {
            mDb.replace(tableName, null, values);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean replaceList(String tableName, ArrayList<ContentValues> values) {
        try {
            mDb.beginTransaction();
            for (ContentValues v : values) {
                mDb.replace(tableName, null, v);
            }
            mDb.setTransactionSuccessful();
            mDb.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean insert(String tableName, ContentValues values) {
        try {
            mDb.insert(tableName, null, values);
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {

        }
        return true;
    }

    public long insertAndGet(String tableName, ContentValues values) {
        try {
            return mDb.insert(tableName, null, values);
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {

        }
        return Long.valueOf(0);
    }

    public boolean deleteAll() {
        try {
            mDb.execSQL("Delete from case_types");
            mDb.execSQL("Delete from comments");
            mDb.execSQL("Delete from company_contacts");
            mDb.execSQL("Delete from courts");
            mDb.execSQL("Delete from custom_hearings");
            mDb.execSQL("Delete from hearings");
            mDb.execSQL("Delete from matter_users");
            mDb.execSQL("Delete from matters");
            mDb.execSQL("Delete from notes");
            mDb.execSQL("Delete from posts");
            mDb.execSQL("Delete from tasks");
            mDb.execSQL("Delete from users");
            mDb.execSQL("Delete from person_contacts");
            mDb.execSQL("Delete from notifications");
            mDb.execSQL("Delete from notification_status");
            mDb.execSQL("Delete from orders");
            mDb.execSQL("Delete from company_contacts_matters");
            mDb.execSQL("Delete from matters_person_contacts");
            mDb.execSQL("Delete from uploads");
            mDb.execSQL("Delete from uploads_assets");
            mDb.execSQL("Delete from case_history");
            mDb.execSQL("VACUUM");
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return true;
    }


    public Cursor getDetails(String query) {
        Cursor c = null;
        try {
            c = mDb.rawQuery(query, null);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return c;
    }

    public boolean delete(String tableName, String column, String id) {
        try {
            mDb.delete(tableName, column + " =? ", new String[]{id});
        } catch (SQLiteException e) {

        }
        return true;
    }

    public boolean insertList(String tableName, ArrayList<ContentValues> values) {
        try {
            mDb.beginTransaction();
            for (ContentValues v : values) {
                mDb.insert(tableName, null, v);
            }
            mDb.setTransactionSuccessful();
            mDb.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateList(String key) {
        try {
            mDb.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put(PersonContacts.KEY, "101");
            mDb.update(CaseType.T_NAME, cv, CaseType.KEY + " =? ", new String[]{key});
            mDb.update(CCMatters.T_NAME, cv, CCMatters.KEY + " =? ", new String[]{key});
            mDb.update(Comments.T_NAME, cv, Comments.KEY + " =? ", new String[]{key});
            mDb.update(CompanyContacts.T_NAME, cv, CompanyContacts.KEY + " =? ", new String[]{key});
            mDb.update(Courts.T_NAME, cv, Courts.KEY + " =? ", new String[]{key});
            mDb.update(CustomHearing.T_NAME, cv, CustomHearing.KEY + " =? ", new String[]{key});
            mDb.update(Hearing.T_NAME, cv, Hearing.KEY + " =? ", new String[]{key});
            mDb.update(Matters.T_NAME, cv, Matters.KEY + " =? ", new String[]{key});
            mDb.update(MatterUsers.T_NAME, cv, MatterUsers.KEY + " =? ", new String[]{key});
            mDb.update(MPContacts.T_NAME, cv, MPContacts.KEY + " =? ", new String[]{key});
            mDb.update(Notes.T_NAME, cv, Notes.KEY + " =? ", new String[]{key});
            mDb.update(Notifications.T_NAME, cv, Notifications.KEY + " =? ", new String[]{key});
            mDb.update(NotificationsStatus.T_NAME, cv, NotificationsStatus.KEY + " =? ", new String[]{key});
            mDb.update(Orders.T_NAME, cv, Orders.KEY + " =? ", new String[]{key});
            mDb.update(PersonContacts.T_NAME, cv, PersonContacts.KEY + " =? ", new String[]{key});
            mDb.update(Post.T_NAME, cv, Post.KEY + " =? ", new String[]{key});
            mDb.update(Tasks.T_NAME, cv, Tasks.KEY + " =? ", new String[]{key});
            mDb.update(UploadAssets.T_NAME, cv, UploadAssets.KEY + " =? ", new String[]{key});
            mDb.update(Uploads.T_NAME, cv, Uploads.KEY + " =? ", new String[]{key});
            mDb.update(Users.T_NAME, cv, Users.KEY + " =? ", new String[]{key});

            mDb.setTransactionSuccessful();
            mDb.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteList(String key) {
        try {
            mDb.beginTransaction();

            mDb.execSQL("Delete from case_types where key_name='101' ");
            mDb.execSQL("Delete from comments where key_name='101' ");
            mDb.execSQL("Delete from company_contacts where key_name='101' ");
            mDb.execSQL("Delete from courts where key_name='101' ");
            mDb.execSQL("Delete from custom_hearings where key_name='101' ");
            mDb.execSQL("Delete from hearings where key_name='101' ");
            mDb.execSQL("Delete from matter_users where key_name='101' ");
            mDb.execSQL("Delete from matters where key_name='101' ");
            mDb.execSQL("Delete from notes where key_name='101' ");
            mDb.execSQL("Delete from posts where key_name='101' ");
            mDb.execSQL("Delete from tasks where key_name='101' ");
            mDb.execSQL("Delete from users where key_name='101' ");
            mDb.execSQL("Delete from person_contacts where key_name='101' ");
            mDb.execSQL("Delete from notifications where key_name='101' ");
            mDb.execSQL("Delete from notification_status where key_name='101' ");
            mDb.execSQL("Delete from orders where key_name='101' ");
            mDb.execSQL("Delete from company_contacts_matters where key_name='101' ");
            mDb.execSQL("Delete from matters_person_contacts where key_name='101' ");
            mDb.execSQL("Delete from uploads where key_name='101' ");
            mDb.execSQL("Delete from uploads_assets where key_name='101' ");

            mDb.setTransactionSuccessful();
            mDb.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean update(String tableName, ContentValues values,
                          String column, String id) {
        try {
            mDb.update(tableName, values, column + " =? ", new String[]{id});
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean update(String tableName, ContentValues values,
                          String column1, String column2, String id1, String id2) {
        try {
            mDb.update(tableName, values, column1 + " =? and " + column2 + " =? ", new String[]{id1, id2});
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateQuery(String tableName, ContentValues values,
                               String where, String [] val) {
        try {
            mDb.update(tableName, values, where, val);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return true;
    }

}
