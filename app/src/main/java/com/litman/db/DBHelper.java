package com.litman.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "Biqe.sqlite";// Environment.getExternalStorageDirectory()+"/"+//

    private static final int DATABASE_VERSION = 1;

    private Context ctx;

    private final String TAG = DBHelper.class.getSimpleName();

    String CREATE_TABLE_CASE_TYPE = "CREATE TABLE case_types (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, " +
            " remoteid integer UNIQUE," +
            " case_type character varying," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " court_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_COMMENTS = "CREATE TABLE comments (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, " +
            " remoteid integer UNIQUE," +
            " text text," +
            " creator_id integer," +
            " commentable_id integer," +
            " commentable_type character varying," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " admin_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_COMPANY_CONTACTS = "CREATE TABLE company_contacts (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " name character varying," +
            " email character varying," +
            " telephone character varying," +
            " website character varying," +
            " address1 character varying," +
            " address2 character varying," +
            " city character varying," +
            " zip character varying," +
            " state character varying," +
            " country character varying," +
            " creator_id integer," +
            " admin_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " logourl character varying," +
            " smallurl character varying," +
            " is_client boolean," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_COURTS = "CREATE TABLE courts (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " name character varying," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_CUSTOM_HEARING = "CREATE TABLE custom_hearings (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_id integer," +
            " on_date timestamp without time zone," +
            " judge_name character varying," +
            " added_on timestamp without time zone," +
            " court_room_no character varying," +
            " item_no character varying," +
            " hearing_id integer," +
            " creator_id integer," +
            " admin_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " attendee_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_HEARING = "CREATE TABLE hearings (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_id integer," +
            " on_date timestamp without time zone," +
            " judge_name character varying," +
            " added_on timestamp without time zone," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " court_room_no character varying," +
            " item_no character varying," +
            " api_hearing_id integer," +
            " created_by integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_MATTER_USER = "CREATE TABLE matter_users (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " user_id integer," +
            " matter_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " my_client character varying," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_MATTER = "CREATE TABLE matters (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_number character varying," +
            " plaintiff_attorney character varying," +
            " diffendent_attorney character varying," +
            " internal_status text," +
            " court_status text," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " plaintiff character varying," +
            " defendant character varying," +
            " client_type integer DEFAULT 0," +
            " case_type character varying," +
            " case_year character varying," +
            " court_id integer," +
            " creator_id integer," +
            " admin_id integer," +
            " case_type_id integer," +
            " api_matter_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_NOTES = "CREATE TABLE notes (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " user_id integer," +
            " matter_id integer," +
            " description text," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " admin_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying," +
            " teams character varying DEFAULT ''," +
            " contacts character varying DEFAULT ''," +
            " emails character varying DEFAULT ''" +
            ")";

    String CREATE_TABLE_POST = "CREATE TABLE posts (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " text text," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " postable_id integer," +
            " postable_type character varying," +
            " creator_id integer," +
            " admin_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_TASK = "CREATE TABLE tasks (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_id integer," +
            " user_id integer," +
            " completed boolean," +
            " title character varying," +
            " due_at timestamp without time zone," +
            " assigned_to integer," +
            " category character varying DEFAULT 0," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " admin_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_USER = "CREATE TABLE users (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL," +
            " remoteid integer UNIQUE," +
            " first_name character varying," +
            " middle_name character varying," +
            " last_name character varying," +
            " company_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " is_company boolean DEFAULT false," +
            " company_name character varying," +
            " company_admin_id integer," +
            " location_id integer," +
            " area_of_expertise character varying," +
            " contact_number character varying," +
            " bar_council_no character varying," +
            " user_type character varying," +
            " company_type character varying," +
            " role integer DEFAULT 0," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " avatarurl character varying," +
            " avatarthumburl character varying," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_P_CONTACT = "CREATE TABLE person_contacts (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " creator_id integer," +
            " admin_id integer," +
            " contact_type character varying," +
            " title character varying," +
            " first_name character varying," +
            " last_name character varying," +
            " company_contact_id integer," +
            " address1 character varying," +
            " address2 character varying," +
            " city character varying," +
            " zip character varying," +
            " state character varying," +
            " country character varying," +
            " is_client boolean," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " avatarurl character varying," +
            "avatarthumburl character varying," +
            " email character varying," +
            " telephone character varying," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_NOTI = "CREATE TABLE notifications (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL," +
            " remoteid integer UNIQUE," +
            " creator_id integer," +
            " notice_type character varying," +
            " link character varying," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " notification_text text," +
            " notificationable_type character varying," +
            " notificationable_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_NOTI_STATUS = "CREATE TABLE notification_status (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL," +
            " notification_id integer," +
            " member_id integer," +
            " read integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying," +
            " UNIQUE ( notification_id, member_id ) ON CONFLICT REPLACE " +
            ")";

    String CREATE_TABLE_ORDERS = "CREATE TABLE orders (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_id integer," +
            " description text," +
            " date timestamp without time zone," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " link character varying," +
            " api_order_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_C_C_MATTERS = "CREATE TABLE company_contacts_matters (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " company_contact_id integer," +
            " matter_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_M_P_C = "CREATE TABLE matters_person_contacts (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " matter_id integer," +
            " person_contact_id integer," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_UPLOAD = "CREATE TABLE uploads (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " remoteid integer UNIQUE," +
            " uploadable_type character varying," +
            " uploadable_id integer," +
            " category character varying," +
            " created_at timestamp without time zone NOT NULL," +
            " updated_at timestamp without time zone NOT NULL," +
            " description character varying," +
            " creator_id integer," +
            " subcategory character varying," +
            " admin_id integer," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying" +
            ")";

    String CREATE_TABLE_UPLOAD_ASSETS = "CREATE TABLE uploads_assets (" +
            " id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL, " +
            " upload_id integer," +
            " url character varying," +
            " updated integer DEFAULT 0," +
            " inserted integer DEFAULT 0," +
            " deleted integer DEFAULT 0," +
            " key_name character varying," +
            " UNIQUE ( upload_id, url ) ON CONFLICT REPLACE " +
            ")";

    String CREATE_TABLE_CASE_HISTORY = "CREATE TABLE case_history (" +
            "  id integer PRIMARY KEY AUTOINCREMENT UNIQUE  NOT NULL," +
            "  remoteid integer UNIQUE," +
            "  matterId integer," +
            "  serialNumber integer," +
            "  caseNumber character varying," +
            "  caseLink character varying," +
            "  details character varying," +
            "  status integer DEFAULT 0," +
            "  orderdt timestamp without time zone," +
            "  addedOn timestamp without time zone " +
            ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreateTable(db);
    }

    private void CreateTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CASE_TYPE);
        db.execSQL(CREATE_TABLE_COMMENTS);
        db.execSQL(CREATE_TABLE_COMPANY_CONTACTS);
        db.execSQL(CREATE_TABLE_COURTS);
        db.execSQL(CREATE_TABLE_CUSTOM_HEARING);
        db.execSQL(CREATE_TABLE_HEARING);
        db.execSQL(CREATE_TABLE_MATTER);
        db.execSQL(CREATE_TABLE_MATTER_USER);
        db.execSQL(CREATE_TABLE_NOTES);
        db.execSQL(CREATE_TABLE_POST);
        db.execSQL(CREATE_TABLE_TASK);
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_P_CONTACT);
        db.execSQL(CREATE_TABLE_NOTI);
        db.execSQL(CREATE_TABLE_NOTI_STATUS);
        db.execSQL(CREATE_TABLE_ORDERS);
        db.execSQL(CREATE_TABLE_C_C_MATTERS);
        db.execSQL(CREATE_TABLE_M_P_C);
        db.execSQL(CREATE_TABLE_UPLOAD);
        db.execSQL(CREATE_TABLE_UPLOAD_ASSETS);
        db.execSQL(CREATE_TABLE_CASE_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}