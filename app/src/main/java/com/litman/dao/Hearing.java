package com.litman.dao;

import java.io.Serializable;

/**
 * Created by Namit on 20-04-2017.
 */

public class Hearing implements Serializable {

    private String ID = "";
    private String R_ID = "";
    private String MATTER_ID = "";
    private String ON_DATE = "";
    private String JUDGE_NAME = "";
    private String ADDED_ON = "";
    private String CREATED_AT = "";
    private String UPDATED_AT = "";
    private String COURT_ROOM_NO = "";
    private String ITEM_NO = "";
    private String HEARING_ID = "";
    private String CREATED_ID = "";
    private String UPDATED = "";
    private String INSERTED = "";
    private String DELETED = "";
    private String KEY = "";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getR_ID() {
        return R_ID;
    }

    public void setR_ID(String r_ID) {
        R_ID = r_ID;
    }

    public String getMATTER_ID() {
        return MATTER_ID;
    }

    public void setMATTER_ID(String MATTER_ID) {
        this.MATTER_ID = MATTER_ID;
    }

    public String getON_DATE() {
        return ON_DATE;
    }

    public void setON_DATE(String ON_DATE) {
        this.ON_DATE = ON_DATE;
    }

    public String getJUDGE_NAME() {
        return JUDGE_NAME;
    }

    public void setJUDGE_NAME(String JUDGE_NAME) {
        this.JUDGE_NAME = JUDGE_NAME;
    }

    public String getADDED_ON() {
        return ADDED_ON;
    }

    public void setADDED_ON(String ADDED_ON) {
        this.ADDED_ON = ADDED_ON;
    }

    public String getCREATED_AT() {
        return CREATED_AT;
    }

    public void setCREATED_AT(String CREATED_AT) {
        this.CREATED_AT = CREATED_AT;
    }

    public String getUPDATED_AT() {
        return UPDATED_AT;
    }

    public void setUPDATED_AT(String UPDATED_AT) {
        this.UPDATED_AT = UPDATED_AT;
    }

    public String getCOURT_ROOM_NO() {
        return COURT_ROOM_NO;
    }

    public void setCOURT_ROOM_NO(String COURT_ROOM_NO) {
        this.COURT_ROOM_NO = COURT_ROOM_NO;
    }

    public String getITEM_NO() {
        return ITEM_NO;
    }

    public void setITEM_NO(String ITEM_NO) {
        this.ITEM_NO = ITEM_NO;
    }

    public String getHEARING_ID() {
        return HEARING_ID;
    }

    public void setHEARING_ID(String HEARING_ID) {
        this.HEARING_ID = HEARING_ID;
    }

    public String getCREATED_ID() {
        return CREATED_ID;
    }

    public void setCREATED_ID(String CREATED_ID) {
        this.CREATED_ID = CREATED_ID;
    }

    public String getUPDATED() {
        return UPDATED;
    }

    public void setUPDATED(String UPDATED) {
        this.UPDATED = UPDATED;
    }

    public String getINSERTED() {
        return INSERTED;
    }

    public void setINSERTED(String INSERTED) {
        this.INSERTED = INSERTED;
    }

    public String getDELETED() {
        return DELETED;
    }

    public void setDELETED(String DELETED) {
        this.DELETED = DELETED;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }
}
