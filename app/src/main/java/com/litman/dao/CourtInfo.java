package com.litman.dao;

import java.io.Serializable;


public class CourtInfo implements Serializable {

    public int ID;
    public String NAME;
    public String URL;
    public String COUNT;
    public Boolean SELECTED;

}
