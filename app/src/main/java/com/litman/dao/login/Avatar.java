
package com.litman.dao.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Avatar implements Serializable
{

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("thumb")
    @Expose
    private Thumb thumb;
    private final static long serialVersionUID = -2527353721262203333L;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Thumb getThumb() {
        return thumb;
    }

    public void setThumb(Thumb thumb) {
        this.thumb = thumb;
    }



}
