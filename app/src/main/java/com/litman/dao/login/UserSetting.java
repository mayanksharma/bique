
package com.litman.dao.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class UserSetting implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("web_notification")
    @Expose
    private Boolean webNotification;
    @SerializedName("weekly_digest")
    @Expose
    private Boolean weeklyDigest;
    @SerializedName("email_a_day_before")
    @Expose
    private Boolean emailADayBefore;
    @SerializedName("email_on_action")
    @Expose
    private Boolean emailOnAction;
    @SerializedName("email_on_assign_task")
    @Expose
    private Boolean emailOnAssignTask;
    @SerializedName("email_on_comments_notes")
    @Expose
    private Boolean emailOnCommentsNotes;
    @SerializedName("make_profile_public")
    @Expose
    private Boolean makeProfilePublic;
    @SerializedName("make_matters_public")
    @Expose
    private Boolean makeMattersPublic;
    @SerializedName("make_personal_info_public")
    @Expose
    private Boolean makePersonalInfoPublic;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("avatar")
    @Expose
    private Avatar avatar;
    @SerializedName("notifiy_new_hearing")
    @Expose
    private Boolean notifiyNewHearing;
    @SerializedName("notifiy_new_order")
    @Expose
    private Boolean notifiyNewOrder;
    @SerializedName("remind_scheduled_hearing")
    @Expose
    private Boolean remindScheduledHearing;
    @SerializedName("notifiy_matter_activity")
    @Expose
    private Boolean notifiyMatterActivity;
    @SerializedName("notifiy_post_added_matter")
    @Expose
    private Boolean notifiyPostAddedMatter;
    @SerializedName("notifiy_task_activity")
    @Expose
    private Boolean notifiyTaskActivity;
    @SerializedName("notifiy_note_added_matter")
    @Expose
    private Boolean notifiyNoteAddedMatter;
    @SerializedName("notifiy_file_added_matter")
    @Expose
    private Boolean notifiyFileAddedMatter;
    @SerializedName("notifiy_comment_on_notes_post_timeline_added")
    @Expose
    private Boolean notifiyCommentOnNotesPostTimelineAdded;
    @SerializedName("notifiy_user_added")
    @Expose
    private Boolean notifiyUserAdded;
    @SerializedName("notifiy_comment_on_matter_added")
    @Expose
    private Boolean notifiyCommentOnMatterAdded;
    @SerializedName("notifiy_task_completed")
    @Expose
    private Boolean notifiyTaskCompleted;
    @SerializedName("notifiy_someone_follow")
    @Expose
    private Boolean notifiySomeoneFollow;
    @SerializedName("notifiy_people_following_post_added")
    @Expose
    private Boolean notifiyPeopleFollowingPostAdded;
    @SerializedName("notifiy_someone_comment_on_post")
    @Expose
    private Boolean notifiySomeoneCommentOnPost;
    @SerializedName("notifiy_someone_like_post")
    @Expose
    private Boolean notifiySomeoneLikePost;
    @SerializedName("notifiy_new_matter_added")
    @Expose
    private Boolean notifiyNewMatterAdded;
    @SerializedName("notifiy_new_contact_added")
    @Expose
    private Boolean notifiyNewContactAdded;
    private final static long serialVersionUID = -7676111371485165290L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getWebNotification() {
        return webNotification;
    }

    public void setWebNotification(Boolean webNotification) {
        this.webNotification = webNotification;
    }

    public Boolean getWeeklyDigest() {
        return weeklyDigest;
    }

    public void setWeeklyDigest(Boolean weeklyDigest) {
        this.weeklyDigest = weeklyDigest;
    }

    public Boolean getEmailADayBefore() {
        return emailADayBefore;
    }

    public void setEmailADayBefore(Boolean emailADayBefore) {
        this.emailADayBefore = emailADayBefore;
    }

    public Boolean getEmailOnAction() {
        return emailOnAction;
    }

    public void setEmailOnAction(Boolean emailOnAction) {
        this.emailOnAction = emailOnAction;
    }

    public Boolean getEmailOnAssignTask() {
        return emailOnAssignTask;
    }

    public void setEmailOnAssignTask(Boolean emailOnAssignTask) {
        this.emailOnAssignTask = emailOnAssignTask;
    }

    public Boolean getEmailOnCommentsNotes() {
        return emailOnCommentsNotes;
    }

    public void setEmailOnCommentsNotes(Boolean emailOnCommentsNotes) {
        this.emailOnCommentsNotes = emailOnCommentsNotes;
    }

    public Boolean getMakeProfilePublic() {
        return makeProfilePublic;
    }

    public void setMakeProfilePublic(Boolean makeProfilePublic) {
        this.makeProfilePublic = makeProfilePublic;
    }

    public Boolean getMakeMattersPublic() {
        return makeMattersPublic;
    }

    public void setMakeMattersPublic(Boolean makeMattersPublic) {
        this.makeMattersPublic = makeMattersPublic;
    }

    public Boolean getMakePersonalInfoPublic() {
        return makePersonalInfoPublic;
    }

    public void setMakePersonalInfoPublic(Boolean makePersonalInfoPublic) {
        this.makePersonalInfoPublic = makePersonalInfoPublic;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public Boolean getNotifiyNewHearing() {
        return notifiyNewHearing;
    }

    public void setNotifiyNewHearing(Boolean notifiyNewHearing) {
        this.notifiyNewHearing = notifiyNewHearing;
    }

    public Boolean getNotifiyNewOrder() {
        return notifiyNewOrder;
    }

    public void setNotifiyNewOrder(Boolean notifiyNewOrder) {
        this.notifiyNewOrder = notifiyNewOrder;
    }

    public Boolean getRemindScheduledHearing() {
        return remindScheduledHearing;
    }

    public void setRemindScheduledHearing(Boolean remindScheduledHearing) {
        this.remindScheduledHearing = remindScheduledHearing;
    }

    public Boolean getNotifiyMatterActivity() {
        return notifiyMatterActivity;
    }

    public void setNotifiyMatterActivity(Boolean notifiyMatterActivity) {
        this.notifiyMatterActivity = notifiyMatterActivity;
    }

    public Boolean getNotifiyPostAddedMatter() {
        return notifiyPostAddedMatter;
    }

    public void setNotifiyPostAddedMatter(Boolean notifiyPostAddedMatter) {
        this.notifiyPostAddedMatter = notifiyPostAddedMatter;
    }

    public Boolean getNotifiyTaskActivity() {
        return notifiyTaskActivity;
    }

    public void setNotifiyTaskActivity(Boolean notifiyTaskActivity) {
        this.notifiyTaskActivity = notifiyTaskActivity;
    }

    public Boolean getNotifiyNoteAddedMatter() {
        return notifiyNoteAddedMatter;
    }

    public void setNotifiyNoteAddedMatter(Boolean notifiyNoteAddedMatter) {
        this.notifiyNoteAddedMatter = notifiyNoteAddedMatter;
    }

    public Boolean getNotifiyFileAddedMatter() {
        return notifiyFileAddedMatter;
    }

    public void setNotifiyFileAddedMatter(Boolean notifiyFileAddedMatter) {
        this.notifiyFileAddedMatter = notifiyFileAddedMatter;
    }

    public Boolean getNotifiyCommentOnNotesPostTimelineAdded() {
        return notifiyCommentOnNotesPostTimelineAdded;
    }

    public void setNotifiyCommentOnNotesPostTimelineAdded(Boolean notifiyCommentOnNotesPostTimelineAdded) {
        this.notifiyCommentOnNotesPostTimelineAdded = notifiyCommentOnNotesPostTimelineAdded;
    }

    public Boolean getNotifiyUserAdded() {
        return notifiyUserAdded;
    }

    public void setNotifiyUserAdded(Boolean notifiyUserAdded) {
        this.notifiyUserAdded = notifiyUserAdded;
    }

    public Boolean getNotifiyCommentOnMatterAdded() {
        return notifiyCommentOnMatterAdded;
    }

    public void setNotifiyCommentOnMatterAdded(Boolean notifiyCommentOnMatterAdded) {
        this.notifiyCommentOnMatterAdded = notifiyCommentOnMatterAdded;
    }

    public Boolean getNotifiyTaskCompleted() {
        return notifiyTaskCompleted;
    }

    public void setNotifiyTaskCompleted(Boolean notifiyTaskCompleted) {
        this.notifiyTaskCompleted = notifiyTaskCompleted;
    }

    public Boolean getNotifiySomeoneFollow() {
        return notifiySomeoneFollow;
    }

    public void setNotifiySomeoneFollow(Boolean notifiySomeoneFollow) {
        this.notifiySomeoneFollow = notifiySomeoneFollow;
    }

    public Boolean getNotifiyPeopleFollowingPostAdded() {
        return notifiyPeopleFollowingPostAdded;
    }

    public void setNotifiyPeopleFollowingPostAdded(Boolean notifiyPeopleFollowingPostAdded) {
        this.notifiyPeopleFollowingPostAdded = notifiyPeopleFollowingPostAdded;
    }

    public Boolean getNotifiySomeoneCommentOnPost() {
        return notifiySomeoneCommentOnPost;
    }

    public void setNotifiySomeoneCommentOnPost(Boolean notifiySomeoneCommentOnPost) {
        this.notifiySomeoneCommentOnPost = notifiySomeoneCommentOnPost;
    }

    public Boolean getNotifiySomeoneLikePost() {
        return notifiySomeoneLikePost;
    }

    public void setNotifiySomeoneLikePost(Boolean notifiySomeoneLikePost) {
        this.notifiySomeoneLikePost = notifiySomeoneLikePost;
    }

    public Boolean getNotifiyNewMatterAdded() {
        return notifiyNewMatterAdded;
    }

    public void setNotifiyNewMatterAdded(Boolean notifiyNewMatterAdded) {
        this.notifiyNewMatterAdded = notifiyNewMatterAdded;
    }

    public Boolean getNotifiyNewContactAdded() {
        return notifiyNewContactAdded;
    }

    public void setNotifiyNewContactAdded(Boolean notifiyNewContactAdded) {
        this.notifiyNewContactAdded = notifiyNewContactAdded;
    }


}
