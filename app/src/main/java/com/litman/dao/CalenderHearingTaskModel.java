package com.litman.dao;

import java.io.Serializable;

/**
 * Created by Namit on 22-04-2017.
 */

public class CalenderHearingTaskModel implements Serializable {
    private int hearing;
    private boolean task = false;
    private String dateString;


    public int getHearing() {
        return hearing;
    }

    public void setHearing(int hearing) {
        this.hearing = hearing;
    }

    public boolean getTask() {
        return task;
    }

    public void setTask(boolean task) {
        this.task = task;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }
}
