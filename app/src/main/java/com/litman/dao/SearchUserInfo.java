package com.litman.dao;

import java.io.Serializable;


public class SearchUserInfo implements Serializable {

    public int ID;
    public String NAME;
    public String C_NAME;
    public String URL;
    public String THUMB;
    public String EMAIL;
    public String TELEPHONE;
    public String WEBSITE;
    public int TYPE;

}
