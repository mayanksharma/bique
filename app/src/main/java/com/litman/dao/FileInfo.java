package com.litman.dao;

import java.io.Serializable;


public class FileInfo implements Serializable {

    public int ID;
    public int A_ID;
    public int U_ID;
    public String NAME;
    public String TYPE;
    public String CATEGORY;
    public String SUBCATEGORY;
    public String DESC;
    public String URL;
    public String DATE;
    public String PLAIN;
    public String DIFFE;
    public int UPLOADABLE_ID;

}
