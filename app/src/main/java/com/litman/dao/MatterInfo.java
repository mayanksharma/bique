package com.litman.dao;

import java.io.Serializable;


public class MatterInfo implements Serializable {

    public int ID;
    public String MATTER_NUMBER;
    public String COURT_NAME;
    public String CASE_YEAR;
    public String CASE_TYPE;
    public String PLAINTIFF;
    public String DEFENDANT;
    public String A_PLAINTIFF;
    public String A_DEFENDANT;
    public String CLIENT;
    public int U_ID;
    public String H_DATE;
    public String CH_DATE;
    public String JUDGE;
    public String COURT_ROOM;
    public String ITEM_NO;

}
