package com.litman.dao;

import java.io.Serializable;


public class SearchMatterInfo implements Serializable {

    public int ID;
    public String MATTER_NUMBER;
    public String COURT_NAME;
    public String CASE_YEAR;
    public String CASE_TYPE;
    public String PLAINTIFF;
    public String DEFENDANT;

}
