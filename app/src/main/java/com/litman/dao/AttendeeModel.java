package com.litman.dao;

import java.io.Serializable;

/**
 * Created by Namit on 27-04-2017.
 */

public class AttendeeModel implements Serializable {
    String name;
    int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}