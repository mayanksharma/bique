package com.litman.dao;

import java.io.Serializable;


public class SearchTaskInfo implements Serializable {

    public int ID;
    public int MATTER_ID;
    public int COMPLETED;
    public String TITLE;
    public String DUE_AT;
    public String CATEGORY;
    public String PLAINTIFF;
    public String DEFENDANT;
    public int USER_ID;
    public String NAME;

}
