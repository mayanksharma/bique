package com.litman.dao;

import java.io.Serializable;


public class HomeFeedInfo implements Serializable {

    public int ID;
    public int MATTER_ID;
    public int USER_ID;
    public int COURT_ID;
    public String COURT_NAME;
    public String LINK;
    public String F_NAME;
    public String M_NAME;
    public String L_NAME;
    public String TEXT;
    public String PLAINTIFF;
    public String DEFENDANT;
    public int COURT_ROOM_NO;
    public String JUDGE_NAME;
    public String ITEM_NO;
    public String D_DATE;
    public String S_DATE;
    public String URL;
    public String THUMB;
    public int TYPE;
    public String TYPE_NAME;
    public int COUNT;

    public String A_F_NAME;
    public String A_M_NAME;
    public String A_L_NAME;
    public int A_ID;
    public String CAT;
    public String SUB;
    public String N_TYPE;
    public int N_ID;
    public int READ;
    public String MATTER_NO;
    public String CASE_YEAR;
    public String CASE_TYPE;
    public String CURRENT_ITEM;
    public Boolean IS_SECTION;

    @Override
    public String toString() {
        return "HomeFeedInfo{" +
                "MATTER_ID=" + MATTER_ID +
                ", USER_ID=" + USER_ID +
                ", COURT_NAME='" + COURT_NAME + '\'' +
                ", LINK='" + LINK + '\'' +
                ", F_NAME='" + F_NAME + '\'' +
                ", M_NAME='" + M_NAME + '\'' +
                ", L_NAME='" + L_NAME + '\'' +
                ", TEXT='" + TEXT + '\'' +
                ", PLAINTIFF='" + PLAINTIFF + '\'' +
                ", DEFENDANT='" + DEFENDANT + '\'' +
                ", COURT_ROOM_NO=" + COURT_ROOM_NO +
                ", JUDGE_NAME='" + JUDGE_NAME + '\'' +
                ", ITEM_NO='" + ITEM_NO + '\'' +
                ", D_DATE='" + D_DATE + '\'' +
                ", S_DATE='" + S_DATE + '\'' +
                ", TYPE=" + TYPE +
                '}';
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMATTER_ID() {
        return MATTER_ID;
    }

    public void setMATTER_ID(int MATTER_ID) {
        this.MATTER_ID = MATTER_ID;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getCOURT_NAME() {
        return COURT_NAME;
    }

    public void setCOURT_NAME(String COURT_NAME) {
        this.COURT_NAME = COURT_NAME;
    }

    public String getLINK() {
        return LINK;
    }

    public void setLINK(String LINK) {
        this.LINK = LINK;
    }

    public String getF_NAME() {
        return F_NAME;
    }

    public void setF_NAME(String f_NAME) {
        F_NAME = f_NAME;
    }

    public String getM_NAME() {
        return M_NAME;
    }

    public void setM_NAME(String m_NAME) {
        M_NAME = m_NAME;
    }

    public String getL_NAME() {
        return L_NAME;
    }

    public void setL_NAME(String l_NAME) {
        L_NAME = l_NAME;
    }

    public String getTEXT() {
        return TEXT;
    }

    public void setTEXT(String TEXT) {
        this.TEXT = TEXT;
    }

    public String getPLAINTIFF() {
        return PLAINTIFF;
    }

    public void setPLAINTIFF(String PLAINTIFF) {
        this.PLAINTIFF = PLAINTIFF;
    }

    public String getDEFENDANT() {
        return DEFENDANT;
    }

    public void setDEFENDANT(String DEFENDANT) {
        this.DEFENDANT = DEFENDANT;
    }

    public int getCOURT_ROOM_NO() {
        return COURT_ROOM_NO;
    }

    public void setCOURT_ROOM_NO(int COURT_ROOM_NO) {
        this.COURT_ROOM_NO = COURT_ROOM_NO;
    }

    public String getJUDGE_NAME() {
        return JUDGE_NAME;
    }

    public void setJUDGE_NAME(String JUDGE_NAME) {
        this.JUDGE_NAME = JUDGE_NAME;
    }

    public String getITEM_NO() {
        return ITEM_NO;
    }

    public void setITEM_NO(String ITEM_NO) {
        this.ITEM_NO = ITEM_NO;
    }

    public String getD_DATE() {
        return D_DATE;
    }

    public void setD_DATE(String d_DATE) {
        D_DATE = d_DATE;
    }

    public String getS_DATE() {
        return S_DATE;
    }

    public void setS_DATE(String s_DATE) {
        S_DATE = s_DATE;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getTHUMB() {
        return THUMB;
    }

    public void setTHUMB(String THUMB) {
        this.THUMB = THUMB;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getTYPE_NAME() {
        return TYPE_NAME;
    }

    public void setTYPE_NAME(String TYPE_NAME) {
        this.TYPE_NAME = TYPE_NAME;
    }

    public int getCOUNT() {
        return COUNT;
    }

    public void setCOUNT(int COUNT) {
        this.COUNT = COUNT;
    }

    public String getA_F_NAME() {
        return A_F_NAME;
    }

    public void setA_F_NAME(String a_F_NAME) {
        A_F_NAME = a_F_NAME;
    }

    public String getA_M_NAME() {
        return A_M_NAME;
    }

    public void setA_M_NAME(String a_M_NAME) {
        A_M_NAME = a_M_NAME;
    }

    public String getA_L_NAME() {
        return A_L_NAME;
    }

    public void setA_L_NAME(String a_L_NAME) {
        A_L_NAME = a_L_NAME;
    }

    public int getA_ID() {
        return A_ID;
    }

    public void setA_ID(int a_ID) {
        A_ID = a_ID;
    }

    public String getCAT() {
        return CAT;
    }

    public void setCAT(String CAT) {
        this.CAT = CAT;
    }

    public String getSUB() {
        return SUB;
    }

    public void setSUB(String SUB) {
        this.SUB = SUB;
    }
}
