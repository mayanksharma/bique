package com.litman;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.litman.activity.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional

        Log.e(TAG, "From: " + remoteMessage.getFrom());


        // sendNotification(remoteMessage.getFrom(), "");


        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());


            String dataa = remoteMessage.getData().toString();
            Map<String, String> data = remoteMessage.getData();

            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody(); //data.get("message");
            //String time = data.get("sentTime");

            //   if (AppPreferences.isUserLogin(getApplicationContext())) {
            sendNotification(body, title);
            //    }
            //Calling method to generate notification

        }
    }


    private void sendNotification(String messageBody, String title) {
        Intent intent = new Intent(this, MainActivity.class).putExtra("TYPE", 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setSmallIcon(R.mipmap.androidlogo);
        } else {
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder
                .setContentTitle(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setContentText(messageBody)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int id = (int) System.currentTimeMillis() / 1000;
        notificationManager.notify(id, notificationBuilder.build());
    }

    private Class getTargetActivity(String clickAction) {
        Class c = MainActivity.class;
        if (clickAction == null) {
            return c;
        }

          /*  switch (clickAction) {
                case CLICK_ACTION_ADD_BOOKING:
                case CLICK_ACTION_UPDATE_BOOKING:
                    c = BookingDetailsActivity.class;
                    break;
                case CLICK_ACTION_REVIEW:
                    c = ReviewsActivity.class;
                    break;


            }*/
        return c;

    }


}
