package com.litman.calui.dao;

import com.litman.utils.AppLogger;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tushar Agarwal on 25-Mar-17.
 */

public class CalItemData implements Serializable {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    private final String TAG = CalItemData.class.getSimpleName();

    private String dateString;
    int hearing;
    private Date date;
    private boolean isSelected;
    private Object dataObj;
    boolean task = false;

    public void setTask(boolean task) {
        this.task = task;
    }

    public void setHearing(int hearing) {
        this.hearing = hearing;
    }

    public CalItemData(Date dateOb, String date, int count, boolean isSelected) {
        this.dateString = date;
        this.hearing = hearing;
        this.task = task;
        this.date = dateOb;
        this.isSelected = isSelected;
        this.dataObj = getDataFromDb();
    }

    //TODO : Need to give the definition to get information from database for date
    private Object getDataFromDb() {
        AppLogger.logD(TAG, "Get info for date : " + dateFormat.format(date));
        return null;
    }


    public int getHearing() {
        return hearing;
    }

    public boolean getTask() {
        return task;
    }

    public String getDateString() {
        // AppLogger.logE("TAG", "DateFormat: " + dateFormat.format(date));
        return dateString;
    }

    public int getCount() {
        return hearing;
    }

    public Date getDate() {
        return date;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public Object getDataObj() {
        return dataObj;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "CalItemData{" +
                "dateString='" + dateString + '\'' +
                ", hearing=" + hearing +
                ", task=" + task +
                ", date=" + date +
                ", isSelected=" + isSelected +
                ", dataObj=" + dataObj +
                '}';
    }
}
