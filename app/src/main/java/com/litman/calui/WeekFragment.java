package com.litman.calui;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.adapters.HearingListAdapter;
import com.litman.adapters.TaskListAdapter;
import com.litman.calui.dao.CalItemData;
import com.litman.dao.CalenderHearingTaskModel;
import com.litman.dao.HomeFeedInfo;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class WeekFragment extends Fragment implements View.OnClickListener {

    private final String TAG = WeekFragment.class.getSimpleName();
    private String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private TextView day_0, day_1, day_2, day_3, day_4, day_5, day_6;
    private View taskDot_0, taskDot_1, taskDot_2, taskDot_3, taskDot_4, taskDot_5, taskDot_6;
    private TextView event_count_0, event_count_1, event_count_2, event_count_3, event_count_4, event_count_5, event_count_6;
    private LinearLayout satLL, friLL, thurLL, wedLL, tueLL, monLL, sunLL;
    private NestedScrollView scrollView;
    String court = "-1";

    private RecyclerView hearingList, taskList;
    //  private String startdate = "", courtId, endDate = "";
    private TextView hearingCountTv, taskCountTv;
    private Context context;
    ArrayList<CalItemData> dataArrayList;
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManager2;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public static WeekFragment newInstance(ArrayList<CalItemData> dataList) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("LIST", dataList);
        WeekFragment fragment = new WeekFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.linear_cal_item, container, false);
        scrollView = (NestedScrollView) view.findViewById(R.id.scrollView);
        day_0 = (TextView) view.findViewById(R.id.day_0);
        day_1 = (TextView) view.findViewById(R.id.day_1);
        day_2 = (TextView) view.findViewById(R.id.day_2);
        day_3 = (TextView) view.findViewById(R.id.day_3);
        day_4 = (TextView) view.findViewById(R.id.day_4);
        day_5 = (TextView) view.findViewById(R.id.day_5);
        day_6 = (TextView) view.findViewById(R.id.day_6);

        event_count_0 = (TextView) view.findViewById(R.id.event_count_0);
        event_count_1 = (TextView) view.findViewById(R.id.event_count_1);
        event_count_2 = (TextView) view.findViewById(R.id.event_count_2);
        event_count_3 = (TextView) view.findViewById(R.id.event_count_3);
        event_count_4 = (TextView) view.findViewById(R.id.event_count_4);
        event_count_5 = (TextView) view.findViewById(R.id.event_count_5);
        event_count_6 = (TextView) view.findViewById(R.id.event_count_6);

        satLL = (LinearLayout) view.findViewById(R.id.satLL);
        friLL = (LinearLayout) view.findViewById(R.id.friLL);
        thurLL = (LinearLayout) view.findViewById(R.id.thurLL);
        wedLL = (LinearLayout) view.findViewById(R.id.wedLL);
        monLL = (LinearLayout) view.findViewById(R.id.monLL);
        tueLL = (LinearLayout) view.findViewById(R.id.tueLL);
        sunLL = (LinearLayout) view.findViewById(R.id.sunLL);


        taskDot_0 = view.findViewById(R.id.taskDot_0);
        taskDot_1 = view.findViewById(R.id.taskDot_1);
        taskDot_2 = view.findViewById(R.id.taskDot_2);
        taskDot_3 = view.findViewById(R.id.taskDot_3);
        taskDot_4 = view.findViewById(R.id.taskDot_4);
        taskDot_5 = view.findViewById(R.id.taskDot_5);
        taskDot_6 = view.findViewById(R.id.taskDot_6);

        day_0.setOnClickListener(this);
        day_1.setOnClickListener(this);
        day_2.setOnClickListener(this);
        day_3.setOnClickListener(this);
        day_4.setOnClickListener(this);
        day_5.setOnClickListener(this);
        day_6.setOnClickListener(this);


        hearingList = (RecyclerView) view.findViewById(R.id.hearingList);
        taskList = (RecyclerView) view.findViewById(R.id.taskList);
        taskCountTv = (TextView) view.findViewById(R.id.taskCountTv);
        hearingCountTv = (TextView) view.findViewById(R.id.hearingCountTv);
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        hearingList.setLayoutManager(linearLayoutManager);
        taskList.setLayoutManager(linearLayoutManager2);

        setUpView(null, court);


        return view;
    }

    public void setUpView(ArrayList<CalItemData> arrayList, String court) {
        WeekFragment.this.court = court;

        Bundle bundle = getArguments();


        if (arrayList == null) {
            dataArrayList = (ArrayList<CalItemData>) bundle.getSerializable("LIST");
        } else {
            dataArrayList = arrayList;
        }
        Calendar currentDate = Calendar.getInstance();
        // Calendar calendar2 = Calendar.getInstance();
        // calendar2.setTime(dataArrayList.get(i).getDate());

        AppLogger.logD(TAG, dataArrayList.toString());
        boolean hasCurrentDate = false;
        for (int i = 0; i < 7; i++) {


            try {
                AppLogger.logD(TAG, " D : " + i + ", " + getDayDataFrom(dataArrayList, i).toString());
            } catch (Exception e) {
                AppLogger.logD(TAG, " D : " + i);
            }

            Date date = dataArrayList.get(i).getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.setFirstDayOfWeek(Calendar.SUNDAY);
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.SUNDAY:
                    day_0.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.MONDAY:
                    day_1.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.TUESDAY:
                    day_2.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.WEDNESDAY:
                    day_3.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.THURSDAY:
                    day_4.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.FRIDAY:
                    day_5.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;
                case Calendar.SATURDAY:
                    day_6.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");

                    break;

            }


            if (calendar.get(Calendar.DATE) == currentDate.get(Calendar.DATE) &&
                    calendar.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH) &&
                    calendar.get(Calendar.YEAR) == currentDate.get(Calendar.YEAR)) {
                hasCurrentDate = true;
                setList(i);
                setSelected(i);

            }
        }


        if (!hasCurrentDate) {
            setList(1);
            setSelected(1);
        }


        setWeekDay();
    }


    public void setWeekDay() {

        clearData();


        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(dataArrayList.get(0).getDate());

        Calendar endCalender = Calendar.getInstance();
        endCalender.setTime(dataArrayList.get(dataArrayList.size() - 1).getDate());

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, startCalendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, startCalendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, startCalendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, startCalendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, startCalendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, startCalendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, endCalender.get(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, endCalender.get(Calendar.YEAR));
        end.set(Calendar.MONTH, endCalender.get(Calendar.MONTH));
        end.set(Calendar.HOUR, endCalender.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, endCalender.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, endCalender.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, endCalender.getActualMaximum(Calendar.MILLISECOND));


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");

        SimpleDateFormat minFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");

        String startDate = minFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());

        AppLogger.logE(TAG, "MinFormatDate: " + startDate);
        AppLogger.logE(TAG, "MaxFormatDate: " + endDate);


        Cursor curs = CacheManager.INSTANCE.getCalenderHearing(court.equals("-1") ? getCourts() : court, AppPreferences.getUserId(context), startDate, endDate);

        ArrayList<CalenderHearingTaskModel> list = new ArrayList<>();

        if (curs != null && curs.moveToFirst()) {

            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            while (!curs.isAfterLast()) {
                CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                AppLogger.logE(TAG, "display_date: " + curs.getString(curs.getColumnIndex("display_date")));

                AppLogger.logE(TAG, "total: " + curs.getInt(curs.getColumnIndex("total")));

                calenderHearingTaskModel.setDateString(curs.getString(curs.getColumnIndex("display_date")));
                calenderHearingTaskModel.setHearing(curs.getInt(curs.getColumnIndex("total")));
                list.add(calenderHearingTaskModel);
                curs.moveToNext();
            }
            curs.close();
        }
        Cursor taskcurs = CacheManager.INSTANCE.getTaskList(AppPreferences.getUserId(context), court.equals("-1") ? getCourts() : court, startDate, endDate);


        if (taskcurs != null && taskcurs.moveToFirst()) {

            AppLogger.logE("count", "" + taskcurs.getCount() + "=" + taskcurs.getColumnCount());
            while (!taskcurs.isAfterLast()) {
                CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                //    AppLogger.logE(TAG, "display_date: " + taskcurs.getString(taskcurs.getColumnIndex("display_date")));

//                    AppLogger.logE(TAG, "total: " + taskcurs.getInt(taskcurs.getColumnIndex("total")));
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        CalenderHearingTaskModel calenderHearingTaskModel1 = list.get(i);

                        if (calenderHearingTaskModel1.getDateString().equals(taskcurs.getString(taskcurs.getColumnIndex("due_at")))) {
                            calenderHearingTaskModel1.setTask(true);
                            list.set(i, calenderHearingTaskModel1);
                        }
                    }

                } else {
                    calenderHearingTaskModel.setDateString(taskcurs.getString(taskcurs.getColumnIndex("due_at")));
                    calenderHearingTaskModel.setTask(true);
                    list.add(calenderHearingTaskModel);

                }

                taskcurs.moveToNext();
            }
            taskcurs.close();
        }


        for (int i = 0; i < list.size(); i++) {
            for (int k = 0; k < dataArrayList.size(); k++) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date iDate = null;
                Date kDate = null;
                try {
                    iDate = dateFormat.parse(list.get(i).getDateString());
                    kDate = dateFormat.parse(dataArrayList.get(k).getDateString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (iDate != null && kDate != null) {
                    if (iDate.getDate() == kDate.getDate()) {
                        switch (k) {
                            case 0:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_0.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_0.setText("");
                                }

                                if (list.get(i).getTask()) {
                                    taskDot_0.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_0.setVisibility(View.INVISIBLE);
                                }
                                break;
                            case 1:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_1.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_1.setText("");
                                }

                                if (list.get(i).getTask()) {
                                    taskDot_1.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_1.setVisibility(View.INVISIBLE);
                                }
                                break;
                            case 2:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_2.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_2.setText("");
                                }
                                if (list.get(i).getTask()) {
                                    taskDot_2.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_2.setVisibility(View.INVISIBLE);
                                }

                                break;
                            case 3:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_3.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_3.setText("");
                                }
                                if (list.get(i).getTask()) {
                                    taskDot_3.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_3.setVisibility(View.INVISIBLE);
                                }

                                break;
                            case 4:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_4.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_4.setText("");
                                }

                                if (list.get(i).getTask()) {
                                    taskDot_4.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_4.setVisibility(View.INVISIBLE);
                                }
                                break;
                            case 5:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_5.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_5.setText("");
                                }

                                if (list.get(i).getTask()) {
                                    taskDot_5.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_5.setVisibility(View.INVISIBLE);
                                }

                                break;
                            case 6:
                                if (list.get(i).getHearing() != 0) {
                                    event_count_6.setText(list.get(i).getHearing() + "");
                                } else {
                                    event_count_6.setText("");
                                }


                                if (list.get(i).getTask()) {
                                    taskDot_6.setVisibility(View.VISIBLE);
                                } else {
                                    taskDot_6.setVisibility(View.INVISIBLE);
                                }


                                break;

                        }

                    }
                }
            }
        }
    }

    private void clearData() {
        event_count_0.setText("");
        event_count_1.setText("");
        event_count_2.setText("");
        event_count_3.setText("");
        event_count_4.setText("");
        event_count_5.setText("");
        event_count_6.setText("");

        taskDot_0.setVisibility(View.INVISIBLE);
        taskDot_1.setVisibility(View.INVISIBLE);
        taskDot_2.setVisibility(View.INVISIBLE);
        taskDot_3.setVisibility(View.INVISIBLE);
        taskDot_4.setVisibility(View.INVISIBLE);
        taskDot_5.setVisibility(View.INVISIBLE);
        taskDot_6.setVisibility(View.INVISIBLE);
    }

    private CalItemData getDayDataFrom(ArrayList<CalItemData> list, int dayId) {
        for (int i = 0; i < list.size(); i++) {
            CalItemData itemData = list.get(i);
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTime(itemData.getDate());
            if (calendar.get(Calendar.DAY_OF_WEEK) == dayId) {
                return itemData;
            }
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.day_0:

                setList(0);
                setSelected(0);
                break;


            case R.id.day_1:
                setList(1);
                setSelected(1);
                break;

            case R.id.day_2:
                setList(2);
                setSelected(2);
                break;

            case R.id.day_3:
                setList(3);
                setSelected(3);
                break;

            case R.id.day_4:
                setList(4);
                setSelected(4);
                break;

            case R.id.day_5:
                setList(5);
                setSelected(5);
                break;

            case R.id.day_6:
                setList(6);
                setSelected(6);


                break;

        }


    }


    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }


    public void setList(int position) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataArrayList.get(position).getDate());
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, calendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        end.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        end.set(Calendar.HOUR, calendar.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        SimpleDateFormat minFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
        String startdate = minFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());

        Cursor curs = CacheManager.INSTANCE.getHearingList(AppPreferences.getUserId(context), court.equals("-1") ? getCourts() : court, startdate, endDate);

        ArrayList<HomeFeedInfo> list = new ArrayList<>();

        if (curs != null && curs.moveToFirst()) {
            curs.moveToFirst();
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            while (!curs.isAfterLast()) {
                HomeFeedInfo info = new HomeFeedInfo();
                info.MATTER_ID = curs.getInt(curs.getColumnIndex("matter_id"));
                info.COURT_NAME = curs.getString(curs.getColumnIndex("court_name"));
                info.PLAINTIFF = curs.getString(curs.getColumnIndex("plaintiff"));
                info.DEFENDANT = curs.getString(curs.getColumnIndex("defendant"));
                info.COURT_ROOM_NO = curs.getInt(curs.getColumnIndex("court_room_no"));
                info.JUDGE_NAME = curs.getString(curs.getColumnIndex("judge_name"));
                info.ITEM_NO = curs.getString(curs.getColumnIndex("item_no"));
                info.MATTER_NO = curs.getString(curs.getColumnIndex("matter_number"));
                info.CASE_YEAR = curs.getString(curs.getColumnIndex("case_year"));
                info.CASE_TYPE = curs.getString(curs.getColumnIndex("case_type"));
                info.TYPE = 2;
                list.add(info);
                curs.moveToNext();
            }
            if (curs != null)
                curs.close();
        }
        curs = CacheManager.INSTANCE.getCustomHearingList(AppPreferences.getUserId(context), court.equals("-1") ? getCourts() : court, startdate, endDate);
        if (curs != null && curs.moveToFirst()) {
            curs.moveToFirst();
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            while (!curs.isAfterLast()) {
                HomeFeedInfo info = new HomeFeedInfo();
                //info.ID = curs.getInt(curs.getColumnIndex("remoteid"));
                info.MATTER_ID = curs.getInt(curs.getColumnIndex("matter_id"));
                info.COURT_NAME = curs.getString(curs.getColumnIndex("court_name"));
                info.PLAINTIFF = curs.getString(curs.getColumnIndex("plaintiff"));
                info.DEFENDANT = curs.getString(curs.getColumnIndex("defendant"));
                info.COURT_ROOM_NO = curs.getInt(curs.getColumnIndex("court_room_no"));
                info.JUDGE_NAME = curs.getString(curs.getColumnIndex("judge_name"));
                info.ITEM_NO = curs.getString(curs.getColumnIndex("item_no"));
                info.MATTER_NO = curs.getString(curs.getColumnIndex("matter_number"));
                info.CASE_YEAR = curs.getString(curs.getColumnIndex("case_year"));
                info.CASE_TYPE = curs.getString(curs.getColumnIndex("case_type"));
                info.F_NAME = curs.getString(curs.getColumnIndex("first_name"));
                info.L_NAME = curs.getString(curs.getColumnIndex("last_name"));
                info.USER_ID = curs.getInt(curs.getColumnIndex("attendee_id"));
                info.TYPE = 1;
                list.add(info);
                curs.moveToNext();
            }
            if (curs != null)
                curs.close();
        }

        hearingCountTv.setText("Hearing (" + list.size() + ")");
        CalItemData calItemData = dataArrayList.get(position);

        calItemData.setHearing(list.size());
        dataArrayList.set(position, calItemData);
        curs = CacheManager.INSTANCE.getTaskList(AppPreferences.getUserId(context), court.equals("-1") ? getCourts() : court, startdate, endDate);

        if (curs != null && curs.moveToFirst()) {
            AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
            taskList.setAdapter(new TaskListAdapter(context, curs));
            taskCountTv.setText("Task (" + curs.getCount() + ")");
            CalItemData calItem = dataArrayList.get(position);
            calItem.setTask(true);
            dataArrayList.set(position, calItem);

            if (curs.getCount() > 0) {
                taskList.setVisibility(View.VISIBLE);
                taskCountTv.setVisibility(View.VISIBLE);
            } else {
                taskList.setVisibility(View.GONE);
                taskCountTv.setVisibility(View.GONE);
            }

        } else {
            taskList.setVisibility(View.GONE);
            taskCountTv.setVisibility(View.GONE);
            // taskCountTv.setVisibility(View.GONE);
        }


        if (list.size() < 1) {
            hearingCountTv.setVisibility(View.GONE);
            hearingList.setVisibility(View.GONE);
        } else {
            hearingCountTv.setVisibility(View.VISIBLE);
            hearingList.setVisibility(View.VISIBLE);
        }

        switch (position) {
            case 0:
                if (dataArrayList.get(0).getHearing() != 0) {
                    event_count_0.setText(dataArrayList.get(0).getHearing() + "");
                } else {
                    event_count_0.setText("");
                }
                if (dataArrayList.get(0).getTask()) {
                    taskDot_0.setVisibility(View.VISIBLE);
                } else {
                    taskDot_0.setVisibility(View.INVISIBLE);
                }
                break;
            case 1:
                if (dataArrayList.get(1).getHearing() != 0) {
                    event_count_1.setText(dataArrayList.get(1).getHearing() + "");
                } else {
                    event_count_1.setText("");
                }
                if (dataArrayList.get(1).getTask()) {
                    taskDot_1.setVisibility(View.VISIBLE);
                } else {
                    taskDot_1.setVisibility(View.INVISIBLE);
                }
                break;
            case 2:
                if (dataArrayList.get(2).getHearing() != 0) {
                    event_count_2.setText(dataArrayList.get(2).getHearing() + "");
                } else {
                    event_count_2.setText("");
                }
                if (dataArrayList.get(2).getTask()) {
                    taskDot_2.setVisibility(View.VISIBLE);
                } else {
                    taskDot_2.setVisibility(View.INVISIBLE);
                }

                break;
            case 3:
                if (dataArrayList.get(3).getHearing() != 0) {
                    event_count_3.setText(dataArrayList.get(3).getHearing() + "");
                } else {
                    event_count_3.setText("");
                }
                if (dataArrayList.get(3).getTask()) {
                    taskDot_3.setVisibility(View.VISIBLE);
                } else {
                    taskDot_3.setVisibility(View.INVISIBLE);
                }

                break;
            case 4:
                if (dataArrayList.get(4).getHearing() != 0) {
                    event_count_4.setText(dataArrayList.get(4).getHearing() + "");
                } else {
                    event_count_4.setText("");
                }

                if (dataArrayList.get(4).getTask()) {
                    taskDot_4.setVisibility(View.VISIBLE);
                } else {
                    taskDot_4.setVisibility(View.INVISIBLE);
                }
                break;
            case 5:
                if (dataArrayList.get(5).getHearing() != 0) {
                    event_count_5.setText(dataArrayList.get(5).getHearing() + "");
                } else {
                    event_count_5.setText("");
                }

                if (dataArrayList.get(5).getTask()) {
                    taskDot_5.setVisibility(View.VISIBLE);
                } else {
                    taskDot_5.setVisibility(View.INVISIBLE);
                }

                break;
            case 6:
                if (dataArrayList.get(6).getHearing() != 0) {
                    event_count_6.setText(dataArrayList.get(6).getHearing() + "");
                } else {
                    event_count_6.setText("");
                }
                if (dataArrayList.get(6).getTask()) {
                    taskDot_6.setVisibility(View.VISIBLE);
                } else {
                    taskDot_6.setVisibility(View.INVISIBLE);
                }


                break;

        }

        scrollView.scrollTo(0, 0);
        linearLayoutManager.scrollToPosition(0);
        linearLayoutManager2.scrollToPosition(0);

        hearingList.setAdapter(new HearingListAdapter(context, list));

        if (taskList.getVisibility() == View.GONE && hearingList.getVisibility() == View.GONE) {
            scrollView.setVisibility(View.GONE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
        }

    }


    public void setSelected(int position) {
        switch (position) {
            case 0:

                sunLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

                break;
            case 1:
                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

                break;
            case 2:

                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case 3:
                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case 4:
                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case 5:
                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                satLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

                break;
            case 6:
                sunLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                monLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                tueLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                wedLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                thurLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                friLL.setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
                satLL.setBackground(getResources().getDrawable(R.drawable.selected_week_day));
                taskDot_6.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                taskDot_1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_4.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_5.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                taskDot_0.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

                break;

        }
    }
}
