package com.litman.calui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.litman.R;

class CalView extends FrameLayout {

    private TextView date, event_count;
    private View event_dot;


    public CalView(Context context) {
        super(context);
        initView();
    }

    public CalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.cal_item, null));
        event_count = (TextView) findViewById(R.id.event_count);
        date = (TextView) findViewById(R.id.date);
        event_dot = findViewById(R.id.event_dot);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public TextView getDate() {
        return date;
    }

    public TextView getEvent_count() {
        return event_count;
    }

    public View getEvent_dot() {
        return event_dot;
    }

    public void setSelectedItem(boolean isSelected){
        if(isSelected){
            setBackground(getResources().getDrawable(R.drawable.cal_item_selected));
        }else {
            setBackground(getResources().getDrawable(R.drawable.cal_item_normal));
        }
    }
}
