package com.litman.calui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.litman.activity.AddHearingActivity;
import com.litman.calui.dao.CalItemData;
import com.litman.dao.CalenderHearingTaskModel;
import com.litman.utils.AppLogger;
import com.litman.utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CalendarMonthFragmentForAddHearing extends Fragment {

    private GridView gridView;
    private Context context;
    private ArrayList<CalItemData> dateList;
    private static final String MONTH = "MONTH", YEAR = "YEAR",
            COURT = "COURT", START_DATE = "START_DATE0", END_DATE = "END_DATE";
    private int month, year;
    private String courtId = "-1", startDate, endDate;
    private static final String TAG = CalendarMonthFragmentForAddHearing.class.getSimpleName();
    private int lastSelectedPosition = -1;
    MyAdapter adapter;
    private SimpleDateFormat simpleDateFormat;


    public static CalendarMonthFragmentForAddHearing newInstance(int month, int year, String courtId, String startDate, String endDate, ArrayList<CalenderHearingTaskModel> hearingTaskToList) {
        CalendarMonthFragmentForAddHearing fragment = new CalendarMonthFragmentForAddHearing();
        Bundle bundle = new Bundle();
        AppLogger.logE(TAG, "month: " + month);
        bundle.putInt(MONTH, month);
        bundle.putInt(YEAR, year);
        AppLogger.logE(TAG, "CourtId: " + courtId);

        bundle.putString(COURT, courtId);
        bundle.putString(START_DATE, startDate);
        bundle.putString(END_DATE, endDate);

        bundle.putSerializable("list", hearingTaskToList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        month = bundle.getInt(MONTH);
        year = bundle.getInt(YEAR);
        courtId = bundle.getString(COURT);
        startDate = bundle.getString(START_DATE);
        endDate = bundle.getString(END_DATE);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd EEE");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        gridView = new GridView(context);
        gridView.setNumColumns(7);
        gridView.setHorizontalSpacing(10);
        gridView.setVerticalSpacing(10);
        gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        gridView.setGravity(Gravity.CENTER);

        dateList = new ArrayList<>();
        addDate(dateList);
        setHearingTaskToList();


        adapter = new MyAdapter(dateList);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppLogger.logD(TAG, "Clicked : " + position);
                try {

                    if (lastSelectedPosition != -1) {
                        dateList.get(lastSelectedPosition).setSelected(false);
                        CalItemData data = dateList.get(lastSelectedPosition);
                        data.setSelected(false);
                        dateList.set(lastSelectedPosition, data);
                    }

                    CalItemData data = dateList.get(position);
                    data.setSelected(true);
                    dateList.set(position, data);
                    lastSelectedPosition = position;
                    AppLogger.logD(TAG, dateList.toString());
                    adapter.notifyDataSetChanged();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                if (dateList.get(position).getDate().before(c.getTime())) {
                    AppUtils.openUtilityDialog(getActivity(), "Please select future date");
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    ((AddHearingActivity) context).selectedDate = sdf.format(dateList.get(position).getDate());
                    ((AddHearingActivity) context).dateBelowTv.setText(simpleDateFormat.format(dateList.get(position).getDate()));
                }
            }
        });
        return gridView;
    }


    public void setHearingTaskToList() {
        ArrayList<CalenderHearingTaskModel> hearingTaskModels = (ArrayList<CalenderHearingTaskModel>) getArguments().getSerializable("list");

        try {
            //    AppLogger.logE(TAG, "hearingList: " + hearingTaskModels.size());

            //  AppLogger.logE(TAG, "dateList: " + dateList.size());
            for (int i = 0; i < dateList.size(); i++) {
                if (dateList.get(i) != null) {
                    for (int j = 0; j < hearingTaskModels.size(); j++) {


                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


                        try {
                            Date hearingDate = dateFormat.parse(hearingTaskModels.get(j).getDateString());
                            CalItemData calItemData = dateList.get(i);
                            AppLogger.logE(TAG, "dateList getDay: " + dateList.get(i).getDateString());
                            AppLogger.logE(TAG, "hearing getDate: " + hearingDate.getDate());

                            if (calItemData.getDateString().equals(hearingDate.getDate() + "")) {

                                AppLogger.logE(TAG, "condition: " + i + " True");
                                calItemData.setTask(hearingTaskModels.get(j).getTask());
                                calItemData.setHearing(hearingTaskModels.get(j).getHearing());
                                dateList.set(i, calItemData);


                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }

            }

            // adapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void addDate(ArrayList<CalItemData> dateList) {
        dateList.clear();
        final Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DATE, 1);


        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        AppLogger.logD(TAG, "Date : " + calendar.getTime());

        for (int i = 0; i < dayOfWeek - 1; i++) {
            dateList.add(null);
        }

        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        AppLogger.logD(TAG, "Date : " + daysInMonth);
        AppLogger.logD(TAG, "Date : " + calendar.getTime());


        for (int i = 0; i < daysInMonth; i++) {

            Date date = calendar.getTime();
            AppLogger.logE(TAG, "date: " + date.toString());

            dateList.add(new CalItemData(date, "" + calendar.get(Calendar.DATE), calendar.get(Calendar.DATE), false));
            calendar.add(Calendar.DATE, 1);
        }
    }

    /*public ArrayList<Hearing> getMonthlyHearings() {
        ArrayList<Hearing> hearings = new ArrayList<>();
        DbManager.getInstance().openDatabase();
       Cursor res = DbManager.getInstance().getDetails("select * from " + com.biqe.interfaces.Hearing.T_NAME + " where " + com.biqe.interfaces.Hearing.ON_DATE + " = " + 1 + "");

        Hearing hearing = new Hearing();
        if (res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                hearing.setName(res.getString(res.getColumnIndex(AppVars.COURSE_LIST_ITEMS_NAME)));
                hearing.setOfflinePrice(res.getString(res.getColumnIndex(AppVars.COURSE_LIST_ITEMS_OFFLINE_PRICE)));

            }
            res.close();
        }


    }*/


    class MyAdapter extends BaseAdapter {

        private ArrayList<CalItemData> dateList;

        MyAdapter(ArrayList<CalItemData> dateList) {
            this.dateList = dateList;
        }

        @Override
        public int getCount() {
            return dateList.size();
        }

        @Override
        public CalItemData getItem(int position) {
            return dateList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;
            ViewHolder viewHolder = null;
            CalItemData itemData = getItem(position);
            if (view == null) {
                viewHolder = new ViewHolder();
                viewHolder.calView = new CalView(context);
                view = viewHolder.calView;
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            if (itemData != null) {
                //  AppLogger.logD(TAG, "Position : " + position + " --> " + itemData.toString());
                viewHolder.calView.getDate().setText("" + itemData.getDateString());
                viewHolder.calView.getEvent_count().setText("" + itemData.getHearing());
                if (itemData.getHearing() == 0) {
                    viewHolder.calView.getEvent_count().setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.calView.getEvent_count().setVisibility(View.VISIBLE);
                }

                viewHolder.calView.setSelectedItem(itemData.isSelected());
                //   AppLogger.logD(TAG, "Selected : " + itemData.isSelected());
                //   AppLogger.logD(TAG, "Count : " + itemData.getCount());
                if (!itemData.getTask()) {
                    viewHolder.calView.getEvent_dot().setVisibility(View.INVISIBLE);
                    //    AppLogger.logD(TAG, "Count zero");
                } else {
                    viewHolder.calView.getEvent_dot().setVisibility(View.VISIBLE);
                    //    AppLogger.logD(TAG, "Count greater");
                }
            }

            return viewHolder.calView;
        }


    }

    static class ViewHolder {
        CalView calView;
    }

}
