package com.litman.calui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.litman.R;
import com.litman.utils.AppLogger;

import java.util.Calendar;

public class CalendarPage extends Fragment {

    private final String TAG = CalendarPage.class.getSimpleName();
    private TabLayout tab_layout;
    private ViewPager cal_pager;
    private FloatingActionButton filter_btn;
    private int currentMonth, currentYear;
    private Context context;
    public static final int filterResult = 100;
    private String type = "-1", courtId = "-1";
    private Cursor curs;
    private Object mAdapter;
    private FullCalendarFragment fullCalendarFragment;
    private LinearCalendarFragment linearCalendarFragment;
    private boolean firstTime = true;
    public static Calendar calendar;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    private final String[] tabs = {"Month", "Week"};

    public static CalendarPage newInstance() {
        CalendarPage fragment = new CalendarPage();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cal_page, container, false);
        calendar = Calendar.getInstance();
        setUpTab(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    private void setData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cal_pager.setAdapter(new CalendarPagerAdapter(getChildFragmentManager()));
                tab_layout.setupWithViewPager(cal_pager);
            }
        }, 250);
    }

    private void setUpTab(View view) {
        // dateChange = this;
        fullCalendarFragment = FullCalendarFragment.newInstance();
        linearCalendarFragment = LinearCalendarFragment.newInstance();
        filter_btn = (FloatingActionButton) view.findViewById(R.id.filter_btn);
        filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilter();
            }
        });
        cal_pager = (ViewPager) view.findViewById(R.id.cal_pager);
        tab_layout = (TabLayout) view.findViewById(R.id.tab_layout);

        cal_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    fullCalendarFragment.displayCurrentCal();
                } else {
                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    linearCalendarFragment.displayCurrentCal();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

   /* @Override
    public void DateChange() {
        setData(type, courtId);
    }*/

    class CalendarPagerAdapter extends FragmentPagerAdapter {

        public CalendarPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {


                return fullCalendarFragment;


            } else if (position == 1) {

                return linearCalendarFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }

    private void openFilter() {
        AppLogger.logD(TAG, "Do the filter job");
        if (cal_pager.getCurrentItem() == 0) {
            fullCalendarFragment.startActivityForResult(new Intent(context, CalenderFilterActivity.class).putExtra("TYPE", type).putExtra("COURT", courtId), filterResult);
        } else {
            linearCalendarFragment.startActivityForResult(new Intent(context, CalenderFilterActivity.class).putExtra("TYPE", type).putExtra("COURT", courtId), filterResult);
        }

    }


}
