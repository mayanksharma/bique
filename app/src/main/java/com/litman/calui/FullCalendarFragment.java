package com.litman.calui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.dao.CalenderHearingTaskModel;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;
import com.litman.utils.AppPreferences;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.ContentValues.TAG;


public class FullCalendarFragment extends Fragment implements View.OnClickListener {

    //  private ViewPager pager_data;

    private FrameLayout frameLayout;
    private FragmentManager fragmentManager;
    private TextView monthYearSpan;
    public int currentMonth, currentYear;
    String[] months = new DateFormatSymbols().getMonths();
    //   private MyPager myPager;
    private String type = "-1", courtId = "-1";
    private Cursor curs;
    private Context context;
    private Button nextBtn, previousBtn;


    public static FullCalendarFragment newInstance() {
        FullCalendarFragment fragment = new FullCalendarFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.full_cal_view, container, false);
        frameLayout = (FrameLayout) view.findViewById(R.id.pager_data);
        fragmentManager = getChildFragmentManager();
        previousBtn = (Button) view.findViewById(R.id.previousBtn);
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        setUpView(view);
        previousBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        return view;
    }

    private void setUpView(View view) {
        // myPager = new MyPager(getChildFragmentManager());

        //CalendarPage.calendar = Calendar.getInstance(Locale.getDefault());
        CalendarPage.calendar.setFirstDayOfWeek(Calendar.SUNDAY);

        monthYearSpan = (TextView) view.findViewById(R.id.monthYearSpan);


        setUpCalendarView();


    }

    public void displayCurrentCal() {

        monthYearSpan.setText(months[getMonth()] + " " + getYear());
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

        SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


        String startDate = startDateFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());
        if (courtId.equals("-1")) {
            courtId = getCourts();
        }

        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                R.anim.reverse_enter).replace(frameLayout.getId(),
                CalendarMonthFragment.newInstance(getMonth(), getYear(), courtId, startDate, endDate, setData(type, courtId))).commitAllowingStateLoss();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previousBtn:
                previousYear();
                break;
            case R.id.nextBtn:
                nextYear();
                break;

        }
    }

    private int getYear() {
        return CalendarPage.calendar.get(Calendar.YEAR);
    }

    private int getMonth() {

        return CalendarPage.calendar.get(Calendar.MONTH);
    }

    private void nextYear() {

        CalendarPage.calendar.add(Calendar.MONTH, 1);
        // setUpCalendarView();
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        setUpCalendarView();

    }

    private void previousYear() {
        CalendarPage.calendar.add(Calendar.MONTH, -1);
        // setUpCalendarView();
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        setUpCalendarView();
    }

    private void setUpCalendarView() {
        currentYear = CalendarPage.calendar.get(Calendar.YEAR);
        // pager_data.setAdapter(myPager);
        monthYearSpan.setText(months[getMonth()] + " " + getYear());
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
        start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
        start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
        start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

        end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
        end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
        end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
        end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
        end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
        end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

        SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


        String startDate = startDateFormat.format(start.getTimeInMillis());
        String endDate = simpleDateFormat.format(end.getTimeInMillis());
        if (courtId.equals("-1")) {
            courtId = getCourts();
        }
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                R.anim.reverse_enter).replace(frameLayout.getId(),
                CalendarMonthFragment.newInstance(getMonth(), getYear(), courtId, startDate, endDate, setData("-1", "-1"))).commitAllowingStateLoss();
    }


    private ArrayList<CalenderHearingTaskModel> setData(String type, String courtId) {
        try {
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();

            start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
            start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
            start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
            start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
            start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
            start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

            end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
            end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
            end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
            end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
            end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
            end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

            SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");


            String startDate = startDateFormat.format(start.getTimeInMillis());
            String endDate = simpleDateFormat.format(end.getTimeInMillis());

            AppLogger.logE(TAG, "StartDate: " + startDate);
            AppLogger.logE(TAG, "endDate: " + endDate);

            AppLogger.logE(TAG, "courseID: " + courtId);
            curs = CacheManager.INSTANCE.getCalenderHearing(courtId.equals("-1") ? getCourts() : courtId, AppPreferences.getUserId(context), startDate, endDate);

            ArrayList<CalenderHearingTaskModel> list = new ArrayList<>();

            if (curs != null && curs.moveToFirst()) {

                AppLogger.logE("count", "" + curs.getCount() + "=" + curs.getColumnCount());
                while (!curs.isAfterLast()) {
                    CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                    AppLogger.logE(TAG, "display_date: " + curs.getString(curs.getColumnIndex("display_date")));

                    AppLogger.logE(TAG, "total: " + curs.getInt(curs.getColumnIndex("total")));

                    calenderHearingTaskModel.setDateString(curs.getString(curs.getColumnIndex("display_date")));
                    calenderHearingTaskModel.setHearing(curs.getInt(curs.getColumnIndex("total")));
                    list.add(calenderHearingTaskModel);
                    curs.moveToNext();
                }
                curs.close();
            }
            Cursor taskcurs = CacheManager.INSTANCE.getTaskList(AppPreferences.getUserId(context), courtId.equals("-1") ? getCourts() : courtId, startDate, endDate);


            if (taskcurs != null && taskcurs.moveToFirst()) {

                AppLogger.logE("count", "" + taskcurs.getCount() + "=" + taskcurs.getColumnCount());
                while (!taskcurs.isAfterLast()) {
                    CalenderHearingTaskModel calenderHearingTaskModel = new CalenderHearingTaskModel();
                    //    AppLogger.logE(TAG, "display_date: " + taskcurs.getString(taskcurs.getColumnIndex("display_date")));

//                    AppLogger.logE(TAG, "total: " + taskcurs.getInt(taskcurs.getColumnIndex("total")));
                    if (list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            CalenderHearingTaskModel calenderHearingTaskModel1 = list.get(i);

                            if (calenderHearingTaskModel1.getDateString().equals(taskcurs.getString(taskcurs.getColumnIndex("due_at")))) {
                                calenderHearingTaskModel1.setTask(true);
                                list.set(i, calenderHearingTaskModel1);
                            }
                        }

                    } else {
                        calenderHearingTaskModel.setDateString(taskcurs.getString(taskcurs.getColumnIndex("due_at")));
                        calenderHearingTaskModel.setTask(true);
                        list.add(calenderHearingTaskModel);

                    }

                    taskcurs.moveToNext();
                }
                taskcurs.close();
            }

            return list;

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            AppLogger.logE(TAG, "date: " + data.getExtras().toString());
            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
                type = data.getStringExtra("TYPE");
                courtId = data.getStringExtra("COURT");
                String court = "";
                if (courtId.equals("-1")) {
                    court = getCourts();
                } else {
                    court = courtId;
                }
                monthYearSpan.setText(months[getMonth()] + " " + getYear());

                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();

                start.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                start.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
                start.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
                start.set(Calendar.HOUR, CalendarPage.calendar.getActualMinimum(Calendar.HOUR));
                start.set(Calendar.MINUTE, CalendarPage.calendar.getActualMinimum(Calendar.MINUTE));
                start.set(Calendar.SECOND, CalendarPage.calendar.getActualMinimum(Calendar.SECOND));
                start.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMinimum(Calendar.MILLISECOND));

                end.set(Calendar.DAY_OF_MONTH, CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                end.set(Calendar.YEAR, CalendarPage.calendar.get(Calendar.YEAR));
                end.set(Calendar.MONTH, CalendarPage.calendar.get(Calendar.MONTH));
                end.set(Calendar.HOUR, CalendarPage.calendar.getActualMaximum(Calendar.HOUR));
                end.set(Calendar.MINUTE, CalendarPage.calendar.getActualMaximum(Calendar.MINUTE));
                end.set(Calendar.SECOND, CalendarPage.calendar.getActualMaximum(Calendar.SECOND));
                end.set(Calendar.MILLISECOND, CalendarPage.calendar.getActualMaximum(Calendar.MILLISECOND));

                SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:mm:ss.SSS");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'23:mm:ss.SSS");


                String startDate = startDateFormat.format(start.getTimeInMillis());
                String endDate = simpleDateFormat.format(end.getTimeInMillis());
                if (court.equals("-1")) {
                    court = getCourts();
                }

                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                        R.anim.reverse_enter).replace(frameLayout.getId(),
                        CalendarMonthFragment.newInstance(getMonth(), getYear(), court, startDate, endDate, setData(type, court))).commitAllowingStateLoss();
                //  setData(type, court);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }


}
