package com.litman.calui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.litman.R;
import com.litman.calui.dao.CalItemData;
import com.litman.interfaces.Courts;
import com.litman.manager.CacheManager;
import com.litman.utils.AppLogger;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import static com.litman.calui.dao.CalItemData.dateFormat;


public class LinearCalendarFragment extends Fragment implements View.OnClickListener {
    private Button nextBtn, previousBtn;
    private String type = "-1", courtId = "-1";
    private TextView monthYearSpan;
    private Context context;
    private FragmentManager fragmentManager;
    private FrameLayout frameLayout;
    private String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private final String TAG = LinearCalendarFragment.class.getSimpleName();
    String[] months = new DateFormatSymbols().getMonths();
    private ViewPager pager;
    private boolean calenderFlag = false;
    private ArrayList<WeekFragment> weekFragments;

    //    private ViewPager linear_pager;
    private int lastPage = 0;

    public static LinearCalendarFragment newInstance() {
        LinearCalendarFragment fragment = new LinearCalendarFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.linear_cal_view, container, false);
        frameLayout = (FrameLayout) view.findViewById(R.id.pager_data);
        fragmentManager = getChildFragmentManager();
        previousBtn = (Button) view.findViewById(R.id.previousBtn);
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        monthYearSpan = (TextView) view.findViewById(R.id.monthYearSpan);

        previousBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        // calendar.setTimeInMillis(System.currentTimeMillis());
        pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(new LinearCalPagerAdapter(getChildFragmentManager()));
        // pager.setVisibility(View.INVISIBLE);
        weekFragments = new ArrayList<>();
        CalendarPage.calendar.add(Calendar.DATE, -7);
        weekFragments.add(WeekFragment.newInstance(getWeekDaySelected(CalendarPage.calendar)));

        CalendarPage.calendar.add(Calendar.DATE, 7);
        weekFragments.add(WeekFragment.newInstance(getWeekDaySelected(CalendarPage.calendar)));

        CalendarPage.calendar.add(Calendar.DATE, 7);
        weekFragments.add(WeekFragment.newInstance(getWeekDaySelected(CalendarPage.calendar)));

        CalendarPage.calendar.add(Calendar.DATE, -7);


        setUpView(view);
        //setUpCalendarView();
        pager.setOffscreenPageLimit(3);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previousBtn:
                previousWeek();
                break;
            case R.id.nextBtn:
                nextWeek();
                break;

        }
    }

    private void nextWeek() {
        AppLogger.logE("Before next: ", CalendarPage.calendar.getTime() + "");
        CalendarPage.calendar.add(Calendar.DATE, 0);
        // setUpCalendarView();
        //  setUpCalendarView();
        AppLogger.logE("After next: ", CalendarPage.calendar.getTime() + "");
    }

    private void previousWeek() {
        AppLogger.logE("Before Previous: ", CalendarPage.calendar.getTime() + "");
        CalendarPage.calendar.add(Calendar.DATE, -14);
        AppLogger.logE("After Previous: ", CalendarPage.calendar.getTime() + "");
        // setUpCalendarView();
        // setUpCalendarView();
    }

    private void setUpCalendarView() {
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter,
                R.anim.reverse_enter).replace(frameLayout.getId(),
                WeekFragment.newInstance(getWeekDaySelected(CalendarPage.calendar))).commitAllowingStateLoss();

        monthYearSpan.setText(months[CalendarPage.calendar.get(Calendar.MONTH)] + " " + CalendarPage.calendar.get(Calendar.YEAR));
    }

    public void displayCurrentCal() {
        if (monthYearSpan != null) {
            monthYearSpan.setText(months[CalendarPage.calendar.get(Calendar.MONTH)] + " " + CalendarPage.calendar.get(Calendar.YEAR));
        }
        if (weekFragments != null && weekFragments.size() > 0) {
            weekFragments.get(1).setUpView(getWeekDaySelected(CalendarPage.calendar), courtId);
        }
    }

    private void setUpMonthPager(ViewPager pager) {
        AppLogger.logD(TAG, "Time : " + CalendarPage.calendar.getTime());

        int mm = CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        HashMap<Integer, List<CalItemData>> dataMap = printDateInWeek(CalendarPage.calendar, CalendarPage.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH));

        AppLogger.logD(TAG, dataMap.toString());

        Set<Integer> weekKeys = new TreeSet<>(dataMap.keySet());
        AppLogger.logD(TAG, "Sorted week : " + weekKeys.toString());
        Iterator<Integer> it = weekKeys.iterator();
        int day = 0;
        while (it.hasNext()) {
            int key = it.next();
            day = day + dataMap.get(key).size();
            AppLogger.logD(TAG, "Key " + key + ", week : " + dataMap.get(key).toString());
        }
        AppLogger.logD(TAG, "Day match : " + day + " : " + mm);

        pager.setAdapter(new LinearCalPagerAdapter(getChildFragmentManager()));
    }

    private void setUpView(View view) {
        //   linear_pager = (ViewPager) view.findViewById(R.id.linear_pager);

        //  calendar = Calendar.getInstance(Locale.getDefault());
        //  calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        //  calendar.set(Calendar.DATE, 1);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                AppLogger.logE(TAG, "onPageSelected() :: " + "position: " + position);

                lastPage = position;
                AppLogger.logE(TAG, "DAte: " + CalendarPage.calendar.getTime());
                monthYearSpan.setText(months[CalendarPage.calendar.get(Calendar.MONTH)] + " " + CalendarPage.calendar.get(Calendar.YEAR));


            }


            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    AppLogger.logE(TAG, "onPageSelected() :: " + "lastPage: " + lastPage);
                    if (lastPage == 0) {
                        // move some stuff from the
                        // center to the right here
                        //moveStuff(pages[1], pages[2]);
                        /*
                        Collections.swap(weekFragments, 1, 2);
                        Collections.swap(weekFragments, 0, 1);
                        Collections.swap(weekFragments, 0, 1);*/
                        // move stuff from the left to the center
                        //moveStuff(pages[0], pages[1]);
                        // retrieve new stuff and insert it to the left page
                        CalendarPage.calendar.add(Calendar.DATE, -7);
                        weekFragments.get(0).setUpView(getWeekDaySelected(CalendarPage.calendar), courtId);
                        //  insertStuff(pages[0]);
                    } else if (lastPage == 2) {


                        // move stuff from the center to the left page
                        //moveStuff(pages[1], pages[0]);
                        //  Collections.swap(weekFragments, 1, 0);
                        // Collections.swap(weekFragments, 2, 1);
                        // move stuff from the right to the center page
                        //moveStuff(pages[2], pages[1]);
                        // retrieve stuff and insert it to the right page
                        CalendarPage.calendar.add(Calendar.DATE, 7);
                        // insertStuff(pages[2]);
                        weekFragments.get(2).setUpView(getWeekDaySelected(CalendarPage.calendar), courtId);
                    }

                    // go back to the center allowing to scroll indefinitely
                    // calendar.add(Calendar.DATE, -7);
                    weekFragments.get(1).setUpView(getWeekDaySelected(CalendarPage.calendar), courtId);
                    pager.setCurrentItem(1, false);
                    //    weekFragments.get(1).setUpView(getWeekDaySelected(calendar));

                    // weekFragments.get(1).setUpView(getWeekDaySelected(calendar));
                }
            }


        });

        pager.setCurrentItem(1, true);

    }


    private void printWeekData() {
        CalendarPage.calendar = Calendar.getInstance(Locale.getDefault());
        CalendarPage.calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        int monthsInYear = CalendarPage.calendar.getActualMaximum(Calendar.MONTH);
        int year = CalendarPage.calendar.get(Calendar.YEAR);
        AppLogger.logD(TAG, "Months in Year : " + monthsInYear);

        for (int i = 0; i <= monthsInYear; i++) {
            CalendarPage.calendar.set(Calendar.MONTH, i);
            CalendarPage.calendar.set(Calendar.YEAR, year);
            CalendarPage.calendar.set(Calendar.DATE, 1);
            int mm = CalendarPage.calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            AppLogger.logD(TAG, "Week of month : " + CalendarPage.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH) + ", Month : " + i);
            HashMap<Integer, List<CalItemData>> dataOfMonth = printDateInWeek(CalendarPage.calendar, CalendarPage.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH));
            Set<Integer> weekKeys = new TreeSet<>(dataOfMonth.keySet());
            AppLogger.logD(TAG, "Sorted week : " + weekKeys.toString() + ", Month : " + i);
            Iterator<Integer> it = weekKeys.iterator();
            int day = 0;
            while (it.hasNext()) {
                int key = it.next();
                day = day + dataOfMonth.get(key).size();
                AppLogger.logD(TAG, "Key " + key + ", week : " + dataOfMonth.get(key).toString());
            }
            AppLogger.logD(TAG, "Day match : " + day + " : " + mm);

            CalendarPage.calendar.add(Calendar.MONTH, 1);
        }
    }

    private HashMap<Integer, List<CalItemData>> printDateInWeek(Calendar calendar, int weekCount) {
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        final HashMap<Integer, List<CalItemData>> monthMap = new HashMap<>();

        for (int i = 1; i <= weekCount; i++) {
            List<CalItemData> weekData = new ArrayList<>();
            for (int j = 0; j < 7; j++) {
                if (calendar.get(Calendar.MONTH) > month || calendar.get(Calendar.YEAR) > year) {
                    monthMap.put(i, weekData);
                    return monthMap;
                } else {
                    if (calendar.get(Calendar.YEAR) > year) {
                        monthMap.put(i, weekData);
                        return monthMap;
                    }
                    weekData.add(new CalItemData(calendar.getTime(), "" + calendar.get(Calendar.DATE), calendar.get(Calendar.DATE), false));
                }
                calendar.add(Calendar.DATE, 1);
            }
            monthMap.put(i, weekData);
        }
        return null;
    }

    class LinearCalPagerAdapter extends FragmentPagerAdapter {

        private HashMap<Integer, List<CalItemData>> dataMap;

        public LinearCalPagerAdapter(FragmentManager fm) {
            super(fm);
            this.dataMap = dataMap;
        }

        @Override
        public Fragment getItem(int position) {
            return weekFragments.get(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

    }

    public ArrayList<CalItemData> getWeekDaySelected(Calendar cal) {


        //   c.setTimeInMillis(System.currentTimeMillis());
        Calendar c = Calendar.getInstance();
        c.setTime(cal.getTime());
        ArrayList<CalItemData> days = new ArrayList<>();


        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM/yyyy");

        if (formatter.format(c.getTime()).contains(weekDays[1])) {
            c.add(Calendar.DATE, -1);
        } else if (formatter.format(c.getTime()).contains(weekDays[2])) {
            c.add(Calendar.DATE, -2);
        } else if (formatter.format(c.getTime()).contains(weekDays[3])) {
            c.add(Calendar.DATE, -3);
        } else if (formatter.format(c.getTime()).contains(weekDays[4])) {
            c.add(Calendar.DATE, -4);
        } else if (formatter.format(c.getTime()).contains(weekDays[5])) {
            c.add(Calendar.DATE, -5);
        } else if (formatter.format(c.getTime()).contains(weekDays[6])) {
            c.add(Calendar.DATE, -6);
        }
        //  c.add(Calendar.DATE, 1);
        for (int i = 0; i < 7; i++) {
            CalItemData calItemData = new CalItemData(c.getTime(), dateFormat.format(c.getTime()), 0, false);
            AppLogger.logE("DATE==> ", dateFormat.format(c.getTime()) + "");
            days.add(calItemData);
            c.add(Calendar.DATE, 1);
        }
        return days;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            AppLogger.logE(TAG, "date: " + data.getExtras().toString());
            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
                type = data.getStringExtra("TYPE");
                courtId = data.getStringExtra("COURT");
                String court = "";
                if (courtId.equals("-1")) {
                    court = getCourts();
                } else {
                    court = courtId;
                }
                //    monthYearSpan.setText(months[getMonth()] + " " + getYear());
                weekFragments.get(1).setUpView(getWeekDaySelected(CalendarPage.calendar), court);
                //  setData(type, court);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private String getCourts() {
        StringBuilder sb = new StringBuilder();
        Cursor curs = CacheManager.INSTANCE.getCourt();
        while (curs != null && curs.moveToNext()) {
            sb.append(curs.getInt(curs.getColumnIndex(Courts.R_ID)) + ",");
        }
        if (curs != null) {
            curs.close();
        }
        String str = "";
        if (sb.length() > 1) {
            str = sb.substring(0, sb.length() - 1);
        }
        return str;
    }
}
