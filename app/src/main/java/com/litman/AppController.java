package com.litman;

import android.app.Application;

import com.litman.db.DBHelper;
import com.litman.db.DbManager;

/**
 * Created by Tushar Agarwal on 16-Mar-17.
 */

public class AppController extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        DbManager.initializeInstance(new DBHelper(getApplicationContext()),
                getApplicationContext());
    }
}
